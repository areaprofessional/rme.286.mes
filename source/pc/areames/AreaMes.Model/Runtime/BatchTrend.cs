﻿using AreaMes.Model.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model.Runtime
{
    public class BatchParametri
    {
        public DistintaBaseParametri Parametro { get; set; }
        public object Max { get; set; }
        public object Min { get; set; }
        public object Avg { get; set; }
        public List<BatchTrendParametri> Trend { get; set; }
    }

    public class BatchTrendParametri
    {
        
        public object Valore { get; set; }
        public object ValorePrecedente { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime DataOra { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime DataOraPrecedente { get; set; }
    }



}
