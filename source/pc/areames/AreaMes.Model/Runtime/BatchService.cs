﻿using AreaMes.Meta;
using AreaMes.Model.Design;
using AreaMes.Model.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model.Design
{
   public class BatchService: IDoc
    {
        public string Id { get; set; }
        public StatoBatch Stato { get; set; }
        public string Nome { get; set; }
        public ExternalDoc<Macchina> Macchina { get; set; }
        public string Operatore { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Creazione { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Inizio { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Fine { get; set; }
        public List<Note> Note { get; set; }
        public List<MaterialeService> Materiali { get; set; }
        public ExternalDoc<Azienda> Azienda { get; set; }
        public ExternalDoc<TipiGuastiManutenzione> TipoGuastoManutenzione { get; set; }
        public ExternalDoc<TipiManutenzione> TipoManutenzione { get; set; }
        public ExternalDoc<TipoSegnalazioneManutenzione> TipoSegnalazioneManutenzione { get; set; }
        public ExternalDoc<ManutenzioneCompetenza> ManutenzioneCompetenza { get; set; }
        public TimeTotal DurataTotale { get; set; }
        public TimeTotal DurataPresaInCarico { get; set; }

        public int BsonSizeMb { get; set; }

        public new string ToString
        {
            get
            {
                string res = this.Nome;


                return res;
            }
        }
    }


    public class MaterialeService
    {
        public int Quantita { get; set; }
        public string Materiale { get; set; }

        public string UnitaMisura { get; set; }

        public string Operatore { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Data { get; set; }
    }


    public class Note
    {
        public string Nota { get; set; }

        public string Operatore { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Data { get; set; }
    }


    

   public class TimeTotal
    {
       public int Days { get; set; }
       public int Hours { get; set; }
       public int Minutes { get; set; }
       public int Seconds { get; set; }
    }
}
