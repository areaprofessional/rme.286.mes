﻿using System;
using AreaMes.Model.Enums;
using MongoDB.Bson.Serialization.Attributes;

namespace AreaMes.Model.Runtime
{
    public class BatchFermi
    {
        public String Id { get; set; }
        public int NumeroFase { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Inizio { get; set; }
         [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Fine { get; set; }
        public BatchCausaleFermi Causale { get; set; }
        public string NotaDiRiavvio { get; set; }
        public int TotaleTempo { get; set; }

        public string UserLocked { get; set; }
        public string UserUnLocked { get; set; }
        public ExternalDoc<CausaliRiavvio> CausaliRiavvio { get; set; }
        public string MacchinaId { get; set; }
        public TurnoDiLavoro Turno { get; set; }

       
    }
}
