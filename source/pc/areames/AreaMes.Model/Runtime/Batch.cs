﻿using System;
using System.Collections.Generic;
using AreaMes.Model.Enums;
using AreaMes.Model.Design;
using MongoDB.Bson.Serialization.Attributes;
using AreaMes.Meta;
using System.Runtime.CompilerServices;

namespace AreaMes.Model.Runtime
{
    public class Batch : BaseDoc, IDoc
    {
        public string OdP { get; set; }
        public string OdPParziale { get; set; }
        public string OdV { get; set; }
        public DateTime OdV_DataConsegna { get; set; }
        public string OdV_DescrizioneCliente { get; set; }
        public string Revisione { get; set; }
        public string Commessa { get; set; }
        public Materiale Materiale { get; set; }
        public int PezziDaProdurre { get; set; }
        public int PezziProdotti { get; set; }
        public StatoBatch Stato { get; set; }
        public StatoBatch StatoPrecedente { get; set; }
        public DateTime? StatoPrecedente_At { get; set; }
        public List<Evento> Eventi { get; set; }
        public DistintaBase DistintaBase { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Inizio { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Fine { get; set; }
        public int PercCompletamento { get; set; }
        public List<BatchFermi> Fermi { get; set; }
        public List<BatchCampionatura> Campionature { get; set; }
        public List<BatchFase> Fasi { get; set; }
        public string Id { get; set; }
        public int TotaleFermi { get; set; }
        public int PezziScartati { get; set; }

        public int OEE { get; set; }
        public DateTime? OEE_At { get; set; }
        public int OEEPrecedente { get; set; }
        public DateTime? OEEPrecedente_At { get; set; }

        public int OEE_Rf { get; set; }
        public DateTime? OEE_Rf_At { get; set; }
        public int OEE_RfPrecedente { get; set; }
        public DateTime? OEE_RfPrecedente_At { get; set; }


        public int OEE_Rv { get; set; }
        public DateTime? OEE_Rv_At { get; set; }
        public int OEE_RvPrecedente { get; set; }
        public DateTime? OEE_RvPrecedente_At { get; set; }



        public int OEE_Rq { get; set; }
        public DateTime? OEE_Rq_At { get; set; }
        public int OEE_RqPrecedente { get; set; }
        public DateTime? OEE_RqPrecedente_At { get; set; }

        public double TempoTotaleFermi { get; set; }

        public AndamentoValore OEEAndamento { get; set; }
        public List<BatchParametri> BatchParametri { get; set; }
        public List<BatchAvanzamento> BatchAvanzamenti { get; set; }
        public List<BatchMateriali> BatchMateriali { get; set; }
        public List<BatchMultimedia> BatchMultimedia { get; set; }
        public ExternalDoc<Azienda> Azienda { get; set; }
        public int TempoTeorico { get; set; }
        public int TempoEffettivo { get; set; }
        public ExternalDoc<CausaliRiavvio> CausaleAborto { get; set; }
        public List<BatchTrendParametri> Trend_Oee { get; set; } = new List<BatchTrendParametri>();
        public List<BatchTrendParametri> Trend_Oee_Rf { get; set; } = new List<BatchTrendParametri>();
        public List<BatchTrendParametri> Trend_Oee_Rv { get; set; } = new List<BatchTrendParametri>();
        public List<BatchTrendParametri> Trend_Oee_Rq { get; set; } = new List<BatchTrendParametri>();
        public List<BatchTrendParametri> Trend_Stato { get; set; } = new List<BatchTrendParametri>();

        [BsonIgnore]
        public string StringMacchine { get; set; }

        public int BsonSizeMb { get; set; }
        public int Priorita { get; set; }

        public DateTime? Sospensione { get; set; }

        public void AddEvent(Evento item)
        {
            lock (this)
                this.Eventi.Add(item);
        }


        public Batch()
        {
            this.Eventi = new List<Evento>();
            this.Fermi = new List<BatchFermi>();
        }

        public new string ToString
        {
            get
            {
                string res = this.OdP;

                return res;
            }
        }
    }

    public enum AndamentoValore
    {
        Stabile = 0,
        InSalita = 1,
        InDiscesa = 2,
    }



}
