﻿using System;
using System.Collections.Generic;
using AreaMes.Model.Enums;
using MongoDB.Bson.Serialization.Attributes;

namespace AreaMes.Model.Runtime
{
    public class BatchFase
    {
        public String Id { get; set; }
        public string NumeroFase { get; set; }
        public StatoFasi Stato { get; set; }

         [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime Inizio { get; set; }

         [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime Fine { get; set; }
        public List<BatchFaseMacchina> Macchine { get; set; }
        public List<BatchFaseManoDopera> Manodopera { get; set; }
        public DistintaBaseFasi DistintaBaseFase  { get; set; }
        public string UserLocked { get; set; }
        public DateTime UserLockedDate { get; set; }
        public string BloccoAvanzamentoCausa { get; set; }
        public double TaktMacchina { get; set; }
        public double TaktManodopera { get; set; }
        public double TaktTotale { get; set; }
        public bool IsAndon { get; set; }
        public bool IsCiclica { get; set; }
        public int PezziProdotti { get; set; }
        public int PezziParziali { get; set; }
        public int PezziPianificati { get; set; }

        public BatchFase()
        {
            Macchine = new List<BatchFaseMacchina>();
            Manodopera = new List<BatchFaseManoDopera>();

        }
    }

}
