﻿using AreaMes.Model.Enums;

namespace AreaMes.Model.Runtime
{
    public class BatchCampionaturaRisposta
    {
        public string Codice { get; set; }
        public object Valore { get; set; }
        public Severita SeveritaCalcolata { get; set; }

        public string Nota { get; set; }
    }

}
