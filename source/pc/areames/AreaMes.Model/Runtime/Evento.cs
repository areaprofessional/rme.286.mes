﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AreaMes.Model.Enums;
using AreaMes.Model.Design;
using MongoDB.Bson.Serialization.Attributes;
using AreaMes.Meta;

namespace AreaMes.Model.Runtime
{
    public class Evento : BaseDoc, IDoc
    {
        public string Id { get; set; }
        public TipoEvento TipoEvento { get; set; }
        public string BatchId { get; set; }
        public DateTime? OraEvento { get; set; }
        public DateTime? UltimoAggiornamento { get; set; }
        public string FaseId { get; set; }
        public ExternalDoc<Macchina> Macchina { get; set; }
        public ExternalDoc<Operatore> Operatore { get; set; }
        public ExternalDoc<TipoValori> TipoValore { get; set; }
        public string OggettoId { get; set; }
        public string  Valore { get; set; }
        public ExternalDoc<UnitaDiMisura> UnitaDiMisura { get; set; }
        public Source Source { get; set; }
        public ExternalDoc<Materiale> Materiale { get; set; }
        public string Descrizione1 { get; set; }
        public string Descrizione2 { get; set; }
        public string Descrizione3 { get; set; }
        public string Descrizione4 { get; set; }
        public string Descrizione5 { get; set; }
        public string Valore1 { get; set; }
        public string Valore2 { get; set; }
        public string Valore3 { get; set; }
        public string Valore4 { get; set; }
        public TurnoDiLavoro Turno { get; set; }
        public int BsonSizeMb { get; set; }

        public new string ToString
        {
            get
            {
                string res = this.Id;

                return res;
            }
        }
    }
}
