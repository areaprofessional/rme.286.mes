﻿namespace AreaMes.Model.Enums
{
    public enum Comparazione
    {
        MinoreDi,
        MaggioreDi,
        UgualeA,
        CompresoTra
    }
}
