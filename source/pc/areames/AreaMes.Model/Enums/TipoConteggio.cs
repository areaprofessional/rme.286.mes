﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model.Enums
{

    public enum TipoConteggio
    {
        conteggio_incrementale = 0,
        conteggio_trigger = 1,
    }
}
