﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model.Enums
{
    public enum TipoEvento
    {
        qta_pzsca = 0, //pezzo scarto
        Allarme = 1, 
        qta_pzparz=2 ,//versamento parziale manuale 
        qta_prelievo = 3 
    }
}
