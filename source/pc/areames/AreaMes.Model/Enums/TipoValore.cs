﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model.Enums
{
    public enum TipoValore
    {
        prod_cau_scarto = 0,
        prod_cau_blocco = 1,
        prod_cau_abort = 2,
        manu_cau_richiesta = 3,
        prod_cau_prelievo=4

    }
}
