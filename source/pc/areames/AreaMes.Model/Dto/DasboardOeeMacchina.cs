﻿using AreaMes.Meta;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace AreaMes.Model.Dto
{
    public class DashboardOeeMacchina
    {
        public string Descrizione1 { get; set; }
        public string Descrizione2 { get; set; }
        public string Descrizione3 { get; set; }
        public string Descrizione4 { get; set; }
        public string Descrizione5 { get; set; }
        public double Valore1 { get; set; }
        public double Valore2 { get; set; }
        public double Valore3 { get; set; }
        public double Valore4 { get; set; }
        public double Valore5 { get; set; }
        public double Oee { get; set; }
        public double Oee_Rf { get; set; }
        public double Oee_Rq { get; set; }
        public double Oee_Rv { get; set; }
        public List<DashboardOeeMacchinaDettaglio> listaMacchine { get; set; }
    }

    public class DashboardOeeMacchinaDettaglio
    {
        public int PezziBuoni { get; set; }
        public int PezziScarto { get; set; }
        public string TempoLavorazione { get; set; }
        public string TempoPausa { get; set; }
        public string TempoEmergenza { get; set; }
        public double PercCompleteamento { get; set; }
        public double Oee { get; set; }
        public double Oee_Rf { get; set; }
        public double Oee_Rq { get; set; }
        public double Oee_Rv { get; set; }
        public string Colore { get; set; }
        public string Label { get; set; }
        public string Descrizione1 { get; set; }
        public string Descrizione2 { get; set; }
        public string Descrizione3 { get; set; }
        public string Descrizione4 { get; set; }
        public string Descrizione5 { get; set; }
        public string Descrizione6 { get; set; }
        public string Descrizione7 { get; set; }
        public string Descrizione8 { get; set; }
        public string Descrizione9 { get; set; }
        public string Descrizione10 { get; set; }
        public double Valore1 { get; set; }
        public double Valore2 { get; set; }
        public double Valore3 { get; set; }
        public double Valore4 { get; set; }
        public double Valore5 { get; set; }
        public List<DashboardOeeMacchinaAvanzamento> ListaAvanzamenti { get; set; }
}

    public class DashboardOeeMacchinaAvanzamento
    {
        public string Macchina { get; set; }
        public DateTime Inizio { get; set; }
        public DateTime Fine { get; set; }
        public string StatoLavorazione { get; set; }
        public string Colore { get; set; }
    }
}