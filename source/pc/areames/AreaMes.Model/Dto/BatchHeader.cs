﻿using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model.Dto
{
    public class BatchHeader
    {
        public string Id { get; set; }
        public string OdP { get; set; }
        public string OdV { get; set; }
        public DateTime OdV_DataConsegna { get; set; }
        public string OdV_DescrizioneCliente { get; set; }
        public int PezziDaProdurre { get; set; }
        public int PezziProdotti { get; set; }
        public StatoBatch Stato { get; set; }
        public DateTime? Inizio { get; set; }
        public DateTime? Fine { get; set; }
        public int PercCompletamento { get; set; }
        public int TotaleFermi { get; set; }
        public int OEE { get; set; }
        public double TempoTotaleFermi { get; set; }
        public int OEEPrecedente { get; set; }
        public AndamentoValore OEEAndamento { get; set; }
        public List<BatchFase> FasiAttive { get; set; }
    }


    public class BatchFilterGraphCriteria
    {
        public int TickInterval { get; set; }

        public List<string> ListOfBatches { get; set; }
    }

    public class BatchGraphValues
    {
        public string BatchId { get; set; }

        public List<BatchGraphValue> Values { get; set; }

        public int TickInterval { get; set; }

        public string Name { get; set; }


    }

    public class BatchGraphValue
    {
        public int Index { get; set; }
        public object Value { get; set; }
        public DateTime At { get; set; }
    }
}
