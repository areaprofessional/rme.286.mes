﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;

namespace AreaMes.Model.Dto
{
    public class Dashboard
    {
        public int OperatoriAttivi { get; set; }
        public int OperatoriAttiviOraPrec { get; set; }
        public int OrdiniInAllarme { get; set; }
        public int OrdiniInAllarmeOraPrec { get; set; }
        public int MacchinariAttivi { get; set; }
        public int MacchinariAttiviOraPrec { get; set; }
        public int OEEMedia { get; set; }
        public int OEEMediaOraPrec { get; set; }
        public IEnumerable<Batch> Ordini { get; set; }
        public Dictionary<StatoBatch, int> StatoOrdini { get; set; }

        public Dictionary<string, double> ConsumoEnergiaMezzora { get; set; } 
    }
}
