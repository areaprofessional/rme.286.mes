﻿using AreaMes.Meta;
using MongoDB.Bson.Serialization.Attributes;

namespace AreaMes.Model
{


    public class ExternalDoc<T> where T:IDoc
    {
        [BsonIgnore]
        public T Item { get; set; }

        public string Collection { get; set; }

        public string Id { get; set; }
    }
}
