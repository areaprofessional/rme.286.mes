﻿using System;

namespace AreaMes.Model.Design
{
    public class BaseDoc
    {
        public DateTime LastUpdate { get; set; }

        public DateTime CreateDate { get; set; }

        public string UserLocked { get; set; }

        public DateTime UserLockedDate { get; set; }

    }
}
