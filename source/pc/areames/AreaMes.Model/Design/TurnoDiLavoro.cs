﻿using AreaMes.Meta;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace AreaMes.Model
{
    public class TurnoDiLavoro : IDoc
    {
        public string Id { get; set; }
        public string Codice { get; set; }
        public string Nome { get; set; }
        public bool IsDisabilitato { get; set; }
        public int InizioHH { get; set; }
        public int InizioMM { get; set; }
        public int FineHH { get; set; }
        public int FineMM { get; set; }
        public bool FlagLunedi { get; set; }
        public bool FlagMartedi { get; set; }
        public bool FlagMercoledi { get; set; }
        public bool FlagGiovedi { get; set; }
        public bool FlagVenerdi { get; set; }
        public bool FlagSabato { get; set; }
        public bool FlagDomenica { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime ValidoDal { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime ValidoAl { get; set; }
        public bool IsEccezione { get; set; }
        public List<ExternalDoc<Macchina>> Macchine { get; set; }

        public int BsonSizeMb { get; set; }

        public new string ToString
        {
            get
            {
                string res = this.Codice;
                if (!System.String.IsNullOrWhiteSpace(this.Nome))
                    res += (System.String.IsNullOrWhiteSpace(res) ? "" : " - ") + this.Nome;

                if (System.String.IsNullOrWhiteSpace(res))
                    res = this.Id;

                return res;
            }
        }

    }       
}