﻿namespace AreaMes.Model
{
    public class Foto
    {
        public string Nome { get; set; }

        public string Descrizione { get; set; }

        public byte[] Data { get; set; }
    }
}
