﻿using AreaMes.Meta;

namespace AreaMes.Model
{
    public class OperatoreCompetenza : IDoc
    {
        public string Codice { get; set; }

        public string Nome { get; set; }

        public string Descrizione { get; set; }

        public bool IsDisable { get; set; }

        public string Id { get; set; }

        public int BsonSizeMb { get; set; }

        //public Guid Id { get; set; }

        //public long __DocVersion { get; set; }
        public new string ToString
        {
            get
            {
                string res = this.Codice;
                if (!System.String.IsNullOrWhiteSpace(this.Nome))
                    res += (System.String.IsNullOrWhiteSpace(res) ? "" : " - ") + this.Nome;

                if (System.String.IsNullOrWhiteSpace(res))
                    res = this.Id;

                return res;
            }
        }

    }
}
