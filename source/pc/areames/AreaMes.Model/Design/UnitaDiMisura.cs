﻿using AreaMes.Meta;
using AreaMes.Model.Design;

namespace AreaMes.Model
{
    public class UnitaDiMisura : BaseDoc,IDoc
    {
        public string Codice { get; set; }
 
        public string Nome { get; set; }

        public string Descrizione { get; set; }

        public string Id { get; set; }

        public int BsonSizeMb { get; set; }

        public new string ToString
        {
            get
            {
                string res = this.Codice;
                if (!System.String.IsNullOrWhiteSpace(this.Nome))
                    res += (System.String.IsNullOrWhiteSpace(res) ? "" : " - ") + this.Nome;

                if (System.String.IsNullOrWhiteSpace(res))
                    res = this.Id;

                return res;
            }
        }
    }
}
