﻿namespace AreaMes.Model
{
    public class DistintaBaseMateriale
    {
        public ExternalDoc<Materiale> Materiale { get; set; }
        public string CodiceMateriale { get; set; }
        public double Qta { get; set; }
        public int NumeroFase { get; set; }
        public string Lotto { get; set; }
        public string Descrizione { get; set; }
        public string UnitaDiMisura { get; set; }
    }
}
