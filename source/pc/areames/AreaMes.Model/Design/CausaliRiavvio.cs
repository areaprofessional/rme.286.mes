﻿using AreaMes.Meta;
using System;

namespace AreaMes.Model
{
    public class CausaliRiavvio : IDoc
    {
        public string Codice { get; set; }
        public string Causale { get; set; }
        public string Nome { get; set; }
        public string Id { get; set; }
        public bool IsStopCausal { get; set; }
        public bool IsAboartCausal { get; set; }
        public bool Default { get; set; }
        public bool IsInternal { get; set; }
        public int BsonSizeMb { get; set; }
        public new string ToString
        {
            get
            {
                string res = this.Codice;
                if (!String.IsNullOrWhiteSpace(this.Nome))
                    res += (String.IsNullOrWhiteSpace(res) ? "" : " - ") + this.Nome;

                if (String.IsNullOrWhiteSpace(res))
                    res = this.Id;

                return res;
            }
        }

      
    }
}


