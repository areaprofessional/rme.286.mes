﻿using System.Collections.Generic;
using System;
using AreaMes.Model.Runtime;
using log4net.Appender;
using Newtonsoft.Json;

namespace AreaMes.Model
{
    public class DashboardOee
    {
        public DateTime FiltroDal { get; set; }
        public DateTime FiltroAl { get; set; }
        public List<Macchina> ListaMacchine { get; set; }
        public List<TurnoDiLavoro> ListaTurni { get; set; }
        public List<Materiale> ListaMateriali { get; set; }
        public List<ItemCommessa> ListaCommesse
        {
            get; set;
        }
    }

    public class ItemCommessa
    {
        public string Id { get; set; }
        public string Odp { get; set; }
    }

    public class RequestDataDashboardOee
    {
        public DateTime FiltroDal { get; set; }
        public DateTime FiltroAl { get; set; }
        public bool MostraFuoriTurno { get; set; }
        public List<string> ListIdMateriali { get; set; }
        public List<string> ListCodeTurni { get; set; }
        public List<string> ListIdTurni { get; set; }
        public List<string> ListIdMacchine { get; set; }
        public List<string> ListIdCommesse { get; set; }
    }

    public class ResponseDataDashboardOee
    {
        public int Oee { get; set; }
        public int Oee_Rf { get; set; }
        public int Oee_Rv { get; set; }
        public int Oee_Rq { get; set; }
        public List<Dictionary<string, string>> Tempi { get; set; }
        public int PezziElaborati { get; set; }
        public int PezziTeorici { get; set; }
        public int PezziTarget { get; set; }
        public int ScartiTotali { get; set; }
        public List<DashboardOeeFermiItem> ListFermi { get; set; }
        public List<DashboardOeeFermiGrafici> ListFermiGrafici { get; set; }
        public List<DashboardOeeScartiItem> ListScarti { get; set; }
        public List<DashboardOeeScartiGrafici> ListScartiGrafici { get; set; }
        public Dictionary<string, List<DashboardOeeAvanzamenti>> ListAvanzamenti { get; set; }
        public List<OeeChartLineItem> ListOee { get; set; }
        public List<string> ListOeeField { get; set; }
        public List<string> ListAccordion { get; set; }
        public List<DashboardOeeEfficenza> ListEfficienza { get; set; }
        public List<Batch> ListBatch { get; set; } = new List<Batch>();
        public Dictionary<string, List<TimeDictItem>> GroupedLavTime { get; set; } = new Dictionary<string, List<TimeDictItem>>()
        {
            { nameof(Macchina), new List<TimeDictItem>() },
            { nameof(TurnoDiLavoro), new List<TimeDictItem>() },
            { nameof(Batch), new List<TimeDictItem>() }
        };
        public Dictionary<string, List<TimeDictItem>> GroupedPauseTime { get; set; } = new Dictionary<string, List<TimeDictItem>>()
        {
            { nameof(Macchina), new List<TimeDictItem>() },
            { nameof(TurnoDiLavoro), new List<TimeDictItem>() },
            { nameof(Batch), new List<TimeDictItem>() }
        };
        public Dictionary<string, List<TimeDictItem>> GroupedFermiTime { get; set; } = new Dictionary<string, List<TimeDictItem>>()
        {
            { nameof(Macchina), new List<TimeDictItem>() },
            { nameof(TurnoDiLavoro), new List<TimeDictItem>() },
            { nameof(Batch), new List<TimeDictItem>() }
        };

        public Dictionary<string, List<TimeDictItem2>> GroupedTime { get; set; } = new Dictionary<string, List<TimeDictItem2>>()
        {
            { nameof(Macchina), new List<TimeDictItem2>() },
            { nameof(TurnoDiLavoro), new List<TimeDictItem2>()},
            { nameof(Batch), new List<TimeDictItem2>()}
        };

        public List<string> FermiCausaliPercentSummary { get; set; }
        public List<string> ScartiCausaliPercentSummary { get; set; }
        public List<string> FermiCausaliTimeSummary { get; set; }
        public List<string> FermiCausaliNumberSummary { get; set; }
        public Dictionary<string, Dictionary<string, DevicePercItem>> DeviceFasePercItems { get; set; }

        ///////////////////    ENERGIA    ///////////////////
        public List<EnergyPieChartElement> EnergyPieChartData { get; set; }
        public List<EnergyCompareGridElement> EnergyCompareGridData { get; set; }
        public double TotalEffectiveConsumption { get; set; }
        public double TotalTheoriticalConsumption { get; set; }
    }

    public class OeeChartLineItem
    {
        public DateTime TimeStamp { get; set; }
        public double Oee { get; set; }
        public double OeeRf { get; set; }
        public double OeeRq { get; set; }
        public double OeeRv { get; set; }
    }

    public class DashboardOeeFermiItem
    {
        public string CausaleId { get; set; }
        public string Causale { get; set; }
        public string CodiceCausale { get; set; }
        public string Odp { get; set; }
        public string Fase { get; set; }
        public string MacchinaId { get; set; }
        public string Macchina { get; set; }
        public string Materiale { get; set; }
        public string MaterialeDescrizione { get; set; }
        public double PercentualeTempo { get; set; }
        public double PercentualeTempoTot { get; set; }
        public double PercentualeTempoMacchina { get; set; }
        public double PercentualeTempoCausale { get; set; }
        public double PercentualeTempoMacchinaCausale { get; set; }
        public double PercentualeFermiTot { get; set; }
        public double PercentualeFermiMacchina { get; set; }
        public double PercentualeFermiCausale { get; set; }
        public double PercentualeFermiMacchinaCausale { get; set; }
        public double TempoFermo { get; set; }
        public double TempoFermoInOre { get; set; }
        public string TempoFermoText { get; set; }
        public int NumeroFermi { get; set; }
        public string Turno { get; set; }
    }

    public class DashboardOeeFermiGrafici
    {
        public string Causale { get; set; }
        public string CausaleId { get; set; }
        public double TempoFermo { get; set; }
        public int NumeroFermi { get; set; }
        public int PercentualeCumulativa { get; set; }
        public int PercentualeTempo { get; set; }
        public double PercentualeTempoDouble { get; set; }
        public int PercentualeNumero { get; set; }
    }

    public class DashboardOeeScartiItem
    {
        public string CausaleId { get; set; }
        public string Causale { get; set; }
        public string CodiceCausale { get; set; }
        public string MacchinaId { get; set; }
        public string Macchina { get; set; }
        public string Materiale { get; set; }
        public string MaterialeDescrizione { get; set; }
        public int NumeroScarti { get; set; }
        public double PercentualeScartiTot { get; set; }
        public double PercentualeScartiPerMacchina { get; set; }
        public double PercentualeScartiPerCausale { get; set; }
        public double PercentualeScartiPerMacchinaCausale { get; set; }
    }

    public class DashboardOeeScartiGrafici
    {
        public string Causale { get; set; }
        public string CausaleId { get; set; }
        public int NumeroScarti { get; set; }
        public int PercentualeCumulativa { get; set; }
        public int Percentuale { get; set; }
        public double PercentualeDouble { get; set; }
    }

    public class DashboardOeeAvanzamenti
    {
        public string Fase { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string StatoLavorazione { get; set; }
        public string Colore { get; set; }
    }

    public class DashboardOeeEfficenza
    {
        public string Articolo { get; set; }
        public string Macchina { get; set; }
        public string Commessa { get; set; }
        public float PezziOraTeorico { get; set; }
        public float PezziOraReale { get; set; }
        public int CicloTeorico { get; set; }
        public int CicloReale { get; set; }
        public DashboardOeeEfficenzaGrafico Grafico { get; set; }
    }

    public class DashboardOeeEfficenzaGrafico
    {
        public float PezziOraTeorico { get; set; }
        public float PezziOraReale { get; set; }
    }

    public class ScartiDatiPiani
    {
        public string Odp { get; set; }
        public string Macchina { get; set; }
        public string Causale { get; set; }
        public DateTime DataEvento { get; set; }
        public int Quantità { get; set; }
        public string Fase { get; set; }
        public string Turno { get; set; }
        public string Operatore { get; set; }
        public string CausaleCodice { get; set; }
        public string Materiale { get;set; }
        public string MaterialeDescrizione { get; set; }
    }

    public class FermiDatiPiani
    {
        public string Turno { get; set; }
        public string Macchina { get; set; }
        public string Fase { get; set; }
        public string OpBlocco { get; set; }
        public string OpSblocco { get; set; }
        public string Odp { get; set; }
        public string Causale { get; set; }
        public string Materiale { get; set; }
        public string DescrizioneMateriale { get; set; }
        public DateTime? Inizio { get; set; }
        public DateTime? Fine { get; set; }
    }

    public class TimeDictItem
    {
        public string Key { get; set; }
        public string Time { get; set; }
        public double TimeInHour { get; set; }
        public double TimeInSecond { get; set; }
        public double Percentage { get; set; }

    }

    public class TimeDictItem2
    {
        public string Key { get; set; }
        public double Time { get; set; }
        public string LavTime { get; set; }
        public double LavTimeInHour { get; set; }
        public double LavTimeInSecond { get; set; }
        public double LavPercentage { get; set; }
        public string PauTime { get; set; }
        public double PauTimeInHour { get; set; }
        public double PauTimeInSecond { get; set; }
        public double PauPercentage { get; set; }
        public string EmerTime { get; set; }
        public double EmerTimeInHour { get; set; }
        public double EmerTimeInSecond { get; set; }
        public double EmerPercentage { get; set; }
    }

    public class EnergyPieChartElement
    {
        public string Key { get; set; }
        public double ConsumoTeoricoPercentuale { get; set; }
        public double ConsumoEfettivoPercentuale { get; set; }
        public double ConsumoTeorico { get; set; }
    }

    public class DevicePercItem
    {
        public double TempTot { get; set; }
        public double TempLav { get; set; }
        public double TempEmer { get; set; }
        public double TempPausa { get; set; }
        public double PercLav { get; set; }
        public double PercEmer { get; set; }
        public double PercPausa { get; set; }
        public string ColorLav { get; set; }
        public string ColorEmer { get; set; }
        public string ColorPausa { get; set; }
        public string LabelLav { get; set; }
        public string LabelEmer { get; set; }
        public string LabelPausa { get; set; }
    }

    /////////////////////////////////////////////////////
    ///////////////////    ENERGIA    ///////////////////
    /////////////////////////////////////////////////////

    public class EnergyCompareGridElement
    {
        public string Macchina { get; set; }
        public string StatoFase { get; set; }
        public double ConsumoTeorico { get; set; }
        public double ConsumoEffettivo { get; set; }
        public double ConsumoEffettivoDiff { get; set; }
        public double CostoTeorico { get; set; }
        public double CostoEffettivo { get; set; }
        public double CostoEffettivoDiff { get; set; }
        public double DeltaPercentage { get; set; }
    }
}