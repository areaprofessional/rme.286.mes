﻿using System;
using System.Collections.Generic;
using AreaMes.Model.Design;
using AreaMes.Model.Enums;
using AreaMes.Meta;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel;
using Newtonsoft.Json;

namespace AreaMes.Model
{
    public class DistintaBaseFasi : BaseDoc, IDoc
    {
        public string NumeroFase { get; set; }
        public string Nome { get; set; }
        public string Descrizione { get; set; }
        public string Codice { get; set; }
        public string Barcode { get; set; }
        public bool IsDisabled { get; set; }
        public List<DistintaBaseFasiMacchina> Macchine { get; set; }
        public Tuple<CausaleFase, double> TotaleTempiMacchina { get; set; }
        public List<DistintaBaseFasiManoDopera> Manodopera { get; set; }
        public Tuple<CausaleFase, double> TotaleTempiManodopera { get; set; }
        public bool IsAutomatica { get; set; }
        public bool IsAndon { get; set; }
        public double TaktMacchina { get; set; }
        public double TaktManodopera { get; set; }
        public double TaktTotale { get; set; }
        public bool IsCiclica { get; set; }
        public bool AbilitaAvanzamenti { get; set; } = true;
        public bool AbilitaVersamentiManuali { get; set; } = false;
        public bool AbilitaVersamentiAutomatici { get; set; } = false;
        public bool AbilitaMultimedia { get; set; } = false;
        public bool AbilitaMateriali { get; set; } = false;
        public bool AbilitaPulsanteEmergenza { get; set; } = false;

        public bool Abilitabarcode { get; set; } = false;
        public bool AbilitaLetturaRfid { get; set; } = false;
        public bool AbilitascritturaRfid { get; set; } = false;
        public bool AbilitaAvanzamentiStar { get; set; } = false;
        public bool AbilitaAvanzamentiStop { get; set; } = false;
        public bool AbilitaAvanzamentiPausa { get; set; } = false;
        public bool AbilitaFaseTerminataParzialmente { get; set; } = false;
        public string Visibilitastazione { get; set; }
        public int StileDiVisualizzazioneVersamenti { get; set; }
        public ExternalDoc<OperazioneFase> OperazioneFase { get; set; }
        public string Id { get; set; }
        public List<DistintaBaseFasi> Fasi { get; set; }
        public List<ExternalDoc<Materiale>> Materiali { get; set; }

        public List<DistintaBaseMateriale> Componenti { get; set; }
        public int PezziDaProdurre { get; set; }
        public int PezziProdotti { get; set; }
        public object Campo1 { get; set; }
        public object Campo2 { get; set; }
        public object Campo3 { get; set; }
        public object Campo4 { get; set; }
        public object Campo5 { get; set; }
        public object Campo6 { get; set; }
        public object Campo7 { get; set; }
        public object Campo8 { get; set; }
        public object Campo9 { get; set; }
        public object Campo10 { get; set; }
        public int TempoTeoricoSec { get; set; }

        public int BsonSizeMb { get; set; }

        public DistintaBaseFasi()
        {
            this.Fasi = new List<DistintaBaseFasi>();
        }

        public new string ToString
        {
            get
            {
                string res = this.Codice;
                if (!System.String.IsNullOrWhiteSpace(this.Nome))
                    res += (System.String.IsNullOrWhiteSpace(res) ? "" : " - ") + this.Nome;

                if (System.String.IsNullOrWhiteSpace(res))
                    res = this.Id;

                return res;
            }
        }

        [BsonIgnore]
        [JsonIgnore]
        public const string InLavorazioneColoreDefaultValue = "#30c93d";
        [BsonIgnore]
        [JsonIgnore]
        public const string InLavorazioneLabelDefaultValue = "Lavorazione";
        [BsonIgnore]
        [JsonIgnore]
        public const string InLavorazioneInEmergenzaColoreDefaultValue = "#db1a1a";

        [BsonIgnore]
        [JsonIgnore]
        public const string InLavorazioneInEmergenzaLabelDefaultValue = "Emergenza";
        [BsonIgnore]
        [JsonIgnore]
        public const string InLavorazioneInPausaColoreDefaultValue = "#e8c70e";
        [BsonIgnore]
        [JsonIgnore]
        public const string InLavorazioneInPausaLabelDefaultValue = "Pausa";

        [DefaultValue(InLavorazioneColoreDefaultValue)]
        public string InLavorazioneColore { get; set; }
        [DefaultValue(InLavorazioneLabelDefaultValue)]
        public string InLavorazioneLabel { get; set; }
        public bool InLavorazioneAbilitaTotali { get; set; }

        [DefaultValue(InLavorazioneInEmergenzaColoreDefaultValue)]
        public string InLavorazioneInEmergenzaColore { get; set; }
        [DefaultValue(InLavorazioneInEmergenzaLabelDefaultValue)]
        public string InLavorazioneInEmergenzaLabel { get; set; }
        public bool InLavorazioneInEmergenzaAbilitaTotali { get; set; }
        [DefaultValue(InLavorazioneInPausaColoreDefaultValue)]

        public string InLavorazioneInPausaColore { get; set; }
        [DefaultValue(InLavorazioneInPausaLabelDefaultValue)]
        public string InLavorazioneInPausaLabel { get; set; }
        public bool InLavorazioneInPausaAbilitaTotali { get; set; }

    }
}
