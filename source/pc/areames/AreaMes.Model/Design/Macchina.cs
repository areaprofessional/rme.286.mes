﻿using AreaMes.Meta;
using AreaMes.Model.Design;
using AreaMes.Model.Enums;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;

namespace AreaMes.Model
{

    public class Macchina : BaseDoc, IDoc
    {
        public string Id { get; set; }

        public string Codice { get; set; }

        public string Nome { get; set; }

        public string Descrizione { get; set; }

        public bool IsEsterna { get; set; }

        public bool IsDisable { get; set; }

        public string Barcode { get; set; }

        public ExternalDoc<TipiMacchina> Tipologia { get; set; }

        public ExternalDoc<MacchinaCompetenza> MacchinaCompetenza { get; set; }
        public TipoConteggio TipoConteggio{ get; set; }
        public ExternalDoc<Azienda> Azienda { get; set; }
        public bool IsM2mEnable { get; set; } = false;
        public string CodeM2mDevice { get; set; }

        public new string ToString
        {
            get
            {
                string res = this.Codice;
                if (!String.IsNullOrWhiteSpace(this.Nome))
                    res += (String.IsNullOrWhiteSpace(res) ? "" : " - ") + this.Nome;

                if (String.IsNullOrWhiteSpace(res))
                    res = this.Id;

                return res;
            }
        }

        public bool flagMagazzinoWip { get; set; }

        public int TempoAutogiustificazioneAllarme { get; set; }
        public ExternalDoc<CausaliRiavvio> CausaleAutogiustificazioneAllarme { get; set; }
        public int BsonSizeMb { get; set; }

        public string M2MDriverName { get; set; }

    }

}
