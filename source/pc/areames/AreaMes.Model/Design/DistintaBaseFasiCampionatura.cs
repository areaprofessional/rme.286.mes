﻿using System.Collections.Generic;
using AreaMes.Model.Enums;

namespace AreaMes.Model
{
    public class DistintaBaseFasiCampionatura
    {
        public string Codice { get; set; }
        public string Nome { get; set; }
        public string Descrizione { get; set; }
        public bool IsDisabled { get; set; }
        public int OgniNPezzi { get; set; }
        public int OgniNMinuti { get; set; }
        public Severita AbortisciBatchConAlmenoUnaSeverita { get; set; }
        public List<DistintaBaseFasiCampionaturaDomanda> Domande { get; set; }
    } 
}
