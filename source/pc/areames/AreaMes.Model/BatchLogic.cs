﻿using AreaMes.Meta;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using log4net;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AreaMes.Model
{
    public class BatchLogic
    {
        protected log4net.ILog Default;

        protected IDataContainer _data;
        protected IRepository _repository;
        protected IMessageServices _messages;
        protected IRealTimeManager _areaMesClient;
        protected IAreaM2MClient _areaM2MClient;
        protected IRuntimeTransportContextController _runtimeContext;
        private ISettings _settings;
        protected string _batchId;
        protected Batch _batch { get; set; }

        protected Dictionary<string, BatchParametri> _listOfBatchParams = new Dictionary<string, BatchParametri>();

        private RegistrationInfo subscrb1;
        private RegistrationInfo subscrb2;
        private RegistrationInfo subscrb3;

        public Batch Get()
        {
            return this._batch;
        }

        public void Prepare(ISettings settings)
        {
            _settings = settings;
        }
        public object GetSettingValue(string key)
        {
            return _settings.Values[key];
        }

        public virtual async Task Init(IDataContainer data,
                               IRepository repository,
                               IMessageServices messages,
                               IRealTimeManager areaMESClient,
                               IAreaM2MClient areaM2MClient,
                               IRuntimeTransportContextController runtimeContext,
                               Batch batch)
        {
            await Task.Run(() =>
            {

                Default = log4net.LogManager.GetLogger("Odp." + batch.OdP);

                try
                {
                    _data = data; _repository = repository; _messages = messages; _batchId = batch.Id; _areaMesClient = areaMESClient; _batch = batch; _areaM2MClient = areaM2MClient;
                    _runtimeContext = runtimeContext;
                    subscrb1 = _messages.Subscribe<DeviceValueChangedMessage>(DeviceValueChangedMessage.Name, OnDeviceValueChangedMessage);
                    subscrb2 = _messages.Subscribe<BatchChangedMessage>(_batchId, OnBatchChanged);
                    subscrb3 = _messages.Subscribe<FaseChangedInfo>(_batchId, OnFaseChanged);

                    Default.InfoFormat("Batch Logic '{0}' [Odp:{1}, Commessa:{2}] subscribed to internal messages (DeviceValueChangedMessage, BatchChangedMessage, FaseChangedInfo)", _batchId, _batch.OdP, _batch.Commessa);

                    if (_batch.BatchParametri == null)
                    {
                        _batch.BatchParametri = new List<BatchParametri>();
                        foreach (var par in batch.DistintaBase.DistintaBaseParametri)
                            _batch.BatchParametri.Add(new BatchParametri { Parametro = par, Trend = new List<BatchTrendParametri>() });
                    }

                    foreach (var item in _batch.BatchParametri)
                        _listOfBatchParams.Add(item.Parametro.Codice, item);
                }
                catch (Exception exc)
                {
                    Default.Error(exc);
                }

            });

        }

        public virtual void Start()
        {

        }

        public virtual void Stop()
        {
            if (subscrb1 != null)
                _messages.UnSubscribe(subscrb1);

            if (subscrb2 != null)
                _messages.UnSubscribe(subscrb2);

            if (subscrb3 != null)
                _messages.UnSubscribe(subscrb3);

            _data = null;
            _repository = null;
            _messages = null;
            _areaMesClient = null;
            _areaM2MClient = null;
            _runtimeContext = null;
        }

        protected virtual void OnFaseChanged(object arg1, FaseChangedInfo arg2)
        {

        }

        protected virtual void OnBatchChanged(object arg1, BatchChangedMessage arg2)
        {

        }

        protected virtual void OnDeviceValueChangedMessage(object arg1, DeviceValueChangedMessage arg2)
        {

        }


        protected void registerTrend(DeviceValueChangedMessage message, string visualCode, BatchParametri parametro)
        {
            var oThing = message.FirstOrDefault(o => o.Registry.VisualCode == visualCode);
            if (oThing == null) return;

            lock (parametro.Trend)
            {
                var oLastPar = parametro.Trend.LastOrDefault();
                var oNewPar = new BatchTrendParametri { DataOra = DateTime.Now, Valore = oThing.Value };
                if (oLastPar != null)
                {
                    oNewPar.DataOraPrecedente = oLastPar.DataOra;
                    oNewPar.ValorePrecedente = oLastPar.Valore;
                }

                parametro.Trend.Add(oNewPar);
            }
            calculateStatistics(parametro);
        }

        protected void registerTrend(object value, BatchParametri parametro)
        {
            lock (parametro.Trend)
            {
                var oLastPar = parametro.Trend.LastOrDefault();
                var oNewPar = new BatchTrendParametri { DataOra = DateTime.Now, Valore = value };
                if (oLastPar != null)
                {
                    oNewPar.DataOraPrecedente = oLastPar.DataOra;
                    oNewPar.ValorePrecedente = oLastPar.Valore;
                }

                parametro.Trend.Add(oNewPar);
            }


            calculateStatistics(parametro);
        }

        private void calculateStatistics(BatchParametri parametro)
        {
            if (parametro.Parametro.Tipo == Enums.TipoDato.Numeric)
            {
                lock (parametro.Trend)
                {
                    var temp = parametro.Trend.ToList();

                    parametro.Avg = temp.Average(o => o.Valore == null ? 0 : Convert.ToDouble(o.Valore));
                    parametro.Min = temp.Min(o => o.Valore == null ? 0 : Convert.ToDouble(o.Valore));
                    parametro.Max = temp.Max(o => o.Valore == null ? 0 : Convert.ToDouble(o.Valore));
                }
            }
        }
    }


    public class SupervisorBatchLogicBase
    {

        private ISettings _settings;
        private IRepository _repository;
        protected IRuntimeTransportContextController _runtimeContext;
        protected log4net.ILog Default;

        // protected  StartBatchInfo Start { get; private set; }

        public Func<List<BatchLogic>> BatchLogicInMemory { get; set; }
        protected Action<Batch> Start { get; private set; }
        protected Func<List<Batch>> BatchInMemory { get; private set; }

        public void Prepare(ISettings settings, Action<Batch> startAction, IRepository repository, Func<List<Batch>> batchInMemory, IRuntimeTransportContextController runtimeContext)
        {
            Default = log4net.LogManager.GetLogger("SPRV");
            _settings = settings;
            Start = startAction;
            BatchInMemory = batchInMemory;
            _runtimeContext = runtimeContext;
            _repository = repository;
        }

        public object GetSettingValue(string key)
        {
            object res = null;
            _settings.Values.TryGetValue(key, out res);

            return res;
        }

        public virtual void Init()
        {

        }

        public virtual void OnBatchAdd(BatchLogic batchLogic)
        {

        }

        protected IRepository Repository
        {
            get { return _repository; }
        }
    }


    public static class BatchUtils
    {
        public static async Task PrepareBatchTo_InAttesa(Batch item, IRepository db)
        {
            item.UserLockedDate = DateTime.Now;
            // creo le fasi
            List<BatchFase> listOfFasi = new List<BatchFase>();
            foreach (var oFase in item.DistintaBase.Fasi)
            {
                List<BatchFaseMacchina> bfm = new List<BatchFaseMacchina>();
                List<BatchFaseManoDopera> bfman = new List<BatchFaseManoDopera>();

                if (oFase.Macchine != null)
                    foreach (var oMacchina in oFase.Macchine)
                    {
                        Macchina machine = new Macchina();
                        if (oMacchina.Macchina != null)
                            machine = await db.GetAsync<Macchina>(oMacchina.Macchina.Id);
                        bfm.Add(new BatchFaseMacchina { Macchina = machine, CausaleFase = oMacchina.CausaleFase, Fine = null, Inizio = null });
                    }

                if (oFase.Manodopera != null)
                    foreach (var oManodopera in oFase.Manodopera)
                        bfman.Add(new BatchFaseManoDopera { Operatore = null, CausaleFase = oManodopera.CausaleFase, Fine = null, Inizio = null, Descrizione = oManodopera.Descrizione });


                listOfFasi.Add(new BatchFase
                {
                    DistintaBaseFase = oFase,
                    Macchine = bfm,
                    Manodopera = bfman,
                    NumeroFase = oFase.NumeroFase,
                    Stato = StatoFasi.InAttesa,
                    UserLocked = null,
                    UserLockedDate = item.UserLockedDate,
                    IsAndon = oFase.IsAndon,
                    IsCiclica = oFase.IsCiclica,
                    TaktMacchina = oFase.TaktMacchina,
                    TaktManodopera = oFase.TaktManodopera,
                    TaktTotale = oFase.TaktTotale,
                    PezziPianificati = Convert.ToInt32(oFase.PezziDaProdurre),
                    PezziProdotti = Convert.ToInt32(oFase.PezziProdotti)
                });
            }

            item.Fasi = listOfFasi;

            item.BatchParametri = new List<BatchParametri>();
            foreach (var par in item.DistintaBase.DistintaBaseParametri)
                item.BatchParametri.Add(new BatchParametri { Parametro = par, Trend = new List<BatchTrendParametri>() });

        }


        public static int OEE_Rf_CalculateByAvanzamenti(Batch pBatch)
        {
            if ((pBatch.Inizio.HasValue) && (pBatch.Inizio.Value.Year > 2000))
            {
                // turno != null è stato inserito per evitare di abbassare l'oee in caso di fine turno
                var avanzamentiFermi = pBatch.BatchAvanzamenti.Where(y => (y.Stato == StatoFasi.InLavorazioneInEmergenza || y.Stato == StatoFasi.InLavorazioneInPausa) && y.Turno?.Id != null);
                var avanzamentiFuoriTurno = pBatch.BatchAvanzamenti.Where(y => (y.Stato == StatoFasi.InLavorazioneInPausa) && y.Turno?.Id == null);
                var avanzamentiDisponibilita = pBatch.BatchAvanzamenti.Where(y => (y.Stato == StatoFasi.InLavorazione || y.Stato == StatoFasi.InLavorazioneInEmergenza || y.Stato == StatoFasi.InLavorazioneInPausa) && y.Turno?.Id != null);
                var totTempoDisponibilità = avanzamentiDisponibilita.Sum(o => o.Fine.HasValue ? o.Fine.Value.Subtract(o.Inizio.Value).TotalSeconds : DateTime.Now.Subtract(o.Inizio.Value).TotalSeconds);
                var totTempoFermi = avanzamentiFermi.Sum(o => o.Fine.HasValue ? o.Fine.Value.Subtract(o.Inizio.Value).TotalSeconds : DateTime.Now.Subtract(o.Inizio.Value).TotalSeconds);
                var totTempoFuoriTurno = avanzamentiFuoriTurno.Sum(o => o.Fine.HasValue ? o.Fine.Value.Subtract(o.Inizio.Value).TotalSeconds : DateTime.Now.Subtract(o.Inizio.Value).TotalSeconds);

                var result = 0;
                if(totTempoDisponibilità != 0)
                {
                    double x1 = (totTempoDisponibilità - totTempoFermi);
                    double x = x1 / totTempoDisponibilità;
                    if (x != double.NaN)
                        result = Convert.ToInt32(x * 100);
                    if (result < 0) result = 0;
                }
                return result;
            }
            return 0;
        }

        //public static int OEE_Rf_Calculate(Batch pBatch)
        //{


        //    if ((pBatch.Inizio.HasValue) && (pBatch.Inizio.Value.Year > 2000))
        //    {
        //        double tFermi = 0;
        //        if (pBatch.Fermi != null)
        //            foreach (var singoloStop in pBatch.Fermi)
        //            {
        //                DateTime dtFineStop = DateTime.Now;
        //                if (singoloStop.Fine.HasValue)
        //                    dtFineStop = singoloStop.Fine.Value;

        //                var diff = (dtFineStop - singoloStop.Inizio.Value).TotalSeconds;
        //                if (diff > 0)
        //                    tFermi += diff;
        //            }


        //        double tTempoTotale = 0;
        //        DateTime dtFineBatch = DateTime.Now;
        //        if (pBatch.Fine.HasValue)
        //            dtFineBatch = pBatch.Fine.Value;

        //        tTempoTotale = (dtFineBatch - pBatch.Inizio.Value).TotalSeconds;

        //        double x1 = (tTempoTotale - tFermi);
        //        double x = x1 / tTempoTotale;
        //        return Convert.ToInt32(x * 100);
        //    }

        //    return 0;
        //}
        public static async Task<BatchAutoCounter> GetAutoCounterInfo(IRepository db, bool getNew)
        {
            var ret = new BatchAutoCounter { IsEnabled = false };
            var allpa = await db.All<Parametri>();
            var CNT_BATCH_IS_ENABLED = allpa.FirstOrDefault(o => o.Codice == "CNT_BATCH_IS_ENABLED");

            if ((CNT_BATCH_IS_ENABLED != null) && (CNT_BATCH_IS_ENABLED.ValoreDefault == "1"))
            {

                var CNT_BATCH_GLOBALE = allpa.FirstOrDefault(o => o.Codice == "CNT_BATCH_GLOBALE");
                var CNT_BATCH_PARZIALE = allpa.FirstOrDefault(o => o.Codice == "CNT_BATCH_PARZIALE");
                var CNT_BATCH_PARZIALE_RESET_OGNI = allpa.FirstOrDefault(o => o.Codice == "CNT_BATCH_PARZIALE_RESET_OGNI");
                var CNT_BATCH_PARZIALE_DATA_ULTIMA_ASSEGNAZIONE = allpa.FirstOrDefault(o => o.Codice == "CNT_BATCH_PARZIALE_DATA_ULTIMA_ASSEGNAZIONE");

                var _cntParziale = Convert.ToInt32(CNT_BATCH_PARZIALE.ValoreDefault);
                var _cntGlobale = Convert.ToInt32(CNT_BATCH_GLOBALE.ValoreDefault);


                if (CNT_BATCH_PARZIALE_RESET_OGNI != null)
                {
                    DateTime _dataUltimaAssegnazione = DateTime.Now;
                    DateTime.TryParseExact(CNT_BATCH_PARZIALE_DATA_ULTIMA_ASSEGNAZIONE.ValoreDefault, "dd-MM-yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out _dataUltimaAssegnazione);

                    if (CNT_BATCH_PARZIALE_RESET_OGNI.ValoreDefault == "gg")
                    {
                        // reset ogni giorni
                        if (DateTime.Now.Subtract(_dataUltimaAssegnazione).TotalDays > 1)
                            _cntParziale = 0;

                    }
                    else if (CNT_BATCH_PARZIALE_RESET_OGNI.ValoreDefault == "mm")
                    {
                        // reset ogni mese
                        if (DateTime.Now.Month != _dataUltimaAssegnazione.Month)
                            _cntParziale = 0;
                    }
                    else if (CNT_BATCH_PARZIALE_RESET_OGNI.ValoreDefault == "aa")
                    {
                        // reset ogni anno
                        if (DateTime.Now.Year != _dataUltimaAssegnazione.Year)
                            _cntParziale = 0;
                    }
                    else if (CNT_BATCH_PARZIALE_RESET_OGNI.ValoreDefault == "HH")
                    {
                        // reset ogni ora
                        if (DateTime.Now.Hour != _dataUltimaAssegnazione.Hour)
                            _cntParziale = 0;
                    }
                }

                //if (getNew)
                //{
                _cntGlobale += 1;
                _cntParziale += 1;
                //}

                CNT_BATCH_GLOBALE.ValoreDefault = Convert.ToString(_cntGlobale);
                CNT_BATCH_PARZIALE.ValoreDefault = Convert.ToString(_cntParziale);
                CNT_BATCH_PARZIALE_DATA_ULTIMA_ASSEGNAZIONE.ValoreDefault = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");

                if (getNew)
                {
                    await db.SetAsync(CNT_BATCH_GLOBALE);
                    await db.SetAsync(CNT_BATCH_PARZIALE);
                    await db.SetAsync(CNT_BATCH_PARZIALE_DATA_ULTIMA_ASSEGNAZIONE);
                }

                ret.IsEnabled = true;
                ret.Global = _cntGlobale;
                ret.Partial = _cntParziale;
                ret.LastAssign = DateTime.Now;
            }

            return ret;

        }

        public static int CalculateEffectiveTime(Batch batch)
        {
            int time = 0;
            try
            {
                foreach (var ava in batch.BatchAvanzamenti)
                {
                    if (IsInLavorazione(ava.Stato))
                    {
                        if (!ava.Fine.HasValue) { ava.Fine = DateTime.Now; }

                        if (ava.Inizio.HasValue && ava.Turno is object && ava.Turno.Id != string.Empty)
                        {
                            time += (int)(ava.Fine.Value - ava.Inizio.Value).TotalSeconds;
                        }
                    }
                }
            }
            catch(Exception){}
            
            return time;
        }
        public static async Task<int> GetBatchAutoPriorityCounterInfo(IRepository db)
        {
            var allpa = await db.All<Parametri>();
            var CNT_BATCH_PRIORITY = allpa.FirstOrDefault(o => o.Codice == "CNT_BATCH_PRIORITY");

            if (CNT_BATCH_PRIORITY != null)
            {
                var _cntPriorita = Convert.ToInt32(CNT_BATCH_PRIORITY.ValoreDefault);
                _cntPriorita += 1;
                CNT_BATCH_PRIORITY.ValoreDefault = Convert.ToString(_cntPriorita);

                await db.SetAsync(CNT_BATCH_PRIORITY);

                return _cntPriorita;
            }
            return 0;
        }
        public static bool IsInLavorazione(Batch item)
        {
            return (item.Stato == StatoBatch.InLavorazione) ||
                   (item.Stato == StatoBatch.InLavorazioneInEmergenza) ||
                   (item.Stato == StatoBatch.InLavorazioneInPausa) ||
                   (item.Stato == StatoBatch.InLavorazioneInPausaS);
        }

        public static bool IsInLavorazione(StatoFasi stato)
        {
            return (stato == StatoFasi.InLavorazione) ||
                   (stato == StatoFasi.InLavorazioneInEmergenza) ||
                   (stato == StatoFasi.InLavorazioneInPausa) ||
                   (stato == StatoFasi.InLavorazioneInPausaS);
        }

        public static TurnoDiLavoro GetCurrentShiftActive(List<TurnoDiLavoro> shifts, string idMacchina)
        {
            TurnoDiLavoro turnoActive = null;

            // Davide A, 20/04/2023, modifica per gestire la forzatura del cambio di stato sulla macchina
            // quando la lavorazione è fuori turno. Se fuori turno e la macchina si pone in allarme, allora viene forzato
            // lo stato in pausa
            foreach (var shift in shifts)
            {
                turnoActive = BatchUtils.checkIfShiftIsActive(DateTime.Now, shift, idMacchina);
                if (turnoActive != null)
                    break;
                
            }

            return turnoActive;
        }

        public static TurnoDiLavoro checkIfShiftIsActive(DateTime data, TurnoDiLavoro turno, string idMacchina)
        {

            TurnoDiLavoro turnoReturn = null;
            TimeSpan inizio = new TimeSpan(turno.InizioHH, turno.InizioMM, 0);
            TimeSpan fine = new TimeSpan(turno.FineHH, turno.FineMM, 0);

            turno.ValidoDal = new DateTime(turno.ValidoDal.Year, turno.ValidoDal.Month, turno.ValidoDal.Day);
            turno.ValidoAl = new DateTime(turno.ValidoAl.Year, turno.ValidoAl.Month, turno.ValidoAl.Day);

            // controllo se l'orario è compreso in questo turno
            if (data.TimeOfDay >= inizio && data.TimeOfDay < fine || (turno.IsEccezione == true && data >= turno.ValidoDal && data <= turno.ValidoAl && data.TimeOfDay >= inizio && data.TimeOfDay < fine))
            {
                // controllo se oggi è un giorno flaggato
                if ((
                           DayOfWeek.Monday == data.DayOfWeek && turno.FlagLunedi
                        || DayOfWeek.Tuesday == data.DayOfWeek && turno.FlagMartedi
                        || DayOfWeek.Wednesday == data.DayOfWeek && turno.FlagMercoledi
                        || DayOfWeek.Thursday == data.DayOfWeek && turno.FlagGiovedi
                        || DayOfWeek.Friday == data.DayOfWeek && turno.FlagVenerdi
                        || DayOfWeek.Saturday == data.DayOfWeek && turno.FlagSabato
                        || DayOfWeek.Sunday == data.DayOfWeek && turno.FlagDomenica)
                        && (idMacchina == null || turno.Macchine.FirstOrDefault(x => x.Id == idMacchina) != null)
                        )
                {
                    turnoReturn = turno;
                }
            }
            return turnoReturn;
        }

        /// <summary>
        /// Metodo per gestire il cambio turno per il batch
        /// </summary>
        /// <param name="batch"></param>
        /// <param name="o"></param>
        /// <param name="runtimeContext"></param>
        /// <param name="logger"></param>
        public static void BatchChangeShift(Batch batch, CambioTurnoMessage o, IRuntimeTransportContextController runtimeContext, ILog logger)
        {
            //vengono scaricate tutte le fasi aperte che sono aperte/in emergenza/in pausa
            var listOfPhase = batch?.BatchAvanzamenti?.Where(x => x?.Stato != StatoFasi.Terminato && x?.Stato != StatoFasi.Abortito && x?.Stato != StatoFasi.InAttesa && x?.Stato != StatoFasi.InModifica && x?.Stato != StatoFasi.Eliminato && x?.Fine == null).ToList();
            var dateNow = DateTime.Now;
            if (listOfPhase != null)
            {
                logger.InfoFormat("BatchChangeShift() cycling phase ...");
                foreach (var phase in listOfPhase)
                {
                    foreach (var shiftAfter in o.ListOfAfter)
                    {
                        var machinesPhase = batch.Fasi.FirstOrDefault(x => x.NumeroFase == phase.NumeroFase).Macchine[0].Macchina;
                        var machineSelect = shiftAfter.Macchine.FirstOrDefault(x => x.Id == machinesPhase.Id);

                        if (machineSelect != null || shiftAfter.Macchine.Count == 0)
                        {
                            //controlli se la fase ciclata appartiene una macchina compresa nel turno ciclato
                            //crei una nuova fase con il turno ciclato
                            logger.InfoFormat("BatchChangeShift() split batchAvanzamenti ...");
                            logger.InfoFormat("BatchChangeShift() previous: " + phase.Turno.Codice + " next: " + shiftAfter.Codice + "...");

                            var dateFineVecchioInizioNuovo = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, shiftAfter.InizioHH, shiftAfter.InizioMM, 0);
                            batch.BatchAvanzamenti.Find(x => x.NumeroFase == phase.NumeroFase && x.Inizio == phase.Inizio).Fine = dateFineVecchioInizioNuovo;
                            BatchAvanzamento newFase = new BatchAvanzamento();
                            newFase.NumeroFase = phase.NumeroFase;
                            newFase.Nota = phase.Nota;
                            newFase.Stato = phase.Stato;
                            phase.Fine = dateFineVecchioInizioNuovo;
                            newFase.Inizio = dateFineVecchioInizioNuovo;
                            newFase.Fine = null;
                            newFase.Turno = shiftAfter;
                            newFase.BatchFaseMacchina = phase.BatchFaseMacchina;
                            newFase.BatchFaseManoDopera = phase.BatchFaseManoDopera;
                            batch.BatchAvanzamenti.Add(newFase);

                            logger.InfoFormat("BatchChangeShift() split batchAvanzamenti ... completed");

                            //macchina trovata  e riapri la fase come prima con turno nuovo
                        }
                        else
                        {
                            //vengono controllate le macchine che non sono più nel nuovo turno e chiudo le fasi che gli appartengono 
                            // es T1 (M1, M2) ---> T2 (M1) metto in pausa le fasi in lavoro di M2
                            logger.InfoFormat("BatchChangeShift() find avanzamenti without next shift ...");

                            foreach (var machine in o.Before.Macchine)
                            {
                                var machineId = "";
                                foreach (var turnAfter in o.ListOfAfter)
                                {
                                    var machineTot = turnAfter.Macchine.FirstOrDefault(x => x.Id == machine?.Id);
                                    machineId = machineTot?.Id ?? null;
                                    //var machineProva = o.ListOfAfter.FirstOrDefault(x => x.Macchine.FirstOrDefault(z => z.Id == machine.Id) != null).Macchine;
                                }

                                if (machineId == null)
                                {
                                    var machinesPhasee = batch.Fasi.FirstOrDefault(x => x.NumeroFase == phase.NumeroFase).Macchine[0].Macchina;
                                    var machineSelected = o.Before.Macchine.FirstOrDefault(x => x.Id == machinesPhasee.Id);

                                    if (machineSelected != null)
                                    {
                                        logger.InfoFormat("BatchChangeShift() set avanzamenti on pause without shift ...");
                                        logger.InfoFormat("BatchChangeShift() previous: " + phase.Turno.Codice + " ... ");

                                        var phaseApp = batch.Fasi.FirstOrDefault(so => so.DistintaBaseFase.NumeroFase == phase.NumeroFase);
                                        phaseApp.Stato = Model.Enums.StatoFasi.InLavorazioneInPausa;
                                        runtimeContext.SetFase(new SetFaseInfo { idBatch = batch.Id, fase = phaseApp });
                                        logger.InfoFormat("BatchChangeShift() set avanzamenti on pause without shift ... completed");
                                    }
                                }
                            }
                            logger.InfoFormat("BatchChangeShift() find avanzamenti without next shift ... completed");
                        }
                    }
                    if (o.ListOfAfter.Count == 0)
                    {
                        logger.InfoFormat("ShiftChange() shift ends ... ");
                        var machinesPhase = batch.Fasi.FirstOrDefault(x => x.NumeroFase == phase.NumeroFase).Macchine[0].Macchina;
                        var machineSelect = o.Before.Macchine.FirstOrDefault(x => x.Id == machinesPhase.Id);
                        if (machineSelect != null || o.Before.Macchine.Count == 0)
                        {
                            logger.InfoFormat("ShiftChange() set avanzamenti on pause without shift ...");
                            var phaseApp = batch.Fasi.FirstOrDefault(so => so.DistintaBaseFase.NumeroFase == phase.NumeroFase);
                            if (phase.Stato != StatoFasi.InLavorazione)
                                phase.Stato = Model.Enums.StatoFasi.InLavorazioneInPausa;
                            else
                                phase.Stato = Model.Enums.StatoFasi.InLavorazioneInPausa;
                            runtimeContext.SetFase(new SetFaseInfo { idBatch = batch.Id, fase = phaseApp });
                            logger.InfoFormat("ShiftChange() set avanzamenti on pause without shift ... completed");
                        }
                        logger.InfoFormat("ShiftChange() shift ends ... completed");
                    }
                }
                logger.InfoFormat("BatchChangeShift() cycling phase ... completed");
            }
        }



        /// <summary>
        /// Metodo che chiude i fermi aperti in un batch
        /// </summary>
        /// <param name="item"></param>
        /// <param name="idCausale"></param>
        /// <param name="username"></param>
        /// <param name="notaRiavvio"></param>
        /// <param name="fase"></param>
        /// <param name="idMacchina"></param>
        /// <param name="repository"></param>
        /// <returns></returns>
        public static Batch BatchGestioneFermi(Batch item, string idCausale, string username, string notaRiavvio, BatchFase fase, string idMacchina, IRepository repository, ILog logger)
        {
            logger.InfoFormat($"Start BatchGestioneFermi batch.Odp:'{item.OdP}',idCausale:'{idCausale}',idMacchina:'{idMacchina}',fase.NumeroFase:'{fase.NumeroFase}'...");
            //ottiene tutti i fermi che non hanno valorizzato la data di chiusura
            var listBatchFermi = item.Fermi.Where(f => f.Fine == null && (f.NumeroFase == Convert.ToInt16(fase.NumeroFase) || f.NumeroFase == 0)).ToList();
            logger.InfoFormat($"Start BatchGestioneFermi batch.Odp:'{item.OdP}',listBatchFermi.Count:'{listBatchFermi.Count}'...");

            var justifications = repository.GetWhereAsync<CausaliRiavvio>(x => x.IsStopCausal == true).GetAwaiter().GetResult();

            foreach (var bf in listBatchFermi)
            {
                logger.Debug($"Start BatchGestioneFermi batch.Odp:'{item.OdP}',     bf.Inizio:'{bf.Inizio}',bf.NumeroFase:'{bf.NumeroFase}'...");
                bf.Fine = DateTime.Now;
                bf.UserUnLocked = username;
                bf.NumeroFase = Convert.ToInt32(fase.NumeroFase);
                bf.MacchinaId = idMacchina;
                bf.Causale = BatchCausaleFermi.Emergenza;
                bf.NotaDiRiavvio = notaRiavvio;

                var causale = new ExternalDoc<CausaliRiavvio>();
                //Se la causale è presente viene inserita quella che gli viene passata
                if (idCausale != null && idCausale != "")
                {
                    causale.Id = idCausale;
                }
                else
                {
                    //Se non gli viene passata la causale viene calcolato il tempo di fermo
                    var fine = new DateTime();
                    var inizio = new DateTime();
                    bool flagAutoGiustificato = false;
                    if (bf.Fine != null && bf.Inizio != null)
                    {
                        fine = Convert.ToDateTime(bf.Fine);
                        inizio = Convert.ToDateTime(bf.Inizio);
                        var diffFineInizioInSec = (fine - inizio).TotalSeconds;
                        var macchinaFase = fase.Macchine.First();
                        //Se il tempo di del fermo è minore del tempo di autogiustificazione viene inserita l'autogiustifazione automatica
                        if (macchinaFase.Macchina.TempoAutogiustificazioneAllarme > 0 && diffFineInizioInSec < (macchinaFase.Macchina.TempoAutogiustificazioneAllarme * 60))
                        {
                            causale.Id = macchinaFase.Macchina.CausaleAutogiustificazioneAllarme.Id;
                            flagAutoGiustificato = true;
                        }
                    }

                    if (!flagAutoGiustificato)
                    {
                        //scarica tutti i tipi di causali Riavvio filtrati per IsStopCausal= true
                        //se esiste una justificazione di default la utilizzo altrimenti si utilizza quella di default
                        var justification = justifications.FirstOrDefault(x => x.Default == true);
                        if (justification == null)
                            //se null selezione la causale precaricata 000
                            justification = justifications.FirstOrDefault(x => x.Codice == "AreaMes_Default");
                        causale.Id = justification.Id;
                    }
                }


                if (bf?.CausaliRiavvio == null || bf.CausaliRiavvio.Item?.Codice == "AreaMes_Default")
                    bf.CausaliRiavvio = causale;
                // viene calcolato il tempo di fermo
                var difference = bf.Fine.Value.Subtract(bf.Inizio.Value).TotalSeconds;
                bf.TotaleTempo = Convert.ToInt32(difference);
                logger.Debug($"Start BatchGestioneFermi batch.Odp:'{item.OdP}',     bf.Inizio:'{bf.Inizio}',bf.NumeroFase:'{bf.NumeroFase}',bf.Fine:'{bf.Fine}',bf.TotaleTempo:'{bf.TotaleTempo}',bf.CausaliRiavvio.Id:'{bf.CausaliRiavvio.Id}'... completed");
            }
            logger.InfoFormat($"Start BatchGestioneFermi batch.Odp:'{item.OdP}',idCausale:'{idCausale}',idMacchina:'{idMacchina}'... completed");
            return item;
        }

        public class BatchAutoCounter
        {
            public bool IsEnabled { get; set; }
            public int Global { get; set; }
            public int Partial { get; set; }
            public DateTime? LastAssign { get; set; }

        }




    }


    public class DailyTimer : IDisposable
    {
        private DateTime _lastRunDate;
        private TimeSpan _time;
        private Timer _timer;
        private Action _callback;
        private Action _callbackAfterDelay;
        private double _delaySecondCallback;
        private DateTime _createTime;
        public DailyTimer(TimeSpan time, Action callback, Action callbackAfterDelay = null, double delaySecondCallback = 0)
        {
            _time = time;
            _timer = new Timer(CheckTime, null, TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(30));
            _callback = callback;
            _callbackAfterDelay = callbackAfterDelay;
            _delaySecondCallback = delaySecondCallback;
            _createTime = DateTime.Now;
        }

        private void CheckTime(object state)
        {
            // 2016-08-02 Modifica se _lastRunDate a null perchè altrimenti mi carica la sequenza
            if (_lastRunDate == DateTime.MinValue)
                if ((_createTime.TimeOfDay > _time) && (_createTime.Date == DateTime.Now.Date))
                    return;
            if (_lastRunDate == DateTime.Today)
                return;
            if (DateTime.Now.TimeOfDay < _time)
                return;
            _lastRunDate = DateTime.Today;
            _callback();
            if (_callbackAfterDelay != null)
                Task.Delay(Convert.ToInt32(_delaySecondCallback * 1000)).ContinueWith((_) => { _callbackAfterDelay(); });
        }

        public void Dispose()
        {
            if (_timer == null)
                return;
            _timer.Dispose();
            _timer = null;
        }
    }
}


//public delegate  void StartBatchInfo(Batch batch, bool prepare = false);

