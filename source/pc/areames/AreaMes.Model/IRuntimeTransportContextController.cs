﻿using AreaMes.Model.Design;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model
{
    public interface IRuntimeTransportContextController
    {
        List<Batch> GetSfo(string idStato, string loggedUser, string idStazione);

        List<Batch> GetSfoFromMachine(string idMacchina, string idStato, string loggedUser);

        List<BatchFase> GetFasiFromBatch(string idBatch, string loggedUser);

        Task<string> SetFase(SetFaseInfo utBatch );

        Task<string> SetBatch(SetBatchInfo utBatch);

        Task<string> SetFase2(SetFaseInfo utBatch);

        Task<string> SetBatch2(SetBatchInfo utBatch);

        string AddPiecesToBatch(VersamentoBatch vBatch);

        Task<string> SetBatchMateriale(string idBatch, List<BatchMateriali> materiali);

        Task<SetEventsResponse> SetEvents(SetEventsRequest data);
        Task<SetEvent_PiecesResponse> SetEvent_Pieces_Type(SetEvent_PiecesRequest data);
    }

    /// <summary>
    /// classe di utility per web api contentente idbatch e la fase
    /// </summary>
    public class SetFaseInfo
    {
        public string idBatch { get; set; }
        public BatchFase fase { get; set; }
        public string Username { get; set; }
        public string causaleRiavvio { get; set; }
        public string notaRiavvio { get; set; }
        public StatoFasi? idStato { get; set; }
    }

    public class SuspendBatchInfo
    {
        public string BatchId { get; set; }
        public Operatore Operatore { get; set; }
    }

    public class SetBatchInfo
    {
        public string idBatch { get; set; }
        public StatoBatch idStato { get; set; }
        public string causaleRiavvio { get; set; }
        public DateTime dataRiavvio { get; set; }
        public string notaRiavvio { get; set; }
        public string Username { get; set; }
        public string Macchina { get; set; }
        public string NomeFase { get; set; }
        public string NumeroFase { get; set; }
        public string TurnoId { get; set; }

    }
    public class VersamentoBatch
    {
        public string idBatch { get; set; }
        public string pezziVersati { get; set; }
        public string numeroFase { get; set; }
        public string Username { get; set; }
    }


    public class BaseRequest
    {
        public string IdBatch { get; set; }
        public string IdFase { get; set; }
        public string Barcode { get; set; }
        public string StationId { get; set; }
        public int TimeoutRequest { get; set; }
        public string Username { get; set; }
    }

    public class SendBarcodeRequest : BaseRequest
    {
        public bool IsRfid { get; set; }

        public Materiale Material { get; set; }
    }

    public class SendBarcodeResponse
    {
        public string ErrorMessage { get; set; }

        public bool IsOnError { get; set; }

        public bool IsCompleted { get; set; }
    }


    public class ReadBarcodeRequest : BaseRequest
    {
        public bool IsRfid { get; set; }
    }

    public class ReadBarcodeResponse 
    {
        public string ErrorMessage { get; set; }
        public bool IsOnError { get; set; }
        public bool IsCompleted { get; set; }
        public string Barcode { get; set; }
    }


    public class SetEventsRequest : BaseRequest
    {
        public List<Evento> Events { get; set; }
    }

    public class SetEventsResponse
    {
        public string ErrorMessage { get; set; }
        public bool IsOnError { get; set; }
        public bool IsCompleted { get; set; }
    }


    public class SetEvent_PiecesRequest : BaseRequest
    {
        public Source Source { get; set; }

        public object Valore { get; set; }

        public Macchina Macchina { get; set; }

        public Model.Enums.TipoConteggio TipoConteggio { get; set; }

        public TipoValori Causale { get; set; }
        public Operatore Operatore { get; set; }
        public BatchFase Fase { get; set; }

        public TipoEvento TipoEvento { get; set; }
        public Materiale Materiale { get; set; }
    }

    public class SetEvent_PiecesResponse
    {
        public string ErrorMessage { get; set; }
        public bool IsOnError { get; set; }
        public bool IsCompleted { get; set; }
    }

    public class SetBlocksRequest : BaseRequest
    {
        public List<BatchFermi> Blocks { get; set; }
        public string IdCausal { get; set; }
    }
    public class SetBlocksResponse
    {
        public string ErrorMessage { get; set; }
        public bool IsOnError { get; set; }
        public bool IsCompleted { get; set; }
    }

}


