﻿using AreaMes.Meta;
using AreaMes.Model.Dto;
using AreaMes.Model.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model
{
    public class DeviceValueChangedMessage : List<DataItem>
    {
        public static string Name = "DeviceValueChangedMessage";

        public string DeviceId { get; set; }
    }

    public class BatchChangedMessage
    {
        public static string Name = "BatchChangedMessage";
    }

    public class FaseChangedInfo
    {
        public static string Name = "FaseChangedInfo";

        public BatchAvanzamento Current { get; set; }
        public BatchAvanzamento Previous { get; set; }
        public string Username { get; set; }
    }



    public class FaseChangedInfoBefore
    {
        public static string Name = "FaseChangedInfoBefore";

        public SetFaseInfo Current { get; set; }
        public BatchFase fase { get; set; }

        public string Username { get; set; }
    }

    public class Logon
    {
        public static string Name = "Logon";

        public string Username { get; set; }
    }

    public class Logoff
    {
        public static string Name = "Logoff";

        public string Username { get; set; }
    }


    public class SendBarcodeMessage
    {
        public  SendBarcodeRequest Data { get; set; }

        public SendBarcodeResponse Response { get; set; }

        public static string Name = "SendBarcodeMessage";
    }

    public class ReadBarcodeMessage
    {
        public ReadBarcodeRequest Data { get; set; }

        public ReadBarcodeResponse Response { get; set; }

        public static string Name = "ReadBarcodeMessage";
    }


    public class BatchMaterialeMessage
    {
        public BatchMateriali Data { get; set; }

        public string BatchId { get; set; }

        public static string Name = "BatchMaterialeMessage";
    }

    public class CambioTurnoMessage
    {
        public static string Name = "CambioTurnoMessage";

        public TurnoDiLavoro Before { get; set; }

        public List<TurnoDiLavoro> ListOfAfter { get; set; }

    }


    public class BatchForceSuspending
    {
        public static string Name = "BatchForceSuspending";

        public List<string> BatchIds { get; set; }

        public string Username { get; set; }
    }

    public class RequestStartImportBatch
    {
        public static string Name = "RequestStartImportBatch";

        public string Username { get; set; }
    }

    public class GetDataDashboardOee
    {
        public static string Name = "GetDataDashboardOee";
        public DashboardOeeMacchina Data { get; set; }
    }
}
