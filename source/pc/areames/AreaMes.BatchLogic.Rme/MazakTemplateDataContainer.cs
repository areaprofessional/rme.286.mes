﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Model.Runtime;
using log4net;
using System;
using System.Linq;
using System.Threading;

namespace AreaMes.BatchLogic.Rme
{
    public class MazakTemplateDataContainer
    {
        private DateTime _startAllarm;
        private Timer _startAllarmWatchDog;
        private object _startAll_lock = new object();
        private int _microAllarmMinutes = 2;


        public DataItem plcVar_daPLCaPC_PezziProdotti;
        public DataItem plcVar_daPLCaPC_ToolNumber;
        public DataItem plcVar_daPLCaPC_Execution;
        public DataItem plcVar_daPLCaPC_ControllerMode;
        public DataItem plcVar_daPLCaPC_ProgramComment;
        public DataItem plcVar_daPLCaPC_Program;
        public DataItem plcVar_daPLCaPC_EmergencyStop;
        public string machineCode;
        private IRuntimeTransportContextController _runtimeTransportContextController;
        private ILog _logger;
        private int _reachAllarmAfterMinutes;
        private Action _onSetAllarm;
        private Action _onSetWorking;
        private Action _onSetPause;
        private string _batchOdp;


        public MazakTemplateDataContainer(IRuntimeTransportContextController @runtime,
            ILog @default,
            IDataContainer data,
            string machineCode,
            int reachAllarmAfterMinutes,
             Action onSetAllarm,
             Action onSetWorking,
             Action onSetPause)
        {
            _reachAllarmAfterMinutes = reachAllarmAfterMinutes;
            _runtimeTransportContextController = @runtime;
            this.machineCode = machineCode;
            _onSetAllarm = onSetAllarm;
            _onSetWorking = onSetWorking;
            _onSetPause = onSetPause;
            _logger = @default;
            var _device = data.Get(machineCode);
            if (_device == null) return;
            plcVar_daPLCaPC_PezziProdotti = _device.Items.FirstOrDefault(o => o.Key.EndsWith("daPLCaPC_PezziProdotti")).Value;
            plcVar_daPLCaPC_ToolNumber = _device.Items.FirstOrDefault(o => o.Key.EndsWith("daPLCaPC_ToolNumber")).Value;
            plcVar_daPLCaPC_Execution = _device.Items.FirstOrDefault(o => o.Key.EndsWith("daPLCaPC_Execution")).Value;
            plcVar_daPLCaPC_ControllerMode = _device.Items.FirstOrDefault(o => o.Key.EndsWith("daPLCaPC_ControllerMode")).Value;
            plcVar_daPLCaPC_ProgramComment = _device.Items.FirstOrDefault(o => o.Key.EndsWith("daPLCaPC_ProgramComment")).Value;
            plcVar_daPLCaPC_Program = _device.Items.FirstOrDefault(o => o.Key.EndsWith("daPLCaPC_Program")).Value;
            plcVar_daPLCaPC_EmergencyStop = _device.Items.FirstOrDefault(o => o.Key.EndsWith("daPLCaPC_EmergencyStop")).Value;

            _logger.Debug($"      plcVar_daPLCaPC_PezziProdotti.Value:'{plcVar_daPLCaPC_PezziProdotti.Value}'");
            _logger.Debug($"      plcVar_daPLCaPC_ToolNumber.Value:'{plcVar_daPLCaPC_ToolNumber.Value}'");
            _logger.Debug($"      plcVar_daPLCaPC_Execution.Value:'{plcVar_daPLCaPC_Execution.Value}'");
            _logger.Debug($"      plcVar_daPLCaPC_ControllerMode.Value:'{plcVar_daPLCaPC_ControllerMode.Value}'");
            _logger.Debug($"      plcVar_daPLCaPC_ProgramComment.Value:'{plcVar_daPLCaPC_ProgramComment.Value}'");
            _logger.Debug($"      plcVar_daPLCaPC_Program.Value:'{plcVar_daPLCaPC_Program.Value}'");
            _logger.Debug($"      plcVar_daPLCaPC_EmergencyStop.Value:'{plcVar_daPLCaPC_EmergencyStop.Value}'");

        }

        public void Evaluate_daplcVar_daPLCaPC_Execution(Batch @batch)
        {
            try
            {


            _batchOdp = @batch.OdP;

            var currentValue = Convert.ToString(this.plcVar_daPLCaPC_Execution.Value);
            var controllerMode = Convert.ToString(this.plcVar_daPLCaPC_ControllerMode.Value);

            _logger.Debug($"Batch:'{_batchOdp}', machine:'{machineCode}', controller mode:'{controllerMode}', raised state:'{currentValue}'");

            var isValidState = (controllerMode == "AUTOMATIC" || controllerMode == "SEMI_AUTOMATIC" || controllerMode == "MANUAL");
            if (isValidState)
            {
                // caso di attivazione emergenza
                if (currentValue == "INTERRUPTED")
                {
                    _logger.Debug($"Batch:'{_batchOdp}', machine:'{machineCode}', controller mode:'{controllerMode}', raised state:'{currentValue}', managing micro-stop");

                    this.antirepeatAlarm_Dispose();
                    this.antirepeatAlarm_SetStartAt();
                }
                else
                if (currentValue == "ACTIVE")
                {

                    this.antirepeatAlarm_Dispose();

                    // chiamata esterna
                    _logger.Debug($"Batch:'{_batchOdp}', machine:'{machineCode}', controller mode:'{controllerMode}', raised state:'{currentValue}', call  _onSetWorking.Invoke()...");
                    _onSetWorking.Invoke();
                    _logger.Debug($"Batch:'{_batchOdp}', machine:'{machineCode}', controller mode:'{controllerMode}', raised state:'{currentValue}', call  _onSetWorking.Invoke()... completed");

                }
                else
                {
                    //if (currentValue == "READY" ||
                    //    currentValue == "STOPPED" ||
                    //    currentValue == "PROGRAM_STOPPED" ||
                    //    currentValue == "UNAVAILABLE" ||
                    //    currentValue == "OPTIONALLY_STOPPED" ||
                    //    currentValue == "PROGRAM_COMPLETED" ||
                    //    currentValue == "FEED_HOLD")
                    //-
                    // caso di PAUSA 
                    this.antirepeatAlarm_Dispose();

                    // chiamata esterna
                    _logger.Debug($"Batch:'{_batchOdp}', machine:'{machineCode}', controller mode:'{controllerMode}', raised state:'{currentValue}', call  _onSetPause.Invoke()...");
                    _onSetPause.Invoke();
                    _logger.Debug($"Batch:'{_batchOdp}', machine:'{machineCode}', controller mode:'{controllerMode}', raised state:'{currentValue}', call  _onSetPause.Invoke()... completed");

                }
            }
            else
            {
                this.antirepeatAlarm_Dispose();
                //chiamata esterna

                _logger.Debug($"Batch:'{_batchOdp}', machine:'{machineCode}', controller mode:'{controllerMode}', raised state:'{currentValue}', call  _onSetAllarm.Invoke()...");
                _onSetAllarm.Invoke();
                _logger.Debug($"Batch:'{_batchOdp}', machine:'{machineCode}', controller mode:'{controllerMode}', raised state:'{currentValue}', call  _onSetAllarm.Invoke()... completed");


            }


            }
            catch (Exception ex)
            {
                _logger.Error($"ERROR: '{ex.Message}");
            }
        }

        /// <summary>
        /// Reset del timer interno per la gestione delle microfermate
        /// </summary>
        public void antirepeatAlarm_Dispose()
        {
            _logger.DebugFormat($"Batch:'{_batchOdp}', machine:'{machineCode}', _startAllarmWatchDog.Dispose()...");
            lock (_startAll_lock)
                if (_startAllarmWatchDog != null)
                    _startAllarmWatchDog.Dispose();
            _logger.DebugFormat($"Batch:'{_batchOdp}', machine:'{machineCode}', _startAllarmWatchDog.Dispose()... completed");
        }


        /// <summary>
        /// Avvio del timer interno per la gestione delle microfermate
        /// </summary>
        /// <param name="doAction"></param>
        public void antirepeatAlarm_SetStartAt()
        {
            lock (_startAll_lock)
                _startAllarm = DateTime.Now;

            _startAllarmWatchDog = new Timer((a) =>
            {
                var diff = DateTime.Now.Subtract(_startAllarm).TotalMinutes;
                _logger.DebugFormat($"Batch:'{_batchOdp}', machine:'{machineCode}', _startAllarmWatchDog.Tick ({String.Format("{0:0.##}", diff)} min)...");
                if (DateTime.Now.Subtract(_startAllarm).TotalMinutes >= _reachAllarmAfterMinutes)
                {
                    var currentValue = Convert.ToString(plcVar_daPLCaPC_Execution.Value);
                    var controllerMode = Convert.ToString(plcVar_daPLCaPC_ControllerMode.Value);

                    _logger.Debug($"Batch:'{_batchOdp}', machine:'{machineCode}', controller mode:'{controllerMode}', raised state:'{currentValue}', _startAllarmWatchDog.Tick... after {_microAllarmMinutes} minutes reached");

                    _logger.Debug($"Batch:'{_batchOdp}', machine:'{machineCode}', controller mode:'{controllerMode}', raised state:'{currentValue}', call  _onSetAllarm.Invoke()...");
                    _onSetAllarm.Invoke();
                    _logger.Debug($"Batch:'{_batchOdp}', machine:'{machineCode}', controller mode:'{controllerMode}', raised state:'{currentValue}', call  _onSetAllarm.Invoke()... completed");

                    antirepeatAlarm_Dispose();
                }
            }, null, 500, 5 * 1000);
        }
    }
}
