﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using AreaMes.Server.MongoDb;
using AreaMes.Server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace AreaMes.BatchLogic.Rme
{
    public class Logic : AreaMes.Model.BatchLogic
    {
        private Timer _trendClock;
        private Timer _percClock;
        private Timer _perc_lockTurni;
        private object _perc_lock = new object();
        private bool _isOnClock = false;
        private Dictionary<string, MazakTemplateDataContainer> _LogicTemplateMazak = new Dictionary<string, MazakTemplateDataContainer>();


        public override async Task Init(IDataContainer data, IRepository repository, IMessageServices messages, IRealTimeManager areaMESClient, IAreaM2MClient areaM2MClient, IRuntimeTransportContextController runtimeContext, Batch batch)
        {
            await base.Init(data, repository, messages, areaMESClient, areaM2MClient, runtimeContext, batch);

            try
            {
                //---------------------------------------------------------------------------------
                // gestione dei cambi turno
                MessageServices.Instance.Subscribe(CambioTurnoMessage.Name, (object sender, CambioTurnoMessage o) =>
                {
                    lock (_perc_lockTurni)
                        BatchUtils.BatchChangeShift(batch, o, this._runtimeContext, Default);
                });

                //---------------------------------------------------------------------------------
                // creazione dei driver per le macchine di ogni fase
                var listOfMachine = batch.Fasi.SelectMany(x => x.Macchine.Where(y => y.Macchina != null ? y.Macchina.IsM2mEnable : false)).ToList();
                foreach (var item in listOfMachine)
                {
                    var machine = repository.Get<Macchina>(item?.Macchina?.Id);
                    if (machine != null)
                    {
                        var machineName = (machine.CodeM2mDevice != null) ? machine.CodeM2mDevice : machine.Codice;
                        Default.DebugFormat($"Creating MazakTemplateDataContainer '{machineName}'...");
                        var newInstance = new MazakTemplateDataContainer(runtimeContext, Default, data, machineName, 2,
                            () => { setAllarm(machineName); },
                            () => { setWorking(machineName); },
                            () => { setPause(machineName); });
                        var exist = _LogicTemplateMazak.FirstOrDefault(t => t.Key == machineName).Key;
                        if (exist == null)
                            _LogicTemplateMazak.Add(machineName, newInstance);
                        // viene chiamata la logica di esecuzione per definizione esclusione micro-fermata
                        var currenteFaseState = getBatchFaseFromMachine(machineName);
                        Default.DebugFormat($"    '{machineName}', Fase:'{currenteFaseState.DistintaBaseFase.Codice}', on '{currenteFaseState.Stato}'");
                        if ((currenteFaseState != null) && (currenteFaseState.Stato != StatoFasi.InAttesa))
                        {
                            Default.DebugFormat($"    '{machineName}',      newInstance.Evaluate_daplcVar_daPLCaPC_Execution('{_batch.OdP}')...");
                            newInstance.Evaluate_daplcVar_daPLCaPC_Execution(_batch);
                            Default.DebugFormat($"    '{machineName}',      newInstance.Evaluate_daplcVar_daPLCaPC_Execution('{_batch.OdP}')... completed");
                        }

                        Default.DebugFormat($"Creating MazakTemplateDataContainer '{machineName}'... completed");
                    }
                }

                //---------------------------------------------------------------------------------
                // avvio del timer per il calcolo dei tempi
                _percClock = new Timer((a) =>
                {
                    if (isBatchOnLavorazione())
                        calcPerc();
                }, null, 30 * 1000, 60 * 1000);

                //---------------------------------------------------------------------------------
            }
            catch (Exception exc)
            {
                Default.Error(exc);
            }

        }

        private BatchFase getBatchFaseFromMachine(string machineCode)
        {
            BatchFase faseBatch = null;
            // viene prelevata la fase riferita alla macchina
            foreach (var items in this._batch.Fasi.Where(o => o.Macchine != null))
            {
                var macchina = items.Macchine.FirstOrDefault();
                if ((macchina.Macchina != null) && (macchina.Macchina.IsM2mEnable && macchina.Macchina.CodeM2mDevice == machineCode))
                    faseBatch = items;
            }

            return faseBatch;
        }

        protected override void OnDeviceValueChangedMessage(object id, DeviceValueChangedMessage message)
        {
            try
            {

                foreach (var item in message)
                {
                    var code = item.Registry.Device;
                    Default.DebugFormat($"Batch: '{_batch.OdP}', OnDeviceValueChangedMessage, MachineCode:'{code}'...");

                    MazakTemplateDataContainer instanceOf;
                    Default.DebugFormat($"Batch: '{_batch.OdP}', getBatchFaseFromMachine('{code}')'...");
                    BatchFase faseBatch = getBatchFaseFromMachine(code);
                    Default.DebugFormat($"Batch: '{_batch.OdP}', getBatchFaseFromMachine('{code}')'... completed");

                    // viene prelevata la referenza per il driver della macchina
                    if (faseBatch == null) continue;

                    Default.DebugFormat($"Batch: '{_batch.OdP}', _LogicTemplateMazak.TryGetValue('{code}', out instanceOf)...");
                    _LogicTemplateMazak.TryGetValue(code, out instanceOf);
                    Default.DebugFormat($"Batch: '{_batch.OdP}', _LogicTemplateMazak.TryGetValue('{code}', out instanceOf)... completed");

                    if (instanceOf != null)
                    {
                        Default.DebugFormat($"Batch: '{_batch.OdP}', instanceOf founded for code='{code}'...");
                        //DataItem daplcVar_daPLCaPC_PezziProdotti = new DataItem();
                        //DataItem daplcVar_daPLCaPC_ToolNumber = new DataItem();
                        DataItem daplcVar_daPLCaPC_Execution = null;
                        DataItem daplcVar_daPLCaPC_ControllerMode = new DataItem();
                        //DataItem daplcVar_daPLCaPC_ProgramComment = new DataItem();
                        //DataItem daplcVar_daPLCaPC_Program = new DataItem();
                        //DataItem daplcVar_daPLCaPC_EmergencyStop = new DataItem();

                        //if (item == instanceOf.plcVar_daPLCaPC_PezziProdotti) daplcVar_daPLCaPC_PezziProdotti = instanceOf.plcVar_daPLCaPC_PezziProdotti;
                        //if (item == instanceOf.plcVar_daPLCaPC_ToolNumber) daplcVar_daPLCaPC_ToolNumber = instanceOf.plcVar_daPLCaPC_ToolNumber;
                        if (item == instanceOf.plcVar_daPLCaPC_Execution) daplcVar_daPLCaPC_Execution = instanceOf.plcVar_daPLCaPC_Execution;
                        daplcVar_daPLCaPC_ControllerMode = instanceOf.plcVar_daPLCaPC_ControllerMode;
                        //if (item == instanceOf.plcVar_daPLCaPC_ProgramComment) daplcVar_daPLCaPC_ProgramComment = instanceOf.plcVar_daPLCaPC_ProgramComment;
                        //if (item == instanceOf.plcVar_daPLCaPC_Program) daplcVar_daPLCaPC_Program = instanceOf.plcVar_daPLCaPC_Program;
                        //if (item == instanceOf.plcVar_daPLCaPC_EmergencyStop) daplcVar_daPLCaPC_EmergencyStop = instanceOf.plcVar_daPLCaPC_EmergencyStop;

                        Default.DebugFormat($"Batch: '{_batch.OdP}', MachineCode:'{code}', Fase:'{faseBatch.DistintaBaseFase.Codice}', on '{faseBatch.Stato}'...");

                        if (isBatchOnLavorazione() &&
                            (faseBatch.Stato == StatoFasi.InLavorazione ||
                            faseBatch.Stato == StatoFasi.InLavorazioneInEmergenza ||
                            faseBatch.Stato == StatoFasi.InLavorazioneInPausa) ||
                            faseBatch.Stato == StatoFasi.InPausa)
                        {


                            Default.DebugFormat($"Batch: '{_batch.OdP}', MachineCode:'{code}', Fase:'{faseBatch.DistintaBaseFase.Codice}', on '{faseBatch.Stato}'... entered");

                            //if (daplcVar_daPLCaPC_PezziProdotti != null)
                            //{
                            //    Default.InfoFormat("Batch: {0}, Start calcoli PRODUZIONE", _batch.OdP);
                            //    _batch.Fasi.FirstOrDefault(x => x.Id == faseBatch.Id).PezziProdotti = Convert.ToInt32(daplcVar_daPLCaPC_PezziProdotti.Value);
                            //    Default.InfoFormat("Batch: {0}, Pezzi prodotti aggiornati", _batch.OdP);
                            //}


                            // /ricoalcolo Oee
                            // -----------------------------------------------------------------------------------------------
                            Default.InfoFormat("Batch: '{0}', Start calcoli OEE...", _batch.OdP);
                            // viene effettuato il calcolo della percentuale di produzione e OEE
                            int current = _batch.PercCompletamento;
                            double d0 = _batch.PezziDaProdurre > 0 ? ((double)_batch.PezziProdotti / (double)_batch.PezziDaProdurre) : 0;
                            _batch.PercCompletamento = (d0 <= 1) ? Convert.ToInt32(d0 * 100) : 100;
                            // _batch.OEE = BatchUtils.OEE_Rf_Calculate(_batch);
                            Default.InfoFormat("Batch: '{0}', Start calcoli OEE... completed", _batch.OdP);


                            // --------------------------------------------------------------------------------------------------------
                            // GESTIONE DEGLI STATI MACCHINA e ALLARMI
                            //---------------------------------------------------------------------------------------------------------
                            if ((daplcVar_daPLCaPC_Execution != null))
                            {
                                Default.DebugFormat($"Batch: '{_batch.OdP}', ControllerMode:'{instanceOf.plcVar_daPLCaPC_ControllerMode}',  Execution:'{daplcVar_daPLCaPC_Execution.Value}', faseBatch.Stato:'{faseBatch.Stato}', instanceOf.machineCode:'{instanceOf?.machineCode}'");

                                // viene chiamata la logica di esecuzione per definizione esclusione micro-fermata
                                instanceOf.Evaluate_daplcVar_daPLCaPC_Execution(_batch);
                            }

                            Default.DebugFormat($"Batch: '{_batch.OdP}', MachineCode:'{code}', Fase:'{faseBatch.DistintaBaseFase.Codice}', on '{faseBatch.Stato}'... completed");

                        }
                    }
                }
            }
            catch (Exception exc) { Default.Error(exc); }
        }


        private void setAllarm(string machineName)
        {
            Default.InfoFormat("Batch: '{0}', Start setAllarm...", _batch.OdP);
            BatchFase faseBatch = getBatchFaseFromMachine(machineName);
            if (faseBatch == null) return;

            var idMacchina = faseBatch.Macchine.FirstOrDefault().Macchina.Id;
            var turni = MongoRepository.Instance.All<TurnoDiLavoro>().GetAwaiter().GetResult();

            // Davide A, 20/04/2023, modifica per gestire la forzatura del cambio di stato sulla macchina
            // quando la lavorazione è fuori turno. Se fuori turno e la macchina si pone in allarme, allora viene forzato
            // lo stato in pausa
            var turnoActive = BatchUtils.GetCurrentShiftActive(turni, idMacchina);

            if (turnoActive == null)
            {    
                Default.InfoFormat("Batch: '{0}', Start setAllarm... no shift founded, force PAUSE", _batch.OdP);
                setPause(machineName);
                return;
            }
            else
            {
                Default.InfoFormat("Batch: '{0}', Start setAllarm... current shift found:'{1}'", _batch.OdP, turnoActive.Codice);
            }

            Default.InfoFormat("Batch: '{0}', Start setAllarm... faseBatch.Stato:'{1}'", _batch.OdP, faseBatch.Stato);
            if (faseBatch.Stato != StatoFasi.InLavorazioneInEmergenza)
            {
                Default.InfoFormat("Batch: '{0}', Start setAllarm call SetFase... faseBatch.Stato:'{1}'", _batch.OdP, faseBatch.Stato);
                _runtimeContext.SetFase2(new SetFaseInfo { idBatch = this._batchId, fase = faseBatch, idStato = StatoFasi.InLavorazioneInEmergenza });
            }

            Default.InfoFormat("Batch: '{0}', Start setAllarm... this._batch.Stato:'{1}'", _batch.OdP, this._batch.Stato);
            if (this._batch.Stato != StatoBatch.InLavorazioneInEmergenza)
                _runtimeContext.SetBatch2(new SetBatchInfo { Macchina = idMacchina, idBatch = this._batchId, idStato = StatoBatch.InLavorazioneInEmergenza, NomeFase = faseBatch.DistintaBaseFase.Nome, NumeroFase = faseBatch.NumeroFase });

            Default.InfoFormat("Batch: '{0}', Start setAllarm... completed", _batch.OdP);
        }

        private void setWorking(string machineName)
        {

            BatchFase faseBatch = getBatchFaseFromMachine(machineName);
            if (faseBatch == null) return;

            var idMacchina = faseBatch.Macchine.FirstOrDefault().Macchina.Id;

            if (this._batch.Stato != StatoBatch.InLavorazione)
                _runtimeContext.SetBatch2(new SetBatchInfo { Macchina = idMacchina, idBatch = this._batchId, idStato = Model.Enums.StatoBatch.InLavorazione, NomeFase = faseBatch.DistintaBaseFase.Nome, NumeroFase = faseBatch.NumeroFase });

            if (faseBatch.Stato != StatoFasi.InLavorazione)
                _runtimeContext.SetFase2(new SetFaseInfo { idBatch = this._batchId, fase = faseBatch, idStato = StatoFasi.InLavorazione });
        }

        private void setPause(string machineName)
        {
            BatchFase faseBatch = getBatchFaseFromMachine(machineName);
            if (faseBatch == null) return;
            
            var idMacchina = faseBatch.Macchine.FirstOrDefault().Macchina.Id;

            if (this._batch.Stato != StatoBatch.InLavorazioneInPausa)
                _runtimeContext.SetBatch2(new SetBatchInfo { Macchina = idMacchina, idBatch = this._batchId, idStato = Model.Enums.StatoBatch.InLavorazioneInPausa, NumeroFase = faseBatch.NumeroFase, NomeFase = faseBatch.DistintaBaseFase.Nome });

            if (faseBatch.Stato != StatoFasi.InLavorazioneInPausa)
                _runtimeContext.SetFase2(new SetFaseInfo { idBatch = this._batchId, fase = faseBatch, idStato = StatoFasi.InLavorazioneInPausa });

        }



        protected override void OnBatchChanged(object id, BatchChangedMessage message)
        {

        }


        protected override async void OnFaseChanged(object id, FaseChangedInfo message)
        {
            //if ((message.Current.NumeroFase == "") && (message.Current.Stato == StatoFasi.InLavorazione))
            //    _runtimeContext.SetBatch2(new SetBatchInfo { idBatch = this._batchId, idStato = Model.Enums.StatoBatch.InLavorazioneInEmergenza });

            try
            {
                if (message.Current.Stato == StatoFasi.InLavorazione && message.Previous == null)
                {
                    Default.DebugFormat($"OnFaseChanged Search Phase code: '{message.Current.CodiceFase}'");
                    var singlePhase = this._batch.Fasi.First(x => x.DistintaBaseFase.Codice == message.Current.CodiceFase);
                    Default.DebugFormat($"OnFaseChanged Search Phase code: '{message.Current.CodiceFase}'... Completed");

                    if (singlePhase.Macchine[0].Macchina?.IsM2mEnable == true)
                    {
                        Default.DebugFormat($"OnFaseChanged Machine IsM2mEnable");
                        var code = singlePhase.Macchine.First().Macchina.CodeM2mDevice;
                        Default.DebugFormat($"OnFaseChanged Machine IsM2mEnable and code machine is: '{code}'");
                        MazakTemplateDataContainer instanceOf;
                        Default.DebugFormat($"OnFaseChanged TryGetValue");
                        _LogicTemplateMazak.TryGetValue(code, out instanceOf);
                        Default.DebugFormat($"OnFaseChanged TryGetValue... Completed");
                        Default.DebugFormat($"OnFaseChanged Evaluate_daplcVar_daPLCaPC_Execution");

                        // Default.DebugFormat($"Batch: '{_batch.OdP}', ControllerMode:'{instanceOf.plcVar_daPLCaPC_ControllerMode}',  Execution:'{instanceOf.plcVar_daPLCaPC_Execution}',  instanceOf.machineCode:'{instanceOf?.machineCode}'");
                        instanceOf.Evaluate_daplcVar_daPLCaPC_Execution(this._batch);

                        Default.DebugFormat($"OnFaseChanged Evaluate_daplcVar_daPLCaPC_Execution... Completed");
                    }
                }
            }
            catch (Exception exc)
            {
                Default.Error(exc.Message);
            }
        }

        private void calcPerc()
        {
            try
            {

                if (_batch.Trend_Oee == null) _batch.Trend_Oee = new List<BatchTrendParametri>();
                if (_batch.Trend_Oee_Rf == null) _batch.Trend_Oee_Rf = new List<BatchTrendParametri>();
                if (_batch.Trend_Oee_Rq == null) _batch.Trend_Oee_Rq = new List<BatchTrendParametri>();
                if (_batch.Trend_Oee_Rv == null) _batch.Trend_Oee_Rv = new List<BatchTrendParametri>();
                if (_batch.Trend_Stato == null) _batch.Trend_Stato = new List<BatchTrendParametri>();

                lock (_perc_lock)
                {
                    if (_isOnClock)
                        return;

                    _isOnClock = true;
                }

                if (_batch == null) return;

                int current = _batch.PercCompletamento;
                var dateTimeNow = DateTime.Now;


                double d0 = _batch.PezziDaProdurre > 0 ? ((double)_batch.PezziProdotti / (double)_batch.PezziDaProdurre) : 0;
                _batch.PercCompletamento = (d0 <= 1) ? Convert.ToInt32(d0 * 100) : 100;

                if (current != _batch.PercCompletamento)
                    Default.InfoFormat("Batch {0}, avanzamento percentuale a {1}% ", this._batch.OdP, this._batch.PercCompletamento);


                var fasiInPausa = _batch.Fasi.Where(o1 => o1.Stato == StatoFasi.InLavorazioneInPausa || o1.Stato == StatoFasi.InLavorazioneInPausaS);
                var fasiInAllarme = _batch.Fasi.Where(o1 => o1.Stato == StatoFasi.InLavorazioneInEmergenza);
                var fasiInRun = _batch.Fasi.Where(o1 => o1.Stato == StatoFasi.InLavorazione);


                if (fasiInAllarme.Any())
                    _batch.Stato = StatoBatch.InLavorazioneInEmergenza;
                else if (fasiInPausa.Any())
                    _batch.Stato = StatoBatch.InLavorazioneInPausa;
                else if (fasiInRun.Any())
                    _batch.Stato = StatoBatch.InLavorazione;
                else
                    _batch.Stato = _batch.Inizio.HasValue ? StatoBatch.InLavorazioneInIdle : StatoBatch.InAttesa;

                // ----------------------------------------------------------------------------------
                // calcolo OEE, Disponibilità
                #region calcolo OEE, componente Rf, Disponibilità
                var oee_prec_temp = _batch.OEE_RfPrecedente;
                var oee_prec_at = _batch.OEE_RfPrecedente_At;
                // viene calcolato nuovo valore OEE
                _batch.OEE_Rf = BatchUtils.OEE_Rf_CalculateByAvanzamenti(this._batch);
                _batch.OEE_Rf_At = dateTimeNow;
                // se OEE attuale != da OEE precedente, allora viene registrata la differenza
                if (oee_prec_temp != _batch.OEE_Rf)
                {
                    _batch.Trend_Oee_Rf.Add(new BatchTrendParametri
                    {
                        DataOra = _batch.OEE_Rf_At.Value,
                        Valore = _batch.OEE_Rf,
                        ValorePrecedente = oee_prec_temp,
                        DataOraPrecedente = oee_prec_at != null ? oee_prec_at.Value : _batch.OEE_Rf_At.Value
                    });

                    _batch.OEE_RfPrecedente = _batch.OEE_Rf;
                    _batch.OEE_RfPrecedente_At = _batch.OEE_Rf_At;
                }
                // ----------------------------------------------------------------------------------
                #endregion


                // ----------------------------------------------------------------------------------
                // calcolo OEE
                oee_prec_temp = _batch.OEEPrecedente;
                oee_prec_at = _batch.OEEPrecedente_At;
                // viene calcolato nuovo valore OEE
                _batch.OEE = _batch.OEE_Rf;
                _batch.OEE_At = dateTimeNow;
                // se OEE attuale != da OEE precedente, allora viene registrata la differenza
                if (oee_prec_temp != _batch.OEE)
                {
                    if (_batch.OEEPrecedente < _batch.OEE)
                        _batch.OEEAndamento = AndamentoValore.InSalita;
                    else if (_batch.OEEPrecedente > _batch.OEE)
                        _batch.OEEAndamento = AndamentoValore.InDiscesa;
                    else
                        _batch.OEEAndamento = AndamentoValore.Stabile;

                    _batch.Trend_Oee.Add(new BatchTrendParametri
                    {
                        DataOra = _batch.OEE_At.Value,
                        Valore = _batch.OEE,
                        ValorePrecedente = oee_prec_temp,
                        DataOraPrecedente = oee_prec_at != null ? oee_prec_at.Value : _batch.OEE_At.Value
                    });

                    _batch.OEEPrecedente = _batch.OEE;
                    _batch.OEEPrecedente_At = _batch.OEE_At;
                }
                // ----------------------------------------------------------------------------------



                // ----------------------------------------------------------------------------------
                // calcolo OEE, Disponibilità
                #region calcolo STATI
                if (_batch.StatoPrecedente != _batch.Stato)
                {
                    _batch.Trend_Stato.Add(new BatchTrendParametri
                    {
                        DataOra = dateTimeNow,
                        Valore = _batch.Stato,
                        ValorePrecedente = _batch.StatoPrecedente,
                        DataOraPrecedente = _batch.StatoPrecedente_At != null ? _batch.StatoPrecedente_At.Value : dateTimeNow
                    });

                    _batch.StatoPrecedente = _batch.Stato;
                    _batch.StatoPrecedente_At = _batch.OEE_Rf_At;
                }
                // ----------------------------------------------------------------------------------
                #endregion



            }
            catch (Exception exc)
            {
                Default.Error("_percClock:" + exc.Message);
            }
            finally
            {
                lock (_perc_lock)
                    _isOnClock = false;

            }
        }

        private bool isBatchOnLavorazione()
        {
            return (this._batch.Stato == Model.Enums.StatoBatch.InLavorazione ||
                this._batch.Stato == Model.Enums.StatoBatch.InLavorazioneInEmergenza ||
                this._batch.Stato == Model.Enums.StatoBatch.InLavorazioneInPausa ||
                this._batch.Stato == StatoBatch.InLavorazioneInPausaS);
        }
    }
}
