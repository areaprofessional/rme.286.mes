﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Model.Runtime;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.Rme
{
    public interface ILogic
    {
        Macchina Machine { get; set; }
        string DeviceId { get; set; }
        void OnDeviceValueChangedMessage(ILog logger, IRuntimeTransportContextController runtimeContext, Batch batch, BatchFase fMEP_00, BatchFase fMEP_LAV, DeviceItems deviceItems, object id, DeviceValueChangedMessage message);

        void OnFaseChanged(ILog logger, IRuntimeTransportContextController runtimeContext, Batch batch, BatchFase fMEP_00, BatchFase fMEP_LAV, DeviceItems deviceItems, object id, FaseChangedInfo message);

    }
}
