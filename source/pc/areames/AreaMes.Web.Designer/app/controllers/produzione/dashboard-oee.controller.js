﻿// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------

app.controller('dashboardOeeCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$rootScope', '$translate',
    function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $rootScope, $translate, $timeout) {

        var urlApp = window.location.href.split(':');
        var serverIP = "";
        if (urlApp.length >= 1)
            serverIP = urlApp[1].replace("//", "");

        $scope.signalR = "9001";
        $scope.webApi = "9000";
        $scope.stationId = "99";
        $scope.loginConRfid = $.cookie("loginConRfid");
        $scope.inactivityTimeout = 2147483647;
        $.cookie("SigalRc", "http://" + serverIP + ":" + $scope.signalR, { expires: 70000 });
        $.cookie("WebApiC", "http://" + serverIP + ":" + $scope.webApi + "/api", { expires: 70000 });
        $.cookie("StationId", $scope.stationId, { expires: 70000 });
        $.cookie("LoginConRfid", $scope.loginConRfid, { expires: 70000 });
        $.cookie("InactivityTimeout", $scope.inactivityTimeout, { expires: 70000 });
        $scope.totaleTonTrasformate = 0;

        $scope.filtroVisualizzaBaseGiornaliera = true;

        $scope.idStart = getParameterByName("id");

        $scope.colorByState = {
            Emergenza: "#f5564a",
            Pausa: "#ffc720",
            PausaFuoriTurno: "#e6e6e6",
            Lavorazione: "#97c95c",
            Atterezzaggio: "#1db2f5",
            Commesse: "#cfcfcf"
        };

        $scope.colorOeeLabel = {
            bluScuro: "#033859",
            azzurro: "#22B7F2",
            grigio: "#C4C4C4"
        }

        $scope.bluScuro = "#033859";
        $scope.azzurro = "#22B7F2";
        $scope.grigio = "#C4C4C4";
        $scope.verde = "#50C41D";

        $scope.collapsible = true;
        $scope.multiple = true;
        $scope.filtroDal = new Date();
        $scope.filtroAl = new Date();
        $scope.filtroDalMin = new Date();
        $scope.filtroAlMax = new Date();

        $translate.use($scope.$parent.globals.currentUser.lingua);
        //--------------------------------------------------------------
        $rootScope.signalRconnection = $.cookie("SigalRc");
        connection = $.hubConnection($rootScope.signalRconnection);
        connection.logging = false;
        hub = connection.createHubProxy('AreaMESManager');
        hub.logging = false;

        $scope.filtroDal = new Date(2022, 1, 6);
        $scope.filtroAl = new Date(2022, 1, 6);

        $scope.gridBoxTurniValue = [];
        $scope.gridBoxTurniId = [];

        $scope.gridBoxMacchineValue = [];
        $scope.gridBoxMacchineId = [];
        $scope.gridBoxCommesseId = [];
        $scope.gridBoxCommesseValue = [];

        $scope.filtroDataDalMax = formatDataHtml(new Date());
        $scope.filtroDataAlMax = formatDataHtml(new Date());


        $scope.gridBoxMaterialiValue = [];
        $scope.gridBoxMaterialiId = [];
        $scope.pezziElaborati = 0;
        $scope.pezziTeorici = 0;
        $scope.pezziTarget = 0;
        $scope.oee = 0;
        $scope.oee_rf = 0;
        $scope.oee_rv = 0;
        $scope.oee_rq = 0;
        $scope.listGrigliaDati = [];
        $scope.dataSourceChartLine = [];
        $scope.listAccordion = [];
        $scope.chartFasiTempi = [];
        $scope.listFermi = [];
        $scope.listScarti = [];
        $scope.listFermiGrafici = [];
        $scope.listScartiGrafici = [];
        $scope.listEfficency = [];
        $scope.arrMacchineSelezionate = [];

        $scope.lavMacchinaTimeSummary = [];
        $scope.lavTurnoTimeSummary = [];
        $scope.lavBatchTimeSummary = [];
        $scope.fermiMacchinaTimeSummary = [];
        $scope.fermiTurnoTimeSummary = [];
        $scope.fermiBatchTimeSummary = [];
        $scope.pausaMacchinaTimeSummary = [];
        $scope.pausaTurnoTimeSummary = [];
        $scope.pausaBatchTimeSummary = [];

        $scope.deviceFasePercItems = [];

        $scope.showCheckboxToolTip = true;

        $scope.listBtc = []
        $scope.dxistogrammaPercentualiFasi = {};

        $scope.homeTabId = 0;
        $scope.availabilityTabId = 1;
        $scope.qualityTabId = 2;
        $scope.energyTabId = 3;
        $scope.efficencyTabId = 4;
        $scope.summaryTabId = 5;
        $scope.energyTabId = 6;
        $scope.selectedTabId = $scope.availabilityTabId;

        $scope.fermiCausaliPercentSummary = [];
        $scope.checkBoxBatchDefaultValue = false,
            $scope.percMinPieValue = parseInt(localStorage.getItem("dashboard-oee.html-percMinPie"));
        $scope.maxValueBulletQual = 100;
        $scope.maxValueBulletAva = 100;

        $scope.extraInfoList = [
            {
                key: "Le principali cause di fermo sono:",
                items: []
            },
            {
                key: "Le principali cause di scarto sono:",
                items: []
            },
            {
                key: "I fermi di maggior durata sono:",
                items: []
            },
            {
                key: "I fermi più frequenti sono:",
                items: []
            },
        ]

        setTimeout(function () {
            $scope.refreshRangeDate();

            $("#pieFermi").dxPieChart('instance').option("series.smallValuesGrouping.threshold", $scope.percMinPieValue);
            $("#pieScarti").dxPieChart('instance').option("series.smallValuesGrouping.threshold", $scope.percMinPieValue);
        }, 250);

        $scope.navigateTo = function (tabId) {
            if ($scope.selectedTabId !== tabId) {
                $scope.selectedTabId = tabId;
            }
        }

        //$scope.changeFiltroAl = function () {
        //    $scope.refreshRangeDate();
        //}

        //$scope.changeFiltroDal = function () {
        //    $scope.refreshRangeDate();
        //}

        $scope.yyyymmdd = function (date) {
            if (date != undefined) {
                var mm = date.getMonth() + 1; // getMonth() is zero-based
                var dd = date.getDate();

                return [date.getFullYear(),
                (mm > 9 ? '' : '0') + mm,
                (dd > 9 ? '' : '0') + dd
                ].join('-');
            }
        };

        $scope.refreshRangeDate = function () {
            //$scope.$apply(function () {
            $scope.filtroDalMin.setDate($scope.filtroAl.getDate() - 180);
            $scope.filtroDalMinString = $scope.yyyymmdd($scope.filtroDalMin);
            //});//fine apply
        }

        $scope.selectedSummaryTabId = 0;

        $scope.summaryOrdineTabId = 0;
        $scope.summaryTurnoTabId = 1;
        $scope.summaryMacchinaTabId = 2;
        $scope.navigateTroughSummary = function (tabId) {
            if ($scope.selectedSummaryTabId !== tabId) {
                $scope.selectedSummaryTabId = tabId;
            }
        }

        // Viene intercettato l'evento di cambio stato in SignalR
        $(connection).bind("onStateChanged", function (e, data) {
            if (data.newState === $.signalR.connectionState.connected) {
                doSubscribe();
            }
            else if (data.newState === $.signalR.connectionState.disconnected) {
                setTimeout(function () { connection.start(); }, 5 * 1000); // Restart connection after 5 seconds.
            }
        });

        connection.start();

        const geometry = {
            startAngle: 90,
            endAngle: 90,
        };

        const options = {
            startScaleValue: 0,
            endScaleValue: 35,
            tooltip: {
                enable: false
            },
        };

        $scope.customizeTooltip = function (pointsInfo) {
            return { text: parseInt(pointsInfo.value) + "%" };
        };

        $scope.customizeQualityTooltip = function (pointsInfo) {
            return { text: parseInt(pointsInfo.value) };
        };

        function doSubscribe() {
            hub.invoke('SubscribeToDashboardOee').done(function (res) {
                // caricamento di ulteriori informazioni
                $scope.$apply(function () {
                    if (res != undefined) {
                        $scope.listaTurni = [];
                        $scope.listaMacchine = [];
                        $scope.listaCommesse = [];
                        $scope.filtroDal = new Date(res.FiltroDal);
                        $scope.filtroAl = new Date(res.FiltroAl);
                        $scope.listaMacchine = res.ListaMacchine;
                        $scope.listaCommesse = res.ListaCommesse;
                        $scope.listaTurni = res.ListaTurni;
                        $scope.listaTurni.push(
                            {
                                Id: -1,
                                Codice: 000,
                                Nome: "Fuori turno"
                            });
                        $scope.listaMateriali = res.ListaMateriali;
                    }
                    //document.getElementById('divPrincipale').setAttribute("style", "opacity: 1");
                    //document.getElementsByClassName("loader")[0].style.display = "none";
                });//fine apply

                if ($scope.idStart != null) {
                    $scope.filtredDatiGrafici(true);
                    $scope.idStart = null;
                }
            }).fail(function (error) {
                // errore nella sottoscrizione
            });
        }

        //$scope.refreshRangeSelector = function () {
        //    $("#rangeSelectorOptions").dxRangeSelector("option", "scale.startValue", $scope.filtroDal);
        //    $("#rangeSelectorOptions").dxRangeSelector("option", "scale.endValue", $scope.filtroAl);
        //}
        $scope.filtroVisualizzaFuoriTurno = false;
        $scope.checkBoxFuoriTurno = {
            bindingOptions: {
                value: 'filtroVisualizzaFuoriTurno',
            },
        }

        $scope.modalTabDettagli = function (tab) {

            $scope.tabActive = tab;
            $scope.dettaglio = !$scope.dettaglio;
            //$scope.renderDatiGrafici();
            if (tab != undefined) {
                if (tab == "qua") {
                    $('#btnTabQua').click();
                }
                else if (tab == "eff") {
                    $('#btnTabEff').click();
                }
                else if (tab == "ava") {
                    $('#btnTabAva').click();
                }
            }
        }

        //function onlyUnique(value, index, self) {
        //    return self.indexOf(value) === index;
        //}

        //Queste due variabili servono per evitare la chiamata post se non sono ancora stati filtrati gli oggetti
        //e evitare che ogni volta che si entra nella tab dati piani ricarichi tutto.
        $scope.scartiDatiPianiUploaded = true;
        $scope.fermiDatiPianiUploaded = true;

        $scope.filtredDatiGrafici = function (filterBatch) {

            $scope.showCheckboxToolTip = false;
            $scope.fermiDatiPianiUploaded = false;
            $scope.scartiDatiPianiUploaded = false;
            document.getElementsByClassName("loader")[0].style.display = "block";
            document.getElementById('divPrincipale').setAttribute("style", "opacity: 0.5");


            $scope.requestData = {};
            if (!filterBatch) {
                $scope.requestData.FiltroDal = $scope.filtroDal;
            }
            $scope.requestData.FiltroAl = $scope.filtroAl;
            
            $scope.requestData.ListIdMateriali = $scope.gridBoxMaterialiId;
            var found = $scope.gridBoxTurniId.find((idTurno) => idTurno == -1);
            if (found != null && found != "") {
                $scope.filtroVisualizzaFuoriTurno = true;
                $scope.gridBoxTurniId = $scope.gridBoxTurniId.filter((idTurno) => idTurno != -1);
            } else {
                $scope.filtroVisualizzaFuoriTurno = false; 
            }
            $scope.requestData.MostraFuoriTurno = $scope.filtroVisualizzaFuoriTurno;
            $scope.requestData.ListIdTurni = $scope.gridBoxTurniId;
            $scope.requestData.ListCodeTurni = $scope.gridBoxTurniValue;
            $scope.requestData.ListIdMacchine = $scope.gridBoxMacchineId;
            if (filterBatch) {
                $scope.requestData.ListIdCommesse = [$scope.idStart];
            } else {
                $scope.requestData.ListIdCommesse = $scope.gridBoxCommesseId;
            }
            $scope.pezziElaborati = 0;
            $scope.pezziTeorici = 0;
            $scope.pezziTarget = 0;
            $scope.pezziScartoTot = 0;
            $scope.oee = 0;
            $scope.oee_rf = 0;
            $scope.oee_rv = 0;
            $scope.oee_rq = 0;
            $scope.listGrigliaDati = [];
            $scope.dataSourceChartLine = [];
            $scope.seriesChartLine = [];
            $scope.chartFasiTempi = [];

            $scope.listFermi = [];
            $scope.listScarti = [];
            $scope.listFermiGrafici = [];
            $scope.listScartiGrafici = [];
            $scope.listEfficency = [];

            $scope.scartiCausaliPercentSummary = [];
            $scope.fermiCausaliPercentSummary = [];
            $scope.fermiCausaliTimeSummary = [];
            $scope.fermiCausaliNumberSummary = [];

            hub.invoke('GetDataDashboardOee', $scope.requestData).done(function (res) {
                $scope.$apply(function () {
                    $scope.lavMacchinaTimeSummary = res.GroupedLavTime["Macchina"];
                    $scope.lavTurnoTimeSummary = res.GroupedLavTime["TurnoDiLavoro"];
                    $scope.lavBatchTimeSummary = res.GroupedLavTime["Batch"];
                    $scope.fermiMacchinaTimeSummary = res.GroupedFermiTime["Macchina"];
                    $scope.fermiTurnoTimeSummary = res.GroupedFermiTime["TurnoDiLavoro"];
                    $scope.fermiBatchTimeSummary = res.GroupedFermiTime["Batch"];
                    $scope.pausaMacchinaTimeSummary = res.GroupedPauseTime["Macchina"];
                    $scope.pausaTurnoTimeSummary = res.GroupedPauseTime["TurnoDiLavoro"];
                    $scope.pausaBatchTimeSummary = res.GroupedPauseTime["Batch"];

                    $scope.macchinaTimeSummary = res.GroupedTime["Macchina"];
                    $scope.batchTimeSummary = res.GroupedTime["Batch"];
                    $scope.turnoTimeSummary = res.GroupedTime["TurnoDiLavoro"];

                    $scope.scartiCausaliPercentSummary = res.ScartiCausaliPercentSummary;
                    $scope.fermiCausaliPercentSummary = res.FermiCausaliPercentSummary;
                    $scope.fermiCausaliTimeSummary = res.FermiCausaliTimeSummary;
                    $scope.fermiCausaliNumberSummary = res.FermiCausaliNumberSummary;
                    $scope.listBtc = res.ListBatch;
                    $scope.pezziElaborati = res.PezziElaborati;
                    $scope.pezziTeorici = res.PezziTeorici;
                    $scope.pezziTarget = res.PezziTarget;
                    $scope.oee = res.Oee;
                    $scope.oee_rf = res.Oee_Rf;
                    $scope.oee_rv = res.Oee_Rv;
                    $scope.oee_rq = res.Oee_Rq;
                    $scope.listFermiGrafici = res.ListFermiGrafici;
                    $scope.listGrigliaDati = res.Tempi;
                    $scope.dataSourceChartLine = res.ListOee;
                    $scope.listFermi = res.ListFermi;
                    $scope.listScarti = res.ListScarti;
                    $scope.listScartiGrafici = res.ListScartiGrafici;
                    $scope.listAccordion = res.ListAccordion;
                    $scope.seriesChartLine = [];
                    $scope.listEfficency = res.ListEfficienza;
                    $scope.pezziScartoTot = res.ScartiTotali;
                    if (res.EnergyPieChartData !== null && res.EnergyPieChartData.length !== 0)
                        $scope.listEnergyPieData = res.EnergyPieChartData;
                    $scope.listEnergyGridData = res.EnergyCompareGridData;

                    $scope.istogrammaPercentualiFasi = {};
                    $scope.dxistogrammaPercentualiFasi = {};
                    angular.forEach(res.DeviceFasePercItems, function (devFaseValue, macchinaKey) {
                        $scope.istogrammaPercentualiFasi[macchinaKey] = [];
                        angular.forEach(devFaseValue, function (value, key) {
                            $scope.istogrammaPercentualiFasi[macchinaKey].push({
                                state: key,
                                Lav: value.PercLav,
                                Emer: value.PercEmer,
                                Pau: value.PercPausa,
                                LavTime: value.TempLav,
                                EmerTime: value.TempEmer,
                                PauTime: value.TempPausa,
                                LabelLav: value.LabelLav,
                                LabelEmer: value.LabelEmer,
                                LabelPausa: value.LabelPausa,
                                ColorLav: value.ColorLav,
                                ColorEmer: value.ColorEmer,
                                ColorPausa: value.ColorPausa,
                                ToolTipTextLav: "",
                                ToolTipTextEmer: "",
                                ToolTipTextPau: ""
                            });
                        });
                    });

                    angular.forEach($scope.istogrammaPercentualiFasi, function (value, key) {
                        var chart =
                        {
                            customizePoint(point) {
                                if (point.seriesName == "Lavorazione") {
                                    point.data.ToolTipTextLav = point.argument + "\n" + "Lavorazione: " + $scope.toFixed(point.data.Lav, 2) + "%" + "\n" + $scope.getTimeInFormat(point.data.LavTime);
                                    return { color: point.data.ColorLav, name: point.data.LabelLav }
                                } else if (point.seriesName == "Emergenza") {
                                    point.data.ToolTipTextEmer = point.argument + "\n" + "Emergenza: " + $scope.toFixed(point.data.Emer, 2) + "%" + "\n" + $scope.getTimeInFormat(point.data.EmerTime);
                                    return { color: point.data.ColorEmer, name: point.data.LabelEmer }
                                } else if (point.seriesName == "Pausa") {
                                    point.data.ToolTipTextPau = point.argument + "\n" + "Pausa: " + $scope.toFixed(point.data.Pau, 2) + "%" + "\n" + $scope.getTimeInFormat(point.data.PauTime);
                                    return { color: point.data.ColorPausa, name: point.data.LabelPausa }
                                }
                            },
                            dataSource: $scope.istogrammaPercentualiFasi[key],
                            commonSeriesSettings: {
                                argumentField: "state",
                                type: "stackedBar"
                            },
                            size: {
                                width: "100%",
                            },
                            series: [
                                { valueField: 'Lav', name: 'Lavorazione', },
                                { valueField: 'Emer', name: 'Emergenza', },
                                { valueField: 'Pau', name: 'Pausa', }
                            ],
                            legend: { visible: false },
                            tooltip: {
                                enabled: true,
                                location: "edge",
                                customizeTooltip: function (arg) {
                                    if (arg.seriesName === "Lavorazione") {
                                        return { text: arg.point.data.ToolTipTextLav };
                                    }
                                    else if (arg.seriesName == "Emergenza") {
                                        return { text: arg.point.data.ToolTipTextEmer };
                                    }
                                    else if (arg.seriesName == "Pausa") {
                                        return { text: arg.point.data.ToolTipTextPau };
                                    }
                                }
                            },
                        }
                        $scope.dxistogrammaPercentualiFasi[key] = chart;
                    });

                    $scope.istogrammaPercentualiFasi = {};
                    $scope.dxistogrammaPercentualiFasi = {};
                    angular.forEach(res.DeviceFasePercItems, function (devFaseValue, macchinaKey) {
                        $scope.istogrammaPercentualiFasi[macchinaKey] = [];
                        angular.forEach(devFaseValue, function (value, key) {
                            $scope.istogrammaPercentualiFasi[macchinaKey].push({
                                state: key,
                                Lav: value.PercLav,
                                Emer: value.PercEmer,
                                Pau: value.PercPausa,
                                LavTime: value.TempLav,
                                EmerTime: value.TempEmer,
                                PauTime: value.TempPausa,
                                LabelLav: value.LabelLav,
                                LabelEmer: value.LabelEmer,
                                LabelPausa: value.LabelPausa,
                                ColorLav: value.ColorLav,
                                ColorEmer: value.ColorEmer,
                                ColorPausa: value.ColorPausa,
                                ToolTipTextLav: "",
                                ToolTipTextEmer: "",
                                ToolTipTextPau: ""
                            });
                        });
                    });

                    angular.forEach($scope.istogrammaPercentualiFasi, function (value, key) {
                        var chart =
                        {
                            customizePoint(point) {
                                if (point.seriesName == "Lavorazione") {
                                    point.data.ToolTipTextLav = point.argument + "\n" + "Lavorazione: " + $scope.toFixed(point.data.Lav, 2) + "%" + "\n" + $scope.getTimeInFormat(point.data.LavTime);
                                    return { color: point.data.ColorLav, name: point.data.LabelLav }
                                } else if (point.seriesName == "Emergenza") {
                                    point.data.ToolTipTextEmer = point.argument + "\n" + "Emergenza: " + $scope.toFixed(point.data.Emer, 2) + "%" + "\n" + $scope.getTimeInFormat(point.data.EmerTime);
                                    return { color: point.data.ColorEmer, name: point.data.LabelEmer }
                                } else if (point.seriesName == "Pausa") {
                                    point.data.ToolTipTextPau = point.argument + "\n" + "Pausa: " + $scope.toFixed(point.data.Pau, 2) + "%" + "\n" + $scope.getTimeInFormat(point.data.PauTime);
                                    return { color: point.data.ColorPausa, name: point.data.LabelPausa }
                                }
                            },
                            dataSource: $scope.istogrammaPercentualiFasi[key],
                            commonSeriesSettings: {
                                argumentField: "state",
                                type: "stackedBar"
                            },
                            size: {
                                width: "100%",
                            },
                            series: [
                                { valueField: 'Lav', name: 'Lavorazione', },
                                { valueField: 'Emer', name: 'Emergenza', },
                                { valueField: 'Pau', name: 'Pausa', }
                            ],
                            legend: { visible: false },
                            tooltip: {
                                enabled: true,
                                location: "edge",
                                customizeTooltip: function (arg) {
                                    if (arg.seriesName === "Lavorazione") {
                                        return { text: arg.point.data.ToolTipTextLav };
                                    }
                                    else if (arg.seriesName == "Emergenza") {
                                        return { text: arg.point.data.ToolTipTextEmer };
                                    }
                                    else if (arg.seriesName == "Pausa") {
                                        return { text: arg.point.data.ToolTipTextPau };
                                    }
                                }
                            },
                        }
                        $scope.dxistogrammaPercentualiFasi[key] = chart;
                    });

                    $scope.extraInfoList = [
                        {
                            key: "Le principali cause di scarto sono:",
                            items: res.ScartiCausaliPercentSummary
                        },
                        {
                            key: "Le principali cause di fermo sono:",
                            items: res.FermiCausaliPercentSummary
                        },
                        {
                            key: "I fermi di maggior durata sono:",
                            items: res.FermiCausaliTimeSummary
                        },
                        {
                            key: "I fermi più frequenti sono:",
                            items: res.FermiCausaliNumberSummary
                        },
                    ]

                    var dateDiffApp = $scope.filtroAl.getTime() - $scope.filtroDal.getTime();
                    var dateDiff = (dateDiffApp / 1000 / 60 / 60 / 24);
                    dateDiff = dateDiff + 1;

                    $scope.maxValueBulletAva = 100;

                    angular.forEach(res.ListOeeField, function (value, key) {
                        $scope.seriesChartLine.push({
                            argumentField: "x",
                            valueField: value,
                            name: value,
                            type: "line",
                            point: {
                                visible: false,
                            }
                        });
                    });

                    angular.forEach(res.ListAvanzamenti, function (avanzamentiMacchina, codiceMacchina) {
                        var stringMacchina = "chartFasiTempi" + codiceMacchina;
                        angular.forEach(avanzamentiMacchina, function (ava, key) {
                            ava.Start = elaborateStringToDate(ava.Start);
                            if (ava.End != undefined)
                                ava.End = elaborateStringToDate(ava.End);
                            else
                                ava.End = Date.now();
                        });

                        graficoAvanzamenti = {
                            dataSource: avanzamentiMacchina,
                            loadingIndicator: true,
                            rotated: true,
                            width: "100%",
                            barGroupPadding: 0.1,
                            columnFixing: {
                                enabled: true
                            },
                            valueAxis: {
                                argumentType: 'datetime',
                                label: {
                                    format: "HH:mm:ss \r\n dd/MM/yyyy",
                                }
                            },
                            customizePoint: function (info) {
                                return {
                                    color: info.data.Colore
                                }
                            },
                            commonSeriesSettings: {
                                argumentField: "Fase",
                                type: "rangeBar",
                                barHeight: 50,
                                rangeValue1Field: "Start",
                                rangeValue2Field: "End",
                                ignoreEmptyPoints: true,
                                barOverlapGroup: "Fase",
                            },
                            seriesTemplate: {
                                nameField: "StatoLavorazione"
                            },
                            animation: {
                                enabled: false
                            },
                            scrollBar: {
                                visible: false,
                            },
                            tooltip: {
                                enabled: true,
                                location: "edge",
                                customizeTooltip: function (arg) {
                                    return {
                                        text: "<span style=\"color:" + arg.point.data.Colore + "\">" + arg.rangeValue1.toLocaleString("it-IT") + "<br>" + arg.rangeValue2.toLocaleString("it-IT") + "</span>"
                                    };
                                }
                            },
                            legend: {
                                visible: false,
                                title: "",
                                verticalAlignment: "bottom",
                                horizontalAlignment: "center"
                            }
                        };
                        $scope.chartFasiTempi[stringMacchina] = graficoAvanzamenti;
                    });

                    angular.forEach($scope.dataSourceChartLine, function (value, key) {
                        value["x"] = elaborateStringToDate(value["x"]);
                    });

                    document.getElementById('divPrincipale').setAttribute("style", "opacity: 1");
                    document.getElementsByClassName("loader")[0].style.display = "none";
                    $scope.showCheckboxToolTip = true;

                    //aggiorno series
                    $("#chartLine").dxChart("option", "series", $scope.seriesChartLine);
                    $("#rangeChartLine").dxRangeSelector("option", "chart.series", $scope.seriesChartLine);
                    $("#oee").dxBarGauge("option", "values", [$scope.oee]);
                    $("#ava").dxBarGauge("option", "values", [$scope.oee_rf]);
                    $("#eff").dxBarGauge("option", "values", [$scope.oee_rv]);
                    $("#qua").dxBarGauge("option", "values", [$scope.oee_rq]);

                    // Aggiornamenti grafici
                    $("#pieFermi").dxPieChart("option", "dataSource", $scope.listFermiGrafici);
                    $("#pieScarti").dxPieChart("option", "dataSource", $scope.listScartiGrafici);
                    $("#chartFermiPareto").dxChart("option", "dataSource", $scope.listFermiGrafici);
                    $("#chartScartiPareto").dxChart("option", "dataSource", $scope.listScartiGrafici);
                    $("#grigliaPerOrdine").dxDataGrid("option", "dataSource", $scope.batchTimeSummary);
                    $("#grigliaPerMacchina").dxDataGrid("option", "dataSource", $scope.macchinaTimeSummary);
                    $("#grigliaPerTurno").dxDataGrid("option", "dataSource", $scope.turnoTimeSummary);
                    //$("#grigliaLavPerOrdine").dxDataGrid("option", "dataSource", $scope.lavBatchTimeSummary);
                    //$("#grigliaLavPerMacchina").dxDataGrid("option", "dataSource", $scope.lavMacchinaTimeSummary);
                    //$("#grigliaLavPerTurno").dxDataGrid("option", "dataSource", $scope.lavTurnoTimeSummary);
                    //$("#grigliaFermiPerOrdine").dxDataGrid("option", "dataSource", $scope.fermiBatchTimeSummary);
                    //$("#grigliaFermiPerMacchina").dxDataGrid("option", "dataSource", $scope.fermiMacchinaTimeSummary);
                    //$("#grigliaFermiPerTurno").dxDataGrid("option", "dataSource", $scope.fermiTurnoTimeSummary);
                    //$("#grigliaPausaPerOrdine").dxDataGrid("option", "dataSource", $scope.pausaBatchTimeSummary);
                    //$("#grigliaPausaPerMacchina").dxDataGrid("option", "dataSource", $scope.pausaMacchinaTimeSummary);
                    //$("#grigliaPausaPerTurno").dxDataGrid("option", "dataSource", $scope.pausaTurnoTimeSummary);
                    $("#extraInfoList").dxList("option", "dataSource", $scope.extraInfoList);
                    $("#grigliaDati").dxDataGrid("option", "dataSource", $scope.listGrigliaDati);
                    $("#grigliaFermi").dxDataGrid("option", "dataSource", $scope.listFermi);
                    $("#grigliaScarti").dxDataGrid("option", "dataSource", $scope.listScarti);
                    $("#chartLine").dxChart("option", "dataSource", $scope.dataSourceChartLine);
                    $("#rangeChartLine").dxRangeSelector("option", "dataSource", $scope.dataSourceChartLine);
                    $("#accordionContainer").dxAccordion("option", "dataSource", $scope.listAccordion);
                    $("#grigliaEfficienza").dxDataGrid("option", "dataSource", $scope.listEfficency);
                    $("#checkBox" + $scope.selectedCheckBoxOdp).dxCheckBox("option", "value", $scope.checkBoxBatchDefaultValue);
                    angular.forEach($scope.dxistogrammaPercentualiFasi, function (value, key) {
                        $("#istogrammaPercentualiFasi" + key).dxChart("option", "dataSource", $scope.dxistogrammaPercentualiFasi[key]);
                    });

                    angular.forEach($scope.chartFasiTempi, function (value, key) {
                        $("#" + key).dxChart("instance").render();
                    });
                    $("#chartFermiPareto").dxDataGrid("option", "dataSource", $scope.listFermiGrafici);
                    if ($scope.selectedTabId == $scope.availabilityTabId) {
                        if ($scope.selectedInnerTabId == $scope.tabDatiPianiId) {
                            $scope.fermiLoaderIsVisibile = true;
                            hub.invoke('GetFermiDatiPiani', $scope.requestData).done(function (res) {
                                $scope.fermiDatiPiani = res;
                                $("#tabellaFermiDatiPiani").dxDataGrid("option", "dataSource", $scope.fermiDatiPiani);
                                $scope.fermiLoaderIsVisibile = false;
                                $scope.fermiDatiPianiUploaded = true;
                            });
                        }
                    } else if ($scope.selectedTabId == $scope.qualityTabId) {
                        if ($scope.selectedQualityInnerTabId == $scope.tabScartiDatiPianiId) {
                            $scope.scartiLoaderIsVisibile = true;
                            hub.invoke('GetScartiDatiPiani', $scope.requestData).done(function (res) {
                                $scope.scartiDatiPiani = res;
                                $("#tabellaScartiDatiPiani").dxDataGrid("option", "dataSource", $scope.scartiDatiPiani);
                                $scope.scartiLoaderIsVisibile = false;
                                $scope.scartiDatiPianiUploaded = true;
                            });
                        }
                    }
                });//fine apply
            }).fail(function (error) {
                document.getElementById('divPrincipale').setAttribute("style", "opacity: 1");
                document.getElementsByClassName("loader")[0].style.display = "none";
                $scope.showCheckboxToolTip = true;
            });
        }
        $scope.renderDatiGrafici = function () {
            setTimeout(function () {

                // SCARTI
                $("#chartScartiPareto").dxChart("instance").render();
                $("#pieScarti").dxPieChart("instance").render();
                $("#grigliaScarti").dxDataGrid("instance").refresh();

                // FERMI
                $("#chartFermiPareto").dxChart("instance").render();
                $("#pieFermi").dxPieChart("instance").render();
                $("#grigliaFermi").dxDataGrid("instance").refresh();


                //EFFICIENZA
                $("#grigliaEfficienza").dxDataGrid("instance").refresh();

            }, 50);
        };

        $scope.aggiornaFiltriCommesse = function () {
            //hub.invoke('GetDataDashboardOeeCommesse', $scope.gridBoxMacchineId).done(function (res) {
            //    $scope.$apply(function () {
            //        $scope.listaCommesse = res;
            //    });//fine apply
            //});
        };
        $scope.aggiornaFiltriMateriali = function () {
            hub.invoke('GetDataDashboardOeeMateriali', $scope.gridBoxCommesseId).done(function (res) {
                $scope.$apply(function () {
                    $scope.listaMateriali = res;
                });//fine apply
            });
        };

        $scope.changeFiltroDal = function () {
            var filtroDalString = $scope.yyyymmdd($scope.filtroDal);
        };

        $scope.gridBoxCommesse = {
            bindingOptions: {
                value: "gridBoxCommesseValue"
            },
            onValueChanged: function (e) {
                $scope.gridBoxCommesseValue = e.value || [];
                //$scope.aggiornaFiltriMateriali();
            },
            dropDownOptions: {
                minWidth: 400,
            },
            valueExpr: "Id",
            placeholder: "",
            displayExpr: "Odp",
            showClearButton: false,
            dataGrid: {
                filterRow: {
                    visible: true,
                },
                allowSearch: true,
                columns: [{
                    caption: "Ordine",
                    dataField: "Odp",
                    dataType: "string",
                }
                ],
                bindingOptions: {
                    dataSource: 'listaCommesse'
                },
                hoverStateEnabled: true,
                paging: { enabled: true, pageSize: 10 },
                height: "300",
                width: "100%",
                scrolling: {
                    mode: 'infinite',
                },
                selection: { mode: "multiple", showCheckBoxesMode: "always" },
                onSelectionChanged: function (selectedItems) {
                    $scope.arrMacchineSelezionate = selectedItems.selectedRowKeys;
                    $scope.gridBoxCommesseValue = [];
                    $scope.gridBoxCommesseId = [];
                    angular.forEach(selectedItems.selectedRowKeys, function (item, key) {
                        $scope.gridBoxCommesseValue.push(item.Odp);
                        $scope.gridBoxCommesseId.push(item.Id);
                    });
                },
            }
        };

        $scope.gridBoxMacchine = {
            bindingOptions: {
                value: "gridBoxMacchineValue"
            },
            onValueChanged: function (e) {
                $scope.gridBoxMacchineValue = e.value || [];
                $scope.aggiornaFiltriCommesse();
            },
            onOpened: function (e) {
                var popupInstance = e.component._popup;
                popupInstance.option('width', 450);
            },
            valueExpr: "Id",
            placeholder: "",
            displayExpr: "Nome",
            showClearButton: false,
            dataGrid: {
                filterRow: { visible: true },
                columns: [{
                    caption: "Codice",
                    dataField: "Codice",
                    dataType: "string",
                }, {
                    caption: "Nome",
                    dataField: "Nome",
                    dataType: "string",
                    sortOrder: "asc",
                }],
                bindingOptions: {
                    dataSource: 'listaMacchine'
                },
                hoverStateEnabled: true,
                paging: { enabled: true, pageSize: 10 },
                height: "400",
                width: "100%",
                scrolling: {
                    mode: 'infinite',
                },
                selection: { mode: "multiple", showCheckBoxesMode: "always" },
                onSelectionChanged: function (selectedItems) {
                    $scope.arrMacchineSelezionate = selectedItems.selectedRowKeys;
                    $scope.gridBoxMacchineValue = [];
                    $scope.gridBoxMacchineId = [];
                    angular.forEach(selectedItems.selectedRowKeys, function (item, key) {
                        $scope.gridBoxMacchineValue.push(item.Codice);
                        $scope.gridBoxMacchineId.push(item.Id);
                    });
                },
            }
        };

        $scope.arrTurniSelezionati = [];

        $scope.gridBoxTurni = {
            bindingOptions: {
                value: "gridBoxTurniValue"
            },
            onValueChanged: function (e) {
                $scope.gridBoxTurniValue = e.value || [];
            },
            onOpened: function (e) {
                var popupInstance = e.component._popup;
                popupInstance.option('width', 350);
            },
            valueExpr: "Id",
            placeholder: "",
            displayExpr: "Codice",
            showClearButton: false,
            dataGrid: {
                filterRow: { visible: true },
                columns: [{
                    caption: "Codice",
                    dataField: "Codice",
                    dataType: "string",
                }, {
                    caption: "Nome",
                    dataField: "Nome",
                    dataType: "string",
                }],
                bindingOptions: {
                    dataSource: 'listaTurni'
                },
                hoverStateEnabled: true,
                paging: { enabled: true, pageSize: 10 },
                height: "300",
                width: "100%",
                scrolling: {
                    mode: 'infinite',
                },
                selection: { mode: "multiple", showCheckBoxesMode: "always" },
                onSelectionChanged: function (selectedItems) {
                    $scope.arrTurniSelezionati = selectedItems.selectedRowKeys;
                    $scope.gridBoxTurniValue = [];
                    $scope.gridBoxTurniId = [];
                    angular.forEach(selectedItems.selectedRowKeys, function (item, key) {
                        $scope.gridBoxTurniValue.push(item.Codice);
                        $scope.gridBoxTurniId.push(item.Id);
                    });
                },
            }
        };

        $scope.gridBoxMateriali = {
            bindingOptions: {
                value: "gridBoxMaterialiValue"
            },
            onValueChanged: function (e) {
                $scope.gridBoxMaterialiValue = e.value || [];
            },
            onOpened: function (e) {
                var popupInstance = e.component._popup;
                popupInstance.option('width', 350);
            },
            valueExpr: "Id",
            placeholder: "",
            displayExpr: "Codice",
            showClearButton: false,
            dropDownOptions: {
                minWidth: 700,
            },
            dataGrid: {
                filterRow: { visible: true },
                columns: [{
                    caption: "Codice",
                    dataField: "Codice",
                    dataType: "string",
                    width:90
                },
                {
                    caption: "Nome",
                    dataField: "Nome",
                    dataType: "string",
                    width:150,
                },
                {
                    caption: "Descrizione",
                    dataField: "Descrizione",
                    dataType: "string",
                }],
                scrolling: {
                    mode: 'infinite',
                },
                bindingOptions: {
                    dataSource: 'listaMateriali'
                },
                hoverStateEnabled: true,
                paging: { enabled: true, pageSize: 10 },
                height: "300",
                width: "100%",
                selection: { mode: "multiple", showCheckBoxesMode: "always" },
                onSelectionChanged: function (selectedItems) {
                    $scope.arrMaterialiSelezionati = selectedItems.selectedRowKeys;
                    $scope.gridBoxMaterialiValue = [];
                    $scope.gridBoxMaterialiId = [];
                    angular.forEach(selectedItems.selectedRowKeys, function (item, key) {
                        $scope.gridBoxMaterialiValue.push(item.Codice);
                        $scope.gridBoxMaterialiId.push(item.Id);
                    });
                },
            }
        };

        $scope.percMinPie = {
            dataSource: [0, 5, 10, 15, 20, 25],
            value: $scope.percMinPieValue,
            placeholder: '% minima visualizzata',
            onValueChanged(data) {
                $scope.percMinPieValue = data.value;
                $("#pieFermi").dxPieChart('instance').option("series.smallValuesGrouping.threshold", data.value);
                $("#pieScarti").dxPieChart('instance').option("series.smallValuesGrouping.threshold", data.value);
                localStorage.setItem('dashboard-oee.html-percMinPie', data.value);
            },
        };

        $scope.gridBoxPercentuali = {
            bindingOptions: {
                value: "gridBoxPercentualiValue"
            },
            onValueChanged: function (e) {
                $scope.gridBoxMaterialiValue = e.value || [];
            },
            onOpened: function (e) {
                var popupInstance = e.component._popup;
                popupInstance.option('width', 350);
            },
            valueExpr: "Id",
            placeholder: "Seleziona articolo",
            displayExpr: "Codice",
            showClearButton: false,
            dataGrid: {
                columns: [{
                    caption: "Codice",
                    dataField: "Codice",
                    dataType: "string",
                }],
                bindingOptions: {
                    dataSource: 'gridBoxPercentuali'
                },
                hoverStateEnabled: true,
                paging: { enabled: true, pageSize: 10 },
                height: "300",
                width: "100%",
                selection: { mode: "multiple", showCheckBoxesMode: "always" },
                scrolling: {
                    mode: 'infinite',
                },
                onSelectionChanged: function (selectedItems) {
                    $scope.arrMaterialiSelezionati = selectedItems.selectedRowKeys;
                    $scope.gridBoxMaterialiValue = [];
                    $scope.gridBoxMaterialiId = [];
                    angular.forEach(selectedItems.selectedRowKeys, function (item, key) {
                        $scope.gridBoxMaterialiValue.push(item.Codice);
                        $scope.gridBoxMaterialiId.push(item.Id);
                    });
                },
            }
        };

        const locale = getLocale();
        DevExpress.localization.locale(locale);

        function getLocale() {
            const storageLocale = sessionStorage.getItem('locale');
            return storageLocale != null ? storageLocale : 'it';
        }
        // -------- FILTRER START --------
        $scope.rangeSelectorOptions = {
            margin: {
                top: 10,
            },
            size: {
                height: 120
            },
            scale: {

                startValue: $scope.filtroDal,
                endValue: $scope.filtroAl,
                minorTickInterval: 'day',
                tickInterval: 'day',
                minRange: 'hour',
                maxRange: 'year',
                minorTick: {
                    visible: false,
                },
            },
            value: [$scope.filtroDal, $scope.filtroAl],
            title: '',
        };
        // -------- FILTRER END --------
        // -------- HOME START --------
        $scope.showBorders = false;

        $scope.gauge = {
            oee: {
                geometry,
                values: [0],
                title: {
                    text: "OEE",
                    font: {
                        size: 28,
                    },
                },
                relativeInnerRadius: "0.65",
                backgroundColor: "#22B7F2",
                palette: ["#033859"],
                label: {
                    visible: false
                }
            },

            ava: {
                geometry,
                values: [0],
                relativeInnerRadius: "0.65",
                backgroundColor: "#22B7F2",
                palette: ["#033859"],
                label: {
                    visible: false
                },
                title: {
                    text: "Availability",
                    font: {
                        size: 28,
                    },
                },
            },

            eff: {
                geometry,
                values: [0],
                relativeInnerRadius: "0.65",
                backgroundColor: "#22B7F2",
                palette: ["#033859"],
                label: {
                    visible: false
                },
                title: {
                    text: "Efficiency",
                    font: {
                        size: 28,
                    },
                },
            },

            qua: {
                geometry,
                values: [0],
                relativeInnerRadius: "0.65",
                backgroundColor: "#22B7F2",
                palette: ["#033859"],
                label: {
                    visible: false
                },
                title: {
                    text: "Quality",
                    font: {
                        size: 28,
                    },
                },

            },
        };

        $scope.grigliaDati = {
            dataSource: $scope.listGrigliaDati,
            keyExpr: "campo",
            allowColumnResizing: true,
            height: "100%",
            showBorders: true,
            columnAutoWidth: true,
            columnsAutoWidth: true,
            loadPanel: {
                enabled: true
            },
            scrolling: {
                mode: "virtual"
            },
            hoverStateEnabled: true,
            showBorders: true,
            scrolling: { mode: "infinite" },
            showBorders: true,
            columns: [
                {
                    caption: "",
                    dataField: "campo",
                    dataType: "string",
                },
                {
                    caption: "T1",
                    dataField: "MATT",
                    dataType: "string",
                },
                {
                    caption: "T2",
                    dataField: "POM",
                    dataType: "string",
                },
                {
                    caption: "T3",
                    dataField: "NOTT",
                    dataType: "string",
                },
                {
                    caption: "Tot",
                    dataField: "TOT",
                    dataType: "string",
                },
                {
                    caption: "%",
                    dataField: "perc",
                    dataType: "string",
                    format: "percent",
                    allowGrouping: false,
                    cellTemplate: "bulletPerc",
                    cssClass: "bullet",
                },
            ],
            onSelectionChanged: function (selectedItems) {
                $scope.selectedEmployee = selectedItems.selectedRowsData[0];
                $scope.showItemDetail(selectedItems.selectedRowsData[0], false);
            },
            bindingOptions: {
                showBorders: 'showBorders'
            }
        };

        $scope.chartLine = {
            palette: 'Dark Moon',
            dataSource: $scope.dataSourceChartLine,
            //type: 'area',
            commonSeriesSettings: {
                type: "area",
                area: 'area',
                argumentField: 'x',
            },
            margin: {
                bottom: 20,
            },
            argumentAxis: {
                valueMarginsEnabled: false,
                discreteAxisDivisionMode: 'crossLabels',
                grid: {
                    visible: true,
                },
            },
            series: $scope.seriesChartLine,
            legend: {
                verticalAlignment: 'bottom',
                horizontalAlignment: 'center',
                itemTextPosition: 'bottom',
            },
            onLegendClick(e) {
                const series = e.target;
                if (series.isVisible()) {
                    series.hide();
                } else {
                    series.show();
                }
            },
            tooltip: {
                customizeTooltip: function (arg) {
                    return {
                        text: "<span>Valore:" + arg.point.data.PercentualeTempoDouble + "%<br>" + arg.argumentText + "</span>"
                    };
                }
            }
        };

        $scope.rangeChartLine = {
            size: {
                width: "100%",
            },
            margin: {
                bottom: 10
            },
            scale: {
                minorTickCount: 1,
            },
            dataSource: null,
            chart: {
                palette: 'Dark Moon',
                series: null,
            },
            behavior: {
                callValueChanged: 'onMoving',
            },
            onValueChanged(e) {
                const zoomedChart = $('#chartLine').dxChart('instance');
                zoomedChart.getArgumentAxis().visualRange(e.value);
            },
            tooltip: {
                enabled: true,
            },
        };
        // -------- HOME END --------
        // -------- EFF START --------

        $scope.maxValueBulletEff = 250;
        $scope.grigliaEfficienza = {
            dataSource: null,
            allowColumnResizing: true,
            height: "100%",
            showBorders: true,
            columnAutoWidth: true,
            loadPanel: {
                enabled: true
            },
            scrolling: {
                mode: "virtual"
            },
            export: {
                enabled: true,
            },
            groupPanel: {
                visible: true,
            },
            hoverStateEnabled: true,
            showBorders: true,
            columns: [
                {
                    caption: "Articolo",
                    dataField: "Articolo",
                    dataType: "string",
                    groupIndex: 0,
                },
                {
                    caption: "Ordine",
                    dataField: "Commessa",
                    dataType: "string",
                },

                {
                    caption: "Pezzi per ora reale",
                    dataField: "PezziOraReale",
                    dataType: "string",
                },
                {
                    caption: "Pezzi per ora teorico",
                    dataField: "PezziOraTeorico",
                    dataType: "string",
                },
                {
                    caption: "Ciclo reale (s)",
                    dataField: "CicloReale",
                    allowGrouping: false,
                    dataType: "string",
                },
                {
                    caption: "Ciclo teorico (s)",
                    dataField: "CicloTeorico",
                    allowGrouping: false,
                    dataType: "string",
                },
                {
                    caption: "Grafico",
                    dataField: "Grafico",
                    dataType: "int",
                    format: "percent",
                    allowGrouping: false,
                    cellTemplate: "bulletEff",
                    cssClass: "bullet",
                },
            ],
            bindingOptions: {
                showBorders: 'showBorders'
            }
        };

        // -------- EFF END --------
        // -------- DISP START --------

        $scope.accordionOptions = {
            dataSource: [],
            itemTemplate: 'customer',
            bindingOptions: {
                collapsible: 'collapsible',
                multiple: 'multiple'
            },
        };

        $scope.refreshAccordion = function () {
            setTimeout(function () { $('#accordionContainer').dxAccordion('instance').repaint(); }, 150);
        };

        var palGreen4 = ["#7CFC00", "#32CD32", "#00FF00", "#228B22", "#008000", "#006400", "#ADFF2F", "#9ACD32", "#00FF7F", "#00FA9A", "#90EE90", "#98FB98", "#8FBC8F", "#3CB371", "#20B2AA", "#2E8B57", "#808000", "#556B2F", "#6B8E23"];

        $scope.pieFermi = {
            title: "Fermi in %",
            commonSeriesSettings: {
                argumentField: "state",
            },
            size: {
                width: "100%",
                height: "100%"
            },
            palette: palGreen4,
            dataSource: null,
            legend: {
                paddingLeftRight: 5,
                paddingTopBottom: 5,
                markerSize: 10,
                margin: {
                    bottom: 5,
                    left: 5,
                    right: 5,
                    top: 5
                },
            },
            series: {
                argumentField: "Causale",
                valueField: "PercentualeTempo",
                label: {
                    visible: true,
                    position: "inside",
                    radialOffset: 30,
                    backgroundColor: "transparent",
                    font: {
                        size: 16,
                    }
                },
                smallValuesGrouping: {
                    mode: 'smallValueThreshold',
                    groupName: "Altri",
                    threshold: $scope.percMinPie,
                }
            }
        };

        $scope.chartFermiPareto = {
            title: "Pareto Fermi",
            palette: 'Harmony Light',
            dataSource: null,
            valueAxis: [{
                name: 'frequency',
                position: 'left',
                tickInterval: 10,
            }, {
                name: 'percentage',
                position: 'right',
                showZero: true,
                label: {
                    customizeText(info) {
                        return `${info.valueText}%`;
                    },
                },
                constantLines: [{
                    value: 80,
                    color: '#fc3535',
                    dashStyle: 'dash',
                    width: 2,
                    label: { visible: true },
                }],
                tickInterval: 40,
                valueMarginsEnabled: false,
            }],
            commonSeriesSettings: {
                argumentField: 'Causale',
            },
            export: {
                enabled: true,
            },
            size: {
                height: "50%",
                width: "100%"
            },
            tooltip: {
                enabled: true,
                location: "edge",
                customizeTooltip: function (arg) {
                    return {
                        text: "<span>Valore:" + arg.point.data.PercentualeTempoDouble + "%<br>" + arg.argumentText + "</span>"
                    };
                }
            },
            series: [{
                type: 'bar',
                valueField: 'PercentualeTempo',
                axis: 'frequency',
                name: 'Numero fermo',
                color: '#00FF00',
            }, {
                type: 'spline',
                valueField: 'PercentualeCumulativa',
                axis: 'percentage',
                name: 'Percentuale',
                color: '#9900AA',
            }],
            legend: {
                verticalAlignment: 'bottom',
                horizontalAlignment: 'center',
            }
        };

        function elaborateStringToDate(value) {
            var valueApp = "";
            if (value != "") {
                var mese = (value.substring(5, 7) - 1);
                valueApp = new Date(value.substring(0, 4), mese, value.substring(8, 10), value.substring(11, 13), value.substring(14, 16), value.substring(17, 19));
            }
            return valueApp;
        };

        $scope.totalSummaryTempo = 0;

        $scope.grigliaFermi = {
            dataSource: null,
            //keyExpr: "id",
            allowColumnResizing: true,
            height: "100%",
            showBorders: true,
            columnAutoWidth: true,
            loadPanel: {
                enabled: true
            },
            scrolling: {
                mode: "virtual"
            },
            export: {
                enabled: true,
            },
            groupPanel: {
                visible: true,
            },
            hoverStateEnabled: true,
            showBorders: true,
            columns: [
                {
                    dataField: 'Macchina',
                    groupIndex: 0,
                },
                {
                    caption: "Causale",
                    dataField: "Causale",
                    dataType: "string",
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    }
                },
                {
                    caption: "Materiale",
                    dataField: "Materiale",
                    allowGrouping: true
                },
                {
                    caption: "Tempi (giorni ore:minuti:secondi)",
                    dataField: "TempoFermo",
                    allowGrouping: false,
                    dataType: "string",
                    format: function (value) {
                        return $scope.getTimeInFormatForFermi(value);
                    },
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    }
                },
                {
                    caption: "Tempi (ore)",
                    dataField: "TempoFermo",
                    allowGrouping: false,
                    dataType: "string",
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    },
                    format: function (value) {
                        return $scope.secondiToOre(value).toFixed(2);
                    },
                },
                {
                    caption: "Numero",
                    dataField: "NumeroFermi",
                    allowGrouping: false,
                    dataType: "string",
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    }
                },
                {
                    caption: "Istogramma",
                    dataField: "NumeroFermi",
                    dataType: "int",
                    format: "percent",
                    allowGrouping: false,
                    cellTemplate: "bulletPerc",
                    cssClass: "bullet",
                    calculateCellValue: function (rowData) {
                        var macchinaPercentageVisible = false;
                        var causalePercentageVisible = false;

                        //Controllo se nei raggruppamenti sono presenti macchina e/o causale
                        var gridColumns = $('#grigliaFermi').dxDataGrid("instance").state().columns;
                        gridColumns.forEach((column) => {
                            if (column.groupIndex == null) {
                                return;
                            }

                            if (column.name === "Macchina") {
                                macchinaPercentageVisible = true;
                            }

                            if (column.name === "Causale" || column.name === "CodiceCausale") {
                                causalePercentageVisible = true;
                            }
                        });
                        var macchinaCausalePercentageVisible =
                            macchinaPercentageVisible &&
                            causalePercentageVisible;


                        if (macchinaCausalePercentageVisible)
                            return rowData.PercentualeFermiMacchinaCausale;

                        if (macchinaPercentageVisible)
                            return rowData.PercentualeFermiMacchina;

                        if (causalePercentageVisible)
                            return rowData.PercentualeFermiCausale;

                        return rowData.PercentualeFermiTot;
                    }
                },
                {
                    caption: "Tempi (%)",
                    dataField: "PercentualeTempo",
                    allowGrouping: false,
                    dataType: "double",
                    format: function (value) {
                        return value + "%";
                    },
                    visible: true,
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    },
                    calculateCellValue: function (rowData) {
                        var macchinaPercentageVisible = false;
                        var causalePercentageVisible = false;

                        //Controllo se nei raggruppamenti sono presenti macchina e/o causale
                        var gridColumns = $('#grigliaFermi').dxDataGrid("instance").state().columns;
                        gridColumns.forEach((column) => {
                            if (column.groupIndex == null) {
                                return;
                            }

                            if (column.name === "Macchina") {
                                macchinaPercentageVisible = true;
                            }

                            if (column.name === "Causale" || column.name === "CodiceCausale") {
                                causalePercentageVisible = true;
                            }
                        });


                        var macchinaCausalePercentageVisible =
                            macchinaPercentageVisible &&
                            causalePercentageVisible;

                        if (macchinaCausalePercentageVisible)
                            return rowData.PercentualeTempoMacchinaCausale;

                        if (macchinaPercentageVisible)
                            return rowData.PercentualeTempoMacchina;

                        if (causalePercentageVisible)
                            return rowData.PercentualeTempoCausale;

                        return rowData.PercentualeTempoTot;

                    }
                },
                {
                    caption: "Numero (%)",
                    dataField: "PercentualeFermiMacchina",
                    allowGrouping: false,
                    dataType: "string",
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    },
                    format: function (value) {
                        return value + "%";
                    },
                    calculateCellValue: function (rowData) {
                        var macchinaPercentageVisible = false;
                        var causalePercentageVisible = false;

                        //Controllo se nei raggruppamenti sono presenti macchina e/o causale
                        var gridColumns = $('#grigliaFermi').dxDataGrid("instance").state().columns;
                        gridColumns.forEach((column) => {
                            if (column.groupIndex == null) {
                                return;
                            }

                            if (column.name === "Macchina") {
                                macchinaPercentageVisible = true;
                            }

                            if (column.name === "Causale" || column.name === "CodiceCausale") {
                                causalePercentageVisible = true;
                            }
                        });


                        var macchinaCausalePercentageVisible =
                            macchinaPercentageVisible &&
                            causalePercentageVisible;

                        if (macchinaCausalePercentageVisible)
                            return rowData.PercentualeFermiMacchinaCausale;

                        if (macchinaPercentageVisible)
                            return rowData.PercentualeFermiMacchina;

                        if (causalePercentageVisible)
                            return rowData.PercentualeFermiCausale;

                        return rowData.PercentualeFermiTot;
                    }
                },
                //Le due seguenti colonne devono rimanare invisibili in quanto servono solamente ad avere una variabile di appoggio
                //a cui poter accedere ogni volta che viene cambiato il raggruppamento
                //permette di cambiare la percentuale in base al raggruppamento
                {
                    caption: "Percentuale Fermi Macchina",
                    dataField: "PercentualeTempoMacchina",
                    allowGrouping: false,
                    dataType: "double",
                    visible: false,
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    }
                },
                {
                    caption: "Percentuale Fermi Causale",
                    dataField: "PercentualeTempoCausale",
                    allowGrouping: false,
                    dataType: "double",
                    visible: false,
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    }
                },
                {
                    caption: "Percentuale Fermi Macchina Causale",
                    dataField: "PercentualeTempoMacchinaCausale",
                    allowGrouping: false,
                    dataType: "double",
                    visible: false,
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    }
                },
                {
                    caption: "Percentuale Fermi Totale",
                    dataField: "PercentualeTempoTot",
                    allowGrouping: false,
                    dataType: "double",
                    visible: false,
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    }
                },
                {
                    caption: "Percentuale Fermi Totale",
                    dataField: "PercentualeFermiTot",
                    allowGrouping: false,
                    dataType: "double",
                    visible: false,
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    }
                },
                {
                    caption: "Percentuale Fermi Macchina Totale",
                    dataField: "PercentualeFermiMacchina",
                    allowGrouping: false,
                    dataType: "double",
                    visible: false,
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    }
                },
                {
                    caption: "Percentuale Fermi Macchina Totale",
                    dataField: "PercentualeFermiCausale",
                    allowGrouping: false,
                    dataType: "double",
                    visible: false,
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    }
                },
                {
                    caption: "Percentuale Fermi Macchina Causale Totale",
                    dataField: "PercentualeFermiMacchinaCausale",
                    allowGrouping: false,
                    dataType: "double",
                    visible: false,
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    }
                },
            ],
            summary: {
                groupItems: [{
                    caption: "Numero fermi",
                    alignByColumn: true,
                    column: 'NumeroFermi',
                    summaryType: 'sum',
                    displayFormat: 'Totale: {0}',
                    showInGroupFooter: true,
                },],
            },
            bindingOptions: {
                showBorders: 'showBorders'
            }
        };

        //InnerTab
        $scope.selectedInnerTabId = 0;
        $scope.tabFermiId = 0;
        $scope.tabDatiPianiId = 1;
        $scope.tabSummaryId = 2;
        $scope.fermiLoaderIsVisibile = true;
        $scope.navigateInAvailability = function (tabId) {
            if ($scope.selectedInnerTabId !== tabId) {
                $scope.selectedInnerTabId = tabId;
                if ($scope.selectedInnerTabId === $scope.tabDatiPianiId && !$scope.fermiDatiPianiUploaded) {
                    $scope.fermiLoaderIsVisibile = true;
                    hub.invoke('GetFermiDatiPiani', $scope.requestData).done(function (res) {
                        $scope.fermiDatiPiani = res;
                        $("#tabellaFermiDatiPiani").dxDataGrid("option", "dataSource", $scope.fermiDatiPiani);
                        $scope.fermiLoaderIsVisibile = false;
                        $scope.fermiDatiPianiUploaded = true;
                    });
                } else {
                    $scope.fermiLoaderIsVisibile = false;
                }
            }
        }

        $scope.tabellaFermiDatiPiani = {
            dataSource: $scope.fermiDatiPiani,
            allowGrouping: true,
            showBorders: false,
            allowColumnResizing: true,
            columnAutoWidth: true,
            filterRow: {
                visible: true,
            },
            scrolling: {
                rowRenderingMode: 'virtual',
            },
            groupPanel: {
                visible: true,
            },
            pager: {
                visible: true,
                allowedPageSizes: [5, 10, 25, 50, 100],
                showPageSizeSelector: true,
                showInfo: true,
                showNavigationButtons: true,
            },
            export: {
                enabled: true,
            },
            paging: {
                pageSize: 10,
            },
            columns: [
                {
                    caption: "Odp",
                    dataField: "Odp"
                },
                {
                    caption: "Macchina",
                    dataField: "Macchina",
                    dataType: "string"
                },
                {
                    caption: "Fase",
                    dataField: "Fase"
                },
                {
                    caption: "Inizio",
                    dataField: "Inizio",
                    dataType: "datetime",
                    allowGrouping: false,
                },
                {
                    caption: "Fine",
                    dataField: "Fine",
                    dataType: 'datetime',
                    allowGrouping: false,
                },
                {
                    caption: "Causale",
                    dataField: "Causale",
                    allowGrouping: false,
                },
                {
                    caption: "Materiale",
                    dataField: "Materiale",
                    allowGrouping:true
                },
                {
                    caption: "Descrizione materiale",
                    dataField: "DescrizioneMateriale",
                },
                {
                    caption: "OpBlocco",
                    dataField: "OpLock",
                    allowGrouping: false,
                },
                {
                    caption: "OpSblocco",
                    dataField: "OpUnlock",
                    allowGrouping: false,
                },
                //{
                //    caption: "Durata",
                //    dataField: "TempoFermoText",
                //    allowGrouping: false,
                //},
                {
                    caption: "Turno",
                    dataField: "Turno"
                },


            ]
        };
        // -------- DISP END --------
        // -------- SCARTI START --------

        $scope.chartScartiPareto = {
            title: "Pareto scarti",
            palette: 'Ocean',
            dataSource: null,
            valueAxis: [{
                name: 'frequency',
                position: 'left',
                tickInterval: 10,
            }, {
                name: 'percentage',
                position: 'right',
                showZero: true,
                label: {
                    customizeText(info) {
                        return `${info.valueText}%`;
                    },
                },
                constantLines: [{
                    value: 80,
                    color: '#fc3535',
                    dashStyle: 'dash',
                    width: 2,
                    label: { visible: false },
                }],
                tickInterval: 40,
                valueMarginsEnabled: false,
            }],
            commonSeriesSettings: {
                argumentField: 'Causale',
            },
            export: {
                enabled: true,
            },
            size: {
                height: "50%",
                width: "100%"
            },
            series: [{
                type: 'bar',
                valueField: 'Percentuale',
                axis: 'frequency',
                name: 'Numero di scarti',
                color: '#00a6ff',
            }, {
                type: 'spline',
                valueField: 'PercentualeCumulativa',
                axis: 'percentage',
                name: 'Percentuale',
                color: '#9900AA',
            }],
            legend: {
                verticalAlignment: 'bottom',
                horizontalAlignment: 'center',
            },
            tooltip: {
                enabled: true,
                location: "edge",
                customizeTooltip: function (arg) {
                    return {
                        text: "<span>Valore:" + arg.valueText + "%<br>" + arg.argumentText + "</span>"
                    };
                }
            },
        };

        var palBlu3 = ["#00BFFF", "#B0C4DE", "#1E90FF", "#6495ED", "#4682B4", "#5F9EA0", "#7B68EE", "#6A5ACD", "#483D8B", "#4169E1", "#0000FF", "#0000CD", "#00008B", "#000080", "#191970", "#8A2BE2", "#4B0082"];

        $scope.pieScarti = {
            title: "Scarti",
            size: {
                width: "100%",
                height: "100%"
            },
            palette: palBlu3,
            dataSource: null,
            legend: {
                markerSize: 10,
            },
            series: {
                argumentField: "Causale",
                valueField: "Percentuale",
                label: {
                    visible: true,
                    position: "inside",
                    radialOffset: 30,
                    backgroundColor: "transparent",
                    font: {
                        size: 16,
                    }
                },
                smallValuesGrouping: {
                    mode: 'smallValueThreshold',
                    groupName: "Altri",
                    threshold: $scope.percMinPie,
                }
            }
        };

        $scope.calculateScartiCellValue = function (rowData) {
            var macchinaPercentageVisible = false;
            var causalePercentageVisible = false;

            //Controllo se nei raggruppamenti sono presenti macchina e/o causale
            var gridColumns = $('#grigliaScarti').dxDataGrid("instance").state().columns;
            gridColumns.forEach((column) => {
                if (column.groupIndex == null) {
                    return;
                }

                if (column.name === "Macchina") {
                    macchinaPercentageVisible = true;
                }

                if (column.name === "Causale" || column.name === "CodiceCausale") {
                    causalePercentageVisible = true;
                }
            });
            var macchinaCausalePercentageVisible =
                macchinaPercentageVisible &&
                causalePercentageVisible;


            if (macchinaCausalePercentageVisible)
                return rowData.PercentualeScartiPerMacchinaCausale;

            if (macchinaPercentageVisible)
                return rowData.PercentualeScartiPerMacchina;

            if (causalePercentageVisible)
                return rowData.PercentualeScartiPerCausale;

            return rowData.PercentualeScartiTot;
        };

        $scope.grigliaScarti = {
            dataSource: null,
            //keyExpr: "id",
            allowColumnResizing: true,
            height: "100%",
            showBorders: true,
            columnAutoWidth: true,
            loadPanel: {
                enabled: true
            },
            scrolling: {
                mode: "virtual"
            },
            export: {
                enabled: true,
            },
            groupPanel: {
                visible: true,
            },
            hoverStateEnabled: true,
            showBorders: true,
            columns: [
                {
                    dataField: 'Macchina',
                    groupIndex: 0,
                },
                {
                    caption: "Codice causale",
                    dataField: "CodiceCausale",
                    dataType: "string",
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    }
                },
                {
                    caption: "Causale",
                    dataField: "Causale",
                    dataType: "string",
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    }
                },
                {
                    caption: "Materiale",
                    dataField: "Materiale",
                    allowGrouping: true
                },
                {
                    caption: "Istogramma",
                    dataField: "NumeroScarti",
                    dataType: "int",
                    allowGrouping: false,
                    cellTemplate: "bulletPerc",
                    cssClass: "bullet",
                    calculateCellValue: $scope.calculateScartiCellValue
                },
                {
                    caption: "Numero scarti (%)",
                    dataField: "PercentualeScartiPerMacchina",
                    allowGrouping: false,
                    visible: true,
                    format: function (value) {
                        return value + "%";
                    },
                    visible: true,
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    },
                    calculateCellValue: $scope.calculateScartiCellValue

                },  
                {
                    caption: "Numero scarti",
                    dataField: "NumeroScarti",
                    dataType: "int",
                    allowGrouping: false,
                    cellTemplate: function (container, options) {
                        container.addClass("textUpper");
                        container.html(options.text);
                    }
                },

                {
                    caption: "PercentualeScartiTot",
                    dataField: "PercentualeScartiTot",
                    allowGrouping: false,
                    visible: false
                },
                {
                    caption: "PercentualeScartiPerMacchina",
                    dataField: "PercentualeScartiPerMacchina",
                    allowGrouping: false,
                    visible: false
                },
                {
                    caption: "PercentualeScartiPerCausale",
                    dataField: "PercentualeScartiPerCausale",
                    allowGrouping: false,
                    visible: false
                },
                {
                    caption: "PercentualeScartiPerMacchinaCausale",
                    dataField: "PercentualeScartiPerMacchinaCausale",
                    allowGrouping: false,
                    visible: false
                },
            ],
            bindingOptions: {
                showBorders: 'showBorders'
            }
        };

        $scope.selectedQualityInnerTabId = 0;
        $scope.tabScartiId = 0;
        $scope.tabScartiDatiPianiId = 1;
        $scope.tabScartiSummaryId = 2;
        $scope.scartiLoaderIsVisibile = true;
        $scope.navigateInQuality = function (tabId) {
            if ($scope.selectedQualityInnerTabId !== tabId) {
                $scope.selectedQualityInnerTabId = tabId;
                if ($scope.selectedQualityInnerTabId == $scope.tabScartiDatiPianiId && !$scope.scartiDatiPianiUploaded) {
                    $scope.scartiLoaderIsVisibile = true;
                    hub.invoke('GetScartiDatiPiani', $scope.requestData).done(function (res) {
                        $scope.scartiDatiPiani = res;
                        $("#tabellaScartiDatiPiani").dxDataGrid("option", "dataSource", $scope.scartiDatiPiani);
                        $scope.scartiLoaderIsVisibile = false;
                        $scope.scartiDatiPianiUploaded = true;
                    });
                } else {
                    $scope.scartiLoaderIsVisibile = false;
                }
            }
        }

        $scope.tabellaScartiDatiPiani = {
            dataSource: $scope.fermiDatiPiani,
            allowGrouping: true,
            showBorders: false,
            allowColumnResizing: true,
            columnAutoWidth: true,
            filterRow: {
                visible: true,
            },
            scrolling: {
                rowRenderingMode: 'virtual',
            },
            groupPanel: {
                visible: true,
            },
            pager: {
                visible: true,
                allowedPageSizes: [5, 10, 25, 50, 100],
                showPageSizeSelector: true,
                showInfo: true,
                showNavigationButtons: true,
            },
            export: {
                enabled: true,
            },
            paging: {
                pageSize: 10,
            },
            columns: [
                {
                    caption: "Odp",
                    dataField: "Odp"
                },
                {
                    caption: "Macchina",
                    dataField: "Macchina"
                },
                {
                    caption: "Fase",
                    dataField: "Fase"
                },
                {
                    caption: "Causale",
                    dataField: "Causale",
                    allowGrouping: false,
                },
                {
                    caption: "Data Evento",
                    dataField: "DataEvento",
                    dataType: "datetime",
                    allowGrouping: false,
                },
                {
                    caption: "Quantità",
                    dataField: "Quantità",
                    allowGrouping: false,
                },
                {
                    caption: "Materiale",
                    dataField: "Materiale",
                    allowGrouping: true,
                },
                {
                    caption: "Descrizione materiale",
                    dataField: "MaterialeDescrizione",
                    allowGrouping: false
                },
                {
                    caption: "Operatore",
                    dataField: "Operatore",
                    allowGrouping: false,
                },
                {
                    caption: "Turno",
                    dataField: "Turno"
                },
            ]
        };
        // -------- SCARTI END --------

        // -------- RIEPILOGO START --------

        $scope.grigliaOrdine = {
            dataSource: $scope.batchTimeSummary,
            columnAutoWidth: true,
            allowColumnReordering: true,
            filterRow: { visible: true },
            width: '100%',
            columns:
                [
                    {
                        caption: 'Odp',
                        dataField: 'Key'
                    },
                    {
                        caption: "Dettaglio",
                        type: "buttons",
                        width:45,
                        buttons: [{
                            hint: "Dettaglio",
                            icon: "fa-solid fa-circle-info",
                            onClick: function (e) {
                                var url = "produzione.html?i=fromDashboard&odp=";
                                url = url + e.row.data.Key;
                                window.open(url);
                            }
                        }]
                    },
                    {
                        caption: "Filtra",
                        type: "buttons",
                        width: 45,
                        buttons: [{
                            hint: "Filtra",
                            icon: "fa-solid fa-filter",
                            onClick: function (e) {
                                var url = "dashboard-oee.html?id=";
                                var id = $scope.listBtc.find((b) => b.OdP == e.row.data.Key).Id;
                                url = url + id;
                                window.open(url);
                            }
                        }]
                    },
                    {
                        caption: "Lavorazione",
                        headerCellTemplate: function (header, info) {
                            $('<div>')
                                .html(info.column.caption)
                                .css('color','green')
                                .css('border-bottom', '1px solid green')
                                .appendTo(header);
                        },
                        columns:
                            [
                                {
                                    caption: "Tempi",
                                    dataField: "LavTimeInSecond",
                                    format: function (value) {
                                        return $scope.getTimeInFormat(value)
                                    }
                                },
                                {
                                    caption: "Tempi (ore)",
                                    dataField: "LavTimeInSecond",
                                    format: function (value) {
                                        return $scope.secondiToOre(value).toFixed(2);
                                    },
                                },
                                {
                                    caption: "Tempi (%)",
                                    dataField: "LavPercentage",
                                    format: function (value) {
                                        return $scope.toFixed(value, 2) + "%";
                                    },
                                }
                            ]
                    },
                    {
                        caption: "Pausa",
                        headerCellTemplate: function (header, info) {
                            $('<div>')
                                .html(info.column.caption)
                                .css('color', 'orange')
                                .css('border-bottom', '1px solid orange')
                                .appendTo(header);
                        },
                        columns:
                            [
                                {
                                    caption: "Tempi",
                                    dataField: "PauTimeInSecond",
                                    format: function (value) {
                                        return $scope.getTimeInFormat(value)
                                    }
                                },
                                {
                                    caption: "Tempi (ore)",
                                    dataField: "PauTimeInSecond",
                                    format: function (value) {
                                        return $scope.secondiToOre(value).toFixed(2);
                                    },
                                },
                                {
                                    caption: "Tempi (%)",
                                    dataField: "PauPercentage",
                                    format: function (value) {
                                        return $scope.toFixed(value, 2) + "%";
                                    },
                                }
                            ]
                    },
                    {
                        caption: "Fermo",
                        headerCellTemplate: function (header, info) {
                            $('<div>')
                                .html(info.column.caption)
                                .css('color', 'red')
                                .css('border-bottom', '1px solid red')
                                .appendTo(header);
                        },
                        columns:
                            [
                                {
                                    caption: "Tempi",
                                    dataField: "EmerTimeInSecond",
                                    format: function (value) {
                                        return $scope.getTimeInFormat(value)
                                    }
                                },
                                {
                                    caption: "Tempi (ore)",
                                    dataField: "EmerTimeInSecond",
                                    format: function (value) {
                                        return $scope.secondiToOre(value).toFixed(2);
                                    },
                                },
                                {
                                    caption: "Tempi (%)",
                                    dataField: "EmerPercentage",
                                    format: function (value) {
                                        return $scope.toFixed(value, 2) + "%";
                                    },
                                }
                            ]
                    }
                ]
        };

        $scope.grigliaMacchina = {
            dataSource: $scope.macchinaTimeSummary,
            columnAutoWidth: true,
            allowColumnReordering: true,
            filterRow: { visible: true },
            width: '100%',
            columns:
                [
                    {
                        caption: 'Macchina',
                        dataField: 'Key'
                    },
                    {
                        caption: "Lavorazione",
                        headerCellTemplate: function (header, info) {
                            $('<div>')
                                .html(info.column.caption)
                                .css('color', 'green')
                                .css('border-bottom', '1px solid green')
                                .appendTo(header);
                        },
                        columns:
                            [
                                {
                                    caption: "Tempi",
                                    dataField: "LavTimeInSecond",
                                    format: function (value) {
                                        return $scope.getTimeInFormat(value)
                                    }
                                },
                                {
                                    caption: "Tempi (ore)",
                                    dataField: "LavTimeInSecond",
                                    format: function (value) {
                                        return $scope.secondiToOre(value).toFixed(2);
                                    },
                                },
                                {
                                    caption: "Tempi (%)",
                                    dataField: "LavPercentage",
                                    format: function (value) {
                                        return $scope.toFixed(value, 2) + "%";
                                    },
                                }
                            ]
                    },
                    {
                        caption: "Pausa",
                        headerCellTemplate: function (header, info) {
                            $('<div>')
                                .html(info.column.caption)
                                .css('color', 'orange')
                                .css('border-bottom', '1px solid orange')
                                .appendTo(header);
                        },
                        columns:
                            [
                                {
                                    caption: "Tempi",
                                    dataField: "PauTimeInSecond",
                                    format: function (value) {
                                        return $scope.getTimeInFormat(value)
                                    }
                                },
                                {
                                    caption: "Tempi (ore)",
                                    dataField: "PauTimeInSecond",
                                    format: function (value) {
                                        return $scope.secondiToOre(value).toFixed(2);
                                    },
                                },
                                {
                                    caption: "Tempi (%)",
                                    dataField: "PauPercentage",
                                    format: function (value) {
                                        return $scope.toFixed(value, 2) + "%";
                                    },
                                }
                            ]
                    },
                    {
                        caption: "Fermo",
                        headerCellTemplate: function (header, info) {
                            $('<div>')
                                .html(info.column.caption)
                                .css('color', 'red')
                                .css('border-bottom', '1px solid red')
                                .appendTo(header);
                        },
                        columns:
                            [
                                {
                                    caption: "Tempi",
                                    dataField: "EmerTimeInSecond",
                                    format: function (value) {
                                        return $scope.getTimeInFormat(value)
                                    }
                                },
                                {
                                    caption: "Tempi (ore)",
                                    dataField: "EmerTimeInSecond",
                                    format: function (value) {
                                        return $scope.secondiToOre(value).toFixed(2);
                                    },
                                },
                                {
                                    caption: "Tempi (%)",
                                    dataField: "EmerPercentage",
                                    format: function (value) {
                                        return $scope.toFixed(value, 2) + "%";
                                    },
                                }
                            ]
                    }
                ]
        };

        $scope.grigliaTurno = {
            dataSource: $scope.macchinaTimeSummary,
            columnAutoWidth: true,
            allowColumnReordering: true,
            filterRow: { visible: true },
            width: '100%',
            columns:
                [
                    {
                        caption: 'Turno',
                        dataField: 'Key'
                    },
                    {
                        caption: "Lavorazione",
                        headerCellTemplate: function (header, info) {
                            $('<div>')
                                .html(info.column.caption)
                                .css('color', 'green')
                                .css('border-bottom', '1px solid green')
                                .appendTo(header);
                        },
                        columns:
                            [
                                {
                                    caption: "Tempi",
                                    dataField: "LavTimeInSecond",
                                    format: function (value) {
                                        return $scope.getTimeInFormat(value)
                                    }
                                },
                                {
                                    caption: "Tempi (ore)",
                                    dataField: "LavTimeInSecond",
                                    format: function (value) {
                                        return $scope.secondiToOre(value).toFixed(2);
                                    },
                                },
                                {
                                    caption: "Tempi (%)",
                                    dataField: "LavPercentage",
                                    format: function (value) {
                                        return $scope.toFixed(value, 2) + "%";
                                    },
                                }
                            ]
                    },
                    {
                        caption: "Pausa",
                        headerCellTemplate: function (header, info) {
                            $('<div>')
                                .html(info.column.caption)
                                .css('color', 'orange')
                                .css('border-bottom', '1px solid orange')
                                .appendTo(header);
                        },
                        columns:
                            [
                                {
                                    caption: "Tempi",
                                    dataField: "PauTimeInSecond",
                                    format: function (value) {
                                        return $scope.getTimeInFormat(value)
                                    }
                                },
                                {
                                    caption: "Tempi (ore)",
                                    dataField: "PauTimeInSecond",
                                    format: function (value) {
                                        return $scope.secondiToOre(value).toFixed(2);
                                    },
                                },
                                {
                                    caption: "Tempi (%)",
                                    dataField: "PauPercentage",
                                    format: function (value) {
                                        return $scope.toFixed(value, 2) + "%";
                                    },
                                }
                            ]
                    },
                    {
                        caption: "Fermo",
                        headerCellTemplate: function (header, info) {
                            $('<div>')
                                .html(info.column.caption)
                                .css('color', 'red')
                                .css('border-bottom', '1px solid red')
                                .appendTo(header);
                        },
                        columns:
                            [
                                {
                                    caption: "Tempi",
                                    dataField: "EmerTimeInSecond",
                                    format: function (value) {
                                        return $scope.getTimeInFormat(value)
                                    }
                                },
                                {
                                    caption: "Tempi (ore)",
                                    dataField: "EmerTimeInSecond",
                                    format: function (value) {
                                        return $scope.secondiToOre(value).toFixed(2);
                                    },
                                },
                                {
                                    caption: "Tempi (%)",
                                    dataField: "EmerPercentage",
                                    format: function (value) {
                                        return $scope.toFixed(value, 2) + "%";
                                    },
                                }
                            ]
                    }
                ]
        };

        //$scope.grigliaLavPerOrdine = {
        //    dataSource: $scope.lavBatchTimeSummary,
        //    allowGrouping: false,
        //    showBorders: false,
        //    allowColumnResizing: true,
        //    scrolling: {
        //        rowRenderingMode: 'virtual',
        //    },
        //    pager: {
        //        visible: true,
        //        allowedPageSizes: [5, 10, 25],
        //        showPageSizeSelector: true,
        //        showInfo: true,
        //        showNavigationButtons: true,
        //    },
        //    export: {
        //        enabled: true,
        //    },
        //    paging: {
        //        pageSize: 10,
        //    },
        //    columns: [
        //        {
        //            caption: "Odp",
        //            dataField: "Key"
        //        },
        //        {
        //            caption: "Tempi",
        //            dataField: "Time"
        //        },
        //        {
        //            caption: "Tempi (ore)",
        //            dataField: "TimeInSecond",
        //            format: function (value) {
        //                return $scope.secondiToOre(value).toFixed(2);
        //            },
        //        },
        //        {
        //            caption: "Tempi (%)",
        //            dataField: "Percentage",
        //            format: function (value) {
        //                return $scope.toFixed(value, 2) + "%";
        //            },
        //        }
        //    ]
        //};

        //$scope.grigliaLavPerTurno = {
        //    dataSource: $scope.lavTurnoTimeSummary,
        //    allowGrouping: false,
        //    showBorders: false,
        //    allowColumnResizing: true,
        //    scrolling: {
        //        rowRenderingMode: 'virtual',
        //    },
        //    pager: {
        //        visible: true,
        //        allowedPageSizes: [5, 10, 25],
        //        showPageSizeSelector: true,
        //        showInfo: true,
        //        showNavigationButtons: true,
        //    },
        //    export: {
        //        enabled: true,
        //    },
        //    paging: {
        //        pageSize: 10,
        //    },
        //    columns: [
        //        {
        //            caption: "Turno",
        //            dataField: "Key"
        //        },
        //        {
        //            caption: "Tempi",
        //            dataField: "Time"
        //        },
        //        {
        //            caption: "Tempi (ore)",
        //            dataField: "TimeInSecond",
        //            format: function (value) {
        //                return $scope.secondiToOre(value, 2).toFixed(2);
        //            },
        //        },
        //        {
        //            caption: "Tempi (%)",
        //            dataField: "Percentage",
        //            format: function (value) {
        //                return $scope.toFixed(value, 2) + "%";
        //            },
        //        }
        //    ]
        //};

        //$scope.grigliaLavPerMacchina = {
        //    dataSource: $scope.lavMacchinaTimeSummary,
        //    allowGrouping: false,
        //    showBorders: false,
        //    allowColumnResizing: true,
        //    scrolling: {
        //        rowRenderingMode: 'virtual',
        //    },
        //    pager: {
        //        visible: true,
        //        allowedPageSizes: [5, 10, 25],
        //        showPageSizeSelector: true,
        //        showInfo: true,
        //        showNavigationButtons: true,
        //    },
        //    export: {
        //        enabled: true,
        //    },
        //    paging: {
        //        pageSize: 10,
        //    },
        //    columns: [
        //        {
        //            caption: "Macchina",
        //            dataField: "Key"
        //        },
        //        {
        //            caption: "Tempi",
        //            dataField: "Time",
        //        },
        //        {
        //            caption: "Tempi (ore)",
        //            dataField: "TimeInSecond",
        //            format: function (value) {
        //                return $scope.secondiToOre(value, 2).toFixed(2);
        //            },
        //        },
        //        {
        //            caption: "Tempi (%)",
        //            dataField: "Percentage",
        //            format: function (value) {
        //                return $scope.toFixed(value, 2) + "%";
        //            },
        //        }
        //    ]
        //};

        //$scope.grigliaFermiPerOrdine = {
        //    dataSource: $scope.fermiBatchTimeSummary,
        //    allowGrouping: false,
        //    showBorders: false,
        //    allowColumnResizing: true,
        //    scrolling: {
        //        rowRenderingMode: 'virtual',
        //    },
        //    pager: {
        //        visible: true,
        //        allowedPageSizes: [5, 10, 25],
        //        showPageSizeSelector: true,
        //        showInfo: true,
        //        showNavigationButtons: true,
        //    },
        //    export: {
        //        enabled: true,
        //    },
        //    paging: {
        //        pageSize: 10,
        //    },
        //    columns: [
        //        {
        //            caption: "Odp",
        //            dataField: "Key"
        //        },
        //        {
        //            caption: "Tempi",
        //            dataField: "Time"
        //        },
        //        {
        //            caption: "Tempi (ore)",
        //            dataField: "TimeInSecond",
        //            format: function (value) {
        //                return $scope.secondiToOre(value, 2).toFixed(2);
        //            },
        //        },
        //        {
        //            caption: "Tempi (%)",
        //            dataField: "Percentage",
        //            format: function (value) {
        //                return $scope.toFixed(value, 2) + "%";
        //            },
        //        }
        //    ]
        //};

        //$scope.grigliaFermiPerTurno = {
        //    dataSource: $scope.fermiTurnoTimeSummary,
        //    allowGrouping: false,
        //    showBorders: false,
        //    allowColumnResizing: true,
        //    scrolling: {
        //        rowRenderingMode: 'virtual',
        //    },
        //    pager: {
        //        visible: true,
        //        allowedPageSizes: [5, 10, 25],
        //        showPageSizeSelector: true,
        //        showInfo: true,
        //        showNavigationButtons: true,
        //    },
        //    export: {
        //        enabled: true,
        //    },
        //    paging: {
        //        pageSize: 10,
        //    },
        //    columns: [
        //        {
        //            caption: "Turno",
        //            dataField: "Key"
        //        },
        //        {
        //            caption: "Tempi",
        //            dataField: "Time"
        //        },
        //        {
        //            caption: "Tempi (ore)",
        //            dataField: "TimeInSecond",
        //            format: function (value) {
        //                return $scope.secondiToOre(value, 2).toFixed(2);
        //            },
        //        },
        //        {
        //            caption: "Tempi (%)",
        //            dataField: "Percentage",
        //            format: function (value) {
        //                return $scope.toFixed(value, 2) + "%";
        //            },
        //        }
        //    ]
        //};

        //$scope.grigliaFermiPerMacchina = {
        //    dataSource: $scope.fermiMacchinaTimeSummary,
        //    allowGrouping: false,
        //    showBorders: false,
        //    allowColumnResizing: true,
        //    scrolling: {
        //        rowRenderingMode: 'virtual',
        //    },
        //    pager: {
        //        visible: true,
        //        allowedPageSizes: [5, 10, 25],
        //        showPageSizeSelector: true,
        //        showInfo: true,
        //        showNavigationButtons: true,
        //    },
        //    export: {
        //        enabled: true,
        //    },
        //    paging: {
        //        pageSize: 10,
        //    },
        //    columns: [
        //        {
        //            caption: "Macchina",
        //            dataField: "Key"
        //        },
        //        {
        //            caption: "Tempi",
        //            dataField: "Time"
        //        },
        //        {
        //            caption: "Tempi (ore)",
        //            dataField: "TimeInSecond",
        //            format: function (value) {
        //                return $scope.secondiToOre(value, 2).toFixed(2);
        //            },
        //        },
        //        {
        //            caption: "Tempi (%)",
        //            dataField: "Percentage",
        //            format: function (value) {
        //                return $scope.toFixed(value, 2) + "%";
        //            },
        //        }
        //    ]
        //};

        //$scope.grigliaPausaPerOrdine = {
        //    dataSource: $scope.pausaBatchTimeSummary,
        //    allowGrouping: false,
        //    showBorders: false,
        //    allowColumnResizing: true,
        //    scrolling: {
        //        rowRenderingMode: 'virtual',
        //    },
        //    pager: {
        //        visible: true,
        //        allowedPageSizes: [5, 10, 25],
        //        showPageSizeSelector: true,
        //        showInfo: true,
        //        showNavigationButtons: true,
        //    },
        //    export: {
        //        enabled: true,
        //    },
        //    paging: {
        //        pageSize: 10,
        //    },
        //    columns: [
        //        {
        //            caption: "Odp",
        //            dataField: "Key"
        //        },
        //        {
        //            caption: "Tempi",
        //            dataField: "Time"
        //        },
        //        {
        //            caption: "Tempi (ore)",
        //            dataField: "TimeInSecond",
        //            format: function (value) {
        //                return $scope.secondiToOre(value, 2).toFixed(2);
        //            },
        //        },
        //        {
        //            caption: "Tempi (%)",
        //            dataField: "Percentage",
        //            format: function (value) {
        //                return $scope.toFixed(value, 2) + "%";
        //            },
        //        }
        //    ]
        //};

        //$scope.grigliaPausaPerTurno = {
        //    dataSource: $scope.pausaTurnoTimeSummary,
        //    allowGrouping: false,
        //    showBorders: false,
        //    allowColumnResizing: true,
        //    scrolling: {
        //        rowRenderingMode: 'virtual',
        //    },
        //    pager: {
        //        visible: true,
        //        allowedPageSizes: [5, 10, 25],
        //        showPageSizeSelector: true,
        //        showInfo: true,
        //        showNavigationButtons: true,
        //    },
        //    export: {
        //        enabled: true,
        //    },
        //    paging: {
        //        pageSize: 10,
        //    },
        //    columns: [
        //        {
        //            caption: "Turno",
        //            dataField: "Key"
        //        },
        //        {
        //            caption: "Tempi",
        //            dataField: "Time",
        //        },
        //        {
        //            caption: "Tempi (ore)",
        //            dataField: "TimeInSecond",
        //            format: function (value) {
        //                return $scope.secondiToOre(value, 2).toFixed(2);
        //            },
        //        },
        //        {
        //            caption: "Tempi (%)",
        //            dataField: "Percentage",
        //            format: function (value) {
        //                return $scope.toFixed(value, 2) + "%";
        //            },
        //        }
        //    ]
        //};

        //$scope.grigliaPausaPerMacchina = {
        //    dataSource: $scope.pausaBatchTimeSummary,
        //    allowGrouping: false,
        //    showBorders: false,
        //    allowColumnResizing: true,
        //    scrolling: {
        //        rowRenderingMode: 'virtual',
        //    },
        //    pager: {
        //        visible: true,
        //        allowedPageSizes: [5, 10, 25],
        //        showPageSizeSelector: true,
        //        showInfo: true,
        //        showNavigationButtons: true,
        //    },
        //    export: {
        //        enabled: true,
        //    },
        //    paging: {
        //        pageSize: 10,
        //    },
        //    columns: [
        //        {
        //            caption: "Macchina",
        //            dataField: "Key"
        //        },
        //        {
        //            caption: "Tempi",
        //            dataField: "Time",
        //        },
        //        {
        //            caption: "Tempi (ore)",
        //            dataField: "TimeInSecond",
        //            format: function (value) {
        //                return $scope.secondiToOre(value, 2).toFixed(2);
        //            },
        //        },
        //        {
        //            caption: "Tempi (%)",
        //            dataField: "Percentage",
        //            format: function (value) {
        //                return $scope.toFixed(value, 2) + "%";
        //            },
        //        }
        //    ]
        //};

        $scope.checkBoxBatch = {
            value: $scope.checkBoxBatchDefaultValue,
            onValueChanged: function (e) {
                if (e.value) {
                    if ($scope.setToTrueByDefault) {
                        $scope.setToTrueByDefault = false;
                        return;
                    }
                    $scope.selectedCheckBoxOdp = e.model.btc.OdP;
                    $scope.filtredDatiGrafici(true, e.model.btc.Id);
                }
                else if ($scope.checkboxFilterActive) {
                    $scope.filtredDatiGrafici(true, e.model.btc.Id);
                }
            },
            onContentReady: function (e) {
                //Ho messo questo timeout perchè se si cambia prima che sia visualizzato in automatico viene rimesso a false
                //da qualcolsa che non sono riuscito a identificare
                setTimeout(function () {
                    $scope.setToTrueByDefault = $scope.checkBoxBatchDefaultValue;
                    $("#checkBox" + $scope.selectedCheckBoxOdp).dxCheckBox("option", "value", $scope.checkBoxBatchDefaultValue);
                }, 100);
            }
        }

        $scope.listOptions = {
            dataSource: $scope.extraInfoList,
            height: '100%',
            grouped: true,
            collapsibleGroups: true,
            //onContentReady: function (e) {
            //    setTimeout(function () {
            //        var items = e.component.option("items");
            //        for (var i = 0; i < items.length; i++)
            //            e.component.collapseGroup(i);
            //    }, 50);
            //},
            groupTemplate(data) {
                return $(`<h3 style="color:#636e7b">${data.key}</h3>`);
            },
        };

        $scope.toFixed = (number, decimalDigits) => {
            return Number.parseFloat(number).toFixed(decimalDigits);
        }

        $scope.getTimeInFormat = function (seconds) {
            var tempo = parseInt(seconds);
            var dd = Math.floor(tempo / 86400);
            tempo %= 86400;
            var hh = Math.floor(tempo / 3600);
            tempo %= 3600;
            var mm = Math.floor(tempo / 60);
            var ss = tempo % 60;

            var ddString = dd > 0 ? dd.toString() + "giorni" : "";
            var hhString = hh > 0 ? hh.toString() + "ore" : "";
            var mmString = mm > 0 ? mm.toString() + "min" : "";
            return `${ddString} ${hhString} ${mmString} ${ss.toString()}sec`;
        }

        $scope.getTimeInFormatForFermi = function (seconds) {
            var tempo = parseInt(seconds);
            var dd = Math.floor(tempo / 86400);
            tempo %= 86400;
            var hh = Math.floor(tempo / 3600);
            tempo %= 3600;
            var mm = Math.floor(tempo / 60);
            var ss = tempo % 60;

            var ddString = dd > 0 ? dd.toString().padStart(2, '0') + "g" : "00g";
            var hhString = hh > 0 ? hh.toString().padStart(2, '0') + "o" : "00o";
            var mmString = mm > 0 ? mm.toString().padStart(2, '0') + "m" : "00m";
            return `${ddString} ${hhString}:${mmString}:${ss.toString()}s`;
        }



        $scope.getEnergyInScale = function (kWh) {
            let absKWh = Math.abs(kWh);
            if (absKWh >= 1000000) {
                // Converte da kWh a GWh
                return $scope.toFixed(kWh / 1000000, 2) + " GWh";
            } else if (absKWh >= 1000) {
                // Converte da kWh a MWh
                return $scope.toFixed(kWh / 1000, 2) + " MWh";
            } else if (absKWh < 0.1) {
                return $scope.toFixed(kWh * 1000, 2) + " Wh";
            }
            else {
                // Rimane in kWh
                return $scope.toFixed(kWh, 2) + " kWh";
            }
        }

        $scope.getEnergyInScaleNumberOnly = function (kWh, decimalDigits) {
            let absKWh = Math.abs(kWh);
            if (absKWh >= 1000000) {
                // Converte da kWh a GWh
                return $scope.toFixed(kWh / 1000000, decimalDigits);
            } else if (absKWh >= 1000) {
                // Converte da kWh a MWh
                return $scope.toFixed(kWh / 1000, decimalDigits);
            } else if (absKWh < 1) {
                return $scope.toFixed(kWh * 1000, decimalDigits);
            }
            else {
                // Rimane in kWh
                return $scope.toFixed(kWh, decimalDigits);
            }
        }

        $scope.getEnergyInScaleUnitOnly = function (kWh) {
            let absKWh = Math.abs(kWh);
            if (absKWh >= 1000000000) {
                // Converte da kWh a TWh
                return "TWh";
            }
            else if (absKWh >= 1000000) {
                // Converte da kWh a GWh
                return "GWh";
            } else if (absKWh >= 1000) {
                // Converte da kWh a MWh
                return "MWh";
            } else if (absKWh < 1) {
                return "Wh";
            }
            else {
                // Rimane in kWh
                return "kWh";
            }
        }

        function formatDataHtml(date) {
            var mese = (date.getMonth() + 1);
            if ((date.getMonth() + 1) < 10)
                mese = "0" + (date.getMonth() + 1);

            var giorno = date.getDate();
            if (date.getDate() < 10)
                giorno = "0" + date.getDate();

            return date.getFullYear() + '-' + mese + '-' + giorno;
        }
        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        $scope.secondiToOre = function (tempoInSecondi) {
            // 3600 secondi corrispondono a un'ora
            var ore = tempoInSecondi / 3600;
            return ore;
        }
    }

]);



