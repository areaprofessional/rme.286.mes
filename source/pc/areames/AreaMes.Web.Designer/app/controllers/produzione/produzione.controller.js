﻿// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------

app.controller('ProduzioneCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$rootScope', '$translate', '$location', '$timeout',
    function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $rootScope, $translate, $timeout, $location, $timeout) {

        $scope.IsFullScreenOnEdit = true;
        $scope.items = [];

        $scope.idFase = {};
        // flag per abilitare la seconda tab dei tempi
        //$scope.isEnabledTabTimers = false;

        $scope.isEnableWrite = false;
        $scope.dataGraphOrderStatus = [];
        // flag per visualizzare i controlli sulle fasi
        $scope.isEnabledDettagliFase = false;

        $scope.summary_GraphBar = [{ key: 'OEE', values: [], color: '#0000FF' }, { key: 'OEE Rf', values: [], color: '#009933' }];
        $scope.fermiGraphBar = [{ key: "ISTOGRAMMA (TEMPO)", color: '#1f77b4', values: [] }];
        $scope.fermiGraphPie = [];

        //produzione-analisi
        var totaleSecondiPausa = 0;
        var totaleSecondiEmergenza = 0;
        var totaleSecondiLavorazione = 0;
        var arrIstogramma = {};
        var dataSourceTorta = [];
        var dataSourceIsto = [];
        var dataSourceGraficoFasi = [];
        var dataSourceGraficoFasiApp = {};
        var dataGrigliaLavorazione = {};
        var fermiSummary = [];
        var fermiGraphBar = [{ key: "ISTOGRAMMA (TEMPO)", color: '#1f77b4', values: [] }];
        var fermiGraphPie = [];
        // Preparazione dei fermi macchina per esportazione 
        var exportDataSummaryFermi = [];
        var fileNameSummary = "";
        var exportDataSummaryFermi = ["Causale Fermo", "Numero Fermi", "Durata Totale", "Percentuale"];


        $scope.scartiGraphPie = [];
        $scope.faseSelezionata = -1;
        //$scope.fermiGraphBar = [{ key: "ISTOGRAMMA", color: '#1f77b4', values: [{ label: 'Causale A', value: 1000 }] }];
        $scope.currentItem = null;
        $scope.viewState = Enums.ViewState['R'];
        $scope.titolo = "Dettaglio";
        $scope.dtInstance = {};
        $scope.dtInstance2 = {};
        $scope.dtInstance3 = {};
        $scope.dtInstance4 = {};
        $scope.dtInstance5 = {};
        $scope.dtInstance6 = {};
        $scope.ListaMateriali = null;
        $scope.ListaUnitaDiMisura = null;
        $scope.ListaStatiBatch = null;
        $scope.ListaDistintaBase = null;
        $scope.currentStatus = null;
        $scope.CompetenzeOperatori = null;
        $scope.Macchine = null;
        $scope.CompetenzeMacchine = null;
        $scope.OperazioniCiclo = null;
        $scope.InitFilter = getParameterByName("i");
        $scope.ViewAsInModifica = ($scope.InitFilter == 'inModifica');
        $scope.ViewAsInSospensione = ($scope.InitFilter == 'inSospensione');
        $scope.ViewAsInAttesa = ($scope.InitFilter == 'inAttesa');
        $scope.ViewAsInLavorazione = ($scope.InitFilter == 'inLavorazione');
        $scope.ViewAsTerminato = ($scope.InitFilter == 'terminato');
        $scope.theoreticalTime = { day: 00, hh: 00, mm: 00, ss: 00 };
        $scope.aziendaSelezionata = null;
        $scope.gaugeValue = 0;


        $scope.ViewFromDashboard = ($scope.InitFilter == 'fromDashboard');
        $scope.fromDashboardOdp = getParameterByName("odp");
        $scope.viewState = Enums.ViewState['R'];
        //if ($scope.ViewFromDashboard) {
        //    $scope.viewState = Enums.ViewState['U'];
        //}
        //$scope.filtroDal = null;
        //$scope.filtroAl = null;
        //$scope.macchina = null;
        //$scope.turno = null;
        ////////////////////////////////////////////////////////


        /////////////////////////////////////////////
        $scope.listaTurniBatch = [];
        $scope.listaMacchineBatch = [];

        $translate.use($scope.$parent.globals.currentUser.lingua);
        DataFactory.getMateriali().then(function (response) {
            $scope.ListaMateriali = response.data;
        });

        DataFactory.getUnitaDiMisura().then(function (response) {
            $scope.ListaUnitaDiMisura = response.data;
        });

        DataFactory.GetStatiBatch().then(function (response) {
            $scope.ListaStatiBatch = response.data;
        });


        //scarico le possibili causali di riavvio
        DataFactory.getCausaliRiavvio().then(function (response) {
            $scope.causaliRiavvio = response.data.filter(o => o.isStopCausal == true);
        });

        // funzione per popolare il combo box delle aziende
        $scope.listaAziende = null;
        DataFactory.GetAziende().then(function (response) {
            $scope.listaAziende = response.data;
        });

        // DataTables configurable options table trend
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('stateSave', true)
            .withOption('order', [[1, 'desc']])
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);



        $scope.gridOptions = {
            dataSource: $scope.items,
            keyExpr: "id",
            allowColumnResizing: true,
            height: 600,
            showBorders: true,
            columnAutoWidth: false,
            filterRow: { visible: true },
            onContentReady: function () {
                if ($scope.ViewAsInSospensione)
                    $("#gridContainer").dxDataGrid("columnOption", "Data sospensione", "visible", true);
                else
                    $("#gridContainer").dxDataGrid("columnOption", "Data sospensione", "visible", false);
                if ($scope.ViewAsInModifica)
                    $("#gridContainer").dxDataGrid("columnOption", "", "visible", true);
                else
                    $("#gridContainer").dxDataGrid("columnOption", "", "visible", false);
            },
            /*  filterPanel: { visible: true },*/
            /* headerFilter: { visible: true },*/
            searchPanel: {
                visible: true,
                width: 240,
                placeholder: "Cerca..."
            },
            selection: {
                mode: "single"
            },

            loadPanel: {
                enabled: true
            },
            scrolling: {
                mode: "virtual"
            },
            hoverStateEnabled: true,
            showBorders: true,
            onRowDblClick: function (e) {

                $scope.update(e, e.data)
            },
            customizePoint: function (info) {
                var State = info.data.statoLavorazione;
                var option = {
                    color: null,
                }
                option.color = colorByState[State];
                return option;
            },
            filterBuilder: {
                customOperations: [{
                    name: "weekends",
                    caption: "Weekends",
                    dataTypes: ["date"],
                    icon: "check",
                    hasValue: false,
                    calculateFilterExpression: function () {
                        return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                    }
                }],
                allowHierarchicalFields: true
            },
            filterBuilderPopup: {
                position: { of: window, at: "top", my: "top", offset: { y: 10 } },
            },
            scrolling: { mode: "infinite" },
            showBorders: true,
            columns: [{
                caption: "Stato",
                type: "buttons",
                width: "45",
                buttons: [{
                    hint: "in Modifica",
                    icon: "fas fa-flag",
                    visible: function (e) {
                        return e.row.data.stato == "inModifica";
                    }
                },
                {
                    hint: "in Lavorazione",
                    icon: "fas fa-check",
                    visible: function (e) {
                        return e.row.data.stato == "inLavorazione";
                    }
                },
                {
                    hint: "in Lavorazione In Pausa",
                    icon: "fas fa-pause",
                    visible: function (e) {
                        return e.row.data.stato == "inLavorazioneInPausa";
                    }
                },
                {
                    hint: "in Lavorazione In Emergenza",
                    icon: "fas fa-exclamation",
                    visible: function (e) {
                        return e.row.data.stato == "inLavorazioneInEmergenza";
                    }
                },
                {
                    hint: "in Lavorazione In Idle",
                    icon: "fas fa-flag",
                    visible: function (e) {
                        return e.row.data.stato == "inLavorazioneInIdle";
                    }
                },
                {
                    hint: "Terminato Abortito",
                    icon: "fas fa-ban",
                    visible: function (e) {
                        return e.row.data.stato == "terminatoAbortito";
                    }
                },
                {
                    hint: "Terminato",
                    icon: "fas fa-check-double",
                    visible: function (e) {
                        return e.row.data.stato == "terminato";
                    }
                },
                {
                    hint: "in Attesa",
                    icon: "fas fa-flag",
                    visible: function (e) {
                        return e.row.data.stato == "inAttesa";
                    }
                },
                {
                    hint: "in Sospensione",
                    icon: "fas fa-hand-paper",
                    visible: function (e) {
                        return e.row.data.stato == "inSospensione";
                    }
                }],
            },
            {
                caption: "Priorità",
                dataField: "priorita",
                dataType: "int",
                width: "60",
            },
            {
                caption: "OEE",
                dataField: "oee",
                dataType: "string",
                format: "percent",
                allowGrouping: false,
                cellTemplate: "discountCellTemplate",
                cssClass: "bullet",
                width: 150
            },
            {
                caption: "OdP",
                dataField: "odP",
                dataType: "string",
                sortOrder: "desc",
                width: 80
            },
            {
                caption: "OdV Descrizione",
                dataField: "odV_DescrizioneCliente",
                dataType: "string",
            },
            {
                caption: "Macchine",
                dataField: "stringMacchine",
                dataType: "string",
            },
            {
                caption: "Descrizione Materiale",
                dataField: "materiale.descrizione",
                dataType: "string"
            },
            {
                caption: "Distinta Base",
                dataField: "distintaBase.codice",
                dataType: "string",
                width: 135,
            },
            {
                caption: "Q.tà",
                dataField: "pezziDaProdurre",
                dataType: "int",
                width: 50,
            },
            {
                caption: "Data consegna",
                dataField: "odV_DataConsegna",
                dataType: 'date',
                format: 'dd/MM/yyyy',
                width: 100,
            },
            {
                caption: "Data sospensione",
                dataField: "sospensione",
                dataType: 'date',
                format: 'dd/MM/yyyy',
                width: 100,
            },
            {
                caption: "",
                type: "buttons",
                width: 35,
                buttons: [{
                    text: "Lancia",
                    icon: "exportselected",
                    onClick: function (e) {
                        $scope.update(e, $scope.selectedEmployee);
                        $('#modalConfirmChangeState').modal('show');
                    },
                    visible: function (e) {
                        return e.row.data.stato == "inModifica";
                    }
                }]
            }
            ],
            onSelectionChanged: function (selectedItems) {

                $scope.selectedEmployee = selectedItems.selectedRowsData[0];
                //$scope.currentItem = selectedItems.selectedRowsData[0];
                //$scope.currentItem.id = null;
                $scope.showItemDetail(selectedItems.selectedRowsData[0], false);
            }
        };

        $scope.MainFilter = {};
        $scope.MainFilter.filterByCompanyId = null;
        $scope.MainFilter.disableFilterByCompanyId = false;

        if ($scope.$parent.globals.currentUser.azienda !== null) {
            $scope.MainFilter.filterByCompanyId = $scope.$parent.globals.currentUser.azienda.id;
            $scope.MainFilter.disableFilterByCompanyId = true;
        }

        DataFactory.getCompetenzeOperatori().then(function (response) { $scope.CompetenzeOperatori = response.data; });
        DataFactory.getMacchine().then(function (response) { $scope.Macchine = response.data; });
        DataFactory.getCompetenzeMacchina().then(function (response) { $scope.CompetenzeMacchine = response.data; });
        DataFactory.GetCausaliFase().then(function (response) { $scope.CausaliFase = response.data; });
        DataFactory.GetOperazioniCiclo().then(function (response) { $scope.OperazioniCiclo = response.data; });
        DataFactory.GetAziende().then(function (response) { $scope.listaAziende = response.data; });


        $scope.listFasi = [];
        //$scope.showItemDetail = function (item, fromRefresh) {
        $scope.filtredDatiGrafici = function (fasi, listFasi, livelloSottoFase) {
            var filtri = new Object();
            filtri.filtroDal = document.getElementById('filtroDal').value;
            filtri.filtroAl = document.getElementById('filtroAl').value;
            filtri.macchina = document.getElementById('macchina').value;
            filtri.turno = document.getElementById('turno').value;

            $scope.getDatiGrafici($scope.currentItem, true, filtri);
        }

        $scope.getListFasi = function (fasi, listFasi, livelloSottoFase) {
            var arrayNull = [];
            var preFissoNome = "";
            for (var i = 0; i < livelloSottoFase; i++)
                preFissoNome += "-";
            //preFissoNome += "&nbsp;&nbsp;&nbsp;&nbsp;";
            //preFissoNome += "&nbsp;&nbsp;&nbsp;&nbsp";
            //if (livelloSottoFase > 0)
            //preFissoNome += "|_";

            angular.forEach(fasi, function (fase, key) {
                fase.nome = preFissoNome + fase.nome;
                listFasi.push(fase);
                if (fase.fasi != null) { // controllo se sono presenti delle sottofasi
                    if (fase.fasi.length > 0) {
                        livelloSottoFase++;
                        var app = $scope.getListFasi(fase.fasi, arrayNull, livelloSottoFase); // calcolo la loro durata delle sottofasi
                        listFasi = listFasi.concat(app); // calcolo la loro durata delle sottofasi
                        livelloSottoFase--;
                    }
                }
            });

            return listFasi;
        }



        $scope.showItemDetail = function (item, fromRefresh) {

            // popolo le varie dropdown
            setTotaleTempi($scope, item);
            setRiepilogo($scope, item);
            //$scope.currentItem = new Object();
            if (item.stato != null) {
                $scope.currentStatus = item.stato;
            }
            else {
                $scope.currentStatus = ListaStatiBatch[0].value;
            }
            if (!fromRefresh) {
                basicCrud_doShowItemDetail($scope, $http, subUrlBatch, Enums, item, function () {
                    // carico le distinte basi

                    DataFactory.GetDistinteBasiFromMateriale(item.materiale.id).then(function (response) {
                        $scope.ListaDistintaBase = response.data;

                        if (item.distintaBase == null) return;
                        // carico le fasi dalla distinta base 

                        $scope.selectedDistintaBase = $scope.currentItem.distintaBase;
                        //angular.forEach($scope.ListaDistintaBase, function (db, key) {
                        //    if (db.id === item.distintaBase.id) {
                        //        $scope.selectedDistintaBase = db;
                        //    }
                        //});
                        createOrRefreshTreeView();

                    });


                    $scope.isEnableWrite = (item.stato === 'inModifica') || (item.stato === 'inAttesa') || (item.stato === 'inSospensione');

                    $scope.nodesTableArr = [];
                    if ($scope.currentItem != null) {
                        //$scope.initializeNodeTreeTable($scope.currentItem.distintaBase.fasi, $scope.nodesTableArr);
                        $scope.convertSecondToHour();
                    }
                    // ---------------------------------------------------------------------------------------
                    // Preparazione esportazione dati per gli eventi
                    if (item.distintaBase) {
                        $scope.exportData = [];
                        $scope.fileName = "Report stato Fasi";
                        $scope.exportData.push(["NUMERO FASE", "NOTA", "STATO", "INIZIO", "FINE"]);
                        angular.forEach(item.batchAvanzamenti, function (value, key) {
                            var singleItem = jQuery.grep(item.distintaBase.fasi, function (a) { return a.numeroFase == value.numeroFase; });
                            $scope.exportData.push([singleItem[0].nome + '(' + value.numeroFase + ')', value.nota, value.stato, $scope.dFormat(value.inizio, value.nota), $scope.dFormat(value.fine, value.nota)]);
                        });
                    }

                    // ---------------------------------------------------------------------------------------
                    // Preparazione dei parametri di trend per i grafici
                    $scope.exportDataTrend = [];
                    $scope.fileNameTrend = "Report parametri";
                    $scope.exportDataTrend.push(["PARAMETRO", "DATA/ORA", "VALORE"]);

                    $scope.parametersData = [];

                    angular.forEach(item.batchParametri, function (db, key) {
                        if (db.parametro.isTrend) {
                            var tTemp = {
                                "id": key,
                                "nome": db.parametro.nome,
                                "vavg": parseFloat(db.avg).toFixed(2),
                                "vmax": db.max,
                                "vmin": db.min,
                                "data": []
                            };
                            $scope.parametersData.push(tTemp);
                            angular.forEach(db.trend, function (db1, key1) {
                                tTemp.data.push({ x: db1.dataOra, y: db1.valore });
                                $scope.exportDataTrend.push([db.parametro.nome, db1.dataOra, db1.valore]);
                            });
                        }
                    });

                    if ($scope.currentItem != null) {
                        $scope.loadingGraphicPzSca($scope.currentItem.eventi)

                        // ---------------------------------------------------------------------------------------
                        // Preparazione dei grafici per  Analisi Fermi e Analisi Batch
                        $scope.changeTickIntervalGraph($scope.currentItem, 5 * 60);
                        $scope.loadingGraphic($scope.currentItem);
                        $scope.tempoTeoricoCalculate = $scope.CalculateTime($scope.currentItem.tempoTeorico);
                        if ($scope.currentItem.inizio != null && $scope.currentItem.fine != null) {
                            var tempoEffettivo = $scope.currentItem.tempoEffettivo;
                            //Il calcolo viene fatto lato server quando il batch terminato viene rimosso dalla rama.
                            //Se però è un batch che risale a prima della modifica avrà tempoEffettivo === 0 e quindi lo calcolo lato client
                            if (tempoEffettivo === 0) {
                                $scope.currentItem.batchAvanzamenti.forEach(function (ava) {
                                    var stato = ava.stato;
                                    if (stato == 'inLavorazione') {
                                        if (!(ava.turno?.id == null || ava.turno.id == '')) {
                                            var fineStr = ava.fine;
                                            if (fineStr == null || fineStr === '') {
                                                fineStr = new Date().toString();
                                            }
                                            var fine = new Date(fineStr);
                                            var inizio = new Date(ava.inizio);
                                            var secondi = (fine.getTime() - inizio.getTime()) / 1000;
                                            tempoEffettivo += secondi;
                                        }
                                    }
                                });
                            }
                            $scope.tempoEffettivoCalculate = getDurataFormattata2(Math.trunc(tempoEffettivo));
                        }
                        else
                            $scope.tempoEffettivoCalculate = "0 Secondi";

                        $scope.tempoTotaleFermiCalculate = $scope.CalculateTime($scope.currentItem.tempoTotaleFermi);

                        // ---------------------------------------------------------------------------------------
                        // Preparazione Dettaglio Fermi Odp per esportazione
                        $scope.exportDataFermi = [];
                        $scope.fileNameDataFermi = "Dettaglio_Fermi_Odp_" + $scope.currentItem.odP;
                        $scope.exportDataFermi.push(["Odp", "Fase", "Inizio", "Fine", "Causale", "Operatore Blocco", "Operatore Sblocco", "Durata (Secondi)"]);
                        //$scope.parametersData = [];
                        angular.forEach($scope.currentItem.fermi, function (fermo, key) {
                            if (fermo.causaliRiavvio.item.codice[0] != "n.d.") {
                                //mentre vengono ciclati i fermi converto il totaleTempo in una stringa del tipo ore giorni minuti secondi
                                var time = $scope.CalculateTime(fermo.totaleTempo);
                                var timeString = $scope.CalculateTimeString(time);
                                $scope.currentItem.fermi[key].timeToString = timeString;
                                $scope.exportDataFermi.push([$scope.currentItem.odP, $scope.currentItem.fasi[fermo.numeroFase - 1].distintaBaseFase.nome, fermo.inizio, fermo.fine, fermo.causaliRiavvio.item.causale, fermo.userLocked, fermo.userUnLocked, fermo.totaleTempo]);
                            }
                        });

                        var pass = true;
                        angular.forEach($scope.currentItem.batchAvanzamenti, function (batchAvanzamento, key) {
                            if (batchAvanzamento.turno.id != null) {
                                pass = true;
                                angular.forEach($scope.listaTurniBatch, function (batchAvanzamentoSalvato, key2) {
                                    if (batchAvanzamentoSalvato.id == batchAvanzamento.turno.id)
                                        pass = false;
                                });

                                if (pass)
                                    $scope.listaTurniBatch.push(batchAvanzamento.turno);
                            }

                            if (batchAvanzamento.batchFaseMacchina != null)
                                if (batchAvanzamento.batchFaseMacchina.macchina != null) {
                                    pass = true;
                                    angular.forEach($scope.listaMacchineBatch, function (batchAvanzamentoSalvato, key2) {
                                        if (batchAvanzamentoSalvato.id == batchAvanzamento.batchFaseMacchina.macchina.id)
                                            pass = false;
                                    });

                                    if (pass)
                                        $scope.listaMacchineBatch.push(batchAvanzamento.batchFaseMacchina.macchina);
                                }
                        });



                    }
                    //$scope.getListFasi = function (fasi, listFasi, livelloSottoFase) {
                    var arr = [];
                    if ($scope.currentItem != null)
                        $scope.listFasi = $scope.getListFasi($scope.currentItem.distintaBase.fasi, arr, 0);

                    $scope.getDatiGrafici($scope.currentItem, fromRefresh, null);


                    //$("#pie").dxPieChart("instance").render();
                    $scope.gaugeValue = $scope.currentItem.oee;

                    if ($scope.ViewFromDashboard) {
                        $scope.viewState = Enums.ViewState['U'];
                    }

                });

            }

            else {

                $scope.isEnableWrite = (item.stato === 'inModifica') || (item.stato === 'inAttesa');

                $scope.nodesTableArr = [];
                if ($scope.currentItem != null) {
                    //$scope.initializeNodeTreeTable($scope.currentItem.distintaBase.fasi, $scope.nodesTableArr);
                    $scope.convertSecondToHour();
                }
                // ---------------------------------------------------------------------------------------
                // Preparazione esportazione dati per gli eventi
                if (item.distintaBase) {
                    $scope.exportData = [];
                    $scope.fileName = "Report stato Fasi";
                    $scope.exportData.push(["NUMERO FASE", "NOTA", "STATO", "INIZIO", "FINE"]);
                    angular.forEach(item.batchAvanzamenti, function (value, key) {
                        var singleItem = jQuery.grep(item.distintaBase.fasi, function (a) { return a.numeroFase == value.numeroFase; });
                        $scope.exportData.push([singleItem[0].nome + '(' + value.numeroFase + ')', value.nota, value.stato, $scope.dFormat(value.inizio, value.nota), $scope.dFormat(value.fine, value.nota)]);
                    });
                }

                // ---------------------------------------------------------------------------------------
                // Preparazione dei parametri di trend per i grafici
                $scope.exportDataTrend = [];
                $scope.fileNameTrend = "Report parametri";
                $scope.exportDataTrend.push(["PARAMETRO", "DATA/ORA", "VALORE"]);

                $scope.parametersData = [];

                angular.forEach(item.batchParametri, function (db, key) {
                    if (db.parametro.isTrend) {
                        var tTemp = {
                            "id": key,
                            "nome": db.parametro.nome,
                            "vavg": parseFloat(db.avg).toFixed(2),
                            "vmax": db.max,
                            "vmin": db.min,
                            "data": []
                        };
                        $scope.parametersData.push(tTemp);
                        angular.forEach(db.trend, function (db1, key1) {
                            tTemp.data.push({ x: db1.dataOra, y: db1.valore });
                            $scope.exportDataTrend.push([db.parametro.nome, db1.dataOra, db1.valore]);
                        });
                    }
                });

                if ($scope.currentItem != null) {
                    $scope.loadingGraphicPzSca($scope.currentItem.eventi)

                    // ---------------------------------------------------------------------------------------
                    // Preparazione dei grafici per  Analisi Fermi e Analisi Batch
                    $scope.changeTickIntervalGraph($scope.currentItem, 5 * 60);
                    $scope.loadingGraphic($scope.currentItem);
                    $scope.tempoTeoricoCalculate = $scope.CalculateTime($scope.currentItem.tempoTeorico);
                    if ($scope.currentItem.inizio != null && $scope.currentItem.fine != null) {
                        dt_inizio = new Date($scope.currentItem.inizio);
                        dt_fine = new Date($scope.currentItem.fine);
                        var sec = (dt_fine.getTime() - dt_inizio.getTime()) / 1000;
                        $scope.tempoEffettivoCalculate = getDurataFormattata2(Math.trunc(sec));
                    }
                    else
                        $scope.tempoEffettivoCalculate = "0 Secondi";

                    $scope.tempoTotaleFermiCalculate = $scope.CalculateTime($scope.currentItem.tempoTotaleFermi);

                    // ---------------------------------------------------------------------------------------
                    // Preparazione Dettaglio Fermi Odp per esportazione
                    $scope.exportDataFermi = [];
                    $scope.fileNameDataFermi = "Dettaglio_Fermi_Odp_" + $scope.currentItem.odP;
                    $scope.exportDataFermi.push(["Odp", "Fase", "Inizio", "Fine", "Causale", "Operatore Blocco", "Operatore Sblocco", "Durata (Secondi)"]);
                    //$scope.parametersData = [];
                    angular.forEach($scope.currentItem.fermi, function (fermo, key) {
                        if (fermo.causaliRiavvio.item.codice[0] != "n.d.") {
                            //mentre vengono ciclati i fermi converto il totaleTempo in una stringa del tipo ore giorni minuti secondi
                            var time = $scope.CalculateTime(fermo.totaleTempo);
                            var timeString = $scope.CalculateTimeString(time);
                            $scope.currentItem.fermi[key].timeToString = timeString;
                            $scope.exportDataFermi.push([$scope.currentItem.odP, $scope.currentItem.fasi[fermo.numeroFase - 1].distintaBaseFase.nome, fermo.inizio, fermo.fine, fermo.causaliRiavvio.item.causale, fermo.userLocked, fermo.userUnLocked, fermo.totaleTempo]);
                        }
                    });

                    var pass = true;
                    angular.forEach($scope.currentItem.batchAvanzamenti, function (batchAvanzamento, key) {
                        if (batchAvanzamento.turno.id != null) {
                            pass = true;
                            angular.forEach($scope.listaTurniBatch, function (batchAvanzamentoSalvato, key2) {
                                if (batchAvanzamentoSalvato.id == batchAvanzamento.turno.id)
                                    pass = false;
                            });

                            if (pass)
                                $scope.listaTurniBatch.push(batchAvanzamento.turno);
                        }

                        if (batchAvanzamento.batchFaseMacchina != null)
                            if (batchAvanzamento.batchFaseMacchina.macchina != null) {
                                pass = true;
                                angular.forEach($scope.listaMacchineBatch, function (batchAvanzamentoSalvato, key2) {
                                    if (batchAvanzamentoSalvato.id == batchAvanzamento.batchFaseMacchina.macchina.id)
                                        pass = false;
                                });

                                if (pass)
                                    $scope.listaMacchineBatch.push(batchAvanzamento.batchFaseMacchina.macchina);
                            }
                    });
                }
                //$scope.getListFasi = function (fasi, listFasi, livelloSottoFase) {
                var arr = [];
                if ($scope.currentItem != null)
                    $scope.listFasi = $scope.getListFasi($scope.currentItem.distintaBase.fasi, arr, 0);

                $scope.getDatiGrafici($scope.currentItem, fromRefresh, null);


            }



        };

        $scope.updateFase = function (faseSelezionata) {
            $scope.AnalisiPhaseRowSelected(faseSelezionata);
        };

        //funzione che aggiorna i grafici passanto un parametro tickInterval
        $scope.changeTickIntervalGraph = function (item, secondi) {

            $scope.summary_GraphBar = [{ key: 'OEE', values: [{ x: 0, y: 0 }], color: '#0000FF' }, { key: 'OEE Rf', values: [{ x: 0, y: 0 }], color: '#009933' }];
            //eseguo una chiamata per scaricare i dati dei grafici
            DataFactory.GetBatchGraphValues(secondi, item.id).then(function (response) {
                //se vuoto return
                if (response.data.length === 0) return;
                //ciclo gli array e viene inserito nell'array del grafico
                angular.forEach(response.data[0].values, function (data, key1) {
                    if (data.value != null)
                        $scope.summary_GraphBar[0].values.push({ x: data.at, y: data.value });
                });
                angular.forEach(response.data[1].values, function (data, key1) {
                    if (data.value != null)
                        $scope.summary_GraphBar[1].values.push({ x: data.at, y: data.value });
                });

            });
        };


        $scope.loadingGraphic = function (item) {

            // vengono filtrati solo i fermi effettivamente presenti nel batch
            var listOfUniqueFermi = [];
            var totFermi = 0;
            angular.forEach(item.fermi, function (casuale, key1) {
                if (!casuale.causaliRiavvio)
                    casuale.causaliRiavvio = { item: { codice: ["n.d."] } };

                var itemFounded = listOfUniqueFermi.filter(o => o.codice === casuale.causaliRiavvio.item.codice);
                if (itemFounded.length == 0)
                    if (casuale.causaliRiavvio != null)
                        listOfUniqueFermi.push(casuale.causaliRiavvio.item)

                totFermi += casuale.totaleTempo;
            });

            //----------------------------------------------------------------------------------------
            //ciclo per tutte le casuali scaricate e filtrate per riavvio
            //angular.forEach($scope.currentItem.fermi, function (causual, key1) {
            //    var time = $scope.CalculateTime(causual.totaleTempo);
            //    var timeString = $scope.CalculateTimeString(time);
            //    $scope.currentItem.fermi[key1].timeToString = timeString
            //});

            //inizializzo le variabili dei grafici
            $scope.fermiSummary = [];
            $scope.fermiGraphBar = [{ key: "ISTOGRAMMA (TEMPO)", color: '#1f77b4', values: [] }];
            $scope.fermiGraphPie = [];
            // Preparazione dei fermi macchina per esportazione 
            $scope.exportDataSummaryFermi = [];
            $scope.fileNameSummary = "Riepilogo_Fermi_Odp_" + $scope.currentItem.odP;
            $scope.exportDataSummaryFermi.push(["Causale Fermo", "Numero Fermi", "Durata Totale", "Percentuale"]);

            angular.forEach(listOfUniqueFermi, function (causuale, key1) {
                //se non sono presenti fermi return
                if (!item.fermi)
                    return;

                //scarica tutti i fermi giltrati per casuale
                var dataFilterCode = item.fermi.filter(o => o.causaliRiavvio.item.codice === causuale.codice);
                //inizializzo due variabili di appoggio
                var totaleFermiPercausale = 0;
                var totaleTempoFermoPerCausale = 0;

                //per ogni fermo incremento la variabile numerofermi e sommo il tempo di fermata 
                angular.forEach(dataFilterCode, function (causual, key1) {
                    totaleFermiPercausale++;
                    totaleTempoFermoPerCausale += causual.totaleTempo;
                });

                //calcolo la percentuale
                var totalePercentuale = Math.round((totaleTempoFermoPerCausale * 100) / totFermi);

                //se sono presente i dati 
                if (dataFilterCode.length != 0) {
                    //passo alla funzione i secondi totali e mi ritorna un oggetto .day,.hh,.mm,.ss
                    var time = $scope.CalculateTime(totaleTempoFermoPerCausale);
                    //aggiorno un array che servirà per popolare la table e in caso di export to exel
                    $scope.fermiSummary.push({ codiceCausale: causuale.codice, nomeCausale: causuale.causale, totaleFermi: totaleFermiPercausale, totaleTempoSecondi: totaleTempoFermoPerCausale, totaleTempo: time, percentuale: totalePercentuale });
                    //viene aggiunto il fermo all'istogramma con casuale,  e tempo totale per casuale
                    $scope.fermiGraphBar[0].values.push({ "label": causuale.causale, "value": totaleTempoFermoPerCausale });
                    //viene aggiunto al grafico a torta casuale e numero di fermate per casuale
                    $scope.fermiGraphPie.push({ key: causuale.causale, y: totaleFermiPercausale });
                    //aggiungo al file per l'esportazione
                    $scope.exportDataSummaryFermi.push([causuale.causale, totaleFermiPercausale, totaleTempoFermoPerCausale, totalePercentuale + " %"]);
                }
            });

            //$scope.fermiSummary = $scope.fermiSummary.OrderBy(o => o.totalePercentuale).ToList();
            //$scope.initializeGraphBar();
        };

        //utility in ingresso riceve i secondi in uscita restituisce un oggetto di tipo .day,.hh,.mm,.ss
        $scope.CalculateTime = function (secondi) {
            $scope.time = [];
            var seconds = secondi
            var minutes = Math.floor(seconds / 60);
            var hours = Math.floor(minutes / 60);
            var days = Math.floor(hours / 24);

            hours = hours - (days * 24);
            minutes = minutes - (days * 24 * 60) - (hours * 60);
            seconds = seconds - (days * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);

            //aggiorno le variabili
            $scope.time.day = days;
            $scope.time.hh = hours;
            $scope.time.mm = minutes;
            $scope.time.ss = seconds;
            return $scope.time;
        };
        //utility in ingresso un oggetto di tipo .day,.hh,.mm,.ss in uscito una stringa con N ore N minuti N secondi...
        $scope.CalculateTimeString = function (time) {

            var timeString = "";
            if (time.day === 1)
                timeString = timeString + time.day + " Giorno, ";
            if (time.day > 1)
                timeString = timeString + time.day + " Giorni, ";
            if (time.hh === 1)
                timeString = timeString + time.hh + " Ora, ";
            if (time.hh > 1)
                timeString = timeString + time.hh + " Ore, ";
            if (time.mm === 1)
                timeString = timeString + time.mm + " Minuto, ";
            if (time.mm > 1)
                timeString = timeString + time.mm + " Minuti, ";
            if (time.ss === 1)
                timeString = timeString + time.ss + " Secondo, ";
            if (time.ss > 1)
                timeString = timeString + time.ss + " Secondi, ";

            len = timeString.length
            timeString = timeString.substring(0, len - 2)
            return timeString;
        };

        $scope.reload = function () {
            //basicCrud_doReload($scope, $http, subUrlBatch, filter);
            if ($scope.ViewFromDashboard) {
                if ($scope.fromDashboardOdp !== null) {
                    DataFactory.GetBatchFromOdp($scope.fromDashboardOdp).then(function (response) {
                        $scope.update(null, response.data);
                    });
                }
            } else {
                DataFactory.GetListBatchFiltered($scope.InitFilter).then(function (response) {
                    $scope.items = response.data;

                    $("#gridContainer").dxDataGrid("option", "dataSource", $scope.items);

                    function getOrderDay(rowData) {
                        return (new Date(rowData.OrderDate)).getDay();
                    }
                });
            }


        };

        $scope.customizeTooltip = function (pointsInfo) {
            return { text: parseInt(pointsInfo.originalValue) + "%" };
        };

        $scope.insert = function () {
            $scope.viewState = Enums.ViewState['C'];
            $scope.isBatchAutoCounterEnabled = false;
            $scope.currentItem = new Object();
            $scope.isEnableWrite = true;
            $scope.currentStatus = "inModifica";
            $scope.currentItem.azienda = {};
            $scope.CausaliFase = { 0: { id: 0, value: "Tempi" } };

            if ($scope.MainFilter.disableFilterByCompanyId) {
                $scope.currentItem.azienda.id = $scope.MainFilter.filterByCompanyId;
            }

            // autocaricamento del materiale se ne è presente uno solo
            if ($scope.ListaMateriali.length == 1) {
                $scope.currentItem.materiale = $scope.ListaMateriali[0];
                DataFactory.GetDistinteBasiFromMateriale($scope.currentItem.materiale.id).then(function (response) {
                    $scope.ListaDistintaBase = response.data;
                    if ($scope.ListaDistintaBase.length == 1) {
                        $scope.currentItem.distintaBase = $scope.ListaDistintaBase[0];
                    }
                });
            }

            // verifica dell'auto-contatore
            DataFactory.GetBatchAutoCounterInfo().then(function (response) {
                $scope.isBatchAutoCounterEnabled = response.data.isEnabled;
                if (response.data.isEnabled) {
                    $scope.currentItem.odP = response.data.global;
                    $scope.currentItem.odPParziale = response.data.partial;
                }

            });
            ResetAllFields();
        };


        // ---------------------------------------------------------------------------
        // Andrea.g, 09/09/2022
        // Logica di gestione del pulsante Import e relativo conto alla rovescia.
        $scope.import = function () {
            DataFactory.GetBatchImportManual($rootScope.globals.currentUser.username).then(function (response) {
                if (response.data == true) {
                    count = 30;
                    $scope.btnImportDowncounterValue = "";
                    $scope.btnImportIsDisabled = true;
                    var interval = setInterval(function () {
                        $scope.$apply(function () {
                            count--;
                            $scope.btnImportDowncounterValue = "(" + count + "sec)";
                            if (count == 0) {
                                $scope.btnImportIsDisabled = false;
                                $scope.btnImportDowncounterValue = "";
                                clearInterval(interval);
                                $scope.reload();
                                $("#gridContainer").dxDataGrid("instance").refresh();
                            }
                        });

                    }, 1000);
                };// ---------------------------------------------------------------------------
            });
        }

        $scope.isOdpSelected = function (item) {
            return ($scope.currentItem != null && $scope.currentItem.odP == item.odP)
        }

        $scope.update = function (event, item) {
            if (item.id != undefined) {
                $scope.showItemDetail(item, false);
                if ($scope.currentItem != null && $scope.currentItem.odP == item.odP) {
                    $scope.viewState = Enums.ViewState['U'];
                }
            }

        };

        $scope.cancel = function () {
            $scope.viewState = Enums.ViewState['R']; ResetAllFields(); closeModal('#modalConfirmUndo');

        };

        function basicCrud_doConfirmProd($scope, $http, subUrlApi, Enums) {


        }



        $scope.pause = async function () {
            document.getElementsByClassName("loader")[0].style.display = "block";

            //if ($scope.currentItem.stato == "inAttesa")
            //    $scope.currentItem.statoPrecedente = $scope.currentItem.stato;
            $scope.currentItem.stato = "InSospensione";
            //basicCrud_doConfirm($scope, $http, subUrlBatch, Enums);

            let myPromise = new Promise((resolve, reject) => {
                //document.getElementsByClassName("loader")[0].style.display = "block";
                $http.post(urlApi + '/' + subUrlBatch, $scope.currentItem)
                    .then(function (data) {
                        if ($scope.viewState === Enums.ViewState['C']) {
                            $scope.currentItem = data.data;
                            $scope.reload();
                        }
                        else if ($scope.viewState === Enums.ViewState['U']) {
                            var elementPos = $scope.items.map(function (x) { return x.id; }).indexOf($scope.currentItem.id);
                            $scope.items[elementPos] = $scope.currentItem;
                        }

                        resolve();
                    }, function (e) {
                        reject(e);
                    });
            })
            myPromise.then(function (a) {
                document.getElementsByClassName("loader")[0].style.display = "none";
                closeModal('#modalConfirmPause');
                $("#modalConfirmPauseEnd").modal();
                $scope.reload();
            },
                function (error) {
                    $scope.crudException(error);
                    closeModal('#modalConfirmPause');
                    $("#modalConfirmPauseEnd").modal();
                }
            );
        };

        $scope.reloadInterface = function () {
            $scope.viewState = Enums.ViewState['R'];
        };

        $scope.restart = function () {
            document.getElementsByClassName("loader")[0].style.display = "block";
            //$scope.currentItem.statoPrecedente = "InSospensione";
            $scope.currentItem.stato = "InSospensioneInRipresa";

            let myPromise = new Promise((resolve, reject) => {
                //document.getElementsByClassName("loader")[0].style.display = "block";
                $http.post(urlApi + '/' + subUrlBatch, $scope.currentItem)
                    .then(function (data) {
                        if ($scope.viewState === Enums.ViewState['C']) {
                            $scope.currentItem = data.data;
                            $scope.reload();
                        }
                        else if ($scope.viewState === Enums.ViewState['U']) {
                            var elementPos = $scope.items.map(function (x) { return x.id; }).indexOf($scope.currentItem.id);
                            $scope.items[elementPos] = $scope.currentItem;
                        }

                        resolve();
                    }, function (e) {
                        reject(e);
                    });
            })
            myPromise.then(function (a) {
                document.getElementsByClassName("loader")[0].style.display = "none";
                closeModal('#modalConfirmRestart');
                $("#modalConfirmRestartEnd").modal();
                $scope.reload();
            },
                function (error) {
                    $scope.crudException(error);
                    closeModal('#modalConfirmRestart');
                    $("#modalConfirmRestartEnd").modal();
                }
            );

            //basicCrud_doConfirm($scope, $http, subUrlBatch, Enums);
            //closeModal('#modalConfirmRestart');
            //setTimeout(function () {
            //    $scope.reload();
            //}, 800);
        };

        $scope.confirm = function () {
            basicCrud_doConfirmWithOutReload($scope, $http, subUrlBatch, Enums);
            $("#gridContainer").dxDataGrid("option", "dataSource", $scope.items);
        };

        $scope.refresh = function () {
            document.getElementById('filtroDal').value = "";
            document.getElementById('filtroAl').value = "";
            document.getElementById('macchina').value = "";
            document.getElementById('turno').value = "";
            var objTemp = { id: $scope.currentItem.id };
            $scope.currentItem = { id: "reloading", Codice: "reloading" };
            basicCrud_doShowItemDetail($scope, $http, subUrlBatch, Enums, objTemp, function (item) {
                $scope.currentItem = item;
                $scope.showItemDetail($scope.currentItem, true);

                $scope.viewState = Enums.ViewState['U'];

                angular.forEach($scope.trcurrentItem, function (it) {
                    $scope.trshowItemDetail(it);
                })
                $scope.refreshGrafici();
            });

            $("#gridContainer").dxDataGrid("option", "dataSource", $scope.items);
        }

        $scope.refreshGrafici = function () {
            setTimeout(function () {
                $("#chart_istogramma_fasi_raggruppamenti").dxChart("instance").render();
                $("#chart_fasi_tempi").dxChart("instance").render();
                $("#pieFermi").dxPieChart("instance").render();
                //$("#pieScarti").dxPieChart("instance").render();
                $("#istogrammaFermi").dxChart("instance").render();
                //$("#pie").dxPieChart("instance").render();
                //$("#gauge").dxChart("instance").render();
                // $("#gauge").dxChart("option", "dataSource", $scope.currentItem.oee);
                // $scope.gauge.render()

                //var chartInstance = $("#gauge").dxChart("instance");
                //var allSeries = chartInstance.getAllSeries();
                //var fruitsSeries = chartInstance.getSeriesByName("fruits");

            }, 150);
        };

        $scope.confirmAndClose = function () {

            basicCrud_doConfirm($scope, $http, subUrlBatch, Enums);
            $("#gridContainer").dxDataGrid("option", "dataSource", $scope.items);
        };

        $scope.crudException = function (err) {
            $scope.exceptionMessageText = err.data.exceptionMessage;
            angular.element(document.querySelector('#modalWarning1')).modal('toggle');
        };

        $scope.delete = function (item) { $scope.viewState = Enums.ViewState['D']; basicCrud_doDelete($scope, $http, subUrlBatch, Enums) };

        $scope.title = function () { basicCrud_doTitle($scope, Enums); };

        basicCrud_doInitWatch($scope);

        $scope.reload();

        // funzioni che rendono mutuamente esclusive le drop down
        $scope.selectionMacchine = function (id, fase) {
            $scope.currentItem.distintaBase.fasi.find(x => x.numeroFase == fase).macchine[0].macchina = { id: $scope.modelsSelMacchine[id], collection: null, item: null };
            $scope.isEnabledcompentenzaMacchineSelection[id] = ($scope.modelsSelMacchine[id] == -1);

        }

        $scope.selectionCompetenzaMacchine = function (id) {
            $scope.isEnabledMacchinaSelection[id] = ($scope.modelsSelCompetenzaMacchine[id] == -1);
        }

        // carico le distinte basi a seconda del materiale in uscita
        $scope.loadDistintaBaseFromMateriale = function (id) {
            $scope.ListaDistintaBase = null;
            DataFactory.GetDistinteBasiFromMateriale(id).then(function (response) {
                $scope.ListaDistintaBase = response.data;

            });
        }

        // funzione per cambiare le fasi di una distinta base
        $scope.changeDistintaBase = function (id) {

            ResetAllFields();

            if (id == -1) {
                $scope.selectedDistintaBase = null;
                $scope.currentItem.distintaBase = null;
            }
            else
                angular.forEach($scope.ListaDistintaBase, function (db, key) {
                    if (db.id === id) {
                        $scope.selectedDistintaBase = db;
                        $scope.currentItem.distintaBase = db;
                    }
                });
            $scope.calculateTheoreticalTime();
            createOrRefreshTreeView();
        }


        // --------------------------------------------------------------------------------------------------------
        // Funzione per calcolare il tempo teorico di produzione per ogni fase
        // sulla base dei pezzi da produrre.
        // Se overrideValue = 1, allora viene forzato il valore di "TempoTeoricoSec" per ogni fase.
        // --------------------------------------------------------------------------------------------------------
        function getTempoTeoricoSecRealTime(fasi, pezziDaProdurre, timeTotalSecond, overrideValue) {

            angular.forEach(fasi, function (fase, key) {

                var incValue = 1;
                if (fase.isCiclica === true)
                    incValue = pezziDaProdurre;

                timeTotalSecond += (fase.macchine && fase.macchine.length == 1 ? fase.macchine[0].tempo * incValue : 0);
                timeTotalSecond += (fase.manodopera && fase.manodopera.length == 1 ? fase.manodopera[0].tempo * incValue : 0);

                if (overrideValue)
                    fasi[key].TempoTeoricoSec = timeTotalSecond;

                if (fase.fasi.length > 0) { // controllo se sono presenti delle sottofasi
                    timeTotalSecond += getTempoTeoricoSecRealTime(fase.fasi, pezziDaProdurre, 0, overrideValue); // calcolo la loro durata delle sottofasi
                }
            });

            return timeTotalSecond;
        }
        // --------------------------------------------------------------------------------------------------------

        //// ------------------------------------------------------------------------------------
        ////funzione per calcolare ricorsivamente il tempo teorico di durata di una fase e delle sue sottofasi
        //function getTempoTeoricoSecFasi(fasi) {
        //    angular.forEach(fasi, function (fase, key) {
        //        // calcolo il tempo teorico della fase
        //        var tempoTeoricoSec = 0;
        //        if (fase.isCiclica)
        //            tempoTeoricoSec = (fase.macchine[0].tempo + fase.manodopera[0].tempo) * $scope.currentItem.pezziDaProdurre;
        //        else
        //            tempoTeoricoSec = fase.macchine[0].tempo + fase.manodopera[0].tempo;

        //        fasi[key].TempoTeoricoSec = tempoTeoricoSec;
        //        if (fase.fasi.length > 0) { // controllo se sono presenti delle sottofasi
        //            fase.fasi = getTempoTeoricoSecFasi(fase.fasi); // calcolo la loro durata delle sottofasi
        //        }

        //    });

        //    return fasi;
        //}

        // funzione per cambiare il tempo teorico 
        $scope.calculateTheoreticalTime = function () {
            if ($scope.currentItem.distintaBase != null)
                $scope.currentItem.tempoTeorico = getTempoTeoricoSecRealTime($scope.currentItem.distintaBase.fasi, $scope.currentItem.pezziDaProdurre, 0, 0);
            $scope.tothh = Math.floor($scope.currentItem.tempoTeorico / 3600);
            $scope.totmm = Math.floor(($scope.currentItem.tempoTeorico - ($scope.tothh * 3600)) / 60);
            $scope.totss = Math.floor(($scope.currentItem.tempoTeorico - ($scope.tothh * 3600) - ($scope.totmm * 60)));
            $scope.convertSecondToHour();
        }

        // funzione per cambiare il tempo teorico 
        $scope.convertSecondToHour = function () {

            //aggiorno le variabili
            $scope.theoreticalTime.day = 0;
            $scope.theoreticalTime.hh = 0;
            $scope.theoreticalTime.mm = 0;
            $scope.theoreticalTime.ss = 0;

            var seconds = $scope.currentItem.tempoTeorico;
            var minutes = Math.floor(seconds / 60);
            var hours = Math.floor(minutes / 60);
            var days = Math.floor(hours / 24);

            hours = hours - (days * 24);
            minutes = minutes - (days * 24 * 60) - (hours * 60);
            seconds = seconds - (days * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);

            //aggiorno le variabili
            $scope.theoreticalTime.day = days;
            $scope.theoreticalTime.hh = hours;
            $scope.theoreticalTime.mm = minutes;
            $scope.theoreticalTime.ss = seconds;

        };

        // funzione per lanciare la produzione
        $scope.launch = function () {
            $scope.viewState = Enums.ViewState['C'];
            //item.stato = "1";
            $scope.currentItem.stato = "1";
            //$scope.currentItem.userLocked = $rootScope.globals.currentUser.username;

            basicCrud_doConfirmWithForcedReload($scope, $http, subUrlBatch, Enums);

            // viene associato, ad ogni fase, il tempo teorico di produzione
            getTempoTeoricoSecRealTime($scope.currentItem.distintaBase.fasi, $scope.currentItem.pezziDaProdurre, 0, 1);

            //$scope.currentItem.distintaBase.fasi = getTempoTeoricoSecFasi($scope.currentItem.distintaBase.fasi)
            angular.element(document.querySelector('#modalConfirmChangeState')).modal('hide');
        }

        // opzioni per l'albero
        $scope.treeOptions = {
            nodeChildren: "fasi",
            dirSelectable: true,
            injectClasses: {
                ul: "a1",
                li: "a2",
                liSelected: "a7",
                iExpanded: "a3",
                iCollapsed: "a4",
                iLeaf: "a5",
                label: "a6",
                labelSelected: "a8"
            }
        }

        // nodo base per il treeview
        $scope.dataForTheTree =
            [
                { "numeroFase": "", "nome": "Elenco fasi", "id": "0", "type": "-1", "fasi": [] }
            ];

        $scope.selectedNode = null;

        // quando ho selezionato un ramo
        $scope.showSelected = function (sel) {

            // inizializzazione dei models
            ResetDistintaCicli();

            if (sel.id != 0) {
                $scope.selectedNode = sel;
                $scope.numeroFase = sel.numeroFase;
                $scope.nomeFase = sel.nome;
                $scope.descrizioneFase = sel.descrizione;
                $scope.type = sel.type;

                $scope.IsVisibleFaseCiclica = (sel.type == "0");

                // array per abilitazione macchine e competenze macchine
                $scope.isEnabledMacchinaSelection = {};
                $scope.isEnabledcompentenzaMacchineSelection = {};

                // abilitazione pannello dettagli fase
                $scope.isEnabledDettagliFase = true;

                // abilitazione checkbox di collegamento all'operazione ciclo
                $scope.isEnabledLinkedOption = false;

                fillPhaseItems($scope.selectedDistintaBase.fasi, sel.id)
            }
        };

        function fillPhaseItems(objRoot, phaseId) {  // $scope.currentItem.fasi, tmpDistintaBaseFase
            var arrFasi = objRoot;
            var objRoot = objRoot;
            angular.forEach(arrFasi, function (fase, key) {
                if (fase.id == phaseId) {
                    fillItemsInView(objRoot[key])
                    return;
                }
                if (objRoot[key].fasi != null)
                    fillPhaseItems(objRoot[key].fasi, phaseId);
            })
        }

        //--------------------------------------------------------------------------------------
        // Funzione che ritorna il tempo formattato nel seguente modo: x Ore y Minuti z Secondi
        //--------------------------------------------------------------------------------------
        function getDurataFormattata2(sec) {
            if (sec != null && sec != "") {
                var hh = Math.trunc(sec / 3600);
                var mm = Math.trunc((sec - (hh * 3600)) / 60);
                var ss = Math.trunc(sec - (hh * 3600) - (mm * 60));
                var label_ore = "Ora";
                var label_minuti = "Minuto";
                var label_secondi = "Secondo";

                var str_return = "";

                if (hh != 0) {
                    if (hh > 1)
                        label_ore = "Ore";
                    str_return += hh + " " + label_ore;
                }

                if (mm != 0) {
                    if (mm > 1)
                        label_minuti = "Minuti";
                    str_return += " " + mm + " " + label_minuti;
                }

                if (ss != 0) {
                    if (ss > 1)
                        label_secondi = "Secondi";
                    str_return += " " + ss + " " + label_secondi;
                }

                return str_return;
            }
            else
                return "0 Secondi";
        }

        function addToAllNodes(array, flatArray) {
            flatArray = flatArray || [];
            //if (!array.fasi || !array.fasi.length) {
            //    return;
            //}
            array.forEach(function (node) {
                flatArray.push(node);
                addToAllNodes(node.fasi, flatArray);
            });
            return flatArray;
        };

        // aggiornamento dell'albero
        function createOrRefreshTreeView() {
            $scope.dataForTheTree[0].fasi = [];
            if (($scope.selectedDistintaBase != null) &&
                ($scope.selectedDistintaBase.fasi != null)) {
                var arrayOrdered = orderById($scope.selectedDistintaBase.fasi, "numeroFase", false);
                $scope.dataForTheTree[0].fasi = arrayOrdered;
            }
            //$scope.defaultExpanded = addToAllNodes($scope.dataForTheTree);
            //$scope.treeOptions.defaultExpanded = $scope.defaultExpanded ;
            //$scope.defaultExpanded = [$scope.dataForTheTree[0], $scope.dataForTheTree[0].fasi[0],  $scope.dataForTheTree[0].fasi[1]];
        };

        //function defaultExpanded() {
        //    $scope.expandedNodes = [$scope.dataForTheTree[0]];
        //};


        //funzione per popolare gli elementi della finestra di dettaglio quando seleziono un ramo dal treeview (tempi) o seleziono la voce dalla ddl delle operazioni ciclo
        function fillItemsInView(item) {
            angular.forEach($scope.CausaliFase, function (obj, key) {

                $scope.isEnabledMacchinaSelection[obj.id] = true;
                $scope.isEnabledcompentenzaMacchineSelection[obj.id] = true;

                // valore selezionato per le competenze operatore
                if (item.manodopera[obj.id].operatoreCompetenza != null) {
                    $scope.modelsSelCompetenzaOperatore[obj.id] = item.manodopera[obj.id].operatoreCompetenza.id;
                }

                // recupero il tempo dell'operatore per ogni causale e popolo i vari input text
                var tmpDoubleOperatore = item.manodopera[obj.id].tempo;
                $scope.modelsOreUomo[obj.id] = parseInt(tmpDoubleOperatore / 3600);
                $scope.modelsMinutiUomo[obj.id] = parseInt((tmpDoubleOperatore % 3600) / 60);
                $scope.modelsSecondiUomo[obj.id] = parseInt(tmpDoubleOperatore % 60);



                // popolo le drop down list con il valore selezionato
                if (item.macchine[obj.id].macchina == null) {
                    $scope.modelsSelMacchine[obj.id] = -1;
                    $scope.isEnabledMacchinaSelection[obj.id] = false;
                }
                else
                    $scope.modelsSelMacchine[obj.id] = item.macchine[obj.id].macchina.id;


                if (item.macchine[obj.id].macchinaCompetenza == null) {
                    $scope.modelsSelCompetenzaMacchine[obj.id] = -1;
                    $scope.isEnabledcompentenzaMacchineSelection[obj.id] = false;
                }
                else
                    $scope.modelsSelCompetenzaMacchine[obj.id] = item.macchine[obj.id].macchinaCompetenza.id;

                // caso in cui ho a null tutti i gli oggetti macchine e competenze macchinari
                if (item.macchine[obj.id].macchina == null && item.macchine[obj.id].macchinaCompetenza == null)
                    $scope.isEnabledMacchinaSelection[obj.id] = true;

                // recupero il tempo della macchina per ogni causale e popolo i vari input text
                var tmpDoubleMacchina = item.macchine[obj.id].tempo;
                $scope.modelsOreMacchina[obj.id] = parseInt(tmpDoubleMacchina / 3600);
                $scope.modelsMinutiMacchina[obj.id] = parseInt((tmpDoubleMacchina % 3600) / 60);
                $scope.modelsSecondiMacchina[obj.id] = parseInt(tmpDoubleMacchina % 60);


                // calcolo il tempo teorico della fase
                // se è ciclica va moltiplicato per la quantità
                if (item.isCiclica) {
                    var tmpDoubleMacchinaApp = tmpDoubleMacchina * parseInt($scope.currentItem.pezziDaProdurre);
                    var tmpDoubleOperatoreApp = item.manodopera[obj.id].tempo * parseInt($scope.currentItem.pezziDaProdurre);
                    $scope.modelsOreMacchinaTotale[obj.id] = parseInt(tmpDoubleMacchinaApp / 3600);
                    $scope.modelsMinutiMacchinaTotale[obj.id] = parseInt((tmpDoubleMacchinaApp % 3600) / 60);
                    $scope.modelsSecondiMacchinaTotale[obj.id] = parseInt(tmpDoubleMacchinaApp % 60);
                    $scope.modelsOreUomoTotale[obj.id] = parseInt(tmpDoubleOperatoreApp / 3600);
                    $scope.modelsMinutiUomoTotale[obj.id] = parseInt((tmpDoubleOperatoreApp % 3600) / 60);
                    $scope.modelsSecondiUomoTotale[obj.id] = parseInt(tmpDoubleOperatoreApp % 60);
                }
                else {
                    $scope.modelsOreMacchinaTotale[obj.id] = $scope.modelsOreMacchina[obj.id];
                    $scope.modelsMinutiMacchinaTotale[obj.id] = $scope.modelsMinutiMacchina[obj.id];
                    $scope.modelsSecondiMacchinaTotale[obj.id] = $scope.modelsSecondiMacchina[obj.id];
                    $scope.modelsOreUomoTotale[obj.id] = $scope.modelsOreUomo[obj.id];
                    $scope.modelsMinutiUomoTotale[obj.id] = $scope.modelsMinutiUomo[obj.id];
                    $scope.modelsSecondiUomoTotale[obj.id] = $scope.modelsSecondiUomo[obj.id];
                }
            });
        }

        // ordinamento di json per campo richiesto
        function orderById(items, field, reverse) {
            var filtered = [];
            angular.forEach(items, function (item) {
                filtered.push(item);
            });
            filtered.sort(function (a, b) {
                return (a[field] > b[field] ? 1 : -1);
            });
            if (reverse) filtered.reverse();
            return filtered;
        };

        // funzione per resettare tutti i campi
        function ResetAllFields() {

            ResetDistintaCicli();

            $scope.pcurrentItem = null;
            $scope.parametersData = null;
            $scope.nodesTableArr = null;
            $scope.AnalisiData = null;
            $scope.trcurrentItem = [];
            $scope.data_multiBarChart = [{ key: '', values: [{ x: 0, y: 0 }] }];
            $scope.summary_GraphBar = [{ key: '', values: [{ x: 0, y: 0 }] }, { key: '', values: [{ x: 0, y: 0 }] }];
            $scope.tempoTeoricoCalculate = "";;
            $scope.tempoEffettivoCalculate = "";
            $scope.tempoTotaleFermiCalculate = "";
            //$scope.fermiSummary = [];
            //$scope.fermiGraphBar[0].values = [];
            //$scope.fermiGraphBar = [{ key: "ISTOGRAMMA (TEMPO)", color: '#1f77b4', values: [] }];
            //$scope.fermiGraphPie = [];
        }

        function ResetDistintaCicli() {
            $scope.modelsOreUomo = {};
            $scope.modelsMinutiUomo = {};
            $scope.modelsSecondiUomo = {};
            $scope.modelsOreUomoTotale = {};
            $scope.modelsMinutiUomoTotale = {};
            $scope.modelsSecondiUomoTotale = {};
            $scope.modelsSelCompetenzaOperatore = {};
            $scope.modelsOreMacchinaTotale = {};
            $scope.modelsMinutiMacchinaTotale = {};
            $scope.modelsSecondiMacchinaTotale = {};
            $scope.modelsOreMacchina = {};
            $scope.modelsMinutiMacchina = {};
            $scope.modelsSecondiMacchina = {};
            $scope.modelsSelCompetenzaMacchine = {};
            $scope.modelsSelMacchine = {};

            $scope.modelOperazioniCiclo = {};
            $scope.numeroFase = null;
            $scope.nomeFase = null;
            $scope.descrizioneFase = null;

            $scope.isEnabledDettagliFase = null;

        }

        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        // Definizione del filtro di visualizzazione in griglia
        function filter() {
            if ($scope.InitFilter) {

                $scope.items = $.grep($scope.items, function (e) {

                    var matchCriteria = false;

                    if (($scope.MainFilter === null) ||
                        ($scope.MainFilter.filterByCompanyId === null) ||
                        ($scope.MainFilter.filterByCompanyId === "")) {
                        matchCriteria = true; // caso in cui non sia stata selezionata alcuna voce di tendina Azienda
                    }
                    else if ((e.azienda !== null) && (e.azienda.id !== null) && (e.azienda.id === $scope.MainFilter.filterByCompanyId))
                        matchCriteria = true;


                    matchCriteria = matchCriteria && e.stato.startsWith($scope.InitFilter);

                    return matchCriteria;
                });
            }
        }

        // Viene verificato lo stato della produzione corrente
        $scope.IsOnLavorazione = function () {

            if ($scope.currentItem == null) return false;
            return (($scope.currentItem.stato === 'inLavorazioneInEmergenza')
                || ($scope.currentItem.stato === 'inLavorazione')
                || ($scope.currentItem.stato === 'inLavorazioneInPausa'))
        }

        // Viene verificato lo stato della produzione corrente
        $scope.IsOnTerminato = function () {

            if ($scope.currentItem == null) return false;
            return (($scope.currentItem.stato === 'terminato')
                || ($scope.currentItem.stato === 'terminatoAbortito'))
        }

        // Metodo per richiamare il server e abortire l'ordine corrente
        $scope.abort = function () {
            if ($scope.currentItem == null) return;

            $scope.currentItem.stato = 'terminatoAbortito';
            this.confirmAndClose();
        }

        //------------------------------------------------------------------------------------------------------------------------------
        // produzione-parametri
        //------------------------------------------------------------------------------------------------------------------------------
        $scope.pdtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('stateSave', true)
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);


        // Combo box Tipi Dato
        $scope.cbTipiDato = null;
        DataFactory.GetTipiDato().then(function (response) {
            var xs = response.data;
            angular.forEach(xs, function (x) {
                //x.value = x.value.toLowerCase();
                x.value = x.value.charAt(0).toLowerCase() + x.value.slice(1);
            })
            $scope.cbTipiDato = xs;
        });

        //----------------------


        $scope.pIsFullScreenOnEdit = false;
        //$scope.pitems = [];
        $scope.pcurrentItem = null;
        $scope.pviewState = Enums.ViewState['R'];
        $scope.ptitolo = "Dettaglio";
        $scope.pdtInstance = {};
        $scope.originalPCurrentItem = [];

        $scope.ptitle = function () { pdoTitle($scope, Enums); };
        function pdoTitle($scope, Enums) {
            switch ($scope.pviewState) {
                case Enums.ViewState['R']:
                    $scope.ptitolo = "Dettaglio ";// + $scope.currentItem.codice;
                    break;
                case Enums.ViewState['U']:
                    $scope.ptitolo = "Modifica ";// + $scope.currentItem.codice;
                    break;
                case Enums.ViewState['C']:
                    $scope.ptitolo = "Inserimento ";
                    break;
            }
        };


        $scope.pinsert = function () {
            $scope.PlistaValori = [];
            $scope.pviewState = Enums.ViewState['C'];
            $scope.pcurrentItem = new Object();
            $scope.pcurrentItem.tipologia = new Object();
            $scope.originalPCurrentItem = [];
        };

        $scope.pupdate = function () {
            if (!$scope.pcurrentItem.visibilita || $scope.currentStatus != "inModifica") return;

            // 1. cercare nella distinta base il parametro che ha lo stesso codice del parametro selezionato (= $scope.pcurrentItem).
            // 2. dopo aver trovato il parametro, prelevare il valore di default
            // 3. effettuare lo split del valore di default usando come carattere di split ","
            // 4. popolare l'array di $scope.listaValori con gli elementi splittati
            $scope.pviewState = Enums.ViewState['U'];

            $scope.originalPCurrentItem.push(angular.copy($scope.pcurrentItem));

            if ($scope.pcurrentItem.tipo == "list") {
                $scope.PlistaValori = [];

                var stringa = $.grep($scope.currentItem.distintaBase.distintaBaseParametri,
                    function (o) { return o.codice == $scope.pcurrentItem.codice });

                if (stringa[0].valoreDefault.length <= 1) return;

                $scope.PlistaValori = ["---x---"];
                angular.forEach(stringa[0].valoreDefault.split("\n"), function (item) { $scope.PlistaValori.push(item); });

            }//fine if

        };

        $scope.pcancel = function () {
            $scope.pviewState = Enums.ViewState['R'];
            var itemToUndo = null;
            var allParams = $scope.currentItem.distintaBase.distintaBaseParametri;

            for (var orig = 0; orig < $scope.originalPCurrentItem.length; orig++) {
                itemToUndo = $scope.originalPCurrentItem[orig].codice;
                for (var i = 0; i < allParams.length; i++) {
                    if (allParams[i].codice == itemToUndo) {
                        allParams[i] = $scope.originalPCurrentItem[orig];
                        i = allParams.length;
                    }
                }
                if ($scope.pcurrentItem.codice == itemToUndo)
                    $scope.pcurrentItem = $scope.originalPCurrentItem[orig]; //restore detail
            }

            $scope.originalPCurrentItem = [];
        };

        $scope.pdelete = function (item) {
            $scope.pviewState = Enums.ViewState['D'];
            var itemToRemove = $scope.pcurrentItem.codice;
            var allParams = $scope.currentItem.distintaBase.distintaBaseParametri;
            for (var i = 0; i < allParams.length; i++) {
                if (allParams[i].codice == itemToRemove) {
                    allParams.splice(i, 1);
                    i = allParams.length;
                }
            }
            $scope.pviewState = Enums.ViewState['R'];
            $scope.originalPCurrentItem = [];
            angular.element(document.querySelector('#pmodalConfirmDelete')).modal('hide');
        };

        $scope.pconfirm = function () {
            if ($scope.pviewState === Enums.ViewState['C']) {
                if ($scope.currentItem.distintaBase.distintaBaseParametri == null)
                    $scope.currentItem.distintaBase.distintaBaseParametri = [];

                $scope.currentItem.distintaBase.distintaBaseParametri.push($scope.pcurrentItem)
            }
            else if ($scope.pviewState === Enums.ViewState['U']) {

                if ($scope.pcurrentItem.unitaDiMisura != null) {
                    var um = $.grep($scope.ListaUnitaDiMisura, function (e) { return e.id == $scope.pcurrentItem.unitaDiMisura.id; });
                    $scope.pcurrentItem.unitaDiMisura.item = um[0];
                }

            }
            $scope.originalPCurrentItem = [];
            $scope.pviewState = Enums.ViewState['R'];
        };

        $scope.pshowItemDetail = function (item) {
            if ($scope.pviewState != Enums.ViewState['R'])
                return;
            $scope.pcurrentItem = item;
            if ($scope.pviewState == Enums.ViewState['U'] && !itemExists($scope.originalPCurrentItem, item.codice))
                $scope.originalPCurrentItem.push(angular.copy($scope.pcurrentItem));

        };
        function itemExists(arr, value) {
            var r = false
            angular.forEach(arr, function (item) {
                if (item.codice === value)
                    r = true;
            });
            return r;
        }

        $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
            var x = $scope;
        });


        $scope.$watch('pviewState', function (newValue, oldValue) {
            if (newValue === oldValue) return;
            $scope.ptitle();
        });

        //------------------------------------------------------------------------------------------------------------------------------
        // produzione-parametri
        //------------------------------------------------------------------------------------------------------------------------------

        // funzione in cui viene calcolato il tempo teorico del batch e vengono ricavati ore minuti e secondi
        function setTotaleTempi(_scope, item) {

            totseconds = getTempoTeoricoSecRealTime(item.distintaBase.fasi, item.pezziDaProdurre, 0, 0);
            _scope.tothh = Math.floor(totseconds / 3600);
            _scope.totmm = Math.floor((totseconds - (_scope.tothh * 3600)) / 60);
            _scope.totss = Math.floor((totseconds - (_scope.tothh * 3600) - (_scope.totmm * 60)));
        }

        function setRiepilogo(_scope, item) {

            if (_scope.oreUomo == null) _scope.oreUomo = 0;         // teorici
            if (_scope.minutiUomo == null) _scope.minutiUomo = 0;
            if (_scope.secondiUomo == null) _scope.secondiUomo = 0;
            if (_scope.oreMacchina == null) _scope.oreMacchina = 0;
            if (_scope.minutiMacchina == null) _scope.minutiMacchina = 0;
            if (_scope.secondiMacchina == null) _scope.secondiMacchina = 0;

            if (_scope.oreUomoR == null) _scope.oreUomo = 0;        // realtime
            if (_scope.minutiUomoR == null) _scope.minutiUomoR = 0;
            if (_scope.secondiUomoR == null) _scope.secondiUomoR = 0;
            if (_scope.oreMacchinaR == null) _scope.oreMacchinaR = 0;
            if (_scope.minutiMacchinaR == null) _scope.minutiMacchinaR = 0;
            if (_scope.secondiMacchinaR == null) _scope.secondiMacchinaR = 0;


            var totsecondsU = 0;
            var totsecondsM = 0;
            var totsecondsUR = 0;
            var totsecondsMR = 0;
            if (item.distintaBase == null) return;
            var fasi = item.distintaBase.fasi;
            angular.forEach(fasi, function (fase) {
                if (fase.manodopera != null) {
                    var mans = fase.manodopera;
                    angular.forEach(mans, function (man) {
                        totsecondsU += man.tempo;
                    })
                }
                if (fase.macchine != null) {
                    var macs = fase.macchine;
                    angular.forEach(macs, function (mac) {
                        totsecondsM += mac.tempo;
                    })
                }
            });

            var fasiR = item.fasi;
            angular.forEach(fasiR, function (fase) {
                if (fase.manodopera != null) {
                    var mans = fase.manodopera;
                    angular.forEach(mans, function (man) {
                        if (man.inizio != null && man.fine != null)
                            totsecondsUR += moment(man.fine).diff(moment(man.inizio), 'seconds');
                    })
                }
                if (fase.macchine != null) {
                    var macs = fase.macchine;
                    angular.forEach(macs, function (mac) {
                        if (mac.inizio != null && mac.fine != null)
                            totsecondsMR += moment(mac.fine).diff(moment(mac.inizio), 'seconds');
                    })
                }
            });


            _scope.oreUomo = Math.floor(totsecondsU / 3600);
            _scope.minutiUomo = Math.floor((totsecondsU - (_scope.oreUomo * 3600)) / 60);
            _scope.secondiUomo = Math.floor((totsecondsU - (_scope.oreUomo * 3600) - (_scope.minutiUomo * 60)));
            _scope.oreMacchina = Math.floor(totsecondsM / 3600);
            _scope.minutiMacchina = Math.floor((totsecondsM - (_scope.oreMacchina * 3600)) / 60);
            _scope.secondiMacchina = Math.floor((totsecondsM - (_scope.oreMacchina * 3600) - (_scope.minutiMacchina * 60)));

            _scope.oreUomoR = Math.floor(totsecondsUR / 3600);
            _scope.minutiUomoR = Math.floor((totsecondsUR - (_scope.oreUomoR * 3600)) / 60);
            _scope.secondiUomoR = Math.floor((totsecondsUR - (_scope.oreUomoR * 3600) - (_scope.minutiUomoR * 60)));
            _scope.oreMacchinaR = Math.floor(totsecondsMR / 3600);
            _scope.minutiMacchinaR = Math.floor((totsecondsMR - (_scope.oreMacchinaR * 3600)) / 60);
            _scope.secondiMacchinaR = Math.floor((totsecondsMR - (_scope.oreMacchinaR * 3600) - (_scope.minutiMacchinaR * 60)));
        }

        //------------------------------------------------------------------------------------------------------------------------------
        // produzione-analisi
        //------------------------------------------------------------------------------------------------------------------------------
        $scope.evshowItemDetail = function (item) {
            //$scope.evcurrentItem = item;          // Grid Row Selection -> Disabled //
        };

        $scope.addChildNode = function (node) {
            alert('addChildNode')
        }
        $scope.deleteNode = function (node) {
            alert('deleteNode')
        }
        $scope.editNode = function (node) {
            alert('editNode')
        }

        $scope.AnalisiPhaseRowSelected = function (jsonNode) {
            var node = JSON.parse(jsonNode);
            $scope.AnalisiPhaseSelected = node;
            var batchAvanzamenti = $scope.currentItem.batchAvanzamenti;

            if (node.level == 0 || jsonNode == null)
                $scope.AnalisiData = batchAvanzamenti; // nel caso di selezione della root, vengono prelevati tutti gli elementi
            else {
                if (batchAvanzamenti != null) {
                    $scope.AnalisiData = $.grep(batchAvanzamenti, function (e) {
                        return e.numeroFase == node.numeroFase;
                    });

                    $scope.AnalisiDataFase = $.grep($scope.currentItem.fasi, function (e) {
                        return e.numeroFase === node.numeroFase;
                    });
                }

            }

        }

        $scope.dFormat = function (d, nota) {
            if (!d) return (nota == "Terminato" ? "" : "in corso");
            return d.substr(8, 2) + "/" + d.substr(5, 2) + "/" + d.substr(0, 4) + " " + d.substr(11, 8);
        }

        $scope.getDurataSec = function (inizio, fine) {
            if (inizio != null && fine != null) {
                dt_inizio = new Date(inizio);
                dt_fine = new Date(fine);
                var sec = (dt_fine.getTime() - dt_inizio.getTime()) / 1000;
                return Math.trunc(sec);
            }
            else {
                return "";
            }
        }

        $scope.getDurataMin = function (sec) {
            if (sec != null) {
                var min = sec / 60;
                return Math.trunc(min);
            }
            else {
                return "";
            }
        }

        $scope.getDurataFormattata = function (sec) {
            if (sec != null && sec != "") {
                var hh = Math.trunc(sec / 3600);
                var mm = Math.trunc((sec - (hh * 3600)) / 60);
                var ss = Math.trunc(sec - (hh * 3600) - (mm * 60));
                if (hh < 10)
                    hh = "0" + hh;

                if (mm < 10)
                    mm = "0" + mm;

                if (ss < 10)
                    ss = "0" + ss;
                return hh + ":" + mm + ":" + ss;
            }
            else
                return "";
        }

        //------------------------------------------------------------------    
        $scope.initializeNodeTreeTable = function (pChildArr0, scopeNodes) {
            var pChildArr = [{ id: '0', nome: 'Elenco fasi', fasi: pChildArr0 }];
            var length = pChildArr.length || 0;
            for (var i = 0; i < length; i++) {

                var theElement = pChildArr[i];
                var level = 0;
                var childCount = 0;
                if (theElement.fasi && theElement.fasi.length)
                    childCount = theElement.fasi.length;
                scopeNodes.push({
                    name: theElement.nome,
                    id: theElement.id,
                    numeroFase: theElement.numeroFase,
                    parent: "root",
                    toggleStatus: false,
                    parentId: -1,
                    isShow: true,
                    level: 0,
                    childCount: childCount,
                    isSaveBtn: false,
                    isUpdateBtn: false
                });
                if (theElement.fasi != undefined)
                    $scope.initializeNodeTreeTableChild(theElement.fasi, theElement.nome, theElement.id, level, scopeNodes);

            }
        };

        $scope.initializeNodeTreeTableChild = function (pChildArr, pParentName, pPparentId, pLevel, scopeNodes) {
            var isShowNode = false;
            pLevel = pLevel + 1;
            for (var i = 0; i < pChildArr.length; i++) {
                var node = pChildArr[i];
                var childCount = node.fasi != undefined ? node.fasi.length : 0
                scopeNodes.push({
                    name: node.nome,
                    id: node.id,
                    numeroFase: node.numeroFase,
                    parent: pParentName,
                    toggleStatus: false,
                    parentId: pPparentId,
                    isShow: isShowNode,
                    level: pLevel,
                    childCount: childCount,
                    isSaveBtn: false,
                    isUpdateBtn: false
                });
                if (node.fasi != undefined)
                    $scope.initializeNodeTreeTableChild(node.fasi, node.nome, node.id, pLevel, scopeNodes)
            }
        };

        $scope.toggleStatus = false;

        $scope.toggleDropdown = function (node, scopeNodes) {
            node.toggleStatus = node.toggleStatus == true ? false : true;
            $scope.toggleStatus = node.toggleStatus;
            $scope.toggleDropdownHelper(node.id, $scope.toggleStatus, scopeNodes);
        };

        $scope.toggleDropdownHelper = function (parentNodeId, toggleStatus, scopeNodes) {
            for (var i = 0; i < scopeNodes.length; i++) {
                node = scopeNodes[i];
                if (node.parentId == parentNodeId) {
                    if (toggleStatus == false)
                        $scope.toggleDropdownHelper(node.id, toggleStatus, scopeNodes);
                    scopeNodes[i].isShow = toggleStatus;
                }
            }
        };
        //------------------------------------------------------------------    

        // viewer related functions.
        $scope.selectIndentationClass = function (node) {
            return 'level' + node.level;
        };

        $scope.hasDropdown = function (node) {
            if (node.childCount > 0)
                return "hasDropdown";
            else
                return "noDropdown";
        };

        ////http://nvd3.org/examples/discreteBar.html
        ////http://krispo.github.io/angular-nvd3/#/lineChart
        ////http://nvd3-community.github.io/nvd3/examples/documentation.html#lineChart
        ////https://nvd3-community.github.io/nvd3/examples/documentation.html#discreteBarChart
        //------------------------------------------------------------------------------------------------------------------------------
        // produzione-analisi
        //------------------------------------------------------------------------------------------------------------------------------

        $scope.getDatiGrafici = function (currentItem, fromRefresh, filtri) {

            var colorByState = {
                Emergenza: "#f5564a",
                Pausa: "#ffc720",
                PausaFuoriTurno: "#e6e6e6",
                Lavorazione: "#97c95c",
            };

            totaleSecondiPausa = 0;
            totaleSecondiEmergenza = 0;
            totaleSecondiLavorazione = 0;
            arrIstogramma = [];
            dataSourceIsto = [];
            var batchAvanzamentiFilterd = [];
            dataGrigliaLavorazione = [];
            dataSourceGraficoFasiApp = [];

            fermiSummary = [];
            fermiGraphBar = [{ key: "ISTOGRAMMA (TEMPO)", color: '#1f77b4', values: [] }];
            fermiGraphPie = [];
            // Preparazione dei fermi macchina per esportazione 
            exportDataSummaryFermi = [];
            fileNameSummary = "";
            exportDataSummaryFermi.push(["Causale Fermo", "Numero Fermi", "Durata Totale", "Percentuale"]);
            var listOfUniqueFermi = [];
            var totFermi = 0;

            if (currentItem != null) {
                fileNameSummary = "Riepilogo_Fermi_Odp_" + currentItem.odP;
                // ------------------------------------------------------------------------------------------------------------------------------------

                angular.forEach(currentItem.fermi, function (casuale, key1) {
                    if (!casuale.causaliRiavvio)
                        casuale.causaliRiavvio = { item: { codice: ["n.d."] } };

                    var itemFounded = listOfUniqueFermi.filter(o => o.codice === casuale.causaliRiavvio.item.codice);
                    if (itemFounded.length == 0)
                        if (casuale.causaliRiavvio != null)
                            listOfUniqueFermi.push(casuale.causaliRiavvio.item)

                    totFermi += casuale.totaleTempo;
                });


                //inizializzo le variabili dei grafici
                angular.forEach(listOfUniqueFermi, function (causuale, key1) {
                    //se non sono presenti fermi return
                    if (!currentItem.fermi)
                        return;

                    //scarica tutti i fermi giltrati per casuale
                    var dataFilterCode = currentItem.fermi.filter(o => o.causaliRiavvio.item.codice === causuale.codice);
                    //inizializzo due variabili di appoggio
                    var totaleFermiPercausale = 0;
                    var totaleTempoFermoPerCausale = 0;

                    //per ogni fermo incremento la variabile numerofermi e sommo il tempo di fermata 
                    angular.forEach(dataFilterCode, function (causual, key1) {
                        totaleFermiPercausale++;
                        totaleTempoFermoPerCausale += causual.totaleTempo;
                    });

                    //calcolo la percentuale
                    var totalePercentuale = Math.round((totaleTempoFermoPerCausale * 100) / totFermi);

                    //se sono presente i dati 
                    if (dataFilterCode.length != 0) {
                        //passo alla funzione i secondi totali e mi ritorna un oggetto .day,.hh,.mm,.ss
                        var time = $scope.CalculateTime(totaleTempoFermoPerCausale);
                        //aggiorno un array che servirà per popolare la table e in caso di export to exel
                        fermiSummary.push({ codiceCausale: causuale.codice, nomeCausale: causuale.causale, totaleFermi: totaleFermiPercausale, totaleTempoSecondi: totaleTempoFermoPerCausale, totaleTempo: time, percentuale: totalePercentuale });
                        //viene aggiunto il fermo all'istogramma con casuale,  e tempo totale per casuale
                        fermiGraphBar[0].values.push({ "label": causuale.causale, "value": totaleTempoFermoPerCausale });
                        //viene aggiunto al grafico a torta casuale e numero di fermate per casuale
                        fermiGraphPie.push({ key: causuale.causale, y: totaleFermiPercausale });
                        //aggiungo al file per l'esportazione
                        exportDataSummaryFermi.push([causuale.causale, totaleFermiPercausale, totaleTempoFermoPerCausale, totalePercentuale + " %"]);
                    }
                });

                // ------------------------------------------------------------------------------------------------------------------------------------

                var add = true;
                angular.forEach(currentItem.batchAvanzamenti, function (batchAvanzamento, key) {
                    add = true;
                    var batchAvanzamento2 = new Object();
                    batchAvanzamento2.inizio = batchAvanzamento.inizio;
                    batchAvanzamento2.fine = batchAvanzamento.fine;
                    batchAvanzamento2.barcodeFase = batchAvanzamento.barcodeFase;
                    batchAvanzamento2.batchFaseMacchina = batchAvanzamento.batchFaseMacchina;
                    batchAvanzamento2.batchFaseManoDopera = batchAvanzamento.batchFaseManoDopera;
                    batchAvanzamento2.campo1 = batchAvanzamento.campo1;
                    batchAvanzamento2.campo2 = batchAvanzamento.campo2;
                    batchAvanzamento2.campo3 = batchAvanzamento.campo3;
                    batchAvanzamento2.campo4 = batchAvanzamento.campo4;
                    batchAvanzamento2.campo5 = batchAvanzamento.campo5;
                    batchAvanzamento2.campo6 = batchAvanzamento.campo6;
                    batchAvanzamento2.campo7 = batchAvanzamento.campo7;
                    batchAvanzamento2.campo8 = batchAvanzamento.campo8;
                    batchAvanzamento2.campo8 = batchAvanzamento.campo8;
                    batchAvanzamento2.codiceFase = batchAvanzamento.codiceFase;
                    batchAvanzamento2.id = batchAvanzamento.id;
                    batchAvanzamento2.nota = batchAvanzamento.nota;
                    batchAvanzamento2.numeroFase = batchAvanzamento.numeroFase;
                    batchAvanzamento2.splittedSourceId = batchAvanzamento.splittedSourceId;
                    batchAvanzamento2.stato = batchAvanzamento.stato;
                    batchAvanzamento2.turno = batchAvanzamento.turno;
                    batchAvanzamento2.userLocked = batchAvanzamento.userLocked;

                    if (filtri != null) {


                        if (filtri.turno != "" && batchAvanzamento.turno.id != filtri.turno)
                            add = false;

                        if (filtri.macchina != "" && batchAvanzamento.batchFaseMacchina.macchina.id != filtri.macchina)
                            add = false;

                        if (filtri.filtroDal != "" && filtri.filtroDal >= batchAvanzamento.fine)
                            add = false;
                        else if (filtri.filtroDal != "" && filtri.filtroDal > batchAvanzamento.inizio && filtri.filtroDal < batchAvanzamento.fine)
                            batchAvanzamento2.inizio = filtri.filtroDal;

                        if (filtri.filtroAl != "" && filtri.filtroAl <= batchAvanzamento.inizio)
                            add = false;
                        else if (filtri.filtroAl != "" && filtri.filtroAl < batchAvanzamento.fine && filtri.filtroAl > batchAvanzamento.inizio)
                            batchAvanzamento2.fine = filtri.filtroAl;
                    }
                    if (add) {
                        if (batchAvanzamento2.stato != "terminato") {
                            if (!batchAvanzamento2.fine) {
                                var today = new Date();
                                var month = today.getMonth() + 1;
                                if (month < 10)
                                    month = "0" + month;

                                var day = today.getDate();
                                if (day < 10)
                                    day = "0" + day;

                                var hour = today.getHours();
                                if (hour < 10)
                                    hour = "0" + hour;

                                var min = today.getMinutes();
                                if (min < 10)
                                    min = "0" + min;

                                var secAppTime = today.getSeconds();
                                if (secAppTime < 10)
                                    secAppTime = "0" + secAppTime;

                                batchAvanzamento2.fine = today.getFullYear() + "-" + month + "-" + day + "T" + hour + ":" + min + ":" + secAppTime;
                            }
                            var nomeFase = "";
                            var str_stato = "";
                            var keepGoing = true;
                            angular.forEach(currentItem.fasi, function (fase, key) {

                                if (keepGoing) {
                                    if (fase.numeroFase == batchAvanzamento2.numeroFase) {
                                        nomeFase = fase.distintaBaseFase.nome;
                                        keepGoing = false;
                                    }
                                }
                            });

                            var secApp = 0;
                            var pos = -1;
                            for (var ii = 0; ii < arrIstogramma.length; ii++) {
                                if (arrIstogramma[ii][4] == batchAvanzamento2.numeroFase) {
                                    pos = ii;
                                    arrApp = arrIstogramma[ii];
                                    break;
                                }
                            }

                            if (pos == -1) {
                                arrApp = [0, 0, 0, "", batchAvanzamento2.numeroFase];
                            }

                            secApp = $scope.getDurataSec(batchAvanzamento2.inizio, batchAvanzamento2.fine);

                            if (batchAvanzamento2.stato == "inLavorazione") {
                                totaleSecondiLavorazione = totaleSecondiLavorazione + secApp;
                                arrApp[0] = arrApp[0] + secApp;
                                str_stato = "Lavorazione";
                            }

                            if (batchAvanzamento2.stato == "inLavorazioneInPausa") {
                                if (batchAvanzamento2.turno.id != null) {
                                    totaleSecondiPausa = totaleSecondiPausa + secApp;
                                    arrApp[1] = arrApp[1] + secApp;
                                    str_stato = "Pausa";
                                }
                                else
                                    str_stato = "PausaFuoriTurno";
                            }

                            if (batchAvanzamento2.stato == "inLavorazioneInEmergenza") {
                                totaleSecondiEmergenza = totaleSecondiEmergenza + secApp;
                                arrApp[2] = arrApp[2] + secApp;
                                str_stato = "Emergenza";
                            }

                            arrApp[3] = nomeFase;
                            if (pos != -1)
                                arrIstogramma[pos] = arrApp;
                            else
                                arrIstogramma.push(arrApp);

                            var startApp = "";
                            if (batchAvanzamento2.inizio != "") {
                                var meseStart = (batchAvanzamento2.inizio.substring(5, 7) - 1);
                                startApp = new Date(batchAvanzamento2.inizio.substring(0, 4), meseStart, batchAvanzamento2.inizio.substring(8, 10), batchAvanzamento2.inizio.substring(11, 13), batchAvanzamento2.inizio.substring(14, 16), batchAvanzamento2.inizio.substring(17, 19));
                                //startApp = batch.inizio.substring(0, 4) + "/" + batch.inizio.substring(5, 7) + "/" + batch.inizio.substring(8, 10) +" "+ batch.inizio.substring(11, 13) +":"+ batch.inizio.substring(14, 16) +":"+ batch.inizio.substring(17, 19);
                            }

                            var endApp = "";
                            var meseEnd = (batchAvanzamento2.fine.substring(5, 7) - 1);
                            endApp = new Date(batchAvanzamento2.fine.substring(0, 4), meseEnd, batchAvanzamento2.fine.substring(8, 10), batchAvanzamento2.fine.substring(11, 13), batchAvanzamento2.fine.substring(14, 16), batchAvanzamento2.fine.substring(17, 19));
                            //endApp = batch.fine.substring(0, 4) + "/" + batch.fine.substring(5, 7) + "/" + batch.fine.substring(8, 10) + " " + batch.fine.substring(11, 13) + ":" + batch.fine.substring(14, 16) + ":" + batch.fine.substring(17, 19);

                            if (endApp != "") {
                                dataSourceGraficoFasiApp.push({
                                    Fase: nomeFase,
                                    start: startApp,
                                    end: endApp,
                                    stato_lavorazione: str_stato
                                });
                            }
                        }
                    }
                    //batchAvanzamentiFilterd.push(batchAvanzamento2);

                });

                //angular.forEach(batchAvanzamentiFilterd, function (batchAvanzamento2, key) {

                //});

                var totSecondi = parseInt(totaleSecondiLavorazione) + parseInt(totaleSecondiEmergenza) + parseInt(totaleSecondiPausa)
                var percLav = parseInt(totaleSecondiLavorazione) * 100 / totSecondi;
                var percEmer = parseInt(totaleSecondiEmergenza) * 100 / totSecondi;
                var percPausa = parseInt(totaleSecondiPausa) * 100 / totSecondi;

                dataSourceTorta = [{
                    country: "Lavorazione",
                    area: Math.round(percLav * 100) / 100
                },
                {
                    country: "Emergenza",
                    area: Math.round(percEmer * 100) / 100
                },
                {
                    country: "Pausa",
                    area: Math.round(percPausa * 100) / 100
                }];

                angular.forEach(arrIstogramma, function (datiIstogramma, key) {
                    var totSecondiFase = parseInt(datiIstogramma[0]) + parseInt(datiIstogramma[1]) + parseInt(datiIstogramma[2]);
                    var percLavFase = parseInt(datiIstogramma[0]) * 100 / totSecondiFase;
                    var percEmerFase = parseInt(datiIstogramma[2]) * 100 / totSecondiFase;
                    var percPausaFase = parseInt(datiIstogramma[1]) * 100 / totSecondiFase;
                    var tempoTeoricoFaseSec = 0;
                    // ottengo la fase di cui devo calcolare il tempo teorico
                    var fasePerTempoTeorico = [];

                    var returnLavfase = $scope.getDurataFormattata(datiIstogramma[0]) != "" ? $scope.getDurataFormattata(datiIstogramma[0]) + " (" + (Math.round(percLavFase * 100) / 100) + " %)" : "00:00:00 (0%)";
                    var returnEmerfase = $scope.getDurataFormattata(datiIstogramma[2]) != "" ? $scope.getDurataFormattata(datiIstogramma[2]) + " (" + (Math.round(percEmerFase * 100) / 100) + " %)" : "00:00:00 (0%)";
                    var returnPausafase = $scope.getDurataFormattata(datiIstogramma[1]) != "" ? $scope.getDurataFormattata(datiIstogramma[1]) + " (" + (Math.round(percPausaFase * 100) / 100) + " %)" : "00:00:00 (0%)";

                    if (currentItem.distintaBase != null) {
                        angular.forEach(currentItem.distintaBase.fasi, function (fase, key) {
                            if (fase.numeroFase == datiIstogramma[4]) {
                                fasePerTempoTeorico.push(fase);
                            }
                        });
                    }


                    //}
                    tempoTeoricoFaseSec = getTempoTeoricoSecRealTime(fasePerTempoTeorico, currentItem.pezziDaProdurre, 0, 0);
                    //------
                    var differenzaPer = ((datiIstogramma[0] - tempoTeoricoFaseSec) / tempoTeoricoFaseSec) * 100;
                    dataGrigliaLavorazione.push({
                        "Lavorazione": datiIstogramma[3],
                        "Tempo teorico": $scope.getDurataFormattata(tempoTeoricoFaseSec),
                        "Tempo effettivo": $scope.getDurataFormattata(datiIstogramma[0]),
                        "Totale minuti teorici": $scope.getDurataMin(tempoTeoricoFaseSec),
                        "Totale minuti effettivi": $scope.getDurataMin(datiIstogramma[0]),
                        "Differenza": Math.round(differenzaPer * 100) / 100 + " %",
                        "DifferenzaColor": Math.round(differenzaPer * 100) / 100,
                        "Tempo totale lavorazione": returnLavfase,
                        "Tempo totale emergenza": returnEmerfase,
                        "Tempo totale pausa": returnPausafase,
                    });
                    dataSourceIsto.push({ state: datiIstogramma[3], Lavorazione: Math.round(percLavFase * 100) / 100, Emergenza: Math.round(percEmerFase * 100) / 100, Pausa: Math.round(percPausaFase * 100) / 100 });
                });

                dataSourceGraficoFasi = dataSourceGraficoFasiApp.reverse();

                if (!fromRefresh) {
                    $scope.grigliaLavorazione = {
                        dataSource: dataGrigliaLavorazione,
                        columns: ["Lavorazione", "Tempo teorico", "Tempo effettivo", "Totale minuti teorici", "Totale minuti effettivi", "Tempo totale lavorazione", "Tempo totale pausa", "Tempo totale emergenza", "Differenza"],
                        showBorders: true,
                        onCellPrepared: function (e) {
                            if (e.column.dataField == "Differenza")
                                if (e.data != null)
                                    if (e.data.DifferenzaColor != null) {
                                        var int = parseInt(e.data.DifferenzaColor);
                                        if (int > 0)
                                            e.cellElement.css("color", "#ff0000");
                                        else
                                            e.cellElement.css("color", "#00ff00");
                                    }
                        }
                    };

                    $scope.chart_fasi_tempi = {
                        dataSource: dataSourceGraficoFasi,
                        rotated: true,
                        width: "100%",
                        barGroupPadding: 0.1,
                        valueAxis: {
                            argumentType: 'datetime',
                            label: {
                                format: "HH:mm:ss \r\n dd/MM/yyyy",
                                //or
                                //customizeText: function(data){
                                // return a value here
                                //}
                            }
                        },
                        customizePoint: function (info) {
                            var State = info.data.stato_lavorazione;
                            var option = {
                                color: null,
                            }
                            option.color = colorByState[State]
                            return option;
                        },

                        title: {
                            text: "Ordine n° " + currentItem.odP,
                            subtitle: ""
                        },
                        commonSeriesSettings: {
                            argumentField: "Fase",
                            type: "rangeBar",
                            rangeValue1Field: "start",
                            rangeValue2Field: "end",
                            ignoreEmptyPoints: true,
                            barOverlapGroup: "Fasi",
                        },
                        seriesTemplate: {
                            nameField: "stato_lavorazione"
                        },
                        animation: {
                            enabled: false
                        },
                        scrollBar: {
                            visible: false,
                        },
                        legend: {
                            visible: false,
                            title: "",
                            verticalAlignment: "bottom",
                            horizontalAlignment: "center"
                        },
                    };

                    $scope.chart_istogramma_fasi_raggruppamenti = {
                        dataSource: dataSourceIsto,
                        commonSeriesSettings: {
                            argumentField: "state",
                            type: "stackedBar"
                        },
                        size: {
                            height: 250
                        },
                        series: [
                            { valueField: "Emergenza", name: "Emergenza ", color: colorByState["Emergenza"] },
                            { valueField: "Lavorazione", name: "Lavorazione ", color: colorByState["Lavorazione"] },
                            { valueField: "Pausa", name: "Pausa ", color: colorByState["Pausa"] }
                        ],
                        valueAxis: {
                            title: {
                                text: "%"
                            },
                            position: "left"
                        },
                        title: "Percentuale per fasi",
                        tooltip: {
                            enabled: true,
                            location: "edge",
                            customizeTooltip: function (arg) {
                                return {
                                    text: arg.seriesName + arg.valueText
                                };
                            }
                        }
                    };
                    $scope.gauge = {
                        size: {
                            width: 380,
                            height: 380
                        },
                        scale: {
                            startValue: 0,
                            endValue: 100,
                            tickInterval: 10,
                            label: {
                                customizeText: function (arg) {
                                    return arg.valueText + " %";
                                }
                            }
                        },
                        rangeContainer: {
                            ranges: [
                                { startValue: 0, endValue: 30, color: "#CE2029" },
                                { startValue: 30, endValue: 70, color: "#FFD700" },
                                { startValue: 70, endValue: 100, color: "#228B22" }
                            ]
                        },
                        "export": {
                            enabled: true
                        },
                        title: {
                            text: "OEE",
                            font: { size: 28 }
                        },
                        bindingOptions: {
                            value: 'gaugeValue'
                        },
                        /* value: $scope.currentItem.oee,*/

                        onDrawn: toggleLabelsBasedOnSize
                    };

                    function toggleLabelsBasedOnSize(e) {
                        e.component.option("scale.label.visible", e.element.width() > 80);
                    }

                    $scope.pie = {
                        size: {
                            width: 420,
                            height: 420
                        },
                        palette: "bright",
                        customizePoint: function (point) {
                            return {
                                color: colorByState[point.argument]
                            }
                        },
                        dataSource: dataSourceTorta,
                        series: [
                            {
                                argumentField: "country",
                                valueField: "area",
                                label: {
                                    visible: true,
                                    connector: {
                                        visible: true,
                                        width: 1
                                    }
                                }
                            }
                        ],
                        title: "Ordine",
                    };

                    $scope.istogrammaFermi = {
                        dataSource: fermiGraphBar[0].values,
                        rotated: true,
                        series: {
                            argumentField: "label",
                            valueField: "value",
                            name: "Fermi",
                            type: "bar",
                            color: '#393ced'
                        },
                        legend: {
                            horizontalAlignment: 'center',
                            verticalAlignment: 'bottom',
                            itemTextPosition: 'right',
                        },
                    };

                    $scope.pieFermi = {
                        commonSeriesSettings: {
                            argumentField: "state",
                        },
                        size: {
                            width: 420,
                            height: 420
                        },
                        palette: "Bright",
                        dataSource: fermiGraphPie,
                        series: [
                            {
                                argumentField: "key",
                                valueField: "y",
                                label: {
                                    visible: true,
                                    position: "inside",
                                    radialOffset: 30,
                                    backgroundColor: "transparent",
                                    font: {
                                        size: 16,
                                    }
                                }
                            }
                        ],
                    };

                    $scope.pieScarti = {
                        size: {
                            width: 420,
                            height: 420
                        },
                        palette: "Bright",
                        dataSource: $scope.scartiGraphPie,
                        series: [
                            {
                                argumentField: "key",
                                valueField: "y",
                                label: {
                                    visible: true,
                                    position: "inside",
                                    radialOffset: 30,
                                    backgroundColor: "transparent",
                                    font: {
                                        size: 16,
                                    }
                                }
                            }
                        ],
                    };

                }
                $("#chart_istogramma_fasi_raggruppamenti").dxChart("option", "dataSource", dataSourceIsto);
                $("#chart_fasi_tempi").dxChart("option", "dataSource", dataSourceGraficoFasi);
                $("#istogrammaFermi").dxChart("option", "dataSource", fermiGraphBar[0].values);
                $("#pieScarti").dxPieChart("option", "dataSource", $scope.scartiGraphPie);
                $("#pieFermi").dxPieChart("option", "dataSource", fermiGraphPie);
                $("#grigliaLavorazione").dxDataGrid("option", "dataSource", dataGrigliaLavorazione);

                $("#pie").dxPieChart("option", "dataSource", dataSourceTorta);

                $scope.refreshGrafici();
            }
        }

        //------------------------------------------------------------------------------------------------------------------------------
        // produzione-trend
        //------------------------------------------------------------------------------------------------------------------------------
        $scope.pdtOptionsTrend = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('stateSave', true)
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);
        $scope.pdtInstanceTrend = {};

        $scope.trcurrentItem = [];
        $scope.trshowItemDetail = function (item) {
            var found = false;
            for (var i = 0; i < $scope.trcurrentItem.length; i++) {
                if ($scope.trcurrentItem[i].id == item.id) {
                    $scope.trcurrentItem.splice(i, 1);
                    i = $scope.trcurrentItem.length;
                    found = true;
                }
            }
            if (!found)
                $scope.trcurrentItem.push(item);

            delete $scope.data_multiBarChart;
            $scope.data_multiBarChart = generateData();


        };


        $scope.checkIfSelected = function (item) {
            var found = false;
            angular.forEach($scope.trcurrentItem, function (it) {
                if (item.id == it.id) { found = true; return; }
            })
            return found;
        }

        $scope.options_multiBarChart = {
            chart: {
                type: 'multiBarChart',           // 'lineWithFocusChart', //'multiBarChart', 'historicalBarChart'
                height: 450,
                margin: {
                    top: 20,
                    right: 30,
                    bottom: 45,
                    left: 65
                },
                //clipEdge: true,
                //duration: 500,
                //stacked: true,
                //useInteractiveGuideline: true,

                xAxis: {
                    axisLabel: 'Tempo (hh:mm)',
                    showMaxMin: false,
                    //tickFormat: function (d) { return d3.time.format('%d/%m %H:%M:%S')(new Date(d)); }
                    tickFormat: function (x) { return d3.time.format('%d-%m %H:%M:%S')(new Date(x)); }
                },

                x2Axis: {
                    tickFormat: function (y) { return d3.time.format('%d-%m %H:%M:%S')(new Date(y)); }
                },

                yAxis: {
                    axisLabel: 'Valore',
                    axisLabelDistance: 0,
                    tickFormat: function (d) { return d3.format(',.1f')(d); }

                },


            }
        };


        $scope.options_multiBarChart1 = {
            chart: {
                type: 'multiBarChart',           // 'lineWithFocusChart', //'multiBarChart', 'historicalBarChart'
                height: 450,
                margin: {
                    top: 20,
                    right: 30,
                    bottom: 45,
                    left: 65
                },
                //clipEdge: true,
                duration: 500,
                stacked: false,
                groupSpacing: 0.1,
                //useInteractiveGuideline: true,

                xAxis: {
                    axisLabel: 'Tempo (hh:mm)',
                    showMaxMin: false,
                    //tickFormat: function (d) { return d3.time.format('%d/%m %H:%M:%S')(new Date(d)); }
                    tickFormat: function (x) { return d3.time.format('%d-%m %H:%M:%S')(new Date(x)); }
                },

                x2Axis: {
                    tickFormat: function (y) { return d3.time.format('%d-%m %H:%M:%S')(new Date(y)); }
                },

                yAxis: {
                    axisLabel: 'Valore',
                    axisLabelDistance: 0,
                    tickFormat: function (d) { return d3.format(',.1f')(d); }

                },

                //zoom: {
                //    enabled: true,
                //    scaleExtent: [1, 10],
                //    useFixedDomain: false,
                //    useNiceScale: false,
                //    horizontalOff: false,
                //    verticalOff: true//,
                //    //unzoomEventType: "dblclick.zoom"
                //}


                //y1Axis: {
                //    tickFormat: function (d) { return d3.format(',.1f')(d); }
                //},
                //y2Axis: {
                //    tickFormat: function (d) { return d3.format(',.1f')(d); }
                //},
                //y3Axis: {
                //    tickFormat: function (d) { return d3.format(',.1f')(d); }
                //},
                //y4Axis: {
                //    tickFormat: function (d) { return d3.format(',.1f')(d); }
                //}

            }
        };


        function generateData() {
            var yCount = $scope.trcurrentItem.length;
            var res = [];
            for (n = 0; n < $scope.trcurrentItem.length; n++) {
                var paramItem = $scope.trcurrentItem[n];
                res.push({ key: paramItem.nome, values: paramItem.data });
            }

            if (res.length > 0)
                return res;
            else
                return [{ key: '', values: [{ x: 0, y: 0 }] }];
        }


        //------------------------------------------------------------------------------------------------------------------------------
        // produzione-AnalisiFermi
        //------------------------------------------------------------------------------------------------------------------------------

        //$scope.dataGraphOrderStatus.push({ key: 'In Pausa', y: 2 });
        //$scope.dataGraphOrderStatus.push({ key: 'Allarme', y: 1 });
        //$scope.dataGraphOrderStatus.push({ key: 'eee', y: 1 });


        $scope.dtOptions2 = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('stateSave', true)
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        $scope.dtOptions3 = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('stateSave', true)
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);



        $scope.initializeGraphBar = function () {
            var MAX_PERCENT = 60;
            var DURATION = 400;
            var groupSpace = 1;
            var AXISY_WIDTH = 150;
            var EXTED_LABEL_WIDTH = 20;
            var rectangle;
            var updateFocusLine = function () {
                var rect = calculateRect();
                $(rectangle[0]).find('rect').width(rect.width);
            };
            var calculateRect = function () {
                var xAxis = d3.select('.nv-x.nv-axis')[0][0];
                var wrap = d3.select('.nv-multibarHorizontal')[0][0];
                var width = wrap.getBoundingClientRect().width + EXTED_LABEL_WIDTH + AXISY_WIDTH + groupSpace;
                var heightPerBar = parseFloat(d3.selectAll('rect')[0][0].getAttribute('height'));
                var sumPercent = 0;
                var i = 0
                for (; i < percent.length; i++) {
                    if (sumPercent >= MAX_PERCENT) {
                        break;
                    }
                    sumPercent += parseFloat(percent[i]);
                }
                var height = (i * heightPerBar) + (i * groupSpace) + groupSpace / 2;
                return { width: width, height: height }
            }
            var addFocusLine = function () {
                var rect = calculateRect();
                rectangle = d3.selectAll('.nv-barsWrap').insert('g', ":first-child").attr('fill', 'red');
                rectangle.append('rect')
                    .attr("class", "rectangle")
                    .attr("width", rect.width)
                    .attr("height", rect.height)
                    .attr('x', -AXISY_WIDTH + 2)
                    .attr('y', groupSpace / 2)
                    .attr('rx', 10)
                    .attr('ry', 10)
                rectangle.append('text')
                    .attr('text-anchor', 'start')
                    .attr('x', rect.width - AXISY_WIDTH - 100)
                    .attr('y', rect.height - 5)
                    .style('font-weight', 'bold')
                    .text("The max is: " + MAX_PERCENT + "%")
            }
            var getGroupSpace = function () {
                var chart = d3.select('.nv-multibarHorizontal')[0][0];
                var height = chart.getBoundingClientRect().height;
                var numberOfGroup = percent.length;
                var heightPerBar = parseFloat(d3.selectAll('rect')[0][0].getAttribute('height'));
                return height / numberOfGroup - heightPerBar;
            }

            var percent = [];
            var maxVal;
            $scope.fermiGraphBar[0].values.forEach(function (item, index) {
                var tmp = 0;
                $scope.fermiGraphBar.forEach(function (group) {
                    tmp += group.values[index].value;
                })
                if (!maxVal || maxVal < tmp) {
                    maxVal = parseInt((tmp + 20) / 20) * 20;
                }
                percent.push(tmp.toFixed(2));
            })
            var myChart;
            $scope.optionsGraph_Bar = {
                chart: {
                    type: 'multiBarHorizontalChart',
                    height: DURATION,
                    width: 600,

                    x: function (d) {
                        return d.label;
                    },
                    y: function (d) {
                        return d.value;
                    },
                    margin: {
                        left: AXISY_WIDTH
                    },
                    showControls: true,
                    showValues: true,
                    duration: 500,
                    stacked: true,
                    showLegend: false,
                    showControls: false,
                    yDomain: [0, maxVal],
                    groupSpacing: 0.1,
                    xAxis: {
                        showMaxMin: false,
                        valuePadding: 100
                    },
                    yAxis: {
                        showMaxMin: false,
                        axisLabel: 'Secondi',
                        hideTick: true,
                        tickFormat: function (d) {
                            return d3.format(',.2f')(d);
                        }
                    },
                    callback: function (chart) {
                        //console.log(123);
                        chart.multibar.dispatch.on('elementClick', function (e) {
                            console.log('elementClick in callback', e.data);
                        });
                        chart.dispatch.on('changeState', function (e) {
                            console.log('changeState in callback', e.data);
                        });
                        chart.dispatch.on('renderEnd', function () {
                            console.log('render complete');
                            var grap = d3.selectAll('.nv-multibarHorizontal');
                            var groups = grap.selectAll('.nv-group')[0];
                            var lastGroup = groups[groups.length - 1];
                            var children = lastGroup.children;
                            for (var i = 0; i < children.length; i++) {
                                var g = children[i];
                                var text = g.getElementsByTagName('text')[0];
                                var rect = g.getElementsByTagName('rect')[0];
                                text.setAttribute('x', parseFloat(rect.getAttribute('width')) + 5);
                                text.setAttribute('y', parseFloat(rect.getAttribute('height')) / 2);
                                text.setAttribute("text-anchor", "start");
                                text.setAttribute("dy", ".32em");
                                text.textContent = percent[i] + "%";
                            }
                            groupSpace = getGroupSpace();
                            addFocusLine();

                        });
                    }

                }
            };

            nv.utils.windowResize(function () {
                if (rectangle)
                    rectangle.remove();
                $timeout(function () {
                    console.log('render complete');
                    var grap = d3.selectAll('.nv-multibarHorizontal');
                    var groups = grap.selectAll('.nv-group')[0];
                    var lastGroup = groups[groups.length - 1];
                    var children = lastGroup.children;
                    for (var i = 0; i < children.length; i++) {
                        var g = children[i];
                        var text = g.getElementsByTagName('text')[0];
                        var rect = g.getElementsByTagName('rect')[0];
                        text.setAttribute('x', parseFloat(rect.getAttribute('width')) + 5);
                        text.setAttribute('y', parseFloat(rect.getAttribute('height')) / 2);
                        text.setAttribute("text-anchor", "start");
                        text.setAttribute("dy", ".32em");
                        text.textContent = percent[i] + "%";
                    }
                    addFocusLine();
                }, DURATION)
            });

        };

        //inizializzazione grafico a torta
        $scope.fermiGraphPie_Option = {
            chart: {
                type: 'pieChart',
                height: 400,
                width: 350,
                x: function (d) { return d.key + ' (' + d.y + ')'; },
                y: function (d) { return d.y; },
                showLabels: false,
                duration: 500,
                labelThreshold: 0.01,
                labelSunbeamLayout: true,
                color: ['#ff7f0e', 'gray', 'green', 'orange', 'blue', 'red'],
                legend: {
                    margin: {
                        top: 5,
                        right: 5,
                        bottom: 0,
                        left: 0
                    }
                }
            }
        };



        //------------------------------------------------------------------------------------------------------------------------------
        // produzione-AnalisiScarti
        //------------------------------------------------------------------------------------------------------------------------------
        $scope.dtOptions4 = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('stateSave', true)
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        $scope.dtOptions5 = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('stateSave', true)
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        $scope.dtOptions6 = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('stateSave', true)
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);




        $scope.loadingGraphicPzSca = function (items) {
            var totaleScartiPerCausale = 0;


            // vengono filtrati solo i fermi effettivamente presenti nel batch
            var listOfUniqueCausal = [];
            var item = jQuery.grep(items, function (a) { return a.tipoEvento == eventType_ScrapPiece })
            if (item != null) {
                angular.forEach(item, function (causale, key1) {
                    if (causale.tipoValore != null)
                        var itemFounded = listOfUniqueCausal.filter(o => o.codice === causale.tipoValore.item.codice);
                    if (itemFounded.length == null)
                        var a = 0;
                    else if (itemFounded.length == 0)
                        listOfUniqueCausal.push(causale.tipoValore.item)
                });

                //inizializzo le variabili dei grafici
                $scope.scartiSummary = [];
                $scope.scartiGraphPie = [];
                $scope.Scarti_NotJustified = [];


                angular.forEach(listOfUniqueCausal, function (causuale, key1) {
                    //se non sono presenti fermi return
                    if (!$scope.currentItem.eventi)
                        return;
                    totaleScartiPerCausale = 0;
                    //scarica tutti i fermi filtrati per casuale
                    var dataFilterCode = item.filter(o => o.tipoValore.item.codice === causuale.codice);
                    //inizializzo due variabili di appoggio

                    //per ogni fermo incremento la variabile numerofermi e sommo il tempo di fermata 
                    angular.forEach(dataFilterCode, function (causual, key1) {
                        totaleScartiPerCausale += parseInt(causual.valore);
                    });

                    //se sono presente i dati 
                    if (dataFilterCode.length != 0) {
                        $scope.scartiSummary.push({ codiceCausale: causuale.descrizione, totaleScarti: totaleScartiPerCausale });
                        $scope.scartiGraphPie.push({ key: causuale.descrizione, y: totaleScartiPerCausale });

                    }
                });
            }
        };


        var treeDataSource = [
            {
                "ID": 1,
                "name": "Stores",
                "expanded": true
            },
            {
                "ID": "1_1",
                "categoryId": 1,
                "name": "Test Davide",
                "expanded": true
            },
            {
                "ID": "1_1_1",
                "categoryId": "1_1",
                "name": "Sub Test Davide"
            },
            {
                "ID": "1_1_1_1",
                "categoryId": "1_1_1",
                "name": "Sub Sub Test Davide",
                "price": 220
            },
            {
                "ID": "1_1_1_2",
                "categoryId": "1_1_1",
                "name": "SuperHD Video Player",
                "price": 270
            },
            {
                "ID": "1_1_2",
                "categoryId": "1_1",
                "name": "Televisions",
                "expanded": true
            },
            {
                "ID": "1_1_2_1",
                "categoryId": "1_1_2",
                "name": "SuperLCD 42",
                "price": 1200
            },
            {
                "ID": "1_1_2_2",
                "categoryId": "1_1_2",
                "name": "SuperLED 42",
                "price": 1450
            },
            {
                "ID": "1_1_2_3",
                "categoryId": "1_1_2",
                "name": "SuperLED 50",
                "price": 1600
            }
        ];


        $scope.treeBoxValue = "1_1";
        $scope.isGridBoxOpened = false;

        $scope.treeBoxOptions = {
            bindingOptions: {
                value: 'treeBoxValue',
                opened: 'isGridBoxOpened'
            },
            valueExpr: "ID",
            displayExpr: "name",
            placeholder: "Seleziona fase",
            showClearButton: true,
            dataSource: treeDataSource,
            treeView: {
                dataSource: treeDataSource,
                dataStructure: "plain",
                keyExpr: "ID",
                parentIdExpr: "categoryId",
                displayExpr: "name",
                selectByClick: true,
                selectNodesRecursive: false,
                selectionMode: "single",
                onItemSelectionChanged: function (args) {
                    $scope.treeBoxValue = args.component.getSelectedNodeKeys();
                },
                onItemClick: function () {
                    $scope.isGridBoxOpened = false;
                }
            }
        };
    }]);



//=============================================================================================================
app.directive('excelExport',
    function () {
        return {
            restrict: 'A',
            scope: {
                fileName: "@",
                data: "&exportData"
            },
            replace: true,
            template: '<button class="btn btn-primary btn-ef btn-ef-3 btn-ef-3c mb-10" ng-click="download()">Export to Excel <i class="fa fa-download"></i></button>',
            link: function (scope, element) {
                scope.download = function () {

                    function datenum(v, date1904) {
                        if (date1904) v += 1462;
                        var epoch = Date.parse(v);
                        return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
                    };

                    function getSheet(data, opts) {
                        var ws = {};
                        var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
                        for (var R = 0; R != data.length; ++R) {
                            for (var C = 0; C != data[R].length; ++C) {
                                if (range.s.r > R) range.s.r = R;
                                if (range.s.c > C) range.s.c = C;
                                if (range.e.r < R) range.e.r = R;
                                if (range.e.c < C) range.e.c = C;
                                var cell = { v: data[R][C] };
                                if (cell.v == null) continue;
                                var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

                                if (typeof cell.v === 'number') cell.t = 'n';
                                else if (typeof cell.v === 'boolean') cell.t = 'b';
                                else if (cell.v instanceof Date) {
                                    cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                                    cell.v = datenum(cell.v);
                                }
                                else cell.t = 's';

                                ws[cell_ref] = cell;
                            }
                        }
                        if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
                        return ws;
                    };

                    function Workbook() {
                        if (!(this instanceof Workbook)) return new Workbook();
                        this.SheetNames = [];
                        this.Sheets = {};
                    }

                    var wb = new Workbook(), ws = getSheet(scope.data());
                    /* add worksheet to workbook */
                    wb.SheetNames.push(scope.fileName);
                    wb.Sheets[scope.fileName] = ws;

                    var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

                    function s2ab(s) {
                        var buf = new ArrayBuffer(s.length);
                        var view = new Uint8Array(buf);
                        for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                        return buf;
                    }

                    saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), scope.fileName + '.xlsx');

                };
            }
        };
    }
);
//=============================================================================================================



app.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                });
            }
        }
    }
});


// direttiva per il menu con il tasto destro del mouse
app.directive("contextMenu", function ($compile) {
    contextMenu = {};
    contextMenu.restrict = "AE";
    contextMenu.link = function (lScope, lElem, lAttr) {
        lElem.on("contextmenu", function (e) {
            e.preventDefault(); // default context menu is disabled
            //  The customized context menu is defined in the main controller. To function the ng-click functions the, contextmenu HTML should be compiled.
            lElem.append($compile(lScope[lAttr.contextMenu])(lScope));
            // The location of the context menu is defined on the click position and the click position is catched by the right click event.
            //$("#contextmenu-node").css("left", e.clientX);
            //$("#contextmenu-node").css("top", e.clientY);
        });
        lElem.on("mouseleave", function (e) {
            // on mouse leave, the context menu is removed.
            if ($("#contextmenu-node"))
                $("#contextmenu-node").remove();
        });
    };
    return contextMenu;
});

app.directive('date', function (dateFilter) {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {

            var dateFormat = attrs['date'] || 'yyyy-MM-dd';

            ctrl.$formatters.unshift(function (modelValue) {
                return dateFilter(modelValue, dateFormat);
            });
        }
    };
})