﻿// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------

app.controller('oeeCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$rootScope', '$translate',
    function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $rootScope, $translate, $timeout) {

        var urlApp = window.location.href.split(':');
        var serverIP = "";
        if (urlApp.length >= 1)
            serverIP = urlApp[1].replace("//","");
        
        $scope.date = new Date();
        $scope.nMacchinePerRiga = 4;
        $scope.nRighe = 8;
        $scope.signalR = "9001";
        $scope.webApi = "9000";
        $scope.stationId = "99";
        $scope.loginConRfid = $.cookie("loginConRfid");
        $scope.inactivityTimeout = 2147483647;
        $.cookie("SigalRc", "http://" + serverIP + ":" + $scope.signalR, { expires: 70000 });
        $.cookie("WebApiC", "http://" + serverIP + ":" + $scope.webApi + "/api", { expires: 70000 });
        $.cookie("StationId", $scope.stationId, { expires: 70000 });
        $.cookie("LoginConRfid", $scope.loginConRfid, { expires: 70000 });
        $.cookie("InactivityTimeout", $scope.inactivityTimeout, { expires: 70000 });
        $scope.totaleTonTrasformate = 0;
        $scope.arrPage = [];
        $scope.arrRighe = [];
        $scope.arrRiga = [];
        $scope.arrGaugeRefresh = [];
        $scope.chartFasiTempi = {};
        $scope.progressBar = {};
        $scope.pezziBuoniTot = "0";
        $scope.totTempoLav = "0";



        //$scope.bluScuro= "#033859";
        //$scope.azzurro= "#22B7F2";
        //$scope.grigio = "#C4C4C4";
        //$scope.verde = "#50C41D";

        $scope.tonBuone = 0;
        $scope.percScarto = 0;
        $scope.oee = 0;
        $scope.oeeAva = 0;
        $scope.oeeEff = 0;
        $scope.oeeQua = 0;
        $scope.i = 0;

        $translate.use($scope.$parent.globals.currentUser.lingua);
        //--------------------------------------------------------------
        $rootScope.signalRconnection = $.cookie("SigalRc");
        connection = $.hubConnection($rootScope.signalRconnection);
        connection.logging = false;
        hub = connection.createHubProxy('AreaMESManager');
        hub.logging = false;

        hub.on('OnDashboardOee', function (message) {
            $rootScope.$apply(function () {
                $scope.date = new Date();
                // testata
                $scope.tonBuone = message.Data.Valore1;
                $scope.percScarto = message.Data.Descrizione1;
                $scope.oee = message.Data.Oee;
                $scope.oeeAva = message.Data.Oee_Rf;
                $scope.oeeQua = message.Data.Oee_Rq;
                $scope.oeeEff = message.Data.Oee_Rv;
                $scope.pezziBuoniTot = message.Data.Valore2;
                $scope.totTempoLav = message.Data.Descrizione2;
                
                $("#oee").dxBarGauge("option", "values", [$scope.oee]);
                $("#ava").dxBarGauge("option", "values", [$scope.oeeAva]);
                $("#eff").dxBarGauge("option", "values", [$scope.oeeEff]);
                $("#qua").dxBarGauge("option", "values", [$scope.oeeQua]);

                //pagine macchina
                $scope.listObjMacchine = message.Data.listaMacchine;

                $scope.refreshAll = false;

                angular.forEach($scope.listObjMacchine, function (macchina, key) {
                    var stringMacchina = "gauge" + macchina.Descrizione4;
                    var stringMacchina2 = "chartFasiTempi" + macchina.Descrizione4;
                    var stringMacchina3 = "progressBar" + macchina.Descrizione4;

                    angular.forEach(macchina.ListaAvanzamenti, function (value, key2) {

                        if (value.Inizio != "") {
                            var meseStart = (value.Inizio.substring(5, 7) - 1);
                            startApp = new Date(value.Inizio.substring(0, 4), meseStart, value.Inizio.substring(8, 10), value.Inizio.substring(11, 13), value.Inizio.substring(14, 16), value.Inizio.substring(17, 19));
                        }

                        var endApp = "";
                        var meseEnd = (value.Fine.substring(5, 7) - 1);
                        endApp = new Date(value.Fine.substring(0, 4), meseEnd, value.Fine.substring(8, 10), value.Fine.substring(11, 13), value.Fine.substring(14, 16), value.Fine.substring(17, 19));

                        if (endApp != "") {
                            value.Inizio = startApp;
                            value.Fine = endApp;
                        }
                    });

                    if ($scope.gauge[stringMacchina]) {
                        $("#" + stringMacchina).dxCircularGauge("option", "value", macchina.Oee);
                        angular.forEach($scope.arrPage, function (valPage, key01) {
                            angular.forEach(valPage, function (valRiga, key02) {
                                angular.forEach(valRiga, function (item, key03) {
                                    if (item.Descrizione4 == macchina.Descrizione4) {
                                        $scope.arrPage[key01][key02][key03].Colore = macchina.Colore;
                                        $scope.arrPage[key01][key02][key03].Descrizione1 = macchina.Descrizione1;
                                        $scope.arrPage[key01][key02][key03].Descrizione2 = macchina.Descrizione2;
                                        $scope.arrPage[key01][key02][key03].Descrizione3 = macchina.Descrizione3;
                                        $scope.arrPage[key01][key02][key03].Descrizione4 = macchina.Descrizione4;
                                        $scope.arrPage[key01][key02][key03].Descrizione5 = macchina.Descrizione5;
                                        $scope.arrPage[key01][key02][key03].Descrizione6 = macchina.Descrizione6;
                                        $scope.arrPage[key01][key02][key03].Descrizione7 = macchina.Descrizione7;
                                        $scope.arrPage[key01][key02][key03].Descrizione8 = macchina.Descrizione8;
                                        $scope.arrPage[key01][key02][key03].Descrizione9 = macchina.Descrizione9;
                                        $scope.arrPage[key01][key02][key03].Descrizione10 = macchina.Descrizione10;
                                        $scope.arrPage[key01][key02][key03].Label = macchina.Label;
                                        $scope.arrPage[key01][key02][key03].ListaAvanzamenti = macchina.ListaAvanzamenti;
                                        $scope.arrPage[key01][key02][key03].Oee = macchina.Oee;
                                        $scope.arrPage[key01][key02][key03].Oee_Rf = macchina.Oee_Rf;
                                        $scope.arrPage[key01][key02][key03].Oee_Rq = macchina.Oee_Rq;
                                        $scope.arrPage[key01][key02][key03].Oee_Rv = macchina.Oee_Rv;
                                        $scope.arrPage[key01][key02][key03].PercCompleteamento = macchina.PercCompleteamento;
                                        $scope.arrPage[key01][key02][key03].PezziBuoni = macchina.PezziBuoni;
                                        $scope.arrPage[key01][key02][key03].PezziScarto = macchina.PezziScarto;
                                        $scope.arrPage[key01][key02][key03].TempoEmergenza = macchina.TempoEmergenza;
                                        $scope.arrPage[key01][key02][key03].TempoLavorazione = macchina.TempoLavorazione;
                                        $scope.arrPage[key01][key02][key03].TempoPausa = macchina.TempoPausa;
                                        $scope.arrPage[key01][key02][key03].Valore1 = macchina.Valore1;
                                        $scope.arrPage[key01][key02][key03].Valore2 = macchina.Valore2;
                                        $scope.arrPage[key01][key02][key03].Valore3 = macchina.Valore3;
                                        $scope.arrPage[key01][key02][key03].Valore4 = macchina.Valore4;
                                        $scope.arrPage[key01][key02][key03].Valore5 = macchina.Valore5;
                                    }
                                });
                            });
                        });
                    }
                    else {

                        var app = {
                            animation: {
                                enabled: false
                            },
                            title: {
                                text: "OEE",
                                font: { size: 20 },
                                verticalAlignment: "bottom",
                            },
                            scale: {
                                startValue: 0,
                                endValue: 100,
                                tickInterval: 10,
                                label: {
                                    useRangeColors: true,
                                },
                            },
                            rangeContainer: {
                                palette: 'pastel',
                                ranges: [
                                    { startValue: 00, endValue: 65, color: 'red' },
                                    { startValue: 65, endValue: 77, color: 'orange' },
                                    { startValue: 77, endValue: 100, color: 'green' },
                                ],
                            },
                            value: macchina.Oee
                        };
                        $scope.gauge[stringMacchina] = app;
                        $scope.refreshAll = true;
                    }

                    if ($scope.chartFasiTempi[stringMacchina2]) {
                        //$("#" + stringMacchina2).dxChart("option", "dataSource", macchina.ListaAvanzamenti);
                    }
                    else {
                        var app2 = {};
                        app2 = {
                            dataSource: macchina.ListaAvanzamenti,
                            loadingIndicator: true,
                            rotated: true,
                            height: "30",
                            width: "400",
                            barGroupPadding: 0.1,
                            columnFixing: {
                                enabled: true
                            },
                            commonAxisSettings: {
                                visible: false,
                                label: {
                                    visible: false,
                                }
                            },
                            valueAxis: {
                                argumentType: 'datetime',
                                grid: {
                                    color: "#ffffff"
                                },
                                label: {
                                    format: "HH:mm:ss \r\n dd/MM/yyyy",
                                }
                            },
                            customizePoint: function (info) {
                                var option = {
                                    color: null,
                                }
                                option.color = info.data.Colore;
                                return option;
                            },
                            commonSeriesSettings: {
                                argumentField: "Macchina",
                                type: "rangeBar",
                                barHeight: 20,
                                rangeValue1Field: "Inizio",
                                rangeValue2Field: "Fine",
                                ignoreEmptyPoints: true,
                                barOverlapGroup: "Macchina",
                            },
                            seriesTemplate: {
                                nameField: "StatoLavorazione"
                            },
                            animation: {
                                enabled: false
                            },
                            scrollBar: {
                                visible: false,
                            },
                            legend: {
                                visible: false,
                                title: "",
                            },
                        };
                        $scope.chartFasiTempi[stringMacchina2] = app2;
                        $scope.refreshAll = true;
                    }

                    if ($scope.progressBar[stringMacchina3] != undefined) {
                        $("#" + stringMacchina3).dxProgressBar("option", "value", parseInt(macchina.Valore2));
                    }
                    else {
                        var app3 = {};
                        app3 = {
                            min: 0,
                            max: 100,
                            width: '100%',
                            value: parseInt(macchina.Valore2),
                            statusFormat(value) {
                                return parseInt(value * 100) + '%';
                            },
                        };
                        $scope.progressBar[stringMacchina3] = app3;
                        $scope.refreshAll = true;
                    }
                });
                if ($scope.refreshAll) {
                    $scope.changeNumMacchinePerRiga();
                    $scope.refreshAllGaugeCircular(20);
                }

            });
        });

        connection.start();

        const geometry = {
            startAngle: 90,
            endAngle: 90,
        };

        $scope.gauge = {
            oee: {
                geometry,
                //values: [0],
                backgroundColor: "#22B7F2",
                palette: ["#033859"],
                relativeInnerRadius: "0.65",
                bindingOptions: {
                    value: 'iii',
                },
                label: {
                    visible: false
                },
                size: {
                    height: "100%",
                    width: "100%"
                }
            },
            ava: {
                geometry,
                values: [0],
                relativeInnerRadius: "0.65",
                backgroundColor: "#22B7F2",
                palette: ["#033859"],
                label: {
                    visible: false
                },
                title: {
                    text: "Availability",
                    font: {
                        size: 28,
                    },
                },
            },
            eff: {
                geometry,
                values: [0],
                relativeInnerRadius: "0.65",
                backgroundColor: "#22B7F2",
                palette: ["#033859"],
                label: {
                    visible: false
                },
                title: {
                    text: "Efficiency",
                    font: {
                        size: 28,
                    },
                },
            },
            qua: {
                geometry,
                values: [0],
                relativeInnerRadius: "0.65",
                backgroundColor: "#22B7F2",
                palette: ["#033859"],
                label: {
                    visible: false
                },
                title: {
                    text: "Quality",
                    font: {
                        size: 28,
                    },
                },
            },
        };

        $scope.progressBarValue = 3;
        $scope.widthDivPerMacchine = 3;

        $scope.changeNumMacchinePerRiga = function () {
                if ($scope.nMacchinePerRiga == 2)
                    $scope.widthDivPerMacchine = 6;
                else if ($scope.nMacchinePerRiga == 3)
                    $scope.widthDivPerMacchine = 4;
                else if ($scope.nMacchinePerRiga == 4)
                    $scope.widthDivPerMacchine = 3;
                else if ($scope.nMacchinePerRiga == 6)
                    $scope.widthDivPerMacchine = 2;

                $scope.arrPage = [];
                $scope.arrRighe = [];
                $scope.arrRiga = [];

                angular.forEach($scope.listObjMacchine, function (value1, key) {
                    if ($scope.arrRiga.length >= (parseInt($scope.nMacchinePerRiga))) {
                        $scope.arrRighe.push($scope.arrRiga);
                        $scope.arrRiga = [];
                    }

                    $scope.arrRiga.push(value1);
                    if ($scope.arrRighe.length >= $scope.nRighe) {
                        $scope.arrPage.push($scope.arrRighe);
                        $scope.arrRighe = [];
                    }
                });

                if ($scope.arrRiga.length != 0)
                    $scope.arrRighe.push($scope.arrRiga);

                if ($scope.arrRighe.length != 0)
                    $scope.arrPage.push($scope.arrRighe);
        };

        $scope.refreshGaugeCircular = function (id, delay) {
            setTimeout(function () {
                $("#" + id).dxCircularGauge("instance").render();
            }, delay);
        };

        $scope.refreshChart = function (id, delay) {
            setTimeout(function () {
                $("#" + id).dxChart("instance").render();
            }, delay);
        };

        $scope.refreshAllGaugeCircular = function (delay) {
            setTimeout(function () {
                angular.forEach($scope.listObjMacchine, function (value, key) {
                    $("#gauge" + value.Descrizione4).dxCircularGauge("instance").render();
                    //$("#chartFasiTempi" + value.Descrizione4).dxChart("instance").render();
                });
            }, delay);
        };

        setTimeout(function () {
            $scope.$apply(function () {
                $scope.changeNumMacchinePerRiga();
            });
            $scope.refreshAllGaugeCircular(50);
        }, 150);

        $scope.colorByState = {
            BLOCCO: "#f5564a",
            PAUSA: "#ffc720",
            LAVORAZIONE: "#97c95c",
            MONTAGGIO: "#22B7F2",
            SMONTAGGIO: "#033859"
        };



        $('#carousel').on('slide.bs.carousel', function () {
            $scope.refreshAllGaugeCircular(0);
        })



    }
]);