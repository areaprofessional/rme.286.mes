﻿// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------

app.controller('ProduzionePrioritaCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$rootScope', '$translate', '$location', '$timeout',
	function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $rootScope, $translate, $timeout, $location, $timeout) {
		//-----------------------------------------------------------------------------------
		$scope.IsFullScreenOnEdit = true;
		$scope.items = [];
		$scope.InitFilter = "priorita";
		$scope.viewState = Enums.ViewState['R'];
		$scope.listaAziende = null;
		$scope.aziendaSelezionata = null;
		$scope.MainFilter = {};
		$scope.MainFilter.filterByCompanyId = null;
		$scope.MainFilter.disableFilterByCompanyId = false;
		//-----------------------------------------------------------------------------------

		//-----------------------------------------------------------------------------------
		//funzione per recuperare le traduzioni
		$translate.use($scope.$parent.globals.currentUser.lingua);
		//-----------------------------------------------------------------------------------

		//-----------------------------------------------------------------------------------
		// funzione per popolare il combo box delle aziende
		DataFactory.GetAziende().then(function (response) { $scope.listaAziende = response.data; });
		//-----------------------------------------------------------------------------------

		//-----------------------------------------------------------------------------------
		if ($scope.$parent.globals.currentUser.azienda !== null) {
			$scope.MainFilter.filterByCompanyId = $scope.$parent.globals.currentUser.azienda.id;
			$scope.MainFilter.disableFilterByCompanyId = true;
		}
		//-----------------------------------------------------------------------------------

		//-----------------------------------------------------------------------------------
		//funzione per ricaricare la tabella
		$scope.reload = function () {
			//basicCrud_doReload($scope, $http, subUrlBatch, filter);
			DataFactory.GetListBatchFiltered($scope.InitFilter).then(function (response) {
				$scope.items = response.data;

				$("#gridContainer").dxDataGrid("option", "dataSource", $scope.items);

				function getOrderDay(rowData) {
					return (new Date(rowData.OrderDate)).getDay();
				}
			});
		};
		$scope.reload();
		//-----------------------------------------------------------------------------------

		//-----------------------------------------------------------------------------------
        // Andrea.g, 14/09/2022
        // funzione per il salvataggio delle priorità
		$scope.savePriority = function () {
			DataFactory.SetPriority($scope.items, "").then(function (response) {
				if (response) {
					$scope.reload();
					$("#gridContainer").dxDataGrid("instance").refresh();
                }
			});
		}
		//-----------------------------------------------------------------------------------

		//-----------------------------------------------------------------------------------
		//opzioni della tabella
		$scope.gridOptions = {
			dataSource: $scope.items,
			keyExpr: "id",
			allowColumnResizing: true,
			height: "100%",
			showBorders: true,
			columnAutoWidth: false,
			filterRow: { visible: true },
			onContentReady: function () {
				if ($scope.ViewAsInSospensione)
					$("#gridContainer").dxDataGrid("columnOption", "Data sospensione", "visible", true);
				else
					$("#gridContainer").dxDataGrid("columnOption", "Data sospensione", "visible", false);
				if ($scope.ViewAsInModifica)
					$("#gridContainer").dxDataGrid("columnOption", "", "visible", true);
				else
					$("#gridContainer").dxDataGrid("columnOption", "", "visible", false);
			},
			/*  filterPanel: { visible: true },*/
			/* headerFilter: { visible: true },*/
			rowDragging: {
				allowReordering: true,
				dropFeedbackMode: "push",
				showDragIcons: false,
				onReorder(e) {
					var visibleRows = e.component.getVisibleRows();
					var toIndex = $scope.items.findIndex((item) => item.id === visibleRows[e.toIndex].data.id);
					var fromIndex = $scope.items.findIndex((item) => item.id === e.itemData.id);
					$scope.items.splice(fromIndex, 1);
					$scope.items.splice(toIndex, 0, e.itemData);
					e.component.refresh();
				},
			},
			onRowDblClick: function (e) {
				var url = "produzione.html?i=fromDashboard&odp=";
				url = url + e.data.odP;
				window.open(url);
			},
			searchPanel: {
				visible: true,
				width: 240,
				placeholder: "Cerca..."
			},
			selection: {
				mode: "single"
			},

			loadPanel: {
				enabled: true
			},
			scrolling: {
				mode: "virtual"
			},
			hoverStateEnabled: true,
			showBorders: true,
			customizePoint: function (info) {
				var State = info.data.statoLavorazione;
				var option = {
					color: null,
				}
				option.color = colorByState[State];
				return option;
			},
			filterBuilder: {
				customOperations: [{
					name: "weekends",
					caption: "Weekends",
					dataTypes: ["date"],
					icon: "check",
					hasValue: false,
					calculateFilterExpression: function () {
						return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
					}
				}],
				allowHierarchicalFields: true
			},
			filterBuilderPopup: {
				position: { of: window, at: "top", my: "top", offset: { y: 10 } },
			},
			scrolling: { mode: "infinite" },
			showBorders: true,
			export: {
				enabled: true
			},
			columns: [{
				caption: "Stato",
				type: "buttons",
				width: "45",
				buttons: [{
					hint: "in Modifica",
					icon: "fas fa-flag",
					visible: function (e) {
						return e.row.data.stato == "inModifica";
					}
				},
				{
					hint: "in Lavorazione",
					icon: "fas fa-check",
					visible: function (e) {
						return e.row.data.stato == "inLavorazione";
					}
				},
				{
					hint: "in Lavorazione In Pausa",
					icon: "fas fa-pause",
					visible: function (e) {
						return e.row.data.stato == "inLavorazioneInPausa";
					}
				},
				{
					hint: "in Lavorazione In Emergenza",
					icon: "fas fa-exclamation",
					visible: function (e) {
						return e.row.data.stato == "inLavorazioneInEmergenza";
					}
				},
				{
					hint: "in Lavorazione In Idle",
					icon: "fas fa-flag",
					visible: function (e) {
						return e.row.data.stato == "inLavorazioneInIdle";
					}
				},
				{
					hint: "Terminato Abortito",
					icon: "fas fa-ban",
					visible: function (e) {
						return e.row.data.stato == "terminatoAbortito";
					}
				},
				{
					hint: "Terminato",
					icon: "fas fa-check-double",
					visible: function (e) {
						return e.row.data.stato == "terminato";
					}
				},
				{
					hint: "in Attesa",
					icon: "fas fa-flag",
					visible: function (e) {
						return e.row.data.stato == "inAttesa";
					}
				},
				{
					hint: "in Sospensione",
					icon: "fas fa-hand-paper",
					visible: function (e) {
						return e.row.data.stato == "inSospensione";
					}
				}],
			},
			{
				caption: "Priorità",
				dataField: "priorita",
				dataType: "int",
				width: "50",
			},
			{
				caption: "OEE",
				dataField: "oee",
				dataType: "string",
				format: "percent",
				allowGrouping: false,
				cellTemplate: "discountCellTemplate",
				cssClass: "bullet",
				width: 150
			},
			{
				caption: "OdP",
				dataField: "odP",
				dataType: "string",
				width: 80
			},
			{
				caption: "OdV Descrizione",
				dataField: "odV_DescrizioneCliente",
				dataType: "string",
			},
			{
				caption: "Macchine",
				dataField: "stringMacchine",
				dataType: "string",
			},
			//{
			//	caption: "Codice Materiale",
			//	dataField: "materiale.toString",
			//	dataType: "string",
			//	width: 180,
			//},
			{
				caption: "Descrizione Materiale",
				dataField: "materiale.descrizione",
				dataType: "string"
			},
			{
				caption: "Distinta Base",
				dataField: "distintaBase.codice",
				dataType: "string",
				width: 135,
			},
			//{
			//	caption: "Modello Moto",
			//	dataField: "revisione",
			//	dataType: "string",
			//	width: 130,
			//},
			{
				caption: "Q.tà",
				dataField: "pezziDaProdurre",
				dataType: "int",
				width: 50,
			},
			{
				caption: "Data consegna",
				dataField: "odV_DataConsegna",
				dataType: 'date',
				format: 'dd/MM/yyyy',
				width: 100,
			},
			{
				caption: "Data sospensione",
				dataField: "sospensione",
				dataType: 'date',
				format: 'dd/MM/yyyy',
				width: 100,
			},
			{
				caption: "",
				type: "buttons",
				width: 35,
				buttons: [{
					text: "Lancia",
					icon: "exportselected",
					onClick: function (e) {
						$scope.update(e, $scope.selectedEmployee);
						$('#modalConfirmChangeState').modal('show');
					},
					visible: function (e) {
						return e.row.data.stato == "inModifica";
					}
				}]
			},
			]
		};
		//-----------------------------------------------------------------------------------

		//-----------------------------------------------------------------------------------
		//funzione per il tooltip in percentuale del OEE nella tabella
		$scope.customizeTooltip = function (pointsInfo) {
			return { text: parseInt(pointsInfo.originalValue) + "%" };
		};
		//-----------------------------------------------------------------------------------
	}
]);


