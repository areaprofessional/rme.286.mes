﻿// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('QualitaDistintaBaseCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$translate', '$rootScope', '$location', '$timeout',
    function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $translate, $rootScope, $location, $timeout, growl) {

        $scope.IsFullScreenOnEdit = true;
        $scope.items = [];

        $scope.idFase = {};
        $translate.use($scope.$parent.globals.currentUser.lingua);
        // flag per abilitare la seconda tab dei tempi
        //$scope.isEnabledTabTimers = false;

        // flag per visualizzare i controlli sulle fasi
        $scope.isEnabledDettagliFase = false;

        DataFactory.GetCausaliFase().then(function (response) {
            $scope.CausaliFase = response.data;
            // creazione dell'albero
            createOrRefreshTreeView();
        });
        $scope.currentItem = null;
        $scope.viewState = Enums.ViewState['R'];
        $scope.titolo = "Dettaglio";
        $scope.dtInstance = {};
        $scope.CompetenzeOperatori = null;
        $scope.Macchine = null;
        $scope.CompetenzeMacchine = null;
        $scope.ListaMateriali = null;
        $scope.ListaUnitaDiMisura = null;
        $scope.ListaStatiDistintaBase = null;
        $scope.IsVisibleFaseCiclica = true;
        $scope.MainFilter = {};
        $scope.MainFilter.filterByCompanyId = null;
        $scope.MainFilter.disableFilterByCompanyId = false;
        $scope.exceptionMessageText = "";
        // popolo le varie dropdown
        DataFactory.getCompetenzeOperatori().then(function (response) {
            $scope.CompetenzeOperatori = response.data;
        });

        DataFactory.getMacchine().then(function (response) {
            $scope.Macchine = response.data;
        });

        DataFactory.getCompetenzeMacchina().then(function (response) {
            $scope.CompetenzeMacchine = response.data;
        });
        if ($scope.$parent.globals.currentUser.azienda !== null) {
            $scope.MainFilter.filterByCompanyId = $scope.$parent.globals.currentUser.azienda.id;
            $scope.MainFilter.disableFilterByCompanyId = true;
        }



        DataFactory.getMateriali().then(function (response) {
            $scope.ListaMateriali = response.data;
        });

        DataFactory.getUnitaDiMisura().then(function (response) {
            $scope.ListaUnitaDiMisura = response.data;
        });

        DataFactory.GetStatiDistintaBase().then(function (response) {
            $scope.ListaStatiDistintaBase = response.data;
        });

        // funzione per popolare il combo box delle aziende
        DataFactory.GetAziende().then(function (response) {
            $scope.listaAziende = response.data;
        });

        // DataTables configurable options
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(5)
            .withOption('stateSave', true)
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        $scope.showItemDetail = function (item) {

            setTotaleTempi($scope, item);

            //$scope.CausaliFase = ["Lavorazione"];

            $scope.CausaliFase = null;
            $scope.OperazioniCiclo = null;


            basicCrud_doShowItemDetail($scope, $http, subUrlDistintaBase, Enums, item);



            DataFactory.GetCausaliFase().then(function (response) {
                $scope.CausaliFase = response.data;
                // creazione dell'albero
                createOrRefreshTreeView();
            });

            DataFactory.GetOperazioniCiclo().then(function (response) {
                $scope.OperazioniCiclo = response.data;
            });

            // funzione per popolare il combo box delle aziende
            DataFactory.GetAziende().then(function (response) { $scope.listaAziende = response.data; });
        };

        $scope.reload = function () { basicCrud_doReload($scope, $http, subUrlDistintaBase, applyMainFilter); };

        $scope.insert = function () {
            $scope.viewState = Enums.ViewState['C'];
            $scope.currentItem = new Object();
            $scope.currentItem.azienda = {};

            if ($scope.MainFilter.disableFilterByCompanyId) {
                $scope.currentItem.azienda.id = $scope.MainFilter.filterByCompanyId;
            }

            if ($scope.dataForTheTree[0].fasi != null)
                $scope.dataForTheTree[0].fasi = [];
        };

        $scope.reloadMachine = function () {
            $scope.Macchine = null;
            DataFactory.getMacchine().then(function (response) {
                if ($scope.currentItem.azienda.id)
                    $scope.Macchine = response.data.filter(o => o.azienda.id == $scope.currentItem.azienda.id);
                else
                    $scope.Macchine = response.data;
            });
        };

        // ------------------------------------------------------------------------
        // Alberto.p, 01/07/2020, filtro sulla griglia principale
        // Definizione del filtro di visualizzazione in griglia
        function applyMainFilter() {
            if (($scope.MainFilter === null) ||
                ($scope.MainFilter.filterByCompanyId === null) ||
                ($scope.MainFilter.filterByCompanyId === "")) return;

            $scope.items = $.grep($scope.items, function (e) {

                var matchCriteria = false;

                if ((e.azienda !== null) && (e.azienda.id !== null) && (e.azienda.id === $scope.MainFilter.filterByCompanyId))
                    matchCriteria = true;

                return matchCriteria;

            });
        }
        // ------------------------------------------------------------------------


        $scope.update = function () {
            $scope.viewState = Enums.ViewState['U'];
        };

        $scope.cancel = function () { $scope.viewState = Enums.ViewState['R']; resetAllFields(); createOrRefreshTreeView(); closeModal('#modalConfirmUndo'); };

        $scope.confirm = function () {
            resetAllFields();
            $scope.selectedNode = null;
            createOrRefreshTreeView();
            basicCrud_doConfirmWithOutReload($scope, $http, subUrlDistintaBase, Enums);
            // LEONARDO BUG: NON AGGIORNA "item[n] con currentItem //////////////////////////////////////////////////////////////////////
            setTotaleTempi($scope, $scope.currentItem)
        };

        $scope.confirmAndClose = function () {
            resetAllFields();
            createOrRefreshTreeView();
            basicCrud_doConfirm($scope, $http, subUrlDistintaBase, Enums);
        };

        $scope.delete = function (item) { $scope.viewState = Enums.ViewState['D']; basicCrud_doDelete($scope, $http, subUrlDistintaBase, Enums, item) };

        $scope.title = function () { basicCrud_doTitle($scope, Enums); };

        basicCrud_doInitWatch($scope);

        $scope.reload();

        // funzioni che rendono mutuamente esclusive le drop down
        $scope.selectionMacchine = function (id) {
            $scope.isEnabledcompentenzaMacchineSelection[id] = ($scope.modelsSelMacchine[id] == -1);
        }

        $scope.selectionCompetenzaMacchine = function (id) {
            $scope.isEnabledMacchinaSelection[id] = ($scope.modelsSelCompetenzaMacchine[id] == -1);
        }

        // opzioni per l'albero
        $scope.treeOptions = {
            nodeChildren: "fasi",
            isExpanded: true,
            dirSelectable: true,
            injectClasses: {
                ul: "a1",
                li: "a2",
                liSelected: "a7",
                iExpanded: true,
                iCollapsed: "a4",
                iLeaf: "a5",
                label: "a6",
                labelSelected: "a8"
            }
        }

        // nodo base per il treeview
        $scope.dataForTheTree =
            [
                { "numeroFase": "", "nome": "Elenco fasi", "id": "0", "type": "-1", "fasi": [] }
            ];

        // menù con il click destro del mouse
        //$scope.myContextDiv = "<ul id='contextmenu-node' class='nav nav-pills nav-stacked nav-bracket'>" +
        //    "<li class='contextmenu-item' ng-click='insertFaseSemplice()'><i class='fa fa-tag'></i>Fase semplice</li>" +
        //    "<li class='contextmenu-item' ng-click='insertFaseCiclica()'><i class='fa fa-gears'></i>Fase ciclica</li>" +
        //    "<li class='contextmenu-item' ng-click='deleteFase()'><i class='fa fa-eraser'></i>Elimina fase</li>" +
        //    "</ul>";




        // creazione fase semplice, andiamo a popolare solo i nodi del tree view
        $scope.insertFaseSemplice = function () {
            // se non ho selezionato un nodo di fase ciclica devo aggiungere il ramo alla fina dell'albero
            // se ho selezionato un nodo di fase ciclica devo aggiungerlo dentro quest'ultima
            // creo l'id dinamicamente
            var idRoot = "";
            var idItem = "";
            var idLeave = "";
            var parentNode = 0;
            var arrApp = { "causaleFase": "tempi", "macchina": null, "macchinaCompetenza": null, "tempo": 0 };
            var tmpFaseForTree = {
                "numeroFase": idLeave,
                "nome": "Nuova Fase",
                "id": idLeave,
                "descrizione": "",
                "type": "0",
                "parentNode": parentNode,
                "isAndon": false,
                "isAutomatica": false,
                "abilitaAvanzamenti": true,
                "isCiclica": false,
                "fasi": [],
                "macchine": { 0: arrApp },
                "manodopera": { 0: arrApp },
            };

            if ($scope.dataForTheTree[0].fasi != null) {
                if ($scope.dataForTheTree[0].fasi[$scope.dataForTheTree[0].fasi.length - 1] != null) {
                    if ($scope.selectedNode != null) {
                        //try { idItem = $scope.selectedNode.fasi[$scope.selectedNode.fasi.length - 1].id; } catch (err) { idItem = "0"; }
                        idItem = getMaxId($scope.selectedNode.fasi)
                        idRoot = $scope.selectedNode.id;
                        idItem = lastSection(idItem);
                        idLeave = idRoot + "_" + idItem;
                    }
                    else {
                        //idLeave = $scope.dataForTheTree[0].fasi[$scope.dataForTheTree[0].fasi.length - 1].id;
                        idLeave = getMaxId($scope.dataForTheTree[0].fasi)
                        idItem = idLeave;
                    }
                }
                //idLeave = $scope.dataForTheTree[0].fasi[0].fasi[$scope.dataForTheTree[0].fasi[0].fasi.length - 1].id; 
                idLeave = (idLeave == undefined ? "0" : idLeave);
                idItem = (idItem == undefined ? "0" : idItem);
            }
            idLeave = incrementLastSection(idLeave);
            idItem++;

            var bflag = false;
            if ($scope.selectedNode != null) {
                tmpFaseForTree.id = idLeave;

                tmpFaseForTree.numeroFase = idItem;
                if ($scope.selectedNode.fasi == undefined) $scope.selectedNode.fasi = [];
                $scope.selectedNode.fasi.push(tmpFaseForTree);
                createOrRefreshTreeView();
            }
            else { bflag = true; }

            if (bflag) {
                tmpFaseForTree.id = idLeave;
                tmpFaseForTree.numeroFase = idLeave.replace(/_/g, "");
                if (!$scope.currentItem.fasi) $scope.currentItem.fasi = [];
                $scope.currentItem.fasi.push(tmpFaseForTree);
                createOrRefreshTreeView();
            }
        };

        function getMaxId(node) {
            var maxNodeId = "0";
            var maxNum = 0;
            angular.forEach(node, function (item, key) {
                if (lastSection(item.id) > maxNum) {
                    maxNodeId = item.id;
                    maxNum = lastSection(item.id);
                }
            })
            return maxNodeId;
        }

        function incrementLastSection(argStr) {
            var retStr = "";
            var sects = argStr.split('_');
            for (i = 0; i < sects.length - 1; i++)
                retStr += sects[i].toString() + "_";
            retStr += ((++sects[sects.length - 1])).toString();
            return retStr;
        }

        function lastSection(argStr) {
            var sects = argStr.split('_');
            return sects[sects.length - 1];
        }






        // creazione fase ciclica, andiamo a popolare solo i nodi del tree view
        $scope.insertFaseCiclica = function () {
            // se non ho selezionato un nodo di fase ciclica devo aggiungere il ramo alla fina dell'albero
            // se ho selezionato un nodo di fase ciclica devo aggiungerlo dentro quest'ultima
            // creo l'id dinamicamente
            var idRoot = "";
            var idItem = "";
            var idLeave = "";
            var parentNode = 0;
            var arrApp = { "causaleFase": "tempi", "macchina": null, "macchinaCompetenza": null, "tempo": 0 };
            var tmpFaseForTree = {
                "numeroFase": idLeave,
                "nome": "Nuova Fase Ciclica",
                "id": idLeave,
                "descrizione": "",
                "type": "1",
                "parentNode": parentNode,
                "isAndon": false,
                "isAutomatica": false,
                "abilitaAvanzamenti": true,
                "isCiclica": true,
                "fasi": [],
                "macchine": { 0: arrApp },
                "manodopera": { 0: arrApp },
            }
            if ($scope.dataForTheTree[0].fasi != null) {
                if ($scope.dataForTheTree[0].fasi[$scope.dataForTheTree[0].fasi.length - 1] != null) {
                    if ($scope.selectedNode != null) {
                        //try { idItem = $scope.selectedNode.fasi[$scope.selectedNode.fasi.length - 1].id; } catch (err) { idItem = "0"; }
                        idItem = getMaxId($scope.selectedNode.fasi)
                        idRoot = $scope.selectedNode.id;
                        idItem = lastSection(idItem);
                        idLeave = idRoot + "_" + idItem;
                    }
                    else {
                        //idLeave = $scope.dataForTheTree[0].fasi[$scope.dataForTheTree[0].fasi.length - 1].id;
                        idLeave = getMaxId($scope.dataForTheTree[0].fasi)
                        idItem = idLeave;
                    }
                }
                //idLeave = $scope.dataForTheTree[0].fasi[0].fasi[$scope.dataForTheTree[0].fasi[0].fasi.length - 1].id; 
                idLeave = (idLeave == undefined ? "0" : idLeave);
                idItem = (idItem == undefined ? "0" : idItem);
            }
            idLeave = incrementLastSection(idLeave);
            idItem++;

            var bflag = false;
            if ($scope.selectedNode != null) {
                //if ($scope.type == "1") {
                //idLeave = idLeave + $scope.selectedNode.id;
                tmpFaseForTree.id = idLeave;
                //tmpFaseForTree.numeroFase = idLeave.replace(/_/g, ""); 

                tmpFaseForTree.numeroFase = idItem;

                /////////tmpFaseForTree.parentNode = $scope.selectedNode; //$scope.dataForTheTree[0].children[$scope.dataForTheTree[0].children.length - 1].id;
                if ($scope.selectedNode.fasi == undefined) $scope.selectedNode.fasi = [];
                $scope.selectedNode.fasi.push(tmpFaseForTree);
                createOrRefreshTreeView();
                //} else { bflag = true; }
            }
            else { bflag = true; }

            if (bflag) {
                tmpFaseForTree.id = idLeave;
                tmpFaseForTree.numeroFase = idLeave.replace(/_/g, "");
                if (!$scope.currentItem.fasi) $scope.currentItem.fasi = [];
                $scope.currentItem.fasi.push(tmpFaseForTree);

                createOrRefreshTreeView();
            }

        };




        $scope.confirmFase = function () {
            var arrayOrdered = [];
            var shiftAllOtherPosition = false;
            var children = $scope.selectedNode.fasi;

            // creazione dell'oggetto fase
            var tmpDistintaBaseFase = {
                numeroFase: $scope.numeroFase, nome: $scope.nomeFase, descrizione: $scope.descrizioneFase, codice: $scope.codiceFase, fasi: children,
                barcode: $scope.currentItem.barcode, isDisabled: "false", macchine: [],
                totaleTempiMacchina: [], manodopera: [], totaleTempiManodopera: [],
                isAutomatica: $scope.isAutomatica, isAndon: $scope.isAndon, taktMacchina: 0, taktManodopera: 0, taktTotale: 0,
                abilitaAvanzamenti: $scope.abilitaAvanzamenti,
                abilitaMultimedia: $scope.abilitaMultimedia,
                abilitaMateriali: $scope.abilitaMateriali,
                abilitaVersamentiManuali: $scope.abilitaVersamentiManuali,
                abilitaVersamentiAutomatici: $scope.abilitaVersamentiAutomatici,
                stileDiVisualizzazioneVersamenti: $scope.stileDiVisualizzazioneVersamenti,
                abilitabarcode: $scope.abilitabarcode,
                abilitaLetturaRfid: $scope.abilitaLetturaRfid,
                abilitascritturaRfid: $scope.abilitascritturaRfid,
                abilitaAvanzamentiStar: $scope.abilitaAvanzamentiStar,
                abilitaAvanzamentiStop: $scope.abilitaAvanzamentiStop,
                abilitaAvanzamentiPausa: $scope.abilitaAvanzamentiPausa,
                abilitaPulsanteEmergenza: $scope.abilitaPulsanteEmergenza,
                visibilitastazione: $scope.visibilitastazione,
                isCiclica: $scope.isCiclica,
                id: $scope.selectedNode.id  ////id: $scope.numeroFase //!! What ??
            };

            var macchine = [];
            var manodopera = [];
            var totaleTempiMacchina = [];
            var totaleTempiManodopera = [];
            var tmpTempoTotaleManodopera = null;
            var tmpTempoTotaleMacchina = null;

            angular.forEach($scope.CausaliFase, function (obj, key) {

                // recupero il tempo uomo ed il tempo macchina trasformandoli in double
                var tmpTempoUomo = ($scope.modelsOreUomo[obj.id] * 3600) + ($scope.modelsMinutiUomo[obj.id] * 60) + $scope.modelsSecondiUomo[obj.id];
                var tmpTempoMacchina = ($scope.modelsOreMacchina[obj.id] * 3600) + ($scope.modelsMinutiMacchina[obj.id] * 60) + $scope.modelsSecondiMacchina[obj.id];

                // associo la competenza all'oggetto da passare
                var tmpCompetenzaOperatore = null;
                angular.forEach($scope.CompetenzeOperatori, function (singleCompetenzeOperatori, key) {
                    if (singleCompetenzeOperatori.id == $scope.modelsSelCompetenzaOperatore[obj.id]) {
                        tmpCompetenzaOperatore = singleCompetenzeOperatori;
                    }
                })

                var tmpMacchine = null;
                angular.forEach($scope.Macchine, function (singleMacchina, key) {
                    if (singleMacchina.id == $scope.modelsSelMacchine[obj.id]) {
                        tmpMacchine = singleMacchina;
                    }
                })

                var tmpCompetenzaMacchine = null;
                angular.forEach($scope.CompetenzeMacchine, function (singleCompetenzeMacchine, key) {
                    if (singleCompetenzeMacchine.id == $scope.modelsSelCompetenzaMacchine[obj.id]) {
                        tmpCompetenzaMacchine = singleCompetenzeMacchine;
                    }
                })

                // creazione degli oggetti
                var tmpFaseManodopera = {
                    operatoreCompetenza: tmpCompetenzaOperatore,
                    causaleFase: obj.id,
                    tempo: tmpTempoUomo
                }
                var tmpFaseMacchina = {
                    macchina: tmpMacchine,
                    macchinaCompetenza: tmpCompetenzaMacchine,
                    causaleFase: obj.id,
                    tempo: tmpTempoMacchina
                }

                tmpTempoTotaleManodopera = {
                    causaleFase: obj.id,
                    totale: tmpTempoUomo
                }

                tmpTempoTotaleMacchina = {
                    causaleFase: obj.id,
                    totale: tmpTempoMacchina
                }

                // add sull'array
                manodopera.push(tmpFaseManodopera);
                macchine.push(tmpFaseMacchina);

            });

            // chiedere ad Alberto per questa cosa dei tempi totali
            totaleTempiManodopera = tmpTempoTotaleManodopera;
            totaleTempiMacchina = tmpTempoTotaleMacchina;

            tmpDistintaBaseFase.macchine = macchine;
            tmpDistintaBaseFase.totaleTempiMacchina = totaleTempiMacchina;
            tmpDistintaBaseFase.manodopera = manodopera;
            tmpDistintaBaseFase.totaleTempiManodopera = totaleTempiManodopera;

            replacePhase($scope.currentItem.fasi, tmpDistintaBaseFase);
            //aggiornamento dell'albero
            createOrRefreshTreeView();
        }



        function replacePhase(objRoot, toBeReplaced) {  // $scope.currentItem.fasi, tmpDistintaBaseFase
            var arrFasi = objRoot;
            var objRoot = objRoot;
            angular.forEach(arrFasi, function (fase, key) {
                if (fase.id == toBeReplaced.id) {
                    objRoot[key] = toBeReplaced;
                    arrFasi = orderByAndCompact(arrFasi, "numeroFase");
                    $scope.numeroFase = toBeReplaced.numeroFase;            // WARNING
                    return;
                }
                if (objRoot[key].fasi != null)
                    replacePhase(objRoot[key].fasi, toBeReplaced);
            })
        }
        function orderByAndCompact(items, field) {
            items.sort(function (a, b) {
                return (a[field] > b[field] ? 1 : -1);
            });
            for (i = 0; i < items.length; i++) {
                items[i].numeroFase = i + 1;
            }
            return items;
        };



        $scope.selectedNode = null;

        // quando ho selezionato un ramo
        $scope.showSelected = function (sel) {

            // inizializzazione dei models
            resetAllFields();

            if (sel.id != 0) {
                $scope.selectedNode = sel;
                $scope.numeroFase = parseInt(sel.numeroFase);
                $scope.id = sel.id;
                $scope.nomeFase = sel.nome;
                $scope.codiceFase = sel.codice;
                $scope.descrizioneFase = sel.descrizione;
                $scope.type = sel.type;
                $scope.isAndon = sel.isAndon;
                $scope.isAutomatica = sel.isAutomatica;
                $scope.abilitaAvanzamenti = sel.abilitaAvanzamenti;
                $scope.isCiclica = sel.isCiclica;

                $scope.IsVisibleFaseCiclica = (sel.type == "0");
                $scope.abilitaMultimedia = sel.abilitaMultimedia;
                $scope.abilitaMateriali = sel.abilitaMateriali;
                $scope.abilitaVersamentiManuali = sel.abilitaVersamentiManuali;
                $scope.abilitaVersamentiAutomatici = sel.abilitaVersamentiAutomatici;
                $scope.stileDiVisualizzazioneVersamenti = sel.stileDiVisualizzazioneVersamenti;
                $scope.abilitabarcode = sel.abilitabarcode;
                $scope.abilitaLetturaRfid = sel.abilitaLetturaRfid;
                $scope.abilitascritturaRfid = sel.abilitascritturaRfid;
                $scope.abilitaAvanzamentiStar = sel.abilitaAvanzamentiStar;
                $scope.abilitaAvanzamentiStop = sel.abilitaAvanzamentiStop;
                $scope.abilitaAvanzamentiPausa = sel.abilitaAvanzamentiPausa;
                $scope.visibilitastazione = sel.visibilitastazione;
                $scope.abilitaPulsanteEmergenza = sel.abilitaPulsanteEmergenza;
                // array per abilitazione macchine e competenze macchine
                $scope.isEnabledMacchinaSelection = {};
                $scope.isEnabledcompentenzaMacchineSelection = {};

                // abilitazione pannello dettagli fase
                $scope.isEnabledDettagliFase = true;
                $scope.isEnabledLinkedOption = false;

                angular.forEach($scope.CausaliFase, function (obj, key) {
                    $scope.isEnabledMacchinaSelection[obj.id] = true;
                });

                fillPhaseItems($scope.currentItem.fasi, sel.id)
            }
            else $scope.selectedNode = null;

        };

        function fillPhaseItems(objRoot, phaseId) {  // $scope.currentItem.fasi, tmpDistintaBaseFase
            var arrFasi = objRoot;
            var objRoot = objRoot;
            angular.forEach(arrFasi, function (fase, key) {
                if (fase.id == phaseId) {
                    fillItemsInView(objRoot[key])
                    return;
                }
                if (objRoot[key].fasi != null)
                    fillPhaseItems(objRoot[key].fasi, phaseId);
            })
        }



        // importazione da operazioni ciclo
        $scope.selectionOperazioneCiclo = function (id) {
            $scope.isEnabledLinkedOption = false;
            angular.forEach($scope.OperazioniCiclo, function (fase, key) {
                if (fase.id == id) {
                    fillItemsInView(fase);
                    $scope.isEnabledLinkedOption = true;
                }
            });

        }



        // funzione per cancellare un ramo dell'albero
        $scope.deleteFase = function () {
            if ($scope.selectedNode)
                execDeleteFase($scope.currentItem.fasi, $scope.selectedNode.id);
        }
        function execDeleteFase(node, idToDelete) {
            angular.forEach(node, function (fase, key) {
                if (fase.id == idToDelete) {
                    node.splice(key, 1);
                    node = orderByAndCompact(node, "numeroFase");   // WARNING
                    resetAllFields();
                    return;
                }
                if (node[key].fasi != null)
                    execDeleteFase(node[key].fasi, idToDelete);
            })
            createOrRefreshTreeView();
        }



        //funzione per popolare gli elementi della finestra di dettaglio quando seleziono un ramo dal treeview (tempi) o seleziono la voce dalla ddl delle operazioni ciclo
        function fillItemsInView(item) {
            angular.forEach($scope.CausaliFase, function (obj, key) {

                $scope.isEnabledMacchinaSelection[obj.id] = true;
                $scope.isEnabledcompentenzaMacchineSelection[obj.id] = true;

                // valore selezionato per le competenze operatore
                if (item.manodopera[obj.id].operatoreCompetenza != null) {
                    $scope.modelsSelCompetenzaOperatore[obj.id] = item.manodopera[obj.id].operatoreCompetenza.id;
                }

                // recupero il tempo dell'operatore per ogni causale e popolo i vari input text
                var tmpDoubleOperatore = item.manodopera[obj.id].tempo;
                $scope.modelsOreUomo[obj.id] = parseInt(tmpDoubleOperatore / 3600);
                $scope.modelsMinutiUomo[obj.id] = parseInt((tmpDoubleOperatore % 3600) / 60);
                $scope.modelsSecondiUomo[obj.id] = parseInt(tmpDoubleOperatore % 60);

                // popolo le drop down list con il valore selezionato
                if (item.macchine[obj.id].macchina == null) {
                    $scope.modelsSelMacchine[obj.id] = -1;
                    $scope.isEnabledMacchinaSelection[obj.id] = false;
                }
                else
                    $scope.modelsSelMacchine[obj.id] = item.macchine[obj.id].macchina.id;


                if (item.macchine[obj.id].macchinaCompetenza == null) {
                    $scope.modelsSelCompetenzaMacchine[obj.id] = -1;
                    $scope.isEnabledcompentenzaMacchineSelection[obj.id] = false;
                }
                else
                    $scope.modelsSelCompetenzaMacchine[obj.id] = item.macchine[obj.id].macchinaCompetenza.id;

                // caso in cui ho a null tutti i gli oggetti macchine e competenze macchinari
                if (item.macchine[obj.id].macchina == null && item.macchine[obj.id].macchinaCompetenza == null)
                    $scope.isEnabledMacchinaSelection[obj.id] = true;

                // recupero il tempo della macchina per ogni causale e popolo i vari input text
                var tmpDoubleMacchina = item.macchine[obj.id].tempo;
                $scope.modelsOreMacchina[obj.id] = parseInt(tmpDoubleMacchina / 3600);
                $scope.modelsMinutiMacchina[obj.id] = parseInt((tmpDoubleMacchina % 3600) / 60);
                $scope.modelsSecondiMacchina[obj.id] = parseInt(tmpDoubleMacchina % 60);
            });


        }

        // aggiornamento dell'albero
        function createOrRefreshTreeView() {
            var c = angular.copy($scope.dataForTheTree);
            $scope.defaultExpanded = [];
            $scope.dataForTheTree[0].fasi = [];
            if (($scope.currentItem !== null) && ($scope.currentItem.fasi !== null)) {
                var arrayOrdered = orderById($scope.currentItem.fasi, "numeroFase", false);
                var parentNode = 0;
                var i = 0;
                $scope.dataForTheTree[0].fasi = arrayOrdered;
            }
            $scope.defaultExpanded = [$scope.dataForTheTree[0].fasi[0], $scope.dataForTheTree[0].fasi[1]];
        }



        // ordinamento di json per campo richiesto
        function orderById(items, field, reverse) {
            var filtered = [];
            angular.forEach(items, function (item) {
                filtered.push(item);
            });
            filtered.sort(function (a, b) {
                return (a[field] > b[field] ? 1 : -1);
            });
            if (reverse) filtered.reverse();
            return filtered;
        };


        // funzione provvisoria per gestire solo un pulsante di salvataggio
        $scope.globalChange = function () {
            $scope.confirmFase();
        }

        // funzione per resettare tutti i campi
        function resetAllFields() {
            $scope.modelsOreUomo = {};
            $scope.modelsMinutiUomo = {};
            $scope.modelsSecondiUomo = {};
            $scope.modelsSelCompetenzaOperatore = {};
            $scope.modelsOreMacchina = {};
            $scope.modelsMinutiMacchina = {};
            $scope.modelsSecondiMacchina = {};
            $scope.modelsSelCompetenzaMacchine = {};
            $scope.modelsSelMacchine = {};

            $scope.modelOperazioniCiclo = {};
            $scope.numeroFase = null;
            $scope.nomeFase = null;
            $scope.codiceFase = null;
            $scope.descrizioneFase = null;
            $scope.id = null;

            $scope.isEnabledDettagliFase = null;

            $scope.pcurrentItem = null; // Clear "distinta-base-parametri" (Dettaglio)
        }


        //=====================================================================================================================================



        //------------------------------------------------------------------------------------------------------------------------------
        // distinta-base-parametri
        //------------------------------------------------------------------------------------------------------------------------------
        $scope.pdtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(5)
            .withOption('stateSave', true)
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);


        // Combo box Tipi Dato
        $scope.cbTipiDato = null;
        DataFactory.GetTipiDato().then(function (response) {
            var xs = response.data;
            angular.forEach(xs, function (x) {
                //x.value = x.value.toLowerCase();
                x.value = x.value.charAt(0).toLowerCase() + x.value.slice(1);
            })
            $scope.cbTipiDato = xs;
        });

        //----------------------


        $scope.pIsFullScreenOnEdit = false;
        //$scope.pitems = [];
        $scope.pcurrentItem = null;
        $scope.pviewState = Enums.ViewState['R'];
        $scope.ptitolo = "Dettaglio";
        $scope.pdtInstance = {};
        $scope.originalPCurrentItem = [];
        $scope.beforeCrudItems = null;

        $scope.ptitle = function () { pdoTitle($scope, Enums); };
        function pdoTitle($scope, Enums) {
            switch ($scope.pviewState) {
                case Enums.ViewState['R']:
                    $scope.ptitolo = "Dettaglio ";// + $scope.currentItem.codice;
                    break;
                case Enums.ViewState['U']:
                    $scope.ptitolo = "Modifica ";// + $scope.currentItem.codice;
                    break;
                case Enums.ViewState['C']:
                    $scope.ptitolo = "Inserimento ";
                    break;
            }
        };


        $scope.pinsert = function () {
            $scope.pviewState = Enums.ViewState['C'];
            $scope.pcurrentItem = new Object();
            $scope.originalPCurrentItem = [];
            $scope.beforeCrudItems = angular.copy($scope.currentItem.distintaBaseParametri);
        };


        $scope.pupdate = function () {
            $scope.pviewState = Enums.ViewState['U'];
            $scope.originalPCurrentItem.push(angular.copy($scope.pcurrentItem));
            $scope.beforeCrudItems = angular.copy($scope.currentItem.distintaBaseParametri);
        };

        $scope.pcancel = function () {
            $scope.pviewState = Enums.ViewState['R'];
            if ($scope.beforeCrudItems != null) {
                $scope.currentItem.distintaBaseParametri = angular.copy($scope.beforeCrudItems);
                $scope.beforeCrudItems = null;
            }
            $scope.originalPCurrentItem = [];
        };


        $scope.pdelete = function (item) {
            $scope.pviewState = Enums.ViewState['D'];
            var itemToRemove = $scope.pcurrentItem.codice;
            var allParams = $scope.currentItem.distintaBaseParametri;
            for (var i = 0; i < allParams.length; i++) {
                if (allParams[i].codice == itemToRemove) {
                    allParams.splice(i, 1);
                    i = allParams.length;
                }
            }
            $scope.pviewState = Enums.ViewState['R'];
            $scope.originalPCurrentItem = [];
            angular.element(document.querySelector('#pmodalConfirmDelete')).modal('hide');
            $scope.beforeCrudItems = null;
        };


        $scope.pconfirm = function () {
            if ($scope.pviewState === Enums.ViewState['C']) {
                if ($scope.currentItem.distintaBaseParametri == null)
                    $scope.currentItem.distintaBaseParametri = [];

                var dupli = $scope.currentItem.distintaBaseParametri.filter(function (item) {
                    return item.codice === $scope.pcurrentItem.codice;
                })[0];
                if (dupli != null) {
                    angular.element(document.querySelector('#modalWarning1')).modal('toggle');  //("Codice duplicato")
                    return;
                }
                if ($scope.pcurrentItem.codice == null || $scope.pcurrentItem.nome == null || $scope.pcurrentItem.tipo == null) {
                    angular.element(document.querySelector('#modalWarning2')).modal('toggle');  //("Inserire Codice, Nome, Tipo dato")
                    return;
                }
                try {
                    var um = $.grep($scope.ListaUnitaDiMisura, function (e) { return e.id == $scope.pcurrentItem.unitaDiMisura.id; }); //LLLLL
                    $scope.pcurrentItem.unitaDiMisura.item = um[0];
                } catch (err) { };

                $scope.currentItem.distintaBaseParametri.push($scope.pcurrentItem)
            }
            else if ($scope.pviewState === Enums.ViewState['U']) {
                var oldCodice = $scope.originalPCurrentItem[$scope.originalPCurrentItem.length - 1].codice;
                var actCodice = $scope.pcurrentItem.codice;

                var dupli = $scope.currentItem.distintaBaseParametri.filter(function (item) {
                    return item.codice === $scope.pcurrentItem.codice;
                });
                if (dupli.length > 1) {
                    angular.element(document.querySelector('#modalWarning1')).modal('toggle');  //("Codice duplicato")  
                    if (oldCodice != actCodice) {
                        $scope.pcurrentItem.codice = oldCodice;
                    }
                    return;
                }
                try {
                    var um = $.grep($scope.ListaUnitaDiMisura, function (e) { return e.id == $scope.pcurrentItem.unitaDiMisura.id; });
                    $scope.pcurrentItem.unitaDiMisura.item = um[0];
                } catch (err) { };
            }
            $scope.originalPCurrentItem = [];
            $scope.pviewState = Enums.ViewState['R'];
            $scope.beforeCrudItems = null;
        };


        $scope.pshowItemDetail = function (item) {
            if ($scope.pviewState != Enums.ViewState['R'])
                return;
            $scope.pcurrentItem = item;
            if ($scope.pviewState == Enums.ViewState['U'] && !itemExists($scope.originalPCurrentItem, item.codice))
                $scope.originalPCurrentItem.push(angular.copy($scope.pcurrentItem));
        };
        function itemExists(arr, value) {
            var r = false
            angular.forEach(arr, function (item) {
                if (item.codice === value)
                    r = true;
            });
            return r;
        }



        $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
            var x = $scope;
        });




        $scope.$watch('pviewState', function (newValue, oldValue) {
            if (newValue === oldValue) return;
            $scope.ptitle();
        });


        //------------------------------------------------------------------------------------------------------------------------------
        // distinta-base-parametri
        //------------------------------------------------------------------------------------------------------------------------------


        function setTotaleTempi(_scope, item) {
            if (_scope.tothh == null) _scope.tothh = 0;
            if (_scope.totmm == null) _scope.totmm = 0;
            if (_scope.totss == null) _scope.totss = 0;

            var totseconds = 0;
            var minuteseconds = 0;
            var seconds = 0;
            var fasi = item.fasi;
            angular.forEach(fasi, function (fase) {
                if (fase.manodopera != null) {
                    var mans = fase.manodopera;
                    angular.forEach(mans, function (man) {
                        totseconds += man.tempo;
                    })
                }
                if (fase.macchine != null) {
                    var macs = fase.macchine;
                    angular.forEach(macs, function (mac) {
                        totseconds += mac.tempo;
                    })
                }
            });

            _scope.tothh = Math.floor(totseconds / 3600);
            _scope.totmm = Math.floor((totseconds - (_scope.tothh * 3600)) / 60);
            _scope.totss = Math.floor((totseconds - (_scope.tothh * 3600) - (_scope.totmm * 60)));
        }

        $scope.crudException = function (err) {
            $scope.exceptionMessageText = err.data.exceptionMessage;
            angular.element(document.querySelector('#modalWarning1')).modal('toggle');
        };


    }]);





app.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                });
            }
        }
    }
});


// direttiva per il menu con il tasto destro del mouse
app.directive("contextMenu", function ($compile) {
    contextMenu = {};
    contextMenu.restrict = "AE";
    contextMenu.link = function (lScope, lElem, lAttr) {
        lElem.on("contextmenu", function (e) {
            e.preventDefault(); // default context menu is disabled
            //  The customized context menu is defined in the main controller. To function the ng-click functions the, contextmenu HTML should be compiled.
            lElem.append($compile(lScope[lAttr.contextMenu])(lScope));
            // The location of the context menu is defined on the click position and the click position is catched by the right click event.
            //$("#contextmenu-node").css("left", e.clientX);
            //$("#contextmenu-node").css("top", e.clientY);
        });
        lElem.on("mouseleave", function (e) {
            // on mouse leave, the context menu is removed.
            if ($("#contextmenu-node"))
                $("#contextmenu-node").remove();
        });
    };
    return contextMenu;



});

