﻿// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('TurniDiLavoroCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$translate', '$rootScope',
    function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $translate, $rootScope) {

        $scope.IsFullScreenOnEdit = false;
        $scope.items = [];
        $scope.currentItem = null;
        $scope.viewState = Enums.ViewState['R'];
        $scope.titolo = "Dettaglio";
        $scope.dtInstance = {};
        $scope.exceptionMessageText = "";
        $translate.use($scope.$parent.globals.currentUser.lingua);

        DataFactory.getMacchine().then(function (response) {

            $scope.macchine = response.data;
        });

        // DataTables configurable options
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(5)
            .withOption('stateSave', true)
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        $scope.showItemDetail = function (item) {

            basicCrud_doShowItemDetail($scope, $http, subUrlApi_Turni, Enums, item);

            // metodo per far visualizzare la data;
            if ($scope.currentItem) {
                //inizializzo array
                if ($scope.currentItem.macchine == null) {
                    $scope.currentItem.macchine = [];
                }
                var anno1 = $scope.currentItem.validoDal.substring(0, 4);
                var mese1 = parseInt($scope.currentItem.validoDal.substring(5, 7)) - 1;
                var giorno1 = parseInt($scope.currentItem.validoDal.substring(8, 10));

                var anno2 = $scope.currentItem.validoAl.substring(0, 4);
                var mese2 = parseInt($scope.currentItem.validoAl.substring(5, 7)) - 1;
                var giorno2 = parseInt($scope.currentItem.validoAl.substring(8, 10));


                if (anno1 != "1901" && anno1 != "0001")
                    $scope.currentItem.validoDal = new Date(anno1, mese1, giorno1);
                else
                    $scope.currentItem.validoDal = "";

                if (anno2 != "1901" && anno2 != "0001")
                    $scope.currentItem.validoAl = new Date(anno2, mese2, giorno2);
                else
                    $scope.currentItem.validoDal = "";

                //ciclo tutte le macchine 
                angular.forEach($scope.currentItem.macchine, function (macchina, key) {
                    //check tutte le macchine che sono state salvate nel item precedentemente
                    var $check = $("#" + macchina.id);
                    $check.prop("checked", true);})
            }
        };

        $scope.reload = function () { basicCrud_doReload($scope, $http, subUrlApi_Turni); };

        $scope.checkedAllDays = function () {
            $scope.currentItem.flagLunedi = true;
            $scope.currentItem.flagMartedi = true;
            $scope.currentItem.flagMercoledi = true;
            $scope.currentItem.flagGiovedi = true;
            $scope.currentItem.flagVenerdi = true;
            $scope.currentItem.flagSabato = true;
            $scope.currentItem.flagDomenica = true;
        };

        $scope.insert = function () {
            $scope.viewState = Enums.ViewState['C'];
            $scope.currentItem = new Object();
            $scope.currentItem.tipologia = new Object();
            $scope.currentItem.macchine = new Object();
        };
            
        $scope.update = function () {
            $scope.viewState = Enums.ViewState['U'];
        };

        $scope.cancel = function () {
            $scope.viewState = Enums.ViewState['R'];

            //pulisco tutte le check a false
            angular.forEach($scope.macchine, function (macchina, key) {
                var $check = $("#" + macchina.id);
                $check.prop("checked", false);
            })

           };

        $scope.confirm = function () {

            //azzero array
            $scope.currentItem.macchine = [];
            //ciclo tutte le checkbox macchine 
            angular.forEach($scope.macchine, function (macchina, key) {
                //se la check è selezionata inserisco nell'array
                var $check = $("#" + macchina.id);
                if ($check.prop("checked")) {
                    $scope.currentItem.macchine.push(macchina);
                }
                //poi le azzero tutte 
                $check.prop("checked", false);
                })

            //if ($scope.currentItem.validoDal)
            //    $scope.currentItem.validoDal = fixDate($scope.currentItem.validoDal);
            //if ($scope.currentItem.validoAl)
            //    $scope.currentItem.validoAl = fixDate($scope.currentItem.validoAl);
            basicCrud_doConfirm($scope, $http, subUrlApi_Turni, Enums);
        };

        function convertTZ(date, tzString) {
            return new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", { timeZone: tzString }));
        }

        $scope.delete = function (item) {
            $scope.viewState = Enums.ViewState['D'];
            basicCrud_doDelete($scope, $http, subUrlApi_Turni, Enums, item)
            //pulisco tutte le check a false
            angular.forEach($scope.macchine, function (macchina, key) {
                var $check = $("#" + macchina.id);
                $check.prop("checked", false);
            })
        };

        $scope.title = function () { basicCrud_doTitle($scope, Enums, ($scope.currentItem != null ? $scope.currentItem.username : null)); };

        basicCrud_doInitWatch($scope);

        $scope.reload();

        $scope.crudException = function (err) {
            $scope.exceptionMessageText = err.data.exceptionMessage;
            angular.element(document.querySelector('#modalWarning1')).modal('toggle');
        };

        InitUtils($scope);
        //// funzione per restituire un numero formattato con due cifre decimali. Esempio: 9 => 09
        //$scope.getFormattedTT = function (value) {
        //    var intValue = 0;
        //    if (typeof (value) === 'number') intValue = value;
        //    else {
        //        intValue = parseInt(value);
        //    }
        //    if (intValue <= 9) return "0" + intValue;
        //    else return intValue;
        //}

        function fillPhaseItems(objRoot, phaseId) {  // $scope.currentItem.fasi, tmpDistintaBaseFase
            var arrFasi = objRoot;
            var objRoot = objRoot;
            angular.forEach(arrFasi, function (fase, key) {
                if (fase.id == phaseId) {
                    fillItemsInView(objRoot[key])
                    return;
                }
                if (objRoot[key].fasi != null)
                    fillPhaseItems(objRoot[key].fasi, phaseId);
            })
        }

    }
]);