﻿app.controller('ServiceCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$location', '$rootScope',
    function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $location, $rootScope) {
        $scope.IsFullScreenOnEdit = true;
        $rootScope.signalRconnection = $.cookie("SigalRc");
        urlApi = $.cookie("WebApiC");
        $scope.viewState = Enums.ViewState['R'];

        $scope.title = function () { basicCrud_doTitle($scope, Enums); };
        $scope.filterIdStato = "-1";
        $scope.lUser = $rootScope.globals.currentUser.username;

        basicCrud_doInitWatch($scope);
        $scope.items = [];
        $scope.currentItem = null;
        $scope.FormaddMateriali = null;
        $scope.idMachine = -1;
        $scope.currentStatus = null;
        $scope.dtInstance = {};
        $scope.dtInstance02 = {};
        $scope.dtInstance03 = {};
        $scope.Tipomanutenzioni = [];
        $scope.InitFilter = getParameterByName("i");
        $scope.ViewAsInModifica = ($scope.InitFilter == 'inModifica');
        $scope.ViewAsInAttesa = ($scope.InitFilter == 'inAttesa');
        $scope.ViewAsInLavorazione = ($scope.InitFilter == 'inLavorazione');
        $scope.ViewAsTerminato = ($scope.InitFilter == 'terminato');


        //---------------------------------------------------------------
        $scope.Fermi_GraphPie = [];
        $scope.Fermi_GraphPie.push({ key: 'Olio', y: 300 });
        $scope.Fermi_GraphPie.push({ key: 'Lama', y: 100 });
        $scope.Fermi_GraphPie.push({ key: 'Guarnizione', y: 10 });


        // DataTables configurable options
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('stateSave', true)
            .withOption('order', [[1, 'desc']])
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        // DataTables configurable options
        $scope.dtOptions02 = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('stateSave', true)
            .withOption('order', [[1, 'desc']])
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        // DataTables configurable options
        $scope.dtOptions03 = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('stateSave', true)
            .withOption('order', [[1, 'desc']])
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);


        //---------------------------------------------------------------
        //Scarico le macchine 
        DataFactory.getMacchine().then(function (response) {
            $scope.Macchine = jQuery.grep(response.data, function (a) { return a });
        });

        //---------------------------------------------------------------
        //Scarico i tipi di manutenzione
        DataFactory.getTipomanutenzioni().then(function (response) {
            $scope.Tipomanutenzioni = jQuery.grep(response.data, function (a) { return a });
        });

         //---------------------------------------------------------------
        //Scarico i tipi di stati 
        DataFactory.GetStatiBatch().then(function (response) {
            $scope.ListaStatiBatch = response.data;
        });

        // funzione per popolare il combo box delle aziende
        $scope.listaAziende = null;
        DataFactory.GetAziende().then(function (response) {
            $scope.listaAziende = response.data;
        });

        // funzione per popolare il combo box delle aziende
        DataFactory.GetCompetenzeManutenzione().then(function (response) {
            $scope.CompetenzeManutenzione = response.data;
        });

        // funzione per popolare il combo box delle aziende
        DataFactory.GetTipiManutenzione().then(function (response) {
            $scope.TipiManutenzione = response.data;
        });

        // funzione per popolare il combo box delle aziende
        DataFactory.GetTipiGuastiManutenzione().then(function (response) {
            $scope.TipiGuastiManutenzione = response.data;
        });

        // funzione per popolare il combo box delle aziende
        DataFactory.GetTipoSegnalazioneManutenzione().then(function (response) {
            $scope.TipoSegnalazioneManutenzione = response.data;
        });

        $scope.reload = function () {
            basicCrud_doReload($scope, $http, subUrlBatchService, filter);
        };

       

     //---------------------------------------------------------------
     //crudException
        $scope.crudException = function (err) {
            $scope.exceptionMessageText = err.exceptionMessage;
            angular.element(document.querySelector('#modalWarning1')).modal('toggle');
        };

  //---------------------------------------------------------------



        
        //---------------------------------------------------------------

        $scope.showItemDetail = function (item) {
            $scope.currentStatus = item.stato;
            basicCrud_doShowItemDetail($scope, $http, subUrlBatchService, Enums, item);

        };

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        $scope.insert = function () {
            $scope.viewState = Enums.ViewState['C'];
            $scope.currentItem = new Object();
            $scope.isEnableWrite = true;
            $scope.currentStatus = "inModifica";
            $scope.currentItem.operatore = $scope.lUser;
            $scope.currentItem.materiali = [];
            $scope.currentItem.note = []
            ResetAllFields();
        };
        //---------------------------------------------------------------
        //---------------------------------------------------------------

        $scope.addMaterial = function () {

            closeModal('#modalAddMaterial');
            var arrayText = { quantita: $scope.FormaddMateriali.pezziDaProdurre, materiale: $scope.FormaddMateriali.codiceMateriale, operatore: $scope.lUser };
            $scope.currentItem.materiali.push(arrayText);
            $scope.FormaddMateriali.pezziDaProdurre = 1;
            $scope.FormaddMateriali.codiceMateriale = "";
        };
       

  //---------------------------------------------------------------
        //---------------------------------------------------------------

        $scope.dtInstanceCallback = function () {
            //
        }

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        $scope.changeStatus = function (item, state) {
            if (item === null) return;


            if (state === 1) {

                item.stato = "inLavorazione";
            }
            else if (state === 2) {

                item.stato = "inLavorazioneInPausai";
            }
            else if (state === 3) {

                item.stato = "terminato";
            }


            $scope.viewState = Enums.ViewState['C'];
            //item.stato = "1";
            $scope.currentItem = item;
            //$scope.currentItem.userLocked = $rootScope.globals.currentUser.username;

            basicCrud_doConfirm($scope, $http, subUrlBatchService, Enums);
        }


        //---------------------------------------------------------------
        //---------------------------------------------------------------
        $scope.update = function (event, item) {



            $scope.showItemDetail(item);

            if ($scope.currentItem != null) {



                var a = $scope.currentItem.macchina.item;
                $scope.currentItem.macchina = a;
                $scope.viewState = Enums.ViewState['U'];
            }


        };
        //---------------------------------------------------------------
        //---------------------------------------------------------------
        $scope.cancel = function () {
            $scope.viewState = Enums.ViewState['R']; ResetAllFields(); closeModal('#modalConfirmUndo');
        };
        //---------------------------------------------------------------
        //---------------------------------------------------------------
        $scope.refresh = function () {
            var objTemp = { id: $scope.currentItem.id };
            $scope.currentItem = { id: "reloading", Codice: "reloading" };
            basicCrud_doShowItemDetail($scope, $http, subUrlLine, Enums, objTemp, function (item) {
                $scope.showItemDetail($scope.currentItem);
                $scope.viewState = Enums.ViewState['U'];

                angular.forEach($scope.trcurrentItem, function (it) {
                    $scope.trshowItemDetail(it);
                })

            });

        }
        //---------------------------------------------------------------
        //---------------------------------------------------------------
        $scope.confirmAndClose = function () {
            basicCrud_doConfirm($scope, $http, subUrlBatchService, Enums);


        };
        //---------------------------------------------------------------
        //---------------------------------------------------------------
        $scope.crudException = function (err) {
            $scope.exceptionMessageText = err.exceptionMessage;
            angular.element(document.querySelector('#modalWarning1')).modal('toggle');
        };

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        $scope.delete = function (item) {
            $scope.viewState = Enums.ViewState['D'];
            basicCrud_doDelete($scope, $http, subUrlBatchService, Enums)

        };
        //---------------------------------------------------------------
        //---------------------------------------------------------------
        $scope.title = function () { basicCrud_doTitle($scope, Enums); };
        //---------------------------------------------------------------
        //---------------------------------------------------------------
        basicCrud_doInitWatch($scope);
        //---------------------------------------------------------------
        //---------------------------------------------------------------
        $scope.reload();


        //---------------------------------------------------------------
        //---------------------------------------------------------------
        function closeModal(id) {
            angular.element(document.querySelector(id)).modal('hide');
        }

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        function filter() {
            if ($scope.InitFilter) {

                $scope.items = $.grep($scope.items, function (e) {

                    var matchCriteria = false;

                    if (($scope.MainFilter === null) ||
                        ($scope.MainFilter.filterByCompanyId === null) ||
                        ($scope.MainFilter.filterByCompanyId === "")) {
                        matchCriteria = true; // caso in cui non sia stata selezionata alcuna voce di tendina Azienda
                    }
                    else if ((e.azienda !== null) && (e.azienda.id !== null) && (e.azienda.id === $scope.MainFilter.filterByCompanyId))
                        matchCriteria = true;


                    matchCriteria = matchCriteria && e.stato.startsWith($scope.InitFilter);

                    return matchCriteria;
                });
            }
        }

        //---------------------------------------------------------------

        $scope.reload();
        //---------------------------------------------------------------
        // ordinamento di json per campo richiesto
        function orderById(items, field, reverse) {
            var filtered = [];
            angular.forEach(items, function (item) {
                filtered.push(item);
            });
            filtered.sort(function (a, b) {
                return (a[field] > b[field] ? 1 : -1);
            });
            if (reverse) filtered.reverse();
            return filtered;
        };

        //---------------------------------------------------------------
        //---------------------------------------------------------------

        //inizializzazione grafico a torta
        $scope.Fermi_GraphPie_Option = {
            chart: {
                type: 'pieChart',
                height: 400,
                width: 350,
                x: function (d) { return d.key + ' (' + d.y + ')'; },
                y: function (d) { return d.y; },
                showLabels: false,
                duration: 500,
                labelThreshold: 0.01,
                labelSunbeamLayout: true,
                color: ['#ff7f0e', 'gray', 'green', 'orange', 'blue', 'red'],
                legend: {
                    margin: {
                        top: 5,
                        right: 35,
                        bottom: 0,
                        left: 0
                    }
                }
            }
        };


        // funzione per resettare tutti i campi
        function ResetAllFields() {

            //ResetDistintaCicli();


            //$scope.pcurrentItem = null;
            //$scope.parametersData = null;
            //$scope.nodesTableArr = null;
            //$scope.AnalisiData = null;
            //$scope.trcurrentItem = [];
            //$scope.data_multiBarChart = [{ key: '', values: [{ x: 0, y: 0 }] }];
        }


        //---------------------------------------------------------------
        //---------------------------------------------------------------
    
        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        // Definizione del filtro di visualizzazione in griglia
        function filter() {
            if ($scope.InitFilter) {
                $scope.items = $.grep($scope.items, function (e) {
                    return e.stato.startsWith($scope.InitFilter);
                });
            }
        }

        //---------------------------------------------------------------
        //---------------------------------------------------------------

        function generateData() {
            var yCount = $scope.trcurrentItem.length;
            var res = [];
            for (n = 0; n < $scope.trcurrentItem.length; n++) {
                var paramItem = $scope.trcurrentItem[n];
                res.push({ key: paramItem.nome, values: paramItem.data });
            }

            if (res.length > 0)
                return res;
            else
                return [{ key: '', values: [{ x: 0, y: 0 }] }];
        }

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        $scope.toggleStatus = false;

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        $scope.toggleDropdown = function (node, scopeNodes) {
            node.toggleStatus = node.toggleStatus == true ? false : true;
            $scope.toggleStatus = node.toggleStatus;
            $scope.toggleDropdownHelper(node.id, $scope.toggleStatus, scopeNodes);
        };

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        $scope.toggleDropdownHelper = function (parentNodeId, toggleStatus, scopeNodes) {
            for (var i = 0; i < scopeNodes.length; i++) {
                node = scopeNodes[i];
                if (node.parentId == parentNodeId) {
                    if (toggleStatus == false)
                        $scope.toggleDropdownHelper(node.id, toggleStatus, scopeNodes);
                    scopeNodes[i].isShow = toggleStatus;
                }
            }
        };

    }]);


app.directive('date', function (dateFilter) {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {

            var dateFormat = attrs['date'] || 'yyyy-MM-dd';

            ctrl.$formatters.unshift(function (modelValue) {
                return dateFilter(modelValue, dateFormat);
            });
        }
    };
})





