﻿

// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('TabelleTipologieMacchineCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', '$translate',
function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, $translate) {

        $scope.IsFullScreenOnEdit = false;
        $scope.items = [];
        $scope.currentItem = null;
        $scope.viewState = Enums.ViewState['R'];
        $scope.titolo = "Dettaglio";
        $scope.dtInstance = {};
       
        $translate.use($scope.$parent.globals.currentUser.lingua);

        // DataTables configurable options
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(5)
            .withOption('stateSave', true)
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        $scope.showItemDetail = function (item) { basicCrud_doShowItemDetail($scope, $http, subUrlApi_TabelleTipoMacchina, Enums, item); };

        $scope.reload = function () { basicCrud_doReload($scope, $http, subUrlApi_TabelleTipoMacchina); };

        $scope.insert = function () {
            $scope.viewState = Enums.ViewState['C'];
            $scope.currentItem = null;
        };

        $scope.update = function () {  $scope.viewState = Enums.ViewState['U'];  };

        $scope.cancel = function () {  $scope.viewState = Enums.ViewState['R'];  };

        $scope.confirm = function () { basicCrud_doConfirm($scope, $http, subUrlApi_TabelleTipoMacchina, Enums); };

        $scope.delete = function (item) { $scope.viewState = Enums.ViewState['D']; basicCrud_doDelete($scope, $http, subUrlApi_TabelleTipoMacchina, Enums, item) };

        $scope.title = function () { basicCrud_doTitle($scope, Enums); };

        basicCrud_doInitWatch($scope);

        $scope.reload();

    }]);

