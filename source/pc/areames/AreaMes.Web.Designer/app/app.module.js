﻿var urlApi = "http://localhost:9000/api";
var urlLanguageDataTable = './app/localization/datatable/Italian.txt';
var subUrlApi_Macchina = "macchina";
var subUrlApi_TabelleTipoMacchina = "TipiMacchina";
var subUrlApi_MacchinaCompetenza = "macchinacompetenza";
var subUrlApi_Operatore = "operatore";
var subUrlApi_OperatoreCompetenza = "operatorecompetenza";
var subUrlApi_UnitaDiMisura = "unitadimisura";
var subUrlApi_Parametri = "parametri";
var subUrlApi_CausaliRiavvio = "causaliriavvio";
var subUrlApi_TipoValori = "tipovalori";
var subUrlApi_Materiali = "materiale";
var subUrlOperazioniCiclo = "operazioniciclo";
var subUrlDistintaBase = "distintabase";
var subUrlBatch = "batch";
var subUrlApi_Enums = "enum";
var subUrlApi_Dashboard = "dashboard";
var subUrlBatchService = "service";
var subUrlApi_Aziende = "azienda";
var subUrlApi_Turni = "TurniDiLavoro";
var subUrlApi_ManutenzioneCompetenza = "manutenzionecompetenza";
var subUrlApi_TipiManutenzione = "TipiManutenzione";
var subUrlApi_TipiSegnalazioneManutenzione = "TipoSegnalazioneManutenzione";
var subUrlApi_TipiGuastiManutenzione = "TipiGuastiManutenzione";

// action specificche per caricare le enum
var subUrlAction_GetTipiDato = "GetTipiDato";
var subUrlAction_GetUmList = "GetUmList";
var subUrlAction_GetLivelliAccesso = "GetLivelliAccesso";
var subUrlAction_GetTipiMateriale = "GetTipiMateriale";
var subUrlAction_GetCausaliFase = "GetCausaliFase";
var subUrlAction_GetStatiDistintaBase = "GetStatiDistintaBase";
var subUrlActionGetDistinteBasiFromMateriale = "GetDistinteBasiFromMateriale";
var subUrlAction_GetStatiBatch = "GetStatiBatch";
var subUrlAction_GetTranslations = "GetTranslations";
var subUrlAction_GetTipoLingua = "GetTipoLingua";
var subUrlAction_GetAziende = "GetAziende";
var subUrlAction_GetTipoValori = "GetTipoValori";
var subUrlAction_GetTipoConteggio = "GetTipoConteggio";
var subUrlAction_GetBatchGraph = "getAziendegetBatchGraphValues?tickInterval=";
var subUrlAction_GetBatchByOdp = "GetByOdp";
var customerName = "AreaMes";
var subUrlAction_Login = "Login";

//variabili per gestione enum 
var eventType_ScrapPiece = "qta_pzsca";
var typeValue_Prod_Cau_Scarto = "prod_cau_scarto";

//var app = angular.module("AppMacchine", ['$rootScope', '$location', '$cookies', '$http', '$window', 'pascalprecht.translate'])
var app = angular.module("AppMacchine", ['datatables', 'angular.chosen', 'ngCookies', 'treeControl', 'nvd3', 'pascalprecht.translate', 'ngJustGage','dx'])
.run(function ($rootScope, $location, $cookies, $http, $window) {

    /*
    * Function to 'Angular-fy' dynamically loaded content
    * by JQuery. This compiles the new html code and injects it
    * into the DOM so Angular 'knows' about the new code.
    */
    try{
        var Ling= JSON.parse($cookies.get("MesAuth"));
        var lingua = Ling.currentUser.lingua;
        if (lingua == null) {
            lingua = "it";
        }
        urlLanguageDataTable = './app/localization/datatable/' +lingua+'.txt';
    }
    catch (e) { }

    $window.angularfy = function (target, newHtml) {
        // must use JQuery to query for element by id.
        // must wrap as angular element otherwise 'scope' function is not defined.
        var targetScope = angular.element($(target)).scope();

        //        elem.replaceWith( $compile( newHtml )(elemScope)); Displays html wrong.
        //        elem.html( $compile( newHtml )(elemScope)); Does not work.
        $(target)['html']($compile(newHtml)(targetScope)); // Does work!
        targetScope.$apply();
    }

        // keep user logged in after page refresh
        $rootScope.globals = $cookies.getObject('MesAuth');
        if ($rootScope.globals!= null) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        
        }
        var x = $window.location.href;
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if (x.indexOf('/login') == -1 && !$rootScope.globals) {
                $window.location.href = 'login.html';
            }
        });
});


app.config(function ($httpProvider) {
    $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.cache = false;
});

//var translations = {
//    HEADLINE: 'What an awesome moduerewrewrewrewrewrewrewrewrewle!',
//    PARAGRAPH: 'Srsly!'
//};

//app.config(['$translateProvider', function ($translateProvider) {
//    // add translation table
//    $translateProvider.translations('en', translations);
//    $translateProvider.preferredLanguage('en');
//}]);



function basicCrud_doTitle($scope, Enums, itemName) {

    switch ($scope.viewState) {
        case Enums.ViewState['R']:
            $scope.titolo = "Dettaglio " + ((itemName == null) ? $scope.currentItem.codice : itemName);
            break;
        case Enums.ViewState['U']:
            $scope.titolo = "Modifica " + ((itemName == null) ? $scope.currentItem.codice : itemName);
            break;
        case Enums.ViewState['C']:
            $scope.titolo = "Inserimento ";
            break;
    }
};

function basicCrud_doReload($scope, $http, subUrlApi, filterCallback) {
    $http.get(urlApi + '/' + subUrlApi).then(
        function (data)
        {
            $scope.items = [];
            $scope.items = data.data;
            if (filterCallback)
                filterCallback($scope.items);
        },
        function (e)
        {
            console.exception(e);
     
            if (e.data)
                console.exception(e.data.exceptionMessage);
        }
    );
}

function basicCrud_doConfirm($scope, $http, subUrlApi, Enums) {
    $http.post(urlApi + '/' + subUrlApi, $scope.currentItem)
               .then(function (data) {
                   if ($scope.viewState === Enums.ViewState['C']) {
                       $scope.currentItem = data.data;
                       $scope.reload();
                   }
                   else if ($scope.viewState === Enums.ViewState['U']) {
                       var elementPos = $scope.items.map(function (x) { return x.id; }).indexOf($scope.currentItem.id);
                       $scope.items[elementPos] = $scope.currentItem;
                   }

                   $scope.viewState = Enums.ViewState['R'];
               },function (e) {
                   console.exception(e);
                   if (e.data) console.exception(e.data.exceptionMessage);
                   $scope.crudException(e);
               });
}

function basicCrud_doConfirmWithOutReload($scope, $http, subUrlApi, Enums) {
    $http.post(urlApi + '/' + subUrlApi, $scope.currentItem)
               .then(function (data) {
                   if ($scope.viewState === Enums.ViewState['C']) {
                       $scope.currentItem = data.data;
                       //$scope.reload();
                       $scope.reload();
                   }
                   else if ($scope.viewState === Enums.ViewState['U']) {
                       var elementPos = $scope.items.map(function (x) { return x.id; }).indexOf($scope.currentItem.id);
                       $scope.items[elementPos] = $scope.currentItem;
                   }

                   //$scope.viewState = Enums.ViewState['R'];
               },function (e) {
                   console.exception(e);
                   $scope.crudException(e)
               });
}

function basicCrud_doConfirmWithForcedReload($scope, $http, subUrlApi, Enums) {
    $http.post(urlApi + '/' + subUrlApi, $scope.currentItem)
               .then(function (data) {
                   closeModal('#modalConfirmChangeState');
                   if ($scope.viewState === Enums.ViewState['C']) {
                       $scope.currentItem = data.data;
                   }
                   else if ($scope.viewState === Enums.ViewState['U']) {
                       var elementPos = $scope.items.map(function (x) { return x.id; }).indexOf($scope.currentItem.id);
                       $scope.items[elementPos] = $scope.currentItem;
                   }

                   $scope.viewState = Enums.ViewState['R'];
                   $scope.reload();
               },function (e) {
                   console.exception(e);
                   if (e.data) console.exception(e.data.exceptionMessage);

                   $scope.crudException(e)
               });
}



function basicCrud_doShowItemDetail($scope, $http, subUrlApi, Enums, item, onSuccessCallback) {
    if (($scope.currentItem == null) || (item.id !== $scope.currentItem.id)) {
        $http.get(urlApi + '/' + subUrlApi + '/' + item.id)
            .then(function (data) {
                $scope.currentItem = data.data;
                $scope.viewState = Enums.ViewState['R'];
                if (onSuccessCallback != null)
                    onSuccessCallback($scope.currentItem);
            },function(e) {
                console.exception(e);
            $scope.crudException(e)
            });
    }
}

function basicCrud_doInitWatch($scope) {
    $scope.$watch('viewState', function (newValue, oldValue) {
        if (newValue === oldValue) return;
        $scope.title();
    });
    $scope.$watch('currentItem', function (newValue, oldValue) {
        if (newValue === oldValue) return;
        $scope.title();
    });
}

function basicCrud_doDelete($scope, $http, subUrlApi, Enums) {
    $http.delete(urlApi + '/' + subUrlApi + '/' + $scope.currentItem.id)
               .then(function (data) {
                   $scope.reload();
                   $scope.viewState = Enums.ViewState['R'];
                   angular.element(document.querySelector('#modalConfirmDelete')).modal('hide');
               },function (e) {
                   console.log(e);
                   if (e.data) console.log(e.data.exceptionMessage);

                   $scope.crudException(e)
               });
}

function closeModal(id) {
    angular.element(document.querySelector(id)).modal('hide');
}

// funzione per fixare il problema delle date nel caso in cui ci sia un il GMT che viene sempre calcolato quando si fa a fare il toString
// la data viene tornata incrementata/decrementata del tempo del GMT. In questo modo quando viene calcolato il GMT non si hanno problemi di cambio di orario.
function fixDate(data) {
    var app = "";
    if (data != '0001 - 01 - 01T00: 00: 00')
        app = data.toISOString();
    //var app = data.toISOString().substring(0, 10);
    if (data && app != "0001-01-01") {
        dataReturn = new Date(data.getTime() - (data.getTimezoneOffset() * 60000));
        return dataReturn;
    }
}

function fixDateAppoggio(data) {
    //var dataReturn = new Date();
    if (data && data.substring(0, 10) != "0001-01-01") {
        dataReturn = new Date(data.getTime() + (data.getTimezoneOffset() * 60000));
        return dataReturn;
    }
}

// Attivazione della classe utility
function InitUtils($scope) {

    // funzione per restituire un numero formattato con due cifre decimali. Esempio: 9 => 09
    $scope.getFormattedTT = function (value) {
        var intValue = 0;
        if (typeof (value) === 'number')
            intValue = value;
        else
            intValue = parseInt(value);
        
        if (intValue <= 9) return "0" + intValue;
        else return intValue;
    }

    // ...


    // ...


    // ...
}

