﻿
//document.write('<script src="js/jquery-1.10.2.min.js"></script>');
document.write('<script src="js/jquery.3.5.1.js"></script>');
//document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>');

//document.write('<script src="js/jquery-migrate-1.2.1.min.js"></script>');
document.write('<script src="js/jquery-migrate-1.4.min.js"></script>');
//------------------------------------------------------------------------
//document.write('<script src="js/bootstrap.min.js"></script>');
document.write('<script src="js/bootstrap.4.5.3.js"></script>');

document.write('<script src="js/modernizr.min.js"></script>');
document.write('<script src="js/jquery.sparkline.min.js"></script>');
document.write('<script src="js/toggles.min.js"></script>');
document.write('<script src="js/retina.min.js"></script>');
document.write('<script src="js/jquery.cookies.js"></script>');         

//------------------------------------------------------------------------
// Alberto.p, 06/03/2021, aggiornamento 
// ultima versione v1.10.23
document.write('<script type="text/javascript" charset="utf8" src="js/datatables/datatables.min.js"></script>');
// oppure versioni precedenti:
//      document.write('<script type="text/javascript" charset="utf8" src="js/jquery.dataTables.js"></script>');
document.write('<script type="text/javascript" charset="utf8" src="js/fnDisplayStart.js"></script>');
document.write('<script type="text/javascript" charset="utf8" src="js/fnFindCellRowIndexes.js"></script>');
//------------------------------------------------------------------------

document.write('<script src="js/dropzone.min.js"></script>');
document.write('<script src="js/custom.js"></script>');


document.write('<script src="js/angular.1.8.2.js"></script>');
//document.write('<script src="js/angular.min.js"></script>');

//document.write('<script src="js/angular-datatables.min.js"></script>');
document.write('<script src="js/angular-datatables06.min.js"></script>');
document.write('<script src="js/angular-cookies.min.js"></script>');
document.write('<script src="js/angular-tree-control.js"></script>');
document.write('<script src="js/contextMenu.js"></script>');


document.write('<script src="js/justgage.js"></script>');
document.write('<script src="js/ng-justgage.js"></script>');

//document.write('<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-animate.js"></script>');
//document.write('<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-sanitize.js"></script>');

document.write('<script src="js/angular-animate.js"></script>');
document.write('<script src="js/angular-sanitize.js"></script>');


//document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js" charset="utf-8"></script>');
//document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/nvd3/1.8.1/nv.d3.min.js"></script>');
//document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-nvd3/1.0.5/angular-nvd3.min.js"></script>');
//document.write('<script src="js/d3.min.js" charset="utf-8"></script>');   //sostituita da A
//document.write('<script src="js/nv.d3.min.js"></script>');                //sostituita da B
//document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.js"></script>');        //sostituzione di A OK!!!
//document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/5.5.0/d3.js"></script>');        //sostituzione di A  KO!!!
//document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/nvd3/1.8.6/nv.d3.js"></script>');   //sostituzione di B errori
document.write('<script src="js/d3_3.5.6.js"></script>');           //up to date
document.write('<script src="js/angular-nvd3.min.js"></script>');   //up to date
document.write('<script src="js/nv.d3.min_1.7.0.js"></script>');    //sostituzione di B OK!!


//document.write('<script src="js/datetimepicker.js"></script>');
document.write('<script src="js/jquery.datatables.extensions.js"></script>');


//------------------------------------------------------------------------
// Alberto.p, 06/03/2021, aggiornamento 
document.write('<script src="js/ui-bootstrap-tpls-3.0.6.min.js"></script>');
//document.write('<script src="js/ui-bootstrap-tpls-2.1.4.js"></script>');
//------------------------------------------------------------------------

document.write('<script src="js/chosen.jquery.min.js"></script>');
document.write('<script src="js/angular-chosen.min.js"></script>');
document.write('<script src="js/angular-translate.js"></script>');

document.write('<script src="app/app.module.js"></script>');
document.write('<script src="app/controllers/base.controller.js"></script>');
document.write('<script src="app/services/base.service.js"></script>');
document.write('<script src="js/dx.all.js"></script>');
//document.write('<script src="https://cdn3.devexpress.com/jslib/20.2.6/js/localization/dx.messages.it.js"></script>');
//document.write('<script src="js/bootstrap-italia.min.js"></script>');

