﻿
var app = angular.module("appCfg", ['datatables', 'angular.chosen', 'ngCookies', 'treeControl']);


app.controller('cfgCtrl', ['$scope', '$rootScope', '$cookies', function ($scope, $rootScope, $cookies) {


    $scope.serverIP = "localhost";
    $scope.webApi = "9000";
    $scope.inactivityTimeout = 2147483647;

    $scope.confirm = function () {
        $.cookie("WebApiC", "http://" + $scope.serverIP + ":" + $scope.webApi + "/api", { expires: 70000});
        $.cookie("InactivityTimeout", $scope.inactivityTimeout, { expires: 70000  });
        
        window.location.href = "login.html";
    }
}]);

