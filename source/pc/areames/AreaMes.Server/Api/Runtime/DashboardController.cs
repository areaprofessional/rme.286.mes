﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using AreaMes.Model;
using AreaMes.Model.Dto;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;

namespace AreaMes.Server.Api.Runtime
{
    public class DashboardController : ApiController
    {

        private static Dictionary<string, int> _oeePrev = new Dictionary<string, int>();

        public static Dashboard _Dashboard;

        public async Task<Dashboard> Get(string companyId)
        {

            var d = new Dashboard();

            try
            {
                if (MongoRepository.Instance.IsRun == false)
                    return d;

                //var filterBatch = Builders<Batch>.Filter.Where(o => o.Stato != StatoBatch.Eliminato
                //                                                    && o.Stato != StatoBatch.Terminato
                //                                                    && o.Stato != StatoBatch.TerminatoAbortito);
                //var res = await MongoRepository.Instance.Collection<Batch>().FindAsync(filterBatch).Result.ToListAsync();

                var filterBatch = BatchManager.Instance.BatchInMemory();
                if (!String.IsNullOrWhiteSpace(companyId) && companyId != "null")
                    filterBatch = filterBatch.Where(o => o.Azienda?.Id == companyId).ToList();


                d.Ordini = filterBatch.Select(o => new Batch
                {
                    Id = o.Id,
                    Commessa = o.Commessa,
                    PezziDaProdurre = o.PezziDaProdurre,
                    PercCompletamento = o.PercCompletamento,
                    OdP = o.OdP,
                    OdPParziale = o.OdPParziale,
                    OdV = o.OdV,
                    Inizio = o.Inizio,
                    TotaleFermi = o.TotaleFermi,
                    OEE = o.OEE,
                    Stato = o.Stato,
                    Materiale = MongoRepository.Instance.GetAsync<Materiale>(o.Materiale.Id).Result,
                    OdV_DataConsegna = o.OdV_DataConsegna,
                    PezziProdotti = o.PezziProdotti,
                    PezziScartati = o.PezziScartati,
                    Fasi = o.Fasi
                }).ToList();


                //lock (this)
                //{
                //    foreach (var o in d.Ordini)
                //    {
                //        int oo = 0;
                //        if (!_oeePrev.TryGetValue(o.Id, out oo))
                //            _oeePrev.Add(o.Id, o.OEE);
                //        else
                //            _oeePrev[o.Id] = o.OEE;

                //        o.OEEPrecedente = oo;

                //        if (o.OEEPrecedente < o.OEE)
                //            o.OEEAndamento = AndamentoValore.InSalita;
                //        else if (o.OEEPrecedente > o.OEE)
                //            o.OEEAndamento = AndamentoValore.InDiscesa;
                //        else o.OEEAndamento = AndamentoValore.Stabile;

                //        //var fasiInPausa = o.Fasi.Where(o1 => o1.Stato == StatoFasi.InLavorazioneInPausa || o1.Stato == StatoFasi.InLavorazioneInPausaS);
                //        //var fasiInAllarme = o.Fasi.Where(o1 => o1.Stato == StatoFasi.InLavorazioneInEmergenza);

                //        //if (fasiInPausa.Any())
                //        //    o.Stato = StatoBatch.InLavorazioneInPausa;
                //    }
                //}




                d.StatoOrdini = new Dictionary<StatoBatch, int>();
                d.StatoOrdini.Add(StatoBatch.InPausa, d.Ordini.Count(o => o.Stato == StatoBatch.InPausa || o.Stato == StatoBatch.InLavorazioneInPausa));
                //d.StatoOrdini.Add(StatoBatch.InModifica, res.Count(o => o.Stato == StatoBatch.InModifica));
                d.StatoOrdini.Add(StatoBatch.InAttesa, d.Ordini.Count(o => o.Stato == StatoBatch.InAttesa));
                d.StatoOrdini.Add(StatoBatch.InLavorazione, d.Ordini.Count(o => o.Stato == StatoBatch.InLavorazioneInEmergenza || o.Stato == StatoBatch.InLavorazione));
                d.OrdiniInAllarme = d.Ordini.Count(o => o.Stato == StatoBatch.InLavorazioneInEmergenza);
                d.MacchinariAttiviOraPrec = 1;
                d.OperatoriAttivi = SessionManager.Instance.Count();

                d.OEEMedia = Convert.ToInt32(d.Ordini.DefaultIfEmpty(new Batch() { OEE = 0 }).Average(o => o.OEE));
                d.ConsumoEnergiaMezzora = new Dictionary<string, double>();

                //for (int i = 0; i <= 24; i++)
                //{
                //    string sT = String.Empty;
                //    if (i <= 9) sT += "0";
                //    sT += i.ToString();
                //    d.ConsumoEnergiaMezzora.Add(sT + ":00", i * 2);
                //    d.ConsumoEnergiaMezzora.Add(sT + ":30", i * 2 * 2);
                //}

                //var rnd = new Random();
                //for (int i = 0; i <= 48 * 2; i++)
                //    d.ConsumoEnergiaMezzora.Add(i.ToString(), rnd.Next(60, 70));


                _Dashboard = d;
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }



            return d;

        }


    }
}
