﻿
using AreaMes.Model;
using AreaMes.Model.Design;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using AreaMes.Server.Api.Design;
using AreaMes.Server.MongoDb;
using AreaMes.Server.SignalR;
using AreaMes.Server.Utils;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Runtime
{
    [RoutePrefix("api/RuntimeTransportContext")]
    public class RuntimeTransportContextController : ApiController, IRuntimeTransportContextController
    {

        private static RuntimeTransportContextController _instance;
        public static RuntimeTransportContextController Instance { get { return _instance; } }
        static RuntimeTransportContextController()
        {
            _instance = new RuntimeTransportContextController();
            MessageServices.Instance.Subscribe<SetBatchInfo>("SetBatchInfo", async (o, e) =>
            {
                await _instance.SetBatch2(e);
            });

        }



        [Route("GetSfo")]
        [HttpGet]
        public List<Batch> GetSfo(string idStato, string Username, string idStazione)
        {
            if (!MongoRepository.Instance.IsRun)
                return null;

            List<Batch> batchList = AreaMes.Server.BatchManager.Instance.BatchInMemory();

            if (batchList == null)
                return null;

            if (idStato != "-1")
            {
                StatoBatch statusFilter = (StatoBatch)Enum.Parse(typeof(StatoBatch), idStato, true);
                batchList = batchList.Where(o => o.Stato == statusFilter).ToList();

            }
            else
            {
                // filtro i batch per gli stati
                batchList = batchList.Where(o => o.Stato != StatoBatch.Eliminato &&
                                                 o.Stato != StatoBatch.Terminato &&
                                                 o.Stato != StatoBatch.TerminatoAbortito &&
                                                 o.Stato != StatoBatch.InModifica).ToList();
            }

            // filtro per stazione
            if (!String.IsNullOrEmpty(idStazione) && idStazione != "99" && idStazione != "0")
            {
                batchList = batchList.Where(o => o.Fasi.Any(a =>
                        (!String.IsNullOrWhiteSpace(a.DistintaBaseFase.Visibilitastazione) && a.DistintaBaseFase.Visibilitastazione.Split(',').Any(x => x == idStazione)))).ToList();
            }

            // ricreazione dell'oggetto semplificato
            batchList = batchList.Select(o =>
                new Batch
                {
                    Id = o.Id,
                    Inizio = o.Inizio,
                    Fine = o.Fine,
                    DistintaBase = o.DistintaBase,
                    Fasi = o.Fasi,
                    PercCompletamento = o.PercCompletamento,
                    OdP = o.OdP,
                    OdPParziale = o.OdPParziale,
                    OdV = o.OdV,
                    OdV_DataConsegna = o.OdV_DataConsegna,
                    OdV_DescrizioneCliente = o.OdV_DescrizioneCliente,
                    CreateDate = o.CreateDate,
                    Commessa = o.Commessa,
                    Stato = o.Stato,
                    OEE = o.OEE,
                    OEEAndamento = o.OEEAndamento,
                    OEEPrecedente = o.OEEPrecedente,
                    PezziDaProdurre = o.PezziDaProdurre,
                    PezziProdotti = o.PezziProdotti,
                    PezziScartati = o.PezziScartati,
                    UserLocked = o.UserLocked,
                    UserLockedDate = o.UserLockedDate,
                    Revisione = o.Revisione,
                }).ToList();


            // ----------------------------------------
            // Gestione operatore / login
            var lComp = MongoRepository.Instance.Collection<Operatore>().FindAsync(Builders<Operatore>.Filter.Eq("Username", Username)).Result.FirstOrDefault();
            if (lComp != null)
                lComp.OperatoreCompetenza?.Resolve();

            // Solo se l'utente non è super admin allora vengono applicati i filtri
            // per visualizzare i batch che possono essere gestiti dall'operatore
            if ((!String.IsNullOrEmpty(Username) && (!lComp.AreaDiSicurezza_IsSuperAdmin)))
                batchList = batchList.Where(o => o.Fasi != null).Where(o => o.Fasi.Any(a =>
                   (a.DistintaBaseFase.Manodopera == null) ||
                   (a.DistintaBaseFase.Manodopera.All(b => b.OperatoreCompetenza == null) ||
                   (a.DistintaBaseFase.Manodopera != null && a.DistintaBaseFase.Manodopera.Any(x => x.OperatoreCompetenza?.Id == lComp?.OperatoreCompetenza?.Id))))).ToList();
            // ----------------------------------------

            return batchList;
        }

        [Route("GetSfoOdp")]
        [HttpGet]
        public List<string> GetSfoOdp(string idStato, string Username, string idStazione)
        {
            if (!MongoRepository.Instance.IsRun)
                return null;

            List<Batch> batchList = AreaMes.Server.BatchManager.Instance.BatchInMemory();

            if (batchList == null)
                return null;

            if (idStato != "-1")
            {
                StatoBatch statusFilter = (StatoBatch)Enum.Parse(typeof(StatoBatch), idStato, true);
                batchList = batchList.Where(o => o.Stato == statusFilter).ToList();

            }
            else
            {
                // filtro i batch per gli stati
                batchList = batchList.Where(o => o.Stato != StatoBatch.Eliminato &&
                                                 o.Stato != StatoBatch.Terminato &&
                                                 o.Stato != StatoBatch.TerminatoAbortito &&
                                                 o.Stato != StatoBatch.InModifica).ToList();
            }

            // filtro per stazione
            if (!String.IsNullOrEmpty(idStazione) && idStazione != "99" && idStazione != "0")
            {
                batchList = batchList.Where(o => o.Fasi.Any(a =>
                        (!String.IsNullOrWhiteSpace(a.DistintaBaseFase.Visibilitastazione) && a.DistintaBaseFase.Visibilitastazione.Split(',').Any(x => x == idStazione)))).ToList();

            }

            // ricreazione dell'oggetto semplificato
            batchList = batchList.Select(o =>
                new Batch
                {
                    Id = o.Id,
                    OdP = o.OdP,
                }).ToList();


            // ----------------------------------------
            // Gestione operatore / login
            var lComp = MongoRepository.Instance.Collection<Operatore>().FindAsync(Builders<Operatore>.Filter.Eq("Username", Username)).Result.FirstOrDefault();
            if (lComp != null)
                lComp.OperatoreCompetenza?.Resolve();

            // Solo se l'utente non è super admin allora vengono applicati i filtri
            // per visualizzare i batch che possono essere gestiti dall'operatore
            if ((!String.IsNullOrEmpty(Username) && (!lComp.AreaDiSicurezza_IsSuperAdmin)))
                batchList = batchList.Where(o => o.Fasi != null).Where(o => o.Fasi.Any(a =>
                       (a.DistintaBaseFase.Manodopera == null) ||
                       (a.DistintaBaseFase.Manodopera.All(b => b.OperatoreCompetenza == null) ||
                       (a.DistintaBaseFase.Manodopera != null && a.DistintaBaseFase.Manodopera.Any(x => x.OperatoreCompetenza?.Id == lComp?.OperatoreCompetenza?.Id))))).ToList();
            // ----------------------------------------
            List<string> batchListString = new List<string>();
            foreach (var item in batchList)
            {
                batchListString.Add(item.OdP);
            }

            return batchListString;
        }


        [Route("GetSfoFromMachine")]
        [HttpGet]
        public List<Batch> GetSfoFromMachine(string idMacchina, string idStato, string Username)
        {
            if (idStato != "-1")
            {
                StatoBatch MyStatus = (StatoBatch)Enum.Parse(typeof(StatoBatch), idStato, true);
                var filterBatch = Builders<Batch>.Filter.Eq("Stato", MyStatus) &
                                  Builders<Batch>.Filter.Eq("Fasi.Macchine.Macchina._id", idMacchina);
                var batchList = MongoRepository.Instance.Collection<Batch>().FindAsync(filterBatch).Result.ToList();


                if (DashboardController._Dashboard != null)
                    foreach (var item in batchList)
                    {
                        var firstOrDefault = DashboardController._Dashboard.Ordini.FirstOrDefault(o => o.Id == item.Id);
                        if (firstOrDefault != null)
                            item.PercCompletamento = firstOrDefault.PercCompletamento;
                    }


                return batchList;
            }
            else
            {
                // filtro i batch per gli stati
                var filterBatch = Builders<Batch>.Filter.Where(o => o.Stato != StatoBatch.Eliminato && o.Stato != StatoBatch.Terminato) &
                                  Builders<Batch>.Filter.Eq("Fasi.Macchine.Macchina._id", idMacchina);
                var batchList = MongoRepository.Instance.Collection<Batch>().FindAsync(filterBatch).Result.ToList();

                if (DashboardController._Dashboard != null)
                    foreach (var item in batchList)
                    {
                        var firstOrDefault = DashboardController._Dashboard.Ordini.FirstOrDefault(o => o.Id == item.Id);
                        if (firstOrDefault != null)
                            item.PercCompletamento = firstOrDefault.PercCompletamento;
                    }
                return batchList;
            }
        }



        [Route("GetFasiFromBatch")]
        [HttpGet]
        public List<BatchFase> GetFasiFromBatch(string idBatch, string Username)
        {
            // recupero le eventuali distinte basi per quella macchina
            var batch = AreaMes.Server.BatchManager.Instance.Get(idBatch);

            if (batch == null)
                return null;

            if (batch.Fasi == null)
                return null;

            List<BatchFase> listOfFasi = batch.Fasi.OrderBy(o => o.NumeroFase).ToList();

            // ----------------------------------------
            // Gestione operatore / login
            var lComp = MongoRepository.Instance.Collection<Operatore>().FindAsync(Builders<Operatore>.Filter.Eq("Username", Username)).Result.FirstOrDefault();
            if (lComp != null)
                lComp.OperatoreCompetenza?.Resolve();

            if (!String.IsNullOrEmpty(Username))
                listOfFasi = listOfFasi.Where(o =>
                            (o.DistintaBaseFase.Manodopera == null) ||
                            (o.DistintaBaseFase.Manodopera.All(b => b.OperatoreCompetenza == null) ||
                            o.DistintaBaseFase.Manodopera.Any(y => y.OperatoreCompetenza?.Id == lComp?.OperatoreCompetenza?.Id))).ToList();

            // ----------------------------------------

            return listOfFasi;
        }

        [ActionName("GetMaterialeFilter")]
        [HttpGet]
        public List<Materiale> GetMaterialeFilter(string Categoria)
        {
            return MongoRepository.Instance.All<Materiale>().Result.Where(x => x.Categoria == Categoria).ToList();
            //return result;

        }

        [Route("GetBatch")]
        [HttpGet]
        public Batch GetBatch(string idBatch, string Username)
        {
            var batch = AreaMes.Server.BatchManager.Instance.Get(idBatch);
            if (batch != null && batch?.Eventi != null)
                foreach (var item in batch.Eventi)
                {
                    if (item.Macchina?.Id != null)
                        item.Macchina.Resolve();
                    if (item.Operatore?.Id != null)
                        item.Operatore.Resolve();
                    if (item.TipoValore?.Id != null)
                        item.TipoValore.Resolve();
                    if (item.UnitaDiMisura?.Id != null)
                        item.UnitaDiMisura.Resolve();
                    if (item.Materiale?.Id != null)
                        item.Materiale.Resolve();
                }

            if (batch.Fermi != null)
            {
                batch.Fermi.ForEach(fermo =>
                {
                    if (fermo?.CausaliRiavvio != null)
                        fermo?.CausaliRiavvio.Resolve();
                });

            }


            foreach (var item in batch.DistintaBase.Fasi)
            {
                if (item.Macchine.Count > 0 && item.Macchine[0]?.Macchina != null)
                    item.Macchine[0]?.Macchina.Resolve();
            }

            return batch;
        }

        [Route("SuspendBatch")]
        [HttpPost]
        public async Task<string> SospendiBatch(SuspendBatchInfo arguments)
        {
            Logger.Default.Info("SospendiBatch...");
            var batchId = arguments.BatchId;
            var username = arguments.Operatore?.Username ?? "";
            Logger.Default.Info($"batchId={batchId}; username={username};");
            Batch batch = BatchManager.Instance.Get(batchId);
            if (batch is null) { return string.Empty; }
            Logger.Default.Info($"[{batch.OdP}] looking for open phases...");
            var openFasi = batch.Fasi.Where(f => f.Stato != StatoFasi.Terminato && f.Stato != StatoFasi.InAttesa);
            Logger.Default.Info($"[{batch.OdP}] looking for open phases... {openFasi.Count()} found");
            foreach (var openFase in openFasi.ToArray()) //ToArray crea una copia e evita l' errore di modifica dell'oggetto iterato
            {
                SetFaseInfo openFaseInfo = new SetFaseInfo()
                {
                    idBatch = batchId,
                    idStato = StatoFasi.InSospensione,
                    fase = openFase,
                    Username = username
                };
                Logger.Default.Info($"[{batch.OdP}] set phase with number={openFase.NumeroFase}");
                await SetFase2(openFaseInfo);
            }
            batch.StatoPrecedente = batch.Stato;
            batch.Stato = StatoBatch.InSospensione;
            AreaMESManager.Instance.NotifyBatchChanged(batch.Id, batch);
            await BatchManager.Instance.RemoveAsync(batchId);

            return batchId;
        }

        /// <summary>
        /// Set dello stato per la fase scelta
        /// </summary>
        /// <param name="faseInfo"> oggetto contente l'id del batch e la fase</param>
        /// <returns></returns>
        [Route("SetFase2")]
        [HttpPost]
        public async Task<string> SetFase2(SetFaseInfo faseInfo)
        {
            var batch = BatchManager.Instance.Get(faseInfo.idBatch);
            try
            {
                Logger.Default.InfoFormat($"Start SetFase2 OdP:'{batch.OdP}'...");
                string loggedUser = faseInfo.Username;
                var previousState = batch.Stato;
                BatchFase fase = faseInfo.fase;
                var IndexFase = batch.Fasi.IndexOf(fase);
                var countFasi = 0;
                var operatore = faseInfo.fase.Manodopera[0].Operatore;
                var macchina = faseInfo.fase.Macchine[0].Macchina;
                string idBatch = faseInfo.idBatch;
                var bav = new FaseChangedInfo { Username = faseInfo.Username };
                var isBatchChanged = false;
                var batchCorrentiInLavorazione = BatchManager.Instance.BatchInMemory().Where(o => BatchUtils.IsInLavorazione(o) && o.Id != idBatch);

                if (batch.Fasi != null)
                    countFasi = batch.Fasi.Count;
                string idMacchina = "";
                if (macchina != null)
                    idMacchina = macchina.Id;
                var turno = await TurniDiLavoroController.GetCalcolaTurno(DateTime.Now, idMacchina);

                Logger.Default.InfoFormat($"SetFase OdP:'{batch.OdP}',faseInfo.idStato:'{faseInfo.idStato}'");
                if (faseInfo.idStato != null)
                {
                    Logger.Default.InfoFormat($"SetFase OdP:'{batch.OdP}',faseInfo.fase.Stato:'{faseInfo.fase.Stato}',faseInfo.idStato.Value:'{faseInfo.idStato.Value}'");
                    faseInfo.fase.Stato = faseInfo.idStato.Value;
                }

                lock (batch)
                {
                    var res = MessageServices.Instance.PublishAndWaitAll(batch.Id, new FaseChangedInfoBefore { Current = faseInfo }, batch).GetAwaiter().GetResult();
                    foreach (var exc in res)
                        throw exc;

                    var faseInMemory = batch.Fasi.FirstOrDefault(o => o.NumeroFase == fase.NumeroFase);

                    if (fase.Stato == StatoFasi.InLavorazione)
                    {
                        var macchinaPerFaseCorrente = faseInMemory.Macchine.FirstOrDefault();
                        if ((macchinaPerFaseCorrente == null) || (String.IsNullOrEmpty(macchinaPerFaseCorrente?.Macchina?.Codice)))
                        {
                            var batchCorrentInLavorazione = batchCorrentiInLavorazione.FirstOrDefault();
                        }

                        if (!String.IsNullOrWhiteSpace(faseInMemory.BloccoAvanzamentoCausa))
                            throw new Exception(faseInMemory.BloccoAvanzamentoCausa);
                    }

                    if (fase.Stato == StatoFasi.InLavorazione && faseInMemory.Stato != StatoFasi.InLavorazioneInPausa)
                    {
                        var filterOp = Builders<Operatore>.Filter.Eq("Username", fase.UserLocked);
                        var op = MongoRepository.Instance.Collection<Operatore>().Find<Operatore>(filterOp).FirstOrDefault();

                        var x = fase.Manodopera.Select(o => { o.Inizio = DateTime.Now; o.Operatore = op; return o; }).ToList();
                        fase.Manodopera = x;
                        foreach (var item in fase.Macchine)
                            if (!item.Inizio.HasValue)
                                item.Inizio = DateTime.Now;

                        if (fase.Inizio.IsNull())
                            fase.Inizio = DateTime.Now;
                    }

                    if (fase.Stato == StatoFasi.Terminato || fase.Stato == StatoFasi.Abortito)
                        fase.Fine = DateTime.Now;

                    if (Convert.ToBoolean(fase.DistintaBaseFase?.AbilitaFaseTerminataParzialmente))
                    {
                        if (fase?.PezziParziali > 0)
                        {
                            fase.PezziProdotti += fase.PezziParziali;
                            var idPhase = batch.DistintaBase.Fasi.FirstOrDefault(x => x.NumeroFase == fase.NumeroFase).Id;
                            var newitem = new Evento();
                            newitem.Id = ObjectId.GenerateNewId().ToString();
                            newitem.TipoEvento = Model.Enums.TipoEvento.qta_pzparz;
                            newitem.OraEvento = DateTime.Now.ToLocalTime();
                            newitem.UltimoAggiornamento = DateTime.Now;
                            newitem.FaseId = idPhase;
                            newitem.Source = Source.manual;
                            newitem.BatchId = batch.Id;
                            if (faseInfo.Username != null)
                            {
                                var mongoInstance = MongoRepository.New;
                                var listOfOperatori = mongoInstance.All<Operatore>(new string[] { }).Result.ToList().FirstOrDefault(x => x.Username == faseInfo.Username);
                                if (listOfOperatori != null) newitem.Operatore = new ExternalDoc<Operatore> { Id = listOfOperatori.Id };
                            }

                            newitem.Valore = Convert.ToString(fase.PezziParziali);
                            var result = SetEvents(new SetEventsRequest { Events = new List<Evento> { newitem }, IdBatch = batch.Id, IdFase = idPhase });
                            fase.PezziParziali = 0;
                        }
                    }
                    // AVANZAMENTI FASI
                    if (batch.BatchAvanzamenti == null) batch.BatchAvanzamenti = new List<BatchAvanzamento>();
                    var av = batch.BatchAvanzamenti.FirstOrDefault(a => a.NumeroFase == fase.NumeroFase && a.Fine == null);
                    Logger.Default.InfoFormat($"SetFase OdP:'{batch.OdP}',fase.NumeroFase:'{fase.NumeroFase}',fase.DistintaBaseFase.Codice:'{fase.DistintaBaseFase.Codice}',fase.Stato:'{fase.Stato.ToString()}'");
                    Logger.Default.InfoFormat($"SetFase OdP:'{batch.OdP}',av:'{av}'");
                    if (av == null)
                    {
                        bav.Previous = batch.BatchAvanzamenti.LastOrDefault(x => x.NumeroFase == fase.NumeroFase); // viene agganciato l'avanzamento precedente;
                        av = CreateAvanzamento(faseInfo, fase, operatore, macchina, turno);
                        batch.BatchAvanzamenti.Add(av);
                        bav.Current = av;
                    }
                    else
                    {
                        av.Fine = DateTime.Now;
                        bav.Previous = av; // viene agganciato l'avanzamento precedente; 
                                           // creazione di un nuovo avanzamento 
                        BatchAvanzamento an = CreateAvanzamento(faseInfo, fase, operatore, macchina, turno);
                        batch.BatchAvanzamenti.Add(an);
                        bav.Current = an;
                    }

                    if (batch.Fasi.Any(o => o.NumeroFase == fase.NumeroFase))
                    {
                        var x = batch.Fasi.FirstOrDefault(o => o.NumeroFase == fase.NumeroFase);
                        batch.Fasi.Remove(x);
                        batch.Fasi.Add(fase);
                    }

                    if (fase.Stato == StatoFasi.InLavorazione || fase.Stato == StatoFasi.InLavorazioneInPausa)
                    {
                        BatchUtils.BatchGestioneFermi(batch, faseInfo.causaleRiavvio, faseInfo.Username, faseInfo.notaRiavvio, fase, idMacchina, MongoRepository.Instance, Logger.Default);
                    }

                    if (fase.Stato == StatoFasi.InLavorazioneInEmergenza)
                    {
                        var causaleDefault = MongoRepository.Instance.GetWhereAsync<CausaliRiavvio>(x => x.Codice == "AreaMes_Default").GetAwaiter().GetResult().FirstOrDefault();
                        //setto la fase in allarme
                        if (batch.Fermi == null)
                            batch.Fermi = new List<BatchFermi>();

                        batch.Fermi.Add(new BatchFermi { Inizio = DateTime.Now, UserLocked = faseInfo.Username, NumeroFase = Convert.ToInt16(fase.NumeroFase), Turno = turno, MacchinaId = idMacchina, CausaliRiavvio = new ExternalDoc<CausaliRiavvio> { Id = causaleDefault.Id } });
                    }

                    bool onePhaseLav = batch.Fasi.Any(x => x.Stato == StatoFasi.InLavorazione);
                    bool onePhaseEmer = batch.Fasi.Any(x => x.Stato == StatoFasi.InLavorazioneInEmergenza);
                    bool onePhasePau = batch.Fasi.Any(x => x.Stato == StatoFasi.InLavorazioneInPausa);
                    // ------------------------------------------------------------------------------------------------------------
                    // se ho lo stato in attesa controllo che almeno una sua fase è partita, aggiorno lo stato e la data di inizio
                    Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', change batch.Stato OLD batch.Stato:'{batch.Stato}'...");
                    if (batch.Stato == StatoBatch.InAttesa)
                    {
                        Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', batch.Stato == StatoBatch.InAttesa...");
                        if (onePhaseLav)
                        {
                            Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', one phase lav...");
                            batch.Stato = StatoBatch.InLavorazione;
                            batch.Inizio = DateTime.Now;
                            isBatchChanged = true;
                            Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', one phase lav... completed");
                        }
                        Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', batch.Stato == StatoBatch.InAttesa... completed");
                    }
                    else if (batch.Stato != StatoBatch.Terminato)
                    {
                        // inoltre se tutte le fasi sono concluse aggiorno di nuovo il batch
                        Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', batch.Stato != StatoBatch.Terminato...");
                        if (batch.Fasi.All(o => o.Stato == StatoFasi.Terminato))
                        {
                            Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', all phase StatoFasi.Terminato...");
                            batch.Stato = StatoBatch.Terminato;
                            batch.Fine = DateTime.Now;
                            isBatchChanged = true;
                            Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', all phase StatoFasi.Terminato... completed");
                        }
                        Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', batch.Stato != StatoBatch.Terminato... completed");
                    }

                    if (batch.Stato != StatoBatch.InLavorazioneInEmergenza)
                    {
                        Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', batch.Stato != StatoBatch.InLavorazioneInEmergenza, onePhaseEmer:'{onePhaseEmer}'...");
                        if (onePhaseEmer)
                        {
                            batch.Stato = StatoBatch.InLavorazioneInEmergenza;
                            isBatchChanged = true;
                            onePhaseEmer = false;
                        }
                        Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', batch.Stato != StatoBatch.InLavorazioneInEmergenza, onePhaseEmer:'{onePhaseEmer}'... completed");
                    }

                    if (batch.Stato != StatoBatch.InLavorazione)
                    {
                        Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', batch.Stato != StatoBatch.InLavorazione, onePhaseLav:'{onePhaseLav}', onePhaseEmer:'{onePhaseEmer}'...");
                        if (onePhaseLav && !onePhaseEmer)
                        {
                            batch.Stato = StatoBatch.InLavorazione;
                            isBatchChanged = true;
                        }
                        Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', batch.Stato != StatoBatch.InLavorazione, onePhaseLav:'{onePhaseLav}', onePhaseEmer:'{onePhaseEmer}'... completed");
                    }

                    if (batch.Stato != StatoBatch.InLavorazioneInPausa)
                    {
                        Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', batch.Stato != StatoBatch.InLavorazioneInPausa, onePhasePau:'{onePhasePau}', onePhaseLav:'{onePhaseLav}', onePhaseEmer:'{onePhaseEmer}'...");
                        if (onePhasePau && !onePhaseEmer && !onePhaseLav)
                        {
                            batch.Stato = StatoBatch.InLavorazioneInPausa;
                            isBatchChanged = true;
                        }
                        Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', batch.Stato != StatoBatch.InLavorazioneInPausa, onePhasePau:'{onePhasePau}', onePhaseLav:'{onePhaseLav}', onePhaseEmer:'{onePhaseEmer}'... completed");
                    }
                    Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', change batch.Stato NEW batch.Stato:'{batch.Stato}'... completed");


                    if (batch.Stato == StatoBatch.Terminato)
                    {
                        Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', batch.Stato == StatoBatch.Terminato start RemoveAsync...");
                        BatchManager.Instance.RemoveAsync(batch.Id).GetAwaiter().GetResult();
                        isBatchChanged = true;
                        Logger.Default.InfoFormat($"SetFase2 OdP:'{batch.OdP}', batch.Stato == StatoBatch.Terminato start RemoveAsync... completed");
                    }

                }

                if (isBatchChanged)
                    AreaMESManager.Instance.NotifyBatchChanged(batch.Id, batch);

                await MessageServices.Instance.Publish(batch.Id, bav, batch);
                AreaMESManager.Instance.NotifyFaseChanged(batch.Id, bav);
            }
            catch (Exception ex)
            {
                Logger.Default.Error(ex);
            }
            Logger.Default.InfoFormat($"Start SetFase2 OdP:'{batch.OdP}'... completed");

            return batch.Id;
        }

        private static BatchAvanzamento CreateAvanzamento(SetFaseInfo faseInfo, BatchFase fase, Operatore operatore, Macchina macchina, TurnoDiLavoro turno)
        {
            return new BatchAvanzamento
            {
                Inizio = DateTime.Now,
                NumeroFase = fase.NumeroFase,
                CodiceFase = fase.DistintaBaseFase.Codice,
                BarcodeFase = fase.DistintaBaseFase.Barcode,
                Nota = fase.Stato.ToString(),
                Stato = fase.Stato,
                BatchFaseMacchina = new BatchFaseMacchina { Inizio = DateTime.Now, Macchina = macchina },
                BatchFaseManoDopera = new BatchFaseManoDopera { Inizio = DateTime.Now, Operatore = operatore },
                UserLocked = faseInfo.Username,
                Turno = turno
            };
        }

        /// <summary>
        /// Set dello stato per la fase scelta
        /// </summary>
        /// <param name="utBatch"> oggetto contente l'id del batch e la fase</param>
        /// <returns></returns>
        [Route("SetBatch2")]
        [HttpPost]
        public async Task<string> SetBatch2(SetBatchInfo utBatch)
        {
            try
            {
                if (utBatch != null)
                {
                    var batch = BatchManager.Instance.Get(utBatch.idBatch);
                    if (batch.Stato == StatoBatch.Terminato)
                    {
                        batch.Fine = DateTime.Now;
                        BatchManager.Instance.RemoveAsync(batch.Id).GetAwaiter().GetResult();
                    }

                    if (batch.Stato == StatoBatch.InSospensione)
                    {
                        batch.Sospensione = DateTime.Now;
                        BatchManager.Instance.RemoveAsync(batch.Id).GetAwaiter().GetResult();
                    }
                    AreaMESManager.Instance.NotifyBatchChanged(batch.Id, batch);
                }
            }
            catch (Exception ex)
            {
                Logger.Default.Error(ex);
            }

            return "";
        }

        /// <summary>
        /// Set dello stato per la fase scelta
        /// </summary>
        /// <param name="faseInfo"> oggetto contente l'id del batch e la fase</param>
        /// <returns></returns>
        [Route("SetFase")]
        [HttpPost]
        public async Task<string> SetFase(SetFaseInfo faseInfo)
        {

            Logger.Default.InfoFormat("Start SetFase...");
            string loggedUser = faseInfo.Username;
            var operatore = faseInfo.fase.Manodopera[0].Operatore;
            var macchina = faseInfo.fase.Macchine[0].Macchina;
            string idBatch = faseInfo.idBatch;
            BatchFase fase = faseInfo.fase;
            var batch = BatchManager.Instance.Get(idBatch);

            // Alberto.p, 05/03/2022, override dello stato nel caso questo venga passato come parametro
            Logger.Default.InfoFormat($"SetFase OdP:'{batch.OdP}',faseInfo.idStato:'{faseInfo.idStato}'");
            if (faseInfo.idStato != null)
            {
                Logger.Default.InfoFormat($"SetFase OdP:'{batch.OdP}',faseInfo.fase.Stato:'{faseInfo.fase.Stato}',faseInfo.idStato.Value:'{faseInfo.idStato.Value}'");
                faseInfo.fase.Stato = faseInfo.idStato.Value;
            }

            try
            {
                var bav = new FaseChangedInfo { Username = faseInfo.Username };
                var batchCorrentiInLavorazione = BatchManager.Instance.BatchInMemory().Where(o => BatchUtils.IsInLavorazione(o) && o.Id != idBatch);
                string idMacchina = "";
                if (macchina != null)
                    idMacchina = macchina.Id;
                var turno = await TurniDiLavoroController.GetCalcolaTurno(DateTime.Now, idMacchina);

                // Alberto.p, 19/05/2022, gestione della attesa ai sottoscrittori 
                var res = await MessageServices.Instance.PublishAndWaitAll(batch.Id, new FaseChangedInfoBefore { Current = faseInfo }, batch);
                foreach (var exc in res)
                    throw exc;

                lock (batch)
                {
                    // se abbiamo avviato lo start su una fase andiamo a settare le compentenze dell'operatore che ha premuto lo start 
                    // devo controllare che lo stato della fase PRIMA dell'aggiornamento a InLavorazione NON sia InLavorazioneInPausa
                    var faseInMemory = batch.Fasi.FirstOrDefault(o => o.NumeroFase == fase.NumeroFase);

                    if (fase.Stato == StatoFasi.InLavorazione)
                    {
                        // viene verificato se è presente un batch in lavorazione che interessa la macchina in uso per la fase ricevuta in avvio
                        var macchinaPerFaseCorrente = faseInMemory.Macchine.FirstOrDefault();
                        if ((macchinaPerFaseCorrente == null) || (String.IsNullOrEmpty(macchinaPerFaseCorrente?.Macchina?.Codice)))
                        {
                            var batchCorrentInLavorazione = batchCorrentiInLavorazione.FirstOrDefault();
                            //if (batchCorrentInLavorazione != null)
                            //    throw new Exception(String.Format("Non è possibile avviare l'ordine corrente, poichè è attivo l'ordine '{0}' - Odp Parziale:'{1}'", batchCorrentInLavorazione.OdP, batchCorrentInLavorazione.OdPParziale));
                        }
                        else
                        {

                            //var macchineInLavorazione = batchCorrentiInLavorazione.SelectMany(o => o.Fasi)
                            //                                              .Where(o=> BatchUtils.IsInLavorazione(o))
                            //                                              .SelectMany(o => o.Macchine)
                            //                                              .FirstOrDefault(o => o.Macchina.Id == macchinaPerFaseCorrente.Macchina.Id);

                            //if (macchineInLavorazione != null)
                            //    throw new Exception(String.Format("Attenzione non è possibile avviare l'ordine, poichè la macchina '{0}' risulta in uso da un altro ordine!", macchineInLavorazione.Macchina.Codice));
                        }



                        // viene verificato se è presente un lock sulla fase e non è possibile avviarla
                        if (!String.IsNullOrWhiteSpace(faseInMemory.BloccoAvanzamentoCausa))
                            throw new Exception(faseInMemory.BloccoAvanzamentoCausa);
                    }

                    //
                    if (fase.Stato == StatoFasi.InLavorazione && faseInMemory.Stato != StatoFasi.InLavorazioneInPausa)
                    {
                        var filterOp = Builders<Operatore>.Filter.Eq("Username", fase.UserLocked);
                        var op = MongoRepository.Instance.Collection<Operatore>().Find<Operatore>(filterOp).FirstOrDefault();

                        var x = fase.Manodopera.Select(o => { o.Inizio = DateTime.Now; o.Operatore = op; return o; }).ToList();
                        fase.Manodopera = x;
                        // aggiorniamo la data di inizio per le macchine
                        foreach (var item in fase.Macchine)
                            if (!item.Inizio.HasValue)
                                item.Inizio = DateTime.Now;

                        if (fase.Inizio.IsNull())
                            fase.Inizio = DateTime.Now;
                    }

                    if (fase.Stato == StatoFasi.Terminato || fase.Stato == StatoFasi.Abortito)
                        fase.Fine = DateTime.Now;

                    //funzione per il movimento parziale di magazzino sul 
                    if (Convert.ToBoolean(fase.DistintaBaseFase?.AbilitaFaseTerminataParzialmente))
                    {
                        //solo i pezzi sono <0
                        if (fase?.PezziParziali > 0)
                        {
                            fase.PezziProdotti += fase.PezziParziali;
                            var idPhase = batch.DistintaBase.Fasi.FirstOrDefault(x => x.NumeroFase == fase.NumeroFase).Id;
                            //aggiungi evento
                            var newitem = new Evento();
                            newitem.Id = ObjectId.GenerateNewId().ToString();
                            newitem.TipoEvento = Model.Enums.TipoEvento.qta_pzparz;
                            newitem.OraEvento = DateTime.Now.ToLocalTime();
                            newitem.UltimoAggiornamento = DateTime.Now;
                            newitem.FaseId = idPhase;
                            newitem.Source = Source.manual;
                            newitem.BatchId = batch.Id;
                            if (faseInfo.Username != null)
                            {
                                var mongoInstance = MongoRepository.New;
                                var listOfOperatori = mongoInstance.All<Operatore>(new string[] { }).Result.ToList().FirstOrDefault(x => x.Username == faseInfo.Username);
                                if (listOfOperatori != null) newitem.Operatore = new ExternalDoc<Operatore> { Id = listOfOperatori.Id };

                            }

                            newitem.Valore = Convert.ToString(fase.PezziParziali);
                            //creo un evento con tipo qta_pzparz
                            var result = SetEvents(new SetEventsRequest { Events = new List<Evento> { newitem }, IdBatch = batch.Id, IdFase = idPhase });
                            fase.PezziParziali = 0;
                        }
                    }
                    // AVANZAMENTI FASI
                    if (batch.BatchAvanzamenti == null) batch.BatchAvanzamenti = new List<BatchAvanzamento>();

                    //var turno = TurnoDiLavoro.GetCalcolaTurno(DateTime.Now);
                    var av = batch.BatchAvanzamenti.FirstOrDefault(a => a.NumeroFase == fase.NumeroFase && a.Fine == null);

                    Logger.Default.InfoFormat($"SetFase OdP:'{batch.OdP}',fase.NumeroFase:'{fase.NumeroFase}',fase.DistintaBaseFase.Codice:'{fase.DistintaBaseFase.Codice}',fase.Stato:'{fase.Stato.ToString()}'");
                    Logger.Default.InfoFormat($"SetFase OdP:'{batch.OdP}',av:'{av}'");
                    if (av == null)
                    {
                        bav.Previous = batch.BatchAvanzamenti.LastOrDefault(x => x.NumeroFase == fase.NumeroFase); // viene agganciato l'avanzamento precedente;
                        av = new BatchAvanzamento();
                        av.Inizio = DateTime.Now;
                        av.NumeroFase = fase.NumeroFase;
                        av.CodiceFase = fase.DistintaBaseFase.Codice;
                        av.BarcodeFase = fase.DistintaBaseFase.Barcode;
                        av.Stato = fase.Stato;
                        av.Nota = fase.Stato.ToString();
                        av.BatchFaseMacchina = new BatchFaseMacchina { Inizio = DateTime.Now, Macchina = macchina };
                        av.BatchFaseManoDopera = new BatchFaseManoDopera { Inizio = DateTime.Now, Operatore = operatore };
                        av.UserLocked = faseInfo.Username;
                        av.Turno = turno;
                        batch.BatchAvanzamenti.Add(av);
                        bav.Current = av;
                    }
                    else
                    {
                        av.Fine = DateTime.Now;
                        bav.Previous = av; // viene agganciato l'avanzamento precedente;
                                           // creazione di un nuovo avanzamento 
                        var an = new BatchAvanzamento();
                        an.Inizio = DateTime.Now;
                        an.NumeroFase = fase.NumeroFase;
                        an.CodiceFase = fase.DistintaBaseFase.Codice;
                        an.BarcodeFase = fase.DistintaBaseFase.Barcode;
                        an.Nota = fase.Stato.ToString();
                        an.Stato = fase.Stato;
                        an.BatchFaseMacchina = new BatchFaseMacchina { Inizio = DateTime.Now, Macchina = macchina };
                        an.BatchFaseManoDopera = new BatchFaseManoDopera { Inizio = DateTime.Now, Operatore = operatore };
                        an.UserLocked = faseInfo.Username;
                        an.Turno = turno;
                        batch.BatchAvanzamenti.Add(an);
                        bav.Current = an;
                    }
                }


                lock (batch)
                {
                    // update del batch con rispettiva base aggiornata
                    if (batch.Fasi.Any(o => o.NumeroFase == fase.NumeroFase))
                    {
                        var x = batch.Fasi.FirstOrDefault(o => o.NumeroFase == fase.NumeroFase);
                        batch.Fasi.Remove(x);
                        batch.Fasi.Add(fase);
                    }

                    // eventuali aggiornamenti del batch
                    checkUpdates(batch);
                }


                // var obj = new ReadBarcodeMessage { Data = data, Response = res };
                // await MessageServices.Instance.Publish(FaseChangedInfo.Name, bav);
                // notifica inMEMORY
                await MessageServices.Instance.Publish(batch.Id, bav, batch);
                AreaMESManager.Instance.NotifyFaseChanged(batch.Id, bav);

            }
            catch (Exception ex)
            {
                Logger.Default.Error(ex);
            }
            Logger.Default.InfoFormat("Start SetFase... completed");

            return batch.Id;
        }



        /// <summary>
        /// Set dello stato per la fase scelta
        /// </summary>
        /// <param name="utBatch"> oggetto contente l'id del batch e la fase</param>
        /// <returns></returns>
        [Route("SetBatch")]
        [HttpPost]
        public async Task<string> SetBatch(SetBatchInfo utBatch)
        {
            try
            {
                var batch = AreaMes.Server.BatchManager.Instance.Get(utBatch.idBatch);
                string loggedUser = utBatch.Username;
                var previousState = batch.Stato;
                batch.Stato = utBatch.idStato;
                //scarico i dettagli della fase 
                var Fase = batch.Fasi.FirstOrDefault(o => o.DistintaBaseFase.NumeroFase == utBatch.NumeroFase);
                //cerco il numero della fase perchè array non sempre in ordine
                var IndexFase = batch.Fasi.IndexOf(Fase);
                var countFasi = 0;
                if (batch.Fasi != null)
                    countFasi = batch.Fasi.Count;

                Logger.Default.InfoFormat("Test IndexFase: '{0}',Fase.NumeroFase: '{1}', batch.Fasi.Count:{2}", IndexFase, Fase, countFasi);
                var turno = await TurniDiLavoroController.GetCalcolaTurno(DateTime.Now, batch.Fasi[IndexFase != -1 ? IndexFase : 0].Macchine[0].Macchina.Id);

                lock (batch)
                {

                    if (batch.Stato == StatoBatch.InLavorazione)
                    {
                        //if (previousState == StatoBatch.InLavorazioneInEmergenza)
                        //{
                        //setto la fase in lavorazione
                        batch.Fasi[IndexFase].Stato = StatoFasi.InLavorazione;
                        var bf = batch.Fermi.LastOrDefault();
                        if (bf != null)
                            if (bf.Fine == null)
                            {
                                bf.Fine = DateTime.Now;
                                //bf.Fine = utBatch.dataRiavvio;
                                if (utBatch.dataRiavvio.Year == 1)
                                    bf.Fine = DateTime.Now;
                                bf.Id = ObjectId.GenerateNewId().ToString();
                                bf.UserUnLocked = utBatch.Username;
                                bf.NumeroFase = Convert.ToInt32(utBatch.NumeroFase);
                                bf.MacchinaId = utBatch.Macchina;
                                bf.Causale = BatchCausaleFermi.Emergenza;
                                bf.NotaDiRiavvio = utBatch.notaRiavvio;



                                var causale = new ExternalDoc<CausaliRiavvio>();
                                if (utBatch.causaleRiavvio != null)
                                    causale.Id = utBatch.causaleRiavvio;

                                else
                                {
                                    //scarica tutti i tipi di causali Riavvio filtrati per IsStopCausal= true
                                    var justifications = MongoRepository.Instance.Collection<CausaliRiavvio>().FindAsync(Builders<CausaliRiavvio>.Filter.Eq("IsStopCausal", true)).Result.ToList();
                                    //se esiste una justificazione di default la utilizzo altrimenti si utilizza quella di default
                                    var justification = justifications.FirstOrDefault(x => x.Default == true);
                                    if (justification == null)
                                        //se null selezione la causale precaricata 000
                                        justification = justifications.FirstOrDefault(x => x.Codice == "AreaMes_Default");
                                    causale.Id = justification.Id;
                                }
                                bf.CausaliRiavvio = causale;
                                var difference = bf.Fine.Value.Subtract(bf.Inizio.Value).TotalSeconds;
                                // var difference = bf.Inizio.Value.Subtract(bf.Fine.Value).TotalSeconds;
                                bf.TotaleTempo = Convert.ToInt32(difference);

                            }
                    }

                    if (batch.Stato == StatoBatch.InLavorazioneInEmergenza)
                    {
                        //setto la fase in allarme
                        var causaleDefault = MongoRepository.Instance.GetWhereAsync<CausaliRiavvio>(x => x.Codice == "AreaMes_Default").GetAwaiter().GetResult().FirstOrDefault();
                        batch.Fasi[IndexFase].Stato = StatoFasi.InLavorazioneInEmergenza;
                        if (batch.Fermi == null)
                            batch.Fermi = new List<BatchFermi>();

                        batch.Fermi.Add(new BatchFermi { Inizio = DateTime.Now, UserLocked = utBatch.Username, Turno = turno, MacchinaId = utBatch.Macchina, CausaliRiavvio = new ExternalDoc<CausaliRiavvio> { Id = causaleDefault.Id } });
                    }
                }

                if (batch.Stato == StatoBatch.Terminato)
                {
                    batch.Fine = DateTime.Now;
                    await AreaMes.Server.BatchManager.Instance.RemoveAsync(batch.Id);
                }

                if (batch.Stato == StatoBatch.InSospensione)
                {
                    batch.Sospensione = DateTime.Now;
                    await AreaMes.Server.BatchManager.Instance.RemoveAsync(batch.Id);
                }

                // await MessageServices.Instance.Publish(batch.Id, batch);
                AreaMESManager.Instance.NotifyBatchChanged(batch.Id, batch);
            }
            catch (Exception ex)
            {
                Logger.Default.Error(ex);
            }

            return utBatch.idBatch;
        }

        [Route("AddPiecesToBatch")]
        [HttpPost]
        public string AddPiecesToBatch(VersamentoBatch data)
        {
            string idBatch = data.idBatch;
            int pezziVersati = int.Parse(data.pezziVersati);
            string numeroFase = data.numeroFase;
            string loggedUser = data.Username;

            var batch = AreaMes.Server.BatchManager.Instance.Get(idBatch);
            var fase = batch.Fasi.First(f => f.NumeroFase == numeroFase);
            if (loggedUser == fase.UserLocked)
            {
                fase.PezziProdotti += pezziVersati;
                AreaMESManager.Instance.NotifyVersamentoFaseChanged(batch.Id, fase);
            }

            return fase.PezziProdotti.ToString();
        }


        [Route("SendBarcode")]
        [HttpPost]
        public async Task<SendBarcodeResponse> SendBarcode(SendBarcodeRequest data)
        {
            var res = new SendBarcodeResponse();
            try
            {
                var obj = new SendBarcodeMessage
                {
                    Data = data,
                    Response = res
                };
                await MessageServices.Instance.Publish(SendBarcodeMessage.Name, obj);
            }
            catch (Exception exc)
            {
                res.IsOnError = true;
                res.ErrorMessage = exc.Message;
            }
            return res;
        }



        [Route("ReadBarcode")]
        [HttpPost]
        public async Task<ReadBarcodeResponse> ReadBarcode(ReadBarcodeRequest data)
        {
            var res = new ReadBarcodeResponse();
            try
            {
                var obj = new ReadBarcodeMessage { Data = data, Response = res };
                await MessageServices.Instance.Publish(ReadBarcodeMessage.Name, obj);
            }
            catch (Exception exc)
            {
                res.IsOnError = true;
                res.ErrorMessage = exc.Message;
            }
            return res;
        }

        [Route("SetEvents")]
        [HttpPost]
        public async Task<SetEventsResponse> SetEvents(SetEventsRequest data)
        {
            var result = new SetEventsResponse();

            try
            {
                //scarico il batch in questione 
                var batch = AreaMes.Server.BatchManager.Instance.Get(data.IdBatch);

                //vengono ciclati tutti gli Eventi 
                foreach (var item in data.Events)
                {
                    //cerco se esiste già questo evento se esiste estrapolo il puntatore
                    var eventExist = batch.Eventi.Any(o => o.Id == item.Id);

                    if (eventExist)
                    {
                        var itemObj = batch.Eventi.FirstOrDefault(x => x.Id == item.Id);

                        //aggiorno i campi 
                        itemObj.TipoValore = item.TipoValore;
                        itemObj.Valore = item.Valore;
                        itemObj.UltimoAggiornamento = DateTime.Now;

                    }
                    //creo un nuovo evento
                    else
                    {
                        //genero un id per mongo
                        item.Id = ObjectId.GenerateNewId().ToString();
                        if (item?.Operatore != null)
                            item.Source = Source.manual;
                        lock (batch)
                            batch.Eventi.Add(item);
                    }
                }

                AreaMESManager.Instance.NotifyBatchChanged(batch.Id, batch);
            }
            catch (Exception exc)
            {
                result.IsOnError = true;
                result.ErrorMessage = exc.Message;
            }


            return result;
        }

        [Route("SetBlocks")]
        [HttpPost]
        public async Task<SetBlocksResponse> SetBlocks(SetBlocksRequest data)
        {
            var result = new SetBlocksResponse();
            var batchctrl = new BatchController();
            try
            {
                var batch = AreaMes.Server.BatchManager.Instance.Get(data.IdBatch);
                //vengono ciclati tutti i fermi
                foreach (var item in data.Blocks)
                {
                    //cerco se esiste già questo fermo se esiste estrapolo il puntatore
                    var blockExist = batch.Fermi.Any(o => o.Id == item.Id);

                    if (blockExist)
                    {
                        batch.Fermi.FirstOrDefault(x => x.Id == item.Id).CausaliRiavvio.Id = data.IdCausal;
                        //batch.Fermi.FirstOrDefault(x => x.Id == item.Id ).CausaliRiavvio.Resolve();
                    }
                }

                AreaMESManager.Instance.NotifyBatchChanged(batch.Id, batch);
            }
            catch (Exception exc)
            {
                result.IsOnError = true;
                result.ErrorMessage = exc.Message;
            }
            return result;
        }

        [Route("SetEvent_Pieces_Type")]
        [HttpPost]
        public async Task<SetEvent_PiecesResponse> SetEvent_Pieces_Type(SetEvent_PiecesRequest data)
        {
            var result = new SetEvent_PiecesResponse();

            try
            {
                var newitem = new Evento();
                //Scarico le causali di scarto 
                var causali = MongoRepository.Instance.Collection<TipoValori>().FindAsync(Builders<TipoValori>.Filter.Eq("Tipo", Model.Enums.TipoValore.prod_cau_scarto)).Result.ToList();
                var causal = MongoRepository.Instance.Collection<TipoValori>().FindAsync(x => x.Tipo == Model.Enums.TipoValore.prod_cau_scarto || x.Tipo == Model.Enums.TipoValore.prod_cau_abort).Result.ToList();

                var tipovalore = new ExternalDoc<TipoValori>();
                if (data.Causale == null)
                {
                    //controlo se cè una causale di default
                    var causale = causali.FirstOrDefault(x => x.Default);
                    if (causale == null)
                        //se null selezione la causale precaricata 000
                        causale = causali.FirstOrDefault(x => x.Codice == "AreaMes_Default");
                    tipovalore.Id = causale.Id;
                }

                else
                {
                    //controlo se cè una causale di default
                    var causale = causali.FirstOrDefault(x => x.Id == data.Causale.Id);
                    if (causale == null)
                        //se null selezione la causale precaricata 000
                        causale = causali.FirstOrDefault(x => x.Codice == "AreaMes_Default");
                    tipovalore.Id = causale.Id;
                }
                //genero un id per mongo
                newitem.Id = ObjectId.GenerateNewId().ToString();
                // newitem.TipoEvento = Model.Enums.TipoEvento.qta_pzsca;
                newitem.TipoValore = tipovalore;

                newitem.OraEvento = DateTime.Now;
                newitem.UltimoAggiornamento = DateTime.Now;
                newitem.FaseId = data.Fase.Id;
                if (data.Macchina != null)
                {
                    newitem.Turno = await TurniDiLavoroController.GetCalcolaTurno(DateTime.Now, data.Macchina.Id);
                    newitem.Macchina = new ExternalDoc<Macchina> { Id = data.Macchina.Id, Item = data.Macchina };
                }
                newitem.Source = data.Source;
                newitem.BatchId = data.IdBatch;
                newitem.TipoEvento = data.TipoEvento;

                if (data.Materiale != null)
                    newitem.Materiale = new ExternalDoc<Materiale> { Id = data.Materiale.Id };


                if (data.Operatore != null)
                    newitem.Operatore = new ExternalDoc<Operatore> { Id = data.Operatore.Id };
                // -----------------------------------------------------------------------------------------------
                if (newitem.Source == Source.auto)
                {
                    if (data.TipoConteggio == Model.Enums.TipoConteggio.conteggio_incrementale)
                    {
                        var batch = AreaMes.Server.BatchManager.Instance.Get(data.IdBatch);
                        var scartiprodotti = batch.Eventi.Where(x => x.TipoEvento == TipoEvento.qta_pzsca && x.Source == data.Source).Sum(x => Convert.ToInt32(x.Valore));
                        scartiprodotti = Convert.ToInt32(data.Valore) - scartiprodotti;
                        newitem.Valore = Convert.ToString(scartiprodotti);

                    }
                    else if (data.TipoConteggio == Model.Enums.TipoConteggio.conteggio_trigger)
                    {
                        newitem.Valore = Convert.ToString(data.Valore);
                    }
                }
                else if (newitem.Source == Source.manual)
                {
                    newitem.Valore = Convert.ToString(data.Valore);
                }


                await this.SetEvents(new SetEventsRequest { Events = new List<Evento> { newitem }, IdBatch = data.IdBatch, IdFase = data.IdFase });
            }
            catch (Exception exc)
            {
                result.IsOnError = true;
                result.ErrorMessage = exc.Message;
            }


            return result;
        }

        //, string idBatch
        /// <summary>
        /// Metodo di utility per aggiornare il batch
        /// </summary>
        /// <param name="pBatch"></param>
        /// <returns></returns>
        private Batch checkUpdates(Batch pBatch)
        {
            // se ho lo stato in attesa controllo che almeno una sua fase è partita, aggiorno lo stato e la data di inizio
            if (pBatch.Stato == StatoBatch.InAttesa)
            {
                if (pBatch.Fasi.Any(o => o.Stato == StatoFasi.InLavorazione))
                {
                    pBatch.Stato = StatoBatch.InLavorazione;
                    pBatch.Inizio = DateTime.Now;
                }
            }

            // inoltre se tutte le fasi sono concluse aggiorno di nuovo il batch
            if (pBatch.Stato != StatoBatch.Terminato)
            {
                if (pBatch.Fasi.All(o => o.Stato == StatoFasi.Terminato))
                {
                    pBatch.Stato = StatoBatch.Terminato;
                    pBatch.Fine = DateTime.Now;
                }
            }

            return pBatch;

        }

        public async Task<string> SetBatchMateriale(string idBatch, List<BatchMateriali> materiali)
        {
            AreaMESManager.Instance.NotifyBatchMaterialeChanged(idBatch, materiali);

            return String.Empty;
        }
    }



}
