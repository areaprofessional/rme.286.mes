﻿using AreaMes.Model;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
    public class TurniDiLavoroController : BaseController<TurnoDiLavoro>
    {
        public async override Task<IEnumerable<TurnoDiLavoro>> GetAll()
        {
             var allShift= await MongoRepository.Instance.All<TurnoDiLavoro>(new string[] { }); // campi esclusi
            //foreach (var shift in allShift)
            //{
            //    if (shift.Macchine != null)
            //    {
            //        foreach (var machine in shift.Macchine)
            //        {
            //            machine.Resolve();
            //        }
            //    }

            //}

            return allShift;
        }


        public async override Task<TurnoDiLavoro> Get(string id)
        {
            var mi = MongoRepository.Instance;
            var a = await mi.GetAsync<TurnoDiLavoro>(id);
            if (a.Macchine != null)
            {
                foreach (var machine in a.Macchine)
                {
                    machine.Resolve();
                }
            }
            if (a.IsEccezione)
            {
                a.ValidoAl = Convert.ToDateTime(a.ValidoAl).ToLocalTime();
                a.ValidoDal = Convert.ToDateTime(a.ValidoDal).ToLocalTime();
            }
            //if (a != null)
            //{
            //    a.ValidoDalString = Convert.ToString(a.ValidoDal).Substring(0, 10);
            //    a.ValidoAlString = Convert.ToString(a.ValidoAl).Substring(0, 10);
            //}
            return a;
        }

        [HttpPost]
        [HttpPut]
        public async override Task<TurnoDiLavoro> Set([FromBody] TurnoDiLavoro item)
        {
            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Nome))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Nome'"));

            if (String.IsNullOrWhiteSpace(item.Codice))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Codice'"));

            // viene cercato se esiste un record con la stessa chiave
            var batchFoundedByKey = MongoRepository.Instance.Collection<TurnoDiLavoro>().AsQueryable().Where(k => k.Nome == item.Nome).ToList();

            if (batchFoundedByKey.Any(o => o.Id != item.Id))
                throw new Exception(String.Format("Attenzione: è già presente in archivio un elemento con Nome:'{0}'", item.Nome));
            //----- Data Validation ----------------------
            item.Uniquefy();
            await MongoRepository.Instance.SetAsync(item);

            // ricreazione di tutti trigger sul cambio turno
            //ShiftManager.Instance.Init();

            

            if (item.IsEccezione)
            {
                item.ValidoAl = Convert.ToDateTime(item.ValidoAl).ToLocalTime();
                item.ValidoDal = Convert.ToDateTime(item.ValidoDal).ToLocalTime();
            }
            return item;
        }

        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            var b= await MongoRepository.Instance.DeleteAsync<TurnoDiLavoro>(id);

            // ricreazione di tutti trigger sul cambio turno
            //ShiftManager.Instance.Init();

            return b;
        }

        [HttpGet]
        [ActionName("getCalcolaTurno")]
        public static async Task<TurnoDiLavoro> GetCalcolaTurno(DateTime data, string idMacchina)
        {
            if (data == null)
                data = DateTime.Now;

            TurnoDiLavoro turnoReturn = new TurnoDiLavoro();
            var listTurni = await MongoRepository.Instance.All<TurnoDiLavoro>(new string[] { });
            var day = data.DayOfWeek;
            listTurni = listTurni.OrderByDescending(o => o.IsEccezione).ToList();
            foreach (TurnoDiLavoro turno in listTurni)
            {
                turno.ValidoDal = new DateTime(turno.ValidoDal.Year, turno.ValidoDal.Month, turno.ValidoDal.Day);
                turno.ValidoAl = new DateTime(turno.ValidoAl.Year, turno.ValidoAl.Month, turno.ValidoAl.Day);
                if (turno.IsEccezione == false || (turno.IsEccezione == true && data >= turno.ValidoDal && data <= turno.ValidoAl))
                {
                    if (turno.Macchine.Count == 0)
                        idMacchina = null;
                    turnoReturn = checkIfShiftIsActive(data, turno, idMacchina);
                    if(turnoReturn.Id != null)
                        return turnoReturn;
                }
            }
            return turnoReturn;
        }

        // funzione per controllare se un turno è attivo
        public static TurnoDiLavoro checkIfShiftIsActive(DateTime data, TurnoDiLavoro turno, string idMacchina)
        {
            
            TurnoDiLavoro turnoReturn = new TurnoDiLavoro();
            TimeSpan inizio = new TimeSpan(turno.InizioHH, turno.InizioMM, 0);
            TimeSpan fine = new TimeSpan(turno.FineHH, turno.FineMM, 0);

            turno.ValidoDal = new DateTime(turno.ValidoDal.Year, turno.ValidoDal.Month, turno.ValidoDal.Day);
            turno.ValidoAl = new DateTime(turno.ValidoAl.Year, turno.ValidoAl.Month, turno.ValidoAl.Day);

            //bool searchMacchina = false;
            //if(idMacchina != null && turno.Macchine.FirstOrDefault(x => x.Id == idMacchina) != null)
            //        searchMacchina = true;

            // controllo se l'orario è compreso in questo turno
            if (data.TimeOfDay >= inizio && data.TimeOfDay < fine || (turno.IsEccezione == true && data >= turno.ValidoDal && data <= turno.ValidoAl && data.TimeOfDay >= inizio && data.TimeOfDay < fine))
            {
                // controllo se oggi è un giorno flaggato
                if ((
                           DayOfWeek.Monday == data.DayOfWeek && turno.FlagLunedi
                        || DayOfWeek.Tuesday == data.DayOfWeek && turno.FlagMartedi
                        || DayOfWeek.Wednesday == data.DayOfWeek && turno.FlagMercoledi
                        || DayOfWeek.Thursday == data.DayOfWeek && turno.FlagGiovedi
                        || DayOfWeek.Friday == data.DayOfWeek && turno.FlagVenerdi
                        || DayOfWeek.Saturday == data.DayOfWeek && turno.FlagSabato
                        || DayOfWeek.Sunday == data.DayOfWeek && turno.FlagDomenica )
                        && (idMacchina == null || turno.Macchine.FirstOrDefault(x => x.Id == idMacchina) != null)
                        )
                {
                    turnoReturn = turno;
                }
            }

            return turnoReturn;
        }

    }
}
