﻿using AreaMes.Model;
using AreaMes.Server.MongoDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
    public class CausaliRiavvioController : BaseController<CausaliRiavvio>
    {
        public async override Task<IEnumerable<CausaliRiavvio>> GetAll()
        {
            return await MongoRepository.Instance.All<CausaliRiavvio>(new string[] { }); // campi esclusi
        }


        public async override Task<CausaliRiavvio> Get(string id)
        {
            return await MongoRepository.Instance.GetAsync<CausaliRiavvio>(id);
        }

        [HttpPost]
        [HttpPut]
        public async override Task<CausaliRiavvio> Set([FromBody] CausaliRiavvio item)
        {

            //-----------------------------------------------------------------------
            //------Controllo se è un item di default è possibile solo modificare la descrizione 
            //-----------------------------------------------------------------------
            if (item.IsInternal)
            {
                var itemInternal = await MongoRepository.Instance.GetAsync<CausaliRiavvio>(item.Id);
                itemInternal.Causale = item.Causale;
                await MongoRepository.Instance.SetAsync(itemInternal);
                return itemInternal;
            }

            item.Uniquefy();
            await MongoRepository.Instance.SetAsync(item);
            return item;

        }

        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            var itemInternal = await MongoRepository.Instance.GetAsync<CausaliRiavvio>(id);
            if (itemInternal.IsInternal) return await Task.FromResult(false);
            return await MongoRepository.Instance.DeleteAsync<CausaliRiavvio>(id);
        }

    }





}
