﻿using AreaMes.Model.Design;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
   public class TipoSegnalazioneManutenzioneController : BaseController<TipoSegnalazioneManutenzione>
    {
        public async override Task<IEnumerable<TipoSegnalazioneManutenzione>> GetAll()
        {
            return await MongoRepository.Instance.All<TipoSegnalazioneManutenzione>(new string[] { }); // campi esclusi
        }


        public async override Task<TipoSegnalazioneManutenzione> Get(string id)
        {
            var mi = MongoRepository.Instance;

            var a = await mi.GetAsync<TipoSegnalazioneManutenzione>(id);

            return a;

        }

        [HttpPost]
        [HttpPut]
        public async override Task<TipoSegnalazioneManutenzione> Set([FromBody] TipoSegnalazioneManutenzione item)
        {
            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Nome))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Nome'"));

            // viene cercato se esiste un record con la stessa chiave
            var batchFoundedByKey = MongoRepository.Instance.Collection<TipoSegnalazioneManutenzione>().AsQueryable().Where(k => k.Codice == item.Codice).ToList();

            if (batchFoundedByKey.Any(o => o.Id != item.Id))
                throw new Exception(String.Format("Attenzione: è già presente in archivio un elemento con Codice:'{0}'", item.Codice));
            //----- Data Validation ----------------------

            item.Uniquefy();
            await MongoRepository.Instance.SetAsync(item);
            return item;
        }

        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            return await MongoRepository.Instance.DeleteAsync<TipoSegnalazioneManutenzione>(id);
        }


    }
}
