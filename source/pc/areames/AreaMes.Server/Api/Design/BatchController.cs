﻿using AreaMes.Model;
using AreaMes.Model.Dto;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using AreaMes.Server.Api.Runtime;
using AreaMes.Server.MongoDb;
using AreaMes.Server.Utils;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using static AreaMes.Model.BatchUtils;

namespace AreaMes.Server.Api.Design
{
    [RoutePrefix("api/batch")]
    public class BatchController : BaseController<Batch>
    {
        public static string[] GetAll_StandardEscludedField = new string[] { "BatchParametri", "BatchAvanzamenti", "BatchMateriali", "Campionature", "Fermi"};

        [ActionName("GetAll")]
        public async override Task<IEnumerable<Batch>> GetAll()
        {
            var mongoInstance = MongoRepository.New;

            // var x = await MongoRepository.Instance.All<Batch>(GetAll_StandardEscludedField, new string[] { }); // campi esclusi

            // var x = mongoInstance.Collection<Batch>(new string[] { });
            var x = await mongoInstance.All<Batch>(GetAll_StandardEscludedField, new string[] { });
            return x;
        }

        [ActionName("Get")]
        public async override Task<Batch> Get(string id)
        {
            var a = await MongoRepository.Instance.GetAsync<Batch>(id);
            if (a == null) return null;

            if (a.DistintaBase.DistintaBaseParametri != null)
                foreach (var item in a.DistintaBase.DistintaBaseParametri)
                    item.UnitaDiMisura?.Resolve();

            if (a.Azienda != null)
                a.Azienda.Resolve();

            if (a.Fermi == null)
                a.TempoTotaleFermi = 0;
            else
                a.TempoTotaleFermi = Math.Round(a.Fermi.DefaultIfEmpty(new BatchFermi()).Sum(o => ((o.Fine.HasValue ? o.Fine.Value : DateTime.Now) - (o.Inizio.HasValue ? o.Inizio.Value : DateTime.Now)).TotalSeconds));

            if (a.Fermi != null)
                foreach (var item in a.Fermi)
                {
                    if (item.CausaliRiavvio?.Id != null)
                        item.CausaliRiavvio.Resolve();
                }

            if (a.Eventi != null)
                foreach (var item in a.Eventi)
                {
                    if (item.Macchina?.Id != null)
                        item.Macchina.Resolve();
                    if (item.Operatore?.Id != null)
                        item.Operatore.Resolve();
                    if (item.TipoValore?.Id != null)
                        item.TipoValore.Resolve();
                    if (item.UnitaDiMisura?.Id != null)
                        item.UnitaDiMisura.Resolve();
                }
            if (a.BatchParametri != null)
                foreach (var item in a.BatchParametri)
                {
                    if (item.Parametro.UnitaDiMisura != null)
                        item.Parametro.UnitaDiMisura.Resolve();
                }
            return a;
        }

        [HttpGet]
        [Route("GetByOdp")]
        public async Task<Batch> GetByOdp(string odp)
        {
            return (await MongoRepository.Instance.GetWhereAsync<Batch>(b => b.OdP == odp)).FirstOrDefault();
        }

        [HttpPost]
        [HttpPut]
        public async override Task<Batch> Set([FromBody] Batch item)
        {
            Logger.Default.InfoFormat($"Set Batch OdP:'{item.OdP}'");
            //----- Data Validation ------------------------------------------
            if (item.Materiale == null)
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Materiale'"));

            if (item.DistintaBase == null)
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Distinta Base'"));

            if (String.IsNullOrWhiteSpace(item.OdP))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'OdP'"));

            try
            {
                //--------------------------------------------------------------------
                if ((item.Id == null) && (item.OdP != String.Empty))
                {
                    var tmp = await this.GetBatchAutoCounterInfo(true);
                    var odpToInt = 0;
                    if (Int32.TryParse(item.OdP, out odpToInt))
                        if (tmp.Global != Convert.ToInt32(item.OdP))
                        {
                            item.OdP = Convert.ToString(tmp.Global);
                            item.OdPParziale = Convert.ToString(tmp.Partial);
                        }
                }
                if (item.Priorita == null || item.Priorita == 0)
                {
                    //assegno una priorità incrementale
                    item.Priorita = await BatchUtils.GetBatchAutoPriorityCounterInfo(MongoRepository.Instance);
                }
                //----- Data Validation -----------------------------------------------
            }
            catch (Exception exc)
            {
                throw new Exception(String.Format($"Attenzione: errore di assegnazione del contatore automatico. Causa:'{exc.Message}'"));
            }

            item.Uniquefy();
            item.LastUpdate = DateTime.Now;

            // viene cercato se esiste un batch per lo stesso ODP
            var batchFoundedByOdp = MongoRepository.Instance.Collection<Batch>().FindAsync(Builders<Batch>.Filter.Eq("OdP", item.OdP)).Result.ToList();

            if (batchFoundedByOdp.Any(o => o.Id != item.Id))
            {
                throw new Exception(String.Format("Attenzione: è già presente in archivio um batch con Odp:'{0}'", item.OdP));
            }


            var listAziende = await MongoRepository.Instance.All<Azienda>(new string[] { });
            if ((listAziende.Any()) && String.IsNullOrWhiteSpace(item.Azienda.Id))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Azienda'"));


            if (item.Stato == StatoBatch.InModifica)
            {
                var EditedParams = item.DistintaBase?.DistintaBaseParametri;

                if (item.Materiale != null)
                    item.Materiale = await MongoRepository.Instance.GetAsync<Materiale>(item.Materiale.Id);

                if (item.DistintaBase == null)
                    item.DistintaBase = await MongoRepository.Instance.GetAsync<DistintaBase>(item.DistintaBase.Id);

                if (EditedParams != null && EditedParams.Count == item.DistintaBase.DistintaBaseParametri.Count)
                    item.DistintaBase.DistintaBaseParametri = EditedParams;

                foreach (var par in item.DistintaBase.DistintaBaseParametri)
                    par.UnitaDiMisura?.Resolve();


                if (item.CreateDate.IsNull())
                    item.CreateDate = DateTime.Now;
            }
            else if (item.Stato == StatoBatch.InAttesa)
            {
                await BatchUtils.PrepareBatchTo_InAttesa(item, MongoRepository.Instance);

                // viene effettuato il reset della data di sospensione
                item.Sospensione = null;

                // viene aggiunto l'elemento al sistema
                BatchManager.Instance.StartAsync(item);

            }
            else if (item.Stato == StatoBatch.TerminatoAbortito)
            {
                item.Fine = DateTime.Now;
                await AreaMes.Server.BatchManager.Instance.RemoveAsync(item.Id);
            }
            else if (item.Stato == StatoBatch.InSospensione)
            {
                Logger.Default.InfoFormat($"Set Batch OdP:'{item.OdP}', suspending by user, try get batch in memory...");
                try
                {
                    var runtimeContext = new RuntimeTransportContextController();
                    var result = await runtimeContext.SospendiBatch(new SuspendBatchInfo() { BatchId = item.Id });
                    if (result == "") { Logger.Default.Error($"Set Batch OdP:'{item.OdP}', batch is not in memory"); }
                }
                catch (Exception exc) { Logger.Default.Info(exc); }
                //Logger.Default.InfoFormat($"Set Batch OdP:'{item.OdP}', suspending by user, try get batch in memory... completed");

                //var batchInMemory = BatchManager.Instance.BatchInMemory().FirstOrDefault(x => x.Id == item.Id);
                //if(batchInMemory != null)
                //{
                //    item.StatoPrecedente = batchInMemory.Stato;
                //    item.Sospensione = DateTime.Now;
                //    Logger.Default.InfoFormat($"Set Batch OdP:'{item.OdP}', suspending by user, set item.StatoPrecedente == batchInMemory.Stato, item.StatoPrecedente:'{item.StatoPrecedente}', batchInMemory.Stato:'{batchInMemory.Stato}'...");
                //    await AreaMes.Server.BatchManager.Instance.RemoveAsync(item.Id);

            }
            else if (item.Stato == StatoBatch.InSospensioneInRipresa)
            {
                Logger.Default.InfoFormat($"Set Batch OdP:'{item.OdP}', reactivated by user, item.StatoPrecedente:'{item.StatoPrecedente}'");
                item.Stato = (item.StatoPrecedente == StatoBatch.InAttesa) ? StatoBatch.InAttesa : StatoBatch.InLavorazioneInPausa;
                item.StatoPrecedente = StatoBatch.InSospensione;
                item.Sospensione = null;
                foreach (var fase in item.Fasi)
                {
                    Logger.Default.InfoFormat($"Set Batch OdP:'{item.OdP}', reactivated by user, fase codice:'{fase?.DistintaBaseFase?.Codice}', fase stato:'{fase.Stato}'");
                    if (fase.Stato != StatoFasi.Terminato && fase.Stato != StatoFasi.InAttesa)
                    {
                        Logger.Default.InfoFormat($"Set Batch OdP:'{item.OdP}', reactivated by user, set fase codice:'{fase?.DistintaBaseFase?.Codice}' in InLavorazioneInPausa");
                        fase.Stato = StatoFasi.InLavorazioneInPausa;
                    }
                }

                var batchInMemory = BatchManager.Instance.BatchInMemory().FirstOrDefault(x => x.Id == item.Id);
                var btcCount = BatchManager.Instance.BatchInMemory().Count();
                var btcCountList = BatchManager.Instance.BatchInMemory().ToList();
                if (batchInMemory == null)
                {
                    Logger.Default.InfoFormat($"Set Batch OdP:'{item.OdP}', reactivated by user, MongoRepository.Instance.SetAsync");
                    await MongoRepository.Instance.SetAsync<Batch>(item);
                    Logger.Default.InfoFormat($"Set Batch OdP:'{item.OdP}', reactivated by user, BatchManager.Instance.StartAsync");
                    BatchManager.Instance.StartAsync(item);
                    Logger.Default.InfoFormat($"Set Batch OdP:'{item.OdP}', reactivated by user, BatchManager.Instance.StartAsync... completed");
                }
                else
                {
                    Logger.Default.InfoFormat($"Set Batch OdP:'{item.OdP}', reactivated by user, ERROR BATCH ALREADY IN RAM");
                    throw new Exception(String.Format("Attenzione: il batch risulta già ripreso."));
                }
            }

            await MongoRepository.Instance.SetAsync(item);
            Logger.Default.InfoFormat($"Set Batch OdP:'{item.OdP}'... completed");
            return item;
        }

        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            await AreaMes.Server.BatchManager.Instance.RemoveAsync(id);
            return await MongoRepository.Instance.DeleteAsync<Batch>(id);
        }

        [HttpPost]
        [ActionName("getBatchAutoCounterInfo")]
        public async Task<BatchAutoCounter> GetBatchAutoCounterInfo(bool getNew)
        {
            return await BatchUtils.GetAutoCounterInfo(MongoRepository.Instance, getNew);
        }




        [HttpGet]
        [ActionName("getBatchGraphValues")]
        public async Task<List<BatchGraphValues>> GetBatchGraphValues(int tickInterval, string batchId)
        {
            BatchFilterGraphCriteria filterCriteria = new BatchFilterGraphCriteria { TickInterval = tickInterval, ListOfBatches = new List<string>() { batchId } };

            var result = new List<BatchGraphValues>();

            if (filterCriteria.TickInterval == 0)
                filterCriteria.TickInterval = 60; // default 60 secondi;

            if ((filterCriteria.ListOfBatches == null) || (filterCriteria.ListOfBatches.Count == 0))
                throw new Exception(String.Format("Attenzione: è necessario indicare almeno un batch per l'estrazione dati"));

            var listOfBatches = MongoRepository.Instance.Collection<Batch>().AsQueryable()
                                               .Where(o => filterCriteria.ListOfBatches.Contains(o.Id) && o.Inizio.HasValue)
                                               .OrderBy(o => o.Inizio);

            var endDate = DateTime.Now;

            foreach (var item in listOfBatches)
            {
                DateTime dtEnd = endDate;
                DateTime dtStartNeutrally = new DateTime(item.Inizio.Value.Year, item.Inizio.Value.Month, item.Inizio.Value.Day, item.Inizio.Value.Hour, item.Inizio.Value.Minute, item.Inizio.Value.Second);
                if (item.Fine.HasValue)
                    dtEnd = item.Fine.Value;

                var totalSecond = dtEnd.Subtract(item.Inizio.Value).TotalSeconds;
                var totalTicks = (totalSecond / filterCriteria.TickInterval);


                var OEE_graphItems = new BatchGraphValues
                {
                    BatchId = item.Id,
                    TickInterval = filterCriteria.TickInterval,
                    Values = new List<BatchGraphValue>(),
                    Name = "OEE"
                };

                var OEE_Rf_graphItems = new BatchGraphValues
                {
                    BatchId = item.Id,
                    TickInterval = filterCriteria.TickInterval,
                    Values = new List<BatchGraphValue>(),
                    Name = "OEE_Rf"

                };

                var STATI_graphItems = new BatchGraphValues
                {
                    BatchId = item.Id,
                    TickInterval = filterCriteria.TickInterval,
                    Values = new List<BatchGraphValue>(),
                    Name = "STATI"

                };


                for (int i = 0; i <= totalTicks; i++)
                {
                    var dt = dtStartNeutrally.AddSeconds(i * filterCriteria.TickInterval);
                    OEE_graphItems.Values.Add(new BatchGraphValue { At = dt, Index = i, Value = null });
                    OEE_Rf_graphItems.Values.Add(new BatchGraphValue { At = dt, Index = i, Value = null });
                    STATI_graphItems.Values.Add(new BatchGraphValue { At = dt, Index = i, Value = null });

                }

                #region Componente OEE 
                // -------------------------------------------------------------------------------
                // viene iniziato il ciclo sulla OEE   
                int iCntOeeTrend = item.Trend_Oee.Count;
                foreach (var oee_item_trend in item.Trend_Oee)
                {
                    var limitMax = iCntOeeTrend == 1 ? DateTime.Now : oee_item_trend.DataOra;
                    var limitValue = oee_item_trend.Valore;
                    var listOfGraphItems = OEE_graphItems.Values.Where(o => o.At <= limitMax && o.Value == null);
                    foreach (var oee_graphItem in listOfGraphItems)
                        oee_graphItem.Value = limitValue;
                }

                // inserimento dell'elemento array di uscita
                result.Add(OEE_graphItems);
                #endregion

                #region Componente OEE Rf
                // -------------------------------------------------------------------------------
                // viene iniziato il ciclo sulla OEE, componente solo Rf
                int iCntOeeRfTrend = item.Trend_Oee_Rf.Count;
                foreach (var oee_rf_item in item.Trend_Oee_Rf)
                {
                    var limitMax = iCntOeeRfTrend == 1 ? DateTime.Now : oee_rf_item.DataOra;
                    var limitValue = oee_rf_item.Valore;
                    var listOfGraphItems = OEE_Rf_graphItems.Values.Where(o => o.At <= limitMax && o.Value == null);
                    foreach (var oee_graphItem in listOfGraphItems)
                        oee_graphItem.Value = limitValue;
                }

                // inserimento dell'elemento array di uscita
                result.Add(OEE_Rf_graphItems);
                #endregion
            }

            return result;

        }


        [HttpPost]
        [ActionName("getBatchFilter")]
        public async Task<List<Batch>> GetBatchFilter(string idStato)
        {

            var mongoInstance = MongoRepository.New;
            var listAllBatch = await mongoInstance.All<Batch>(GetAll_StandardEscludedField, new string[] { });
            var listMacchina = await mongoInstance.All<Macchina>();

            // filtro per stazione
            if (idStato != "-1")
            {

                if (idStato == "inLavorazione")
                {
                    StatoBatch statusFilter = (StatoBatch)Enum.Parse(typeof(StatoBatch), idStato, true);
                    var listOfBatchFilter = listAllBatch.Where(x => x.Stato == statusFilter || x.Stato == StatoBatch.InLavorazioneInEmergenza || x.Stato == StatoBatch.InLavorazioneInIdle || x.Stato == StatoBatch.InLavorazioneInPausa || x.Stato == StatoBatch.InLavorazioneInPausaS || x.Stato == StatoBatch.InPausa).ToList();
                    GetAllMacchineString(listMacchina, listOfBatchFilter);
                    return listOfBatchFilter;
                }
                else if (idStato == "terminato")
                {
                    StatoBatch statusFilter = (StatoBatch)Enum.Parse(typeof(StatoBatch), idStato, true);
                    var listOfBatchFilter = listAllBatch.Where(x => x.Stato == statusFilter || x.Stato == StatoBatch.TerminatoAbortito).ToList();
                    GetAllMacchineString(listMacchina, listOfBatchFilter);
                    return listOfBatchFilter;
                }
                else if (idStato == "inModifica")
                {
                    var listOfBatchFilter = listAllBatch.Where(x => x.Stato == StatoBatch.InModifica).ToList();
                    GetAllMacchineString(listMacchina, listOfBatchFilter);
                    return listOfBatchFilter;
                }
                else if (idStato == "inSospensione")
                {
                    var listOfBatchFilter = listAllBatch.Where(x => x.Stato == StatoBatch.InSospensione).ToList();
                    GetAllMacchineString(listMacchina, listOfBatchFilter);
                    return listOfBatchFilter;
                }
                else if (idStato == "priorita")
                {
                    var listOfBatchFilter = listAllBatch.Where(x => x.Stato == StatoBatch.InAttesa || x.Stato == StatoBatch.InModifica || x.Stato == StatoBatch.InSospensione).ToList();
                    GetAllMacchineString(listMacchina, listOfBatchFilter);
                    return listOfBatchFilter.OrderBy(x => x.Priorita).ToList();
                }
                else
                {
                    StatoBatch statusFilter = (StatoBatch)Enum.Parse(typeof(StatoBatch), idStato, true);
                    var listOfBatchFilter = listAllBatch.Where(x => x.Stato == statusFilter).ToList();
                    GetAllMacchineString(listMacchina, listOfBatchFilter);
                    return listOfBatchFilter;
                }
            }
            else
            {
                return listAllBatch;
            }


        }

        private static void GetAllMacchineString(List<Macchina> listMacchina, List<Batch> listOfBatchFilter)
        {
            HashSet<string> macchineCheck = new HashSet<string>();
            foreach (var batch in listOfBatchFilter)
            {
                if (batch.Fasi is null) { continue; }
                macchineCheck.Clear();
                var stringMacchine = string.Empty;
                foreach (var fase in batch.Fasi)
                {
                    var macchina = listMacchina.FirstOrDefault(m => m.Id == fase.Macchine?.FirstOrDefault()?.Macchina?.Id);
                    if (macchina is object)
                    {
                        string codice = macchina.Codice;
                        if (macchineCheck.Add(codice))
                        {
                            if (stringMacchine != string.Empty)
                            {
                                stringMacchine += ", ";
                            }
                            stringMacchine += codice;
                        }
                    }
                }
                batch.StringMacchine = stringMacchine;
            }
        }


        //--------------------------------------------------------------------------------------------------------------------------------------
        //FUNZIONE PER PROPAGARE UN MESSAGGIO DI AVVIO IMPORT MANUALE, VERRA' RICEVUTO DALLA LOGICA (SUPERVISOR BATCH) E AVVIERA' UN NUOVO IMPORT
        //--------------------------------------------------------------------------------------------------------------------------------------

        [HttpPost]
        [ActionName("getBatchImportManual")]
        public async Task<bool> GetBatchImportManual(string username)
        {
            await MessageServices.Instance.Publish(RequestStartImportBatch.Name, new RequestStartImportBatch { Username = username });
            return true;
        }


        //--------------------------------------------------------------------------------------------------------------------------------------
        //FUNZIONE PER GESTIRE LA PRIORITA' e settare su mongo/Ram
        //--------------------------------------------------------------------------------------------------------------------------------------

        [HttpPost]
        [Route("getBatchSetPriority")]
        public async Task<bool> GetBatchSetPriority(List<Batch> listOfBatch)
        {
            for (int i = 0; i < listOfBatch.Count; i++)
            {
                var _batch = BatchManager.Instance.BatchInMemory().FirstOrDefault(x => x.Id == listOfBatch[i].Id);
                if (_batch == null)
                {
                    _batch = await MongoRepository.Instance.GetAsync<Batch>(listOfBatch[i].Id);
                    if (_batch == null) return false;
                    _batch.Priorita = i + 1;

                }
                else
                    _batch.Priorita = i + 1;

                await MongoRepository.Instance.SetAsync(_batch);
            }
            return true;
        }
    }


}
