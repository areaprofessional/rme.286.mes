﻿using AreaMes.Model;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
    [RoutePrefix("api/distintabase")]
    public class DistintaBaseController : BaseController<DistintaBase>
    {
        public async override Task<IEnumerable<DistintaBase>> GetAll()
        {
            var x = await MongoRepository.Instance.All<DistintaBase>(new string[] { }, new string[] { }); // campi esclusi
            foreach (var item in x)
            {
                if (item.MaterialeInUscita != null)
                    item.MaterialeInUscita.Resolve();

                if (item.Azienda != null)
                    item.Azienda.Resolve();
            }
            return x;
        }


        public async override Task<DistintaBase> Get(string id)
        {
            var doc = await MongoRepository.Instance.GetAsync<DistintaBase>(id);
            if (doc.DistintaBaseParametri!=null)
                foreach (var item in doc.DistintaBaseParametri)
                    item.UnitaDiMisura?.Resolve();

            if (doc.MaterialeInUscita != null)
                doc.MaterialeInUscita.Resolve();

            if (doc.Azienda != null)
                doc.Azienda.Resolve();

            return doc;
        }

        [HttpPost]
        [HttpPut]
        public async override Task<DistintaBase> Set([FromBody] DistintaBase item)
        {
            if (String.IsNullOrWhiteSpace(item.Codice))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Codice'"));


            if (String.IsNullOrWhiteSpace(item.Nome))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Nome'"));

            item.Uniquefy();
             await MongoRepository.Instance.SetAsync(item);
            return item;
        }

        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            return await MongoRepository.Instance.DeleteAsync<DistintaBase>(id);
        }


        [Route("GetDistinteBasiFromMateriale")]
        [HttpGet]
        public List<DistintaBase> GetDistinteBasiFromMateriale(string idMateriale)
        {
            // recupero le eventuali distinte basi per quella macchina
            var filterDb = Builders<DistintaBase>.Filter.Eq("MaterialeInUscita._id", idMateriale);

            var returnedDb = MongoRepository.Instance.Collection<DistintaBase>().Find<DistintaBase>(filterDb).ToList();
            return returnedDb;
        }
    }
}
