﻿using AreaMes.Model.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AreaMes.Model;
using AreaMes.Server.MongoDb;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
    public class TipoValoriController : BaseController<TipoValori>
    {
        public async override Task<IEnumerable<TipoValori>> GetAll()
        {
            return await MongoRepository.Instance.All<TipoValori>(new string[] { }); // campi esclusi
        }


        public async override Task<TipoValori> Get(string id)
        {
            //return await MongoRepository.Instance.GetAsync<TipoValori>(id);
           var a= await MongoRepository.Instance.GetAsync<TipoValori>(id);
            return a;
        }

        [HttpPost]
        [HttpPut]
        public async override Task<TipoValori> Set([FromBody] TipoValori item)
        {
            //----- Data Validation ------------------------------------------
            if (item.Nome == null)
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Nome'"));

            if (item.Codice == null)
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Codice'"));

            if (String.IsNullOrWhiteSpace(item.Descrizione))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Descrizione'"));

            //-----------------------------------------------------------------------
            //------Controllo se è un item di default è possibile solo modificare la descrizione 
            //-----------------------------------------------------------------------
            if (item.IsInternal)
            {
                var itemInternal = await MongoRepository.Instance.GetAsync<TipoValori>(item.Id);
                itemInternal.Descrizione = item.Descrizione;
                await MongoRepository.Instance.SetAsync(itemInternal);
                return itemInternal;
            }



            item.DataOra = DateTime.Now;
            item.Uniquefy();
            await MongoRepository.Instance.SetAsync(item);
            return item;

        }

        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            var itemInternal = await MongoRepository.Instance.GetAsync<TipoValori>(id);
            if(itemInternal.IsInternal) return await Task.FromResult(false);

            return await MongoRepository.Instance.DeleteAsync<TipoValori>(id);
        }

    }
}
