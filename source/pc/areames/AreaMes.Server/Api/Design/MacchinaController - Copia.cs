﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using AreaMes.Model;
using AreaMes.Model.Design;
using AreaMes.Server.MongoDb;
using ER.Persistance.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace AreaMes.Server.Api.Design
{

    public class MacchinaController2 : BaseController<Macchina>
    {


       
        public async override Task<IEnumerable<Macchina>> GetAll()
        {
            var query = collection.
                Find(Builders<Macchina>.Filter.Empty).
                Project<Macchina>(
                    Builders<Macchina>.Projection
                        .Exclude(doc => doc.Descrizione)
                    );

            var results = await query.ToListAsync();

            return results;

            #region test


            //var b = await collection.Find(Builders<Macchina>.Filter.Empty).Project<Macchina>(pr).ToListAsync();

            //return b;
            //; BsonSerializer.Deserialize<Macchina>();
            //var b = a.Cast<Macchina>();

            //return b;



            //try
            //{
            //    var a = Session.Current.SearchSummary<Macchina>(String.Empty).Select(o => new Macchina
            //    {
            //        Id = o.Master.DocId,
            //        Barcode = o.Master.Barcode,
            //        Codice = o.Master.Codice,
            //        IsEsterna = o.Master.IsEsterna,
            //        Nome = o.Master.Nome
            //    }).ToList();


            //    return a;
            //}
            //catch (Exception ec)
            //{
            //    string s = ec.Message;
            //}

            //return null;

            #endregion

        }
        public async override Task<Macchina> Get(string id)
        {
            var a = await collection.FindAsync(Builders<Macchina>.Filter.Eq("Id", id)).Result.FirstOrDefaultAsync();

            a.Tipologia = MongoDbHelper.GetById<TipiMacchina>(a.TipologiaRef.Id.AsString);

            return a;

            //Guid cg;
            //Guid.TryParse(id, out cg);

            //return await Session.Current.GetAsync<Macchina>(cg);
        }

        [HttpPost]
        [HttpPut]
        public async override Task<string> Set([FromBody] Macchina item)
        {
            item.Uniquefy();
            item.TipologiaRef = MongoDBRef<TipiMacchina>.Create(item.Tipologia.Id); // new TipiMacchina { Id = item.TipologiaRefId }; //  MongoDBRef<TipiMacchina>.Create(item.TipologiaRefId);

            var result = await collection.ReplaceOneAsync(x => x.Id == item.Id, item, new UpdateOptions { IsUpsert = true });

            return result.UpsertedId.ToString();

            #region test

            //try
            //{
            //    var result = await Session.Current.SetAsync(item);

            //    return result.ToString();
            //}
            //catch (Exception exc)
            //{
            //    string s = exc.Message;
            //}

            //return String.Empty;

            #endregion
        }

        [HttpDelete]
        public override void Delete(Macchina item)
        {
          
        } 

    }
}
