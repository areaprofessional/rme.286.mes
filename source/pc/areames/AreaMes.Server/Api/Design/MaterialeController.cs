﻿using AreaMes.Model;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
    public class MaterialeController : BaseController<Materiale>
    {

        public async override Task<IEnumerable<Materiale>> GetAll()
        {
            return await MongoRepository.Instance.All<Materiale>(new string[] { "Descrizione", "PesoLordo", "PesoNetto" }); // campi esclusi
        }


        public async override Task<Materiale> Get(string id)
        {
            var mi = MongoRepository.Instance;

            var a = await mi.GetAsync<Materiale>(id);
            

            return a;

        }

        //[HttpPost]
        //[HttpPut]
        //public async  Task<Dictionary<Materiale,string>> SetBulk([FromBody] List<Materiale> items)
        //{
        //    var res = new Dictionary<Materiale, string>();

        //    foreach (var item in items)
        //    {
        //        try
        //        {
        //            await this.Set(item);

        //            res.Add(item, String.Empty);
        //        }
        //        catch (Exception exc)
        //        {
        //            res.Add(item, exc.Message);

        //        }
        //    }
          
        //    return res;
        //}


        [HttpPost]
        [HttpPut]
        public async override Task<Materiale> Set([FromBody] Materiale item)
        {
            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Codice))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Codice'"));

            // viene cercato se esiste un record con la stessa chiave
            var batchFoundedByKey = MongoRepository.Instance.Collection<Materiale>().AsQueryable().Where(k => k.Codice == item.Codice).ToList();

            if (batchFoundedByKey.Any(o => o.Id != item.Id))
                throw new Exception(String.Format("Attenzione: è già presente in archivio un elemento con Codice:'{0}'", item.Codice));
            //----- Data Validation ----------------------

            item.Uniquefy();
             await MongoRepository.Instance.SetAsync(item);
            return item;
        }


        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            return await MongoRepository.Instance.DeleteAsync<Materiale>(id);
        }

    }
}
