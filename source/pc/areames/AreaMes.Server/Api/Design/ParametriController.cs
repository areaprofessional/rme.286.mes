﻿using AreaMes.Model;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
    public class ParametriController : BaseController<Parametri>
    {
        public async override Task<IEnumerable<Parametri>> GetAll()
        {
            return await MongoRepository.Instance.All<Parametri>(new string[] { }); // campi esclusi
        }


        public async override Task<Parametri> Get(string id)
        {
            var mi = MongoRepository.Instance;

            var a = await mi.GetAsync<Parametri>(id);

            return a;

        }

        [HttpPost]
        [HttpPut]
        public async override Task<Parametri> Set([FromBody] Parametri item)
        {
            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Codice))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Codice'"));

            if (String.IsNullOrWhiteSpace(item.Tipo))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Tipo'"));


            // viene cercato se esiste un record con la stessa chiave
            var batchFoundedByKey = MongoRepository.Instance.Collection<Parametri>().AsQueryable().Where(k => k.Codice == item.Codice).ToList();

            if (batchFoundedByKey.Any(o => o.Id != item.Id))
                throw new Exception(String.Format("Attenzione: è già presente in archivio un elemento a con Codice:'{0}'", item.Codice));
            //----- Data Validation ----------------------

            item.Uniquefy();
            await MongoRepository.Instance.SetAsync(item);
            return item;
        }


        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            return await MongoRepository.Instance.DeleteAsync<Parametri>(id);
        }

    }





}
