﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AreaMes.Model;
using AreaMes.Model.Design;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;

namespace AreaMes.Server.Api.Design
{

    public class MacchinaController : BaseController<Macchina>
    {
       
        public async override Task<IEnumerable<Macchina>> GetAll()
        {
            var x= await MongoRepository.Instance.All<Macchina>(new string[] { "Descrizione" }); // campi esclusi
            if(x != null)
            {
                foreach (var item in x)
                {
                    if (item.Azienda != null)
                        item.Azienda.Resolve();
                }
            }

            return x;
        }


        public async override Task<Macchina> Get(string id)
        {
            var mi = MongoRepository.Instance;

            var a = await mi.GetAsync<Macchina>(id);
            if (a.Tipologia !=null)
                a.Tipologia.Resolve();

            if (a.Azienda != null)
                a.Azienda.Resolve();

            return a;

        }

        [HttpPost]
        [HttpPut]
        public async override Task<Macchina> Set([FromBody] Macchina item)
        {
            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Codice))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Codice'"));

            if (String.IsNullOrWhiteSpace(item.Nome))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Nome'"));

            // viene cercato se esiste un record con la stessa chiave
            var batchFoundedByKey = MongoRepository.Instance.Collection<Macchina>().AsQueryable().Where(k => k.Codice.ToLower().Contains(item.Codice.ToLower())).ToList();

            if (batchFoundedByKey.Any(o => o.Id != item.Id))
                throw new Exception(String.Format("Attenzione: è già presente in archivio un elemento con Codice:'{0}'", item.Codice));
            //----- Data Validation ----------------------


            item.Uniquefy();
            await MongoRepository.Instance.SetAsync(item);
            return item;
        }


        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            return await MongoRepository.Instance.DeleteAsync<Macchina>(id);
        }

    }
}
