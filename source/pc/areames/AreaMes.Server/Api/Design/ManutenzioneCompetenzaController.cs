﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AreaMes.Model.Design;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;
using System.Web.Http;
namespace AreaMes.Server.Api.Design
{
    public class ManutenzioneCompetenzaController : BaseController<ManutenzioneCompetenza>
    {


        public async override Task<IEnumerable<ManutenzioneCompetenza>> GetAll()
        {
            return await MongoRepository.Instance.All<ManutenzioneCompetenza>(new string[] { }); // campi esclusi
        }


        public async override Task<ManutenzioneCompetenza> Get(string id)
        {
            var mi = MongoRepository.Instance;

            var a = await mi.GetAsync<ManutenzioneCompetenza>(id);

            return a;

        }



        [HttpPost]
        [HttpPut]
        public async override Task<ManutenzioneCompetenza> Set([FromBody] ManutenzioneCompetenza item)
        {
            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Codice))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Codice'"));

            // viene cercato se esiste un record con la stessa chiave
            var batchFoundedByKey = MongoRepository.Instance.Collection<ManutenzioneCompetenza>().AsQueryable().Where(name => name.Codice == item.Codice).ToList();

            if (batchFoundedByKey.Any(o => o.Id != item.Id))
                throw new Exception(String.Format("Attenzione: è già presente in archivio un elemento con Codice:'{0}'", item.Codice));
            //----- Data Validation ----------------------

            item.Uniquefy();
            await MongoRepository.Instance.SetAsync(item);
            return item;
        }

        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            return await MongoRepository.Instance.DeleteAsync<ManutenzioneCompetenza>(id);
        }



    }
}
