﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AreaMes.Model.Design;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;

namespace AreaMes.Server.Api.Design
{
    public class TipiMacchinaController : BaseController<TipiMacchina>
    {



        public async override Task<IEnumerable<TipiMacchina>> GetAll()
        {
            return await MongoRepository.Instance.All<TipiMacchina>(null);
        }


        public async override Task<TipiMacchina> Get(string id)
        {
            return await MongoRepository.Instance.GetAsync<TipiMacchina>(id);
        }



        [HttpPost]
        [HttpPut]
        public async override Task<TipiMacchina> Set(TipiMacchina item)
        {
            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Codice))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Codice'"));

            // viene cercato se esiste un record con la stessa chiave
            var batchFoundedByKey = MongoRepository.Instance.Collection<TipiMacchina>().AsQueryable().Where(k => k.Codice == item.Codice).ToList();

            if (batchFoundedByKey.Any(o => o.Id != item.Id))
                throw new Exception(String.Format("Attenzione: è già presente in archivio un elemento con Codice:'{0}'", item.Codice));
            //----- Data Validation ----------------------

            item.Uniquefy();
            await MongoRepository.Instance.SetAsync(item);
            return item; ;

        }

        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            return await MongoRepository.Instance.DeleteAsync<TipiMacchina>(id);
        }


    }
}
