﻿using AreaMes.Model;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
    public class AziendaController : BaseController<Azienda>
    {
        public async override Task<IEnumerable<Azienda>> GetAll()
        {
            return (await MongoRepository.Instance.All<Azienda>(new string[] {  })).OrderBy(o=>o.Nome); // campi esclusi
        }


        public async override Task<Azienda> Get(string id)
        {
            var mi = MongoRepository.Instance;

            var a = await mi.GetAsync<Azienda>(id);

            return a;

        }

        [HttpPost]
        [HttpPut]
        public async override Task<Azienda> Set([FromBody] Azienda item)
        {
            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Nome))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Nome'"));

            // viene cercato se esiste un record con la stessa chiave
            var batchFoundedByKey = MongoRepository.Instance.Collection<Azienda>().AsQueryable().Where(k => k.Nome == item.Nome).ToList();

            if (batchFoundedByKey.Any(o => o.Id != item.Id))
                throw new Exception(String.Format("Attenzione: è già presente in archivio un elemento con Nome:'{0}'", item.Nome));
            //----- Data Validation ----------------------

            item.Uniquefy();
             await MongoRepository.Instance.SetAsync(item);
            return item;
        }



        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            return await MongoRepository.Instance.DeleteAsync<Azienda>(id);
        }


    }
}
