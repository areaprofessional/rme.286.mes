﻿using AreaMes.Model;
using AreaMes.Model.Design;
using AreaMes.Server.MongoDb;
using AreaMes.Server.Utils;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
    [RoutePrefix("api/operatore")]
    public class OperatoreController : BaseController<Operatore>
    {
        public async override Task<IEnumerable<Operatore>> GetAll()
        {
            return await MongoRepository.Instance.All<Operatore>(new string[] { "Username", "Password" }); // campi esclusi
        }


        public async override Task<Operatore> Get(string id)
        {
            var mi = MongoRepository.Instance;

            var a = await mi.GetAsync<Operatore>(id);
            //a.OperatoreCompetenza.Resolve(); // passo le informazioni anche dell'esternal doc (relazioni)
            if (a.Azienda != null)
                a.Azienda.Resolve();

            return a;

        }


        [Route("Login")]
        [HttpGet]
        public async Task<Operatore> Login(string username, string password)
        {
            var f = Builders<Operatore>.Filter.Eq("Username", username) & Builders<Operatore>.Filter.Eq("Password", password);

            var a = MongoRepository.Instance.Collection<Operatore>().Find(f).FirstOrDefault();

            if (a != null)
            {
                SessionManager.Instance.Set(new SessionInfo { Id = a.Username, StartedAt = DateTime.Now, UserName = a.Username });
                await MessageServices.Instance.Publish(Logon.Name, new Logon { Username = a.Username });
            }

            return a;

        }


        [HttpPost]
        [HttpPut]
        public async override Task<Operatore> Set([FromBody] Operatore item)
        {
            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Username))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Username'"));

            // viene cercato se esiste un record con la stessa chiave
            var batchFoundedByKey = MongoRepository.Instance.Collection<Operatore>().AsQueryable().Where(name => name.Username == item.Username).ToList();

            if (batchFoundedByKey.Any(o => o.Id != item.Id))
                throw new Exception(String.Format("Attenzione: è già presente in archivio un elemento con Username:'{0}'", item.Username));
            //----- Data Validation ----------------------

            item.Uniquefy();
            await MongoRepository.Instance.SetAsync(item);
            return item;
        }

        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            return await MongoRepository.Instance.DeleteAsync<Operatore>(id);
        }

    }
}
