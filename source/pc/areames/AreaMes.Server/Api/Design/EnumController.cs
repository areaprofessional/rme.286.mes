﻿using AreaMes.Model;
using AreaMes.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

using AreaMes.Server.MongoDb;
using AreaMes.Server.Utils;
using Newtonsoft.Json;

namespace AreaMes.Server.Api.Design
{
    [RoutePrefix("api/enum")]
    public class EnumController : ApiController
    {

        [Route("GetTipiDato")]
        public List<EnumResult> GetTipiDato()
        {
            var enumValues = Enum.GetValues(typeof(TipoDato));
            return EnumToList(enumValues);
        }


        [Route("GetUmList")]
        public async Task<List<EnumResult>> GetUmList()
        {
            List<UnitaDiMisura> um = await MongoRepository.Instance.All<UnitaDiMisura>(new string[] { }, new string[] { });
            List<EnumResult> result = new List<EnumResult>();
            foreach (var row in um)
            {
                result.Add(new EnumResult { Id = row.Id.ToString(), Value = row.Nome.ToString() });
            }
            return result;
        }

        [Route("GetTipoLingua")]
        public List<EnumResult> GetTipoLingua()
        {
            var enumValues = Enum.GetValues(typeof(Lingua));
            return EnumToList(enumValues);
        }


        [Route("GetLivelliAccesso")]
        public List<EnumResult> GetLivelliAccesso()
        {
            var enumValues = Enum.GetValues(typeof(OperatoreLivelloLogin));
            return EnumToList(enumValues);
        }

        [Route("GetTipiMateriale")]
        public List<EnumResult> GetTipiMateriale()
        {
            
            var enumValues = Enum.GetValues(typeof(TipoMateriale));
            return EnumToList(enumValues);
        }

        [Route("GetCausaliFase")]
        public List<EnumResult> GetCausaliFase()
        {

            var enumValues = Enum.GetValues(typeof(CausaleFase));
            return EnumToList(enumValues);
        }

        [Route("GetStatiDistintaBase")]
        public List<EnumResult> GetStatiDistintaBase()
        {

            var enumValues = Enum.GetValues(typeof(StatoDistintaBase));
            return EnumToList(enumValues);
        }

        [Route("GetStatiBatch")]
        public List<EnumResult> GetStatiBatch()
        {

            var enumValues = Enum.GetValues(typeof(StatoBatch));
            return EnumToList(enumValues);
        }

        [Route("GetTipoValori")]
        public List<EnumResult> GetTipoValori()
        {
            var enumValues = Enum.GetValues(typeof(TipoValore));
            return EnumToList(enumValues);
        }

        [Route("GetTipoConteggio")]
        public List<EnumResult> GetTipoConteggio()
        {
            var enumValues = Enum.GetValues(typeof(TipoConteggio));
            return EnumToList(enumValues);
        }

        [Route("GetTranslations")]
        public Dictionary<string, string> GetTranslations(string lang)
        {
           
            string lang1 = String.IsNullOrWhiteSpace(lang) ? "it" : lang;

            var result = new Dictionary<string, string>();
            foreach (var item in TranslationManager.GetTranslationItems(lang1))
                result.Add(item.Key, item.Value);

            var s = JsonConvert.SerializeObject(result);

            return result;
        }

        /// <summary>
        /// Metodo di elaborazione per restituire gli enum
        /// </summary>
        /// <param name="pArray"></param>
        /// <returns></returns>
        private List<EnumResult> EnumToList(Array pArray)
        {
            List<EnumResult> result = new List<EnumResult>();
           
            int x = 0;
            foreach (var val in pArray)
            {
                result.Add(new EnumResult { Id = x.ToString(), Value = val.ToString() });
                x++;
            }

            return result;
        }

        [Route("GetAziende")]
        public async Task<List<EnumResult>> GetAziende()
        {
            List<Azienda> um = await MongoRepository.Instance.All<Azienda>(new string[] { }, new string[] { });
            List<EnumResult> result = new List<EnumResult>();
            foreach (var row in um)
                result.Add(new EnumResult { Id = row.Id.ToString(), Value = row.Nome.ToString() });
            return result;
        }

    }

    /// <summary>
    /// classe di supporto per costruire un json sulle enumerazioni
    /// </summary>
    public class EnumResult
    {
        public string Id { get; set; }

        public string Value { get; set; }
    }
}
