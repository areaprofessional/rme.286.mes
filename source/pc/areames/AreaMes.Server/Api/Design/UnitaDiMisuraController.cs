﻿using AreaMes.Model;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
    public class UnitaDiMisuraController : BaseController<UnitaDiMisura>
    {
        public async override Task<IEnumerable<UnitaDiMisura>> GetAll()
        {
            return (await MongoRepository.Instance.All<UnitaDiMisura>(new string[] {  })).OrderBy(o=>o.Nome); // campi esclusi
        }


        public async override Task<UnitaDiMisura> Get(string id)
        {
            var mi = MongoRepository.Instance;

            var a = await mi.GetAsync<UnitaDiMisura>(id);

            return a;

        }

        [HttpPost]
        [HttpPut]
        public async override Task<UnitaDiMisura> Set([FromBody] UnitaDiMisura item)
        {
            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Codice))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Codice'"));

            // viene cercato se esiste un record con la stessa chiave
            var batchFoundedByKey = MongoRepository.Instance.Collection<UnitaDiMisura>().AsQueryable().Where(k => k.Codice == item.Codice).ToList();

            if (batchFoundedByKey.Any(o => o.Id != item.Id))
                throw new Exception(String.Format("Attenzione: è già presente in archivio un elemento con Codice:'{0}'", item.Codice));
            //----- Data Validation ----------------------

            item.Uniquefy();
             await MongoRepository.Instance.SetAsync(item);
            return item;
        }

        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            return await MongoRepository.Instance.DeleteAsync<UnitaDiMisura>(id);
        }


    }
}
