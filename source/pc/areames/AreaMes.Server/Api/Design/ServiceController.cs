﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using AreaMes.Model;
using AreaMes.Model.Design;
using AreaMes.Model.Dto;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;

namespace AreaMes.Server.Api.Design
{


    public class ServiceController : BaseController<BatchService>
    {


        public async override Task<IEnumerable<BatchService>> GetAll()
        {
            var a = await MongoRepository.Instance.All<BatchService>(new string[] { });// campi esclusi
            for (int i = 0; i < a.Count; i++)
            {
                if (a[i].Macchina != null)
                    a[i].Macchina.Resolve();
                if (a[i].TipoManutenzione != null)
                    a[i].TipoManutenzione.Resolve();
                if (a[i].Azienda != null)
                    a[i].Azienda.Resolve();
                if (a[i].TipoGuastoManutenzione != null)
                    a[i].TipoGuastoManutenzione.Resolve();
                if (a[i].TipoSegnalazioneManutenzione != null)
                    a[i].TipoSegnalazioneManutenzione.Resolve();
            }

            return a;
        }


        public async override Task<BatchService> Get(string id)
        {
            var mi = MongoRepository.Instance;

            var a = await mi.GetAsync<BatchService>(id);
            if (a.Macchina!=null)
                a.Macchina.Resolve();
            if (a.Azienda != null)
                a.Azienda.Resolve();
            if (a.TipoManutenzione != null)
                a.TipoManutenzione.Resolve();
            return a;

        }



        [HttpPost]
        [HttpPut]
        public async override Task<BatchService> Set([FromBody] BatchService item)
        {
            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Nome))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Nome'"));
            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.TipoManutenzione.Id))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Tipo Manutenzione'"));
            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Macchina.Id))
                throw new Exception(String.Format("Attenzione: è necessario compilare il campo 'Macchina'"));

            ////----- Data Validation ----------------------
            if (item.Stato == StatoBatch.InModifica)
            {
                item.Stato = StatoBatch.InAttesa;


                if (item.Creazione==null)
                    item.Creazione = DateTime.Now;
            }
            else if (item.Stato == StatoBatch.InLavorazione)
            {

                if (item.Inizio == null)
                    item.Inizio = DateTime.Now;
            }
            else if (item.Stato == StatoBatch.TerminatoAbortito || item.Stato == StatoBatch.Terminato)
            {
                item.Fine = DateTime.Now;
                var DurataTotale= DateTime.Now - Convert.ToDateTime(item.Inizio);
                item.DurataTotale = new TimeTotal();
                item.DurataTotale.Days = DurataTotale.Days;
                item.DurataTotale.Hours = DurataTotale.Hours;
                item.DurataTotale.Minutes = DurataTotale.Minutes;
                item.DurataTotale.Seconds = DurataTotale.Seconds;
            }


            item.Uniquefy();
            await MongoRepository.Instance.SetAsync(item);
            return item;
        }

        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            return await MongoRepository.Instance.DeleteAsync<BatchService>(id);
        }


    }
}
