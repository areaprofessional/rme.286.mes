﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Server.Utils
{
    public static class ReflectionExtensions
    {
        private static MethodInfo _gethelper = null;
        private static MethodInfo _sethelper = null;

        private static Dictionary<Type, Dictionary<string, Delegate>> _getdelegates =
            new Dictionary<Type, Dictionary<string, Delegate>>();

        private static Dictionary<Type, Dictionary<string, Delegate>> _setdelegates =
            new Dictionary<Type, Dictionary<string, Delegate>>();

        static ReflectionExtensions()
        {
            _gethelper = typeof (ReflectionExtensions).GetMethod("InternalCreateGetDelegate", BindingFlags.Static |
                                                                                              BindingFlags.NonPublic |
                                                                                              BindingFlags.ExactBinding);

            _sethelper = typeof (ReflectionExtensions).GetMethod("InternalCreateSetDelegate", BindingFlags.Static |
                                                                                              BindingFlags.NonPublic |
                                                                                              BindingFlags.ExactBinding);
        }

        public static void FastSet<T>(this T entity, string propertyname, object value)
        {
            var tt = entity.GetType(); // typeof(T) can be dangerous when entity is object
            lock (_setdelegates)
                if (!_setdelegates.ContainsKey(tt))
                    _setdelegates.Add(tt, new Dictionary<string, Delegate>());

            lock (_setdelegates[tt])
                if (!_setdelegates[tt].ContainsKey(propertyname))
                {
                    var p = tt.GetProperty(propertyname);
                    if (p == null) return;

                    _setdelegates[tt].Add(propertyname, p.FastSetDelegate(tt));
                }

            ((Action<object, object>) _setdelegates[tt][propertyname])(entity, value);
        }

        public static object FastGet<T>(this T entity, string propertyname)
        {
            var tt = entity.GetType(); // typeof(T) can be dangerous when entity is object

            lock (_getdelegates)
                if (!_getdelegates.ContainsKey(tt))
                    _getdelegates.Add(tt, new Dictionary<string, Delegate>());

            lock (_getdelegates[tt])
                if (!_getdelegates[tt].ContainsKey(propertyname))
                {
                    var p = tt.GetProperty(propertyname);
                    if (p == null) return null;
                    _getdelegates[tt].Add(propertyname, tt.GetProperty(propertyname).FastGetDelegate(tt));
                }

            return ((Func<object, object>) _getdelegates[tt][propertyname])(entity);
        }

        public static Func<object, object> FastGetDelegate(this PropertyInfo property, Type t)
        {
            var method = property.GetGetMethod(false);
            MethodInfo ghelper = _gethelper.MakeGenericMethod(t, method.ReturnType);
            return (Func<object, object>) ghelper.Invoke(null, new object[] {method});
        }

        public static Action<object, object> FastSetDelegate(this PropertyInfo property, Type t)
        {
            var method = property.GetSetMethod(false);
            MethodInfo shelper = _sethelper.MakeGenericMethod(t, property.PropertyType);
            return (Action<object, object>) shelper.Invoke(null, new object[] {method});
        }

        private static Func<object, object> InternalCreateGetDelegate<T, TReturn>(MethodInfo method)
        {
            // Convert the slow MethodInfo into a fast, strongly typed, open delegate
            var mdelegate = (Func<T, TReturn>) Delegate.CreateDelegate(typeof (Func<T, TReturn>), method);

            // Now create a more weakly typed delegate which will call the strongly typed one
            // and return a Func delegate using a lambda expression. 
            return (object target) => mdelegate((T) target);
        }

        private static Action<object, object> InternalCreateSetDelegate<T, TValue>(MethodInfo method)
        {
            // Convert the slow MethodInfo into a fast, strongly typed, open delegate
            var mdelegate = (Action<T, TValue>) Delegate.CreateDelegate(typeof (Action<T, TValue>), method);

            // Now create a more weakly typed delegate which will call the strongly typed one
            // and return a Func delegate using a lambda expression. 
            return (object target, object value) => mdelegate((T) target, (TValue) value);
        }

        public static string PropertyName<T, TResult>(this T item, Expression<Func<T, TResult>> property)
        {
            if (property.Body.NodeType == ExpressionType.MemberAccess)
                return ((MemberExpression) property.Body).Member.Name;

            throw new ArgumentException("property must be a single Property selector");
        }


        public static TResult Cast<TResult>(this object item)
            #if NETFX_CORE
             where TResult : class
            #endif
                    {
                        if (item == null) return default(TResult);
            #if NETFX_CORE
                  return item as TResult;
            #else
                        if (typeof(TResult).IsAssignableFrom(item.GetType()))
                            return (TResult)item;
                        return default(TResult);
            #endif
        }

        public static T1 Clone<T1>(this object item)  
        {
            T1 result = default(T1);

            if (item == null) return result;

            if (typeof(ICloneable).IsAssignableFrom(item.GetType()))
                return item.Cast<ICloneable>().Clone().Cast<T1>();

            result = Activator.CreateInstance<T1>();

            foreach (PropertyInfo pi in item.GetType().GetProperties())
                if (pi.CanRead && pi.CanWrite)
                    result.FastSet(pi.Name, item.FastGet(pi.Name));

            return result;
        }
    }

    public static class ExtensionMethod
    {
        public static bool IsNull(this DateTime value)
        {
            if (value == null) return true;
            if (value.Year == 1) return true;

            return false;

        }


    }
}
