﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Server
{
    public static class Logger
    {
        public static readonly log4net.ILog Default = log4net.LogManager.GetLogger("MES");

        public static readonly log4net.ILog M2M = log4net.LogManager.GetLogger("M2M");

        public static readonly log4net.ILog MongoDb = log4net.LogManager.GetLogger("MongoDb");

        public static readonly log4net.ILog ArcaConnector = log4net.LogManager.GetLogger("ArcaConnector");

        public static readonly log4net.ILog ShiftManager = log4net.LogManager.GetLogger("ShiftManager");

        public static readonly log4net.ILog DashboardManager = log4net.LogManager.GetLogger("DashboardManager");

        public static readonly log4net.ILog DashboardOee = log4net.LogManager.GetLogger("DashboardOee");
        
    }
}
