﻿using AreaMes.Model;
using AreaMes.Model.Dto;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using AreaMes.Server.Api.Design;
using AreaMes.Server.MongoDb;
using AreaMes.Server.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AreaMes.Server.Utils
{
    public class DashboardStabilimentoManager
    {
        //ILog logger;
        public static DashboardStabilimentoManager Instance { get; set; } = new DashboardStabilimentoManager();
        private System.Threading.Timer _timer;
        private bool _timerRunning = false;
        private object _timerLock = new object();


        public void Init()
        {

            Logger.DashboardManager.Info("Start configuration...");
            Logger.DashboardManager.Info("Create Timer configuration...");

            _timer = new System.Threading.Timer(async (a) =>
            {

                lock (_timerLock)
                {
                    if (_timerRunning) return;
                    _timerRunning = true;
                }

                try
                {
                    Get();
                }
                catch (Exception exc)
                {
                    Logger.DashboardManager.Error("Error Timer  ", exc);
                }

                lock (_timerLock)
                    _timerRunning = false;
            }, null, 10000, 10 * 1000);
            Logger.DashboardManager.Info("Create Timer configuration... Completed");
        }


        public async void Get()
        {
            try
            {
                var ret = new DashboardOeeMacchina();
                var listaMacchine = await MongoRepository.Instance.All<Macchina>();
                listaMacchine = listaMacchine.Where(x => x.Id != null).ToList();
                var listBatch = BatchManager.Instance.BatchInMemory();

                foreach (var macchina in listaMacchine)
                {
                    //var listBatchMacchina = listBatch.Where(x => {
                    //    if(x.Fasi != null)
                    //    {
                    //        var y = x.Fasi.FirstOrDefault();
                    //        if(y.Macchine != null)
                    //        {
                    //            var z = y.Macchine.FirstOrDefault();
                    //            if (z.Macchina.Id == macchina.Id)
                    //                return true;
                    //            else
                    //                return false;
                    //        }
                    //        else
                    //            return false;
                    //    }
                    //    else
                    //        return false;
                    //}).ToList();

                    //var listBatchMacchina = listBatch.Where(x => 

                    //        x.Fasi.Where( xx => { if (xx.Macchine.FirstOrDefault().Macchina.Id == macchina.Id) return true; else return false; });

                    //).ToList();

                    var listBatchMacchina = new List<Batch>();
                    var batch = new Batch();
                    foreach (var btc in listBatch)
                    {
                        foreach (var fase in btc.Fasi)
                        {
                            if(fase.Macchine.FirstOrDefault().Macchina != null)
                                if (fase.Macchine.FirstOrDefault().Macchina.Id == macchina.Id && (fase.Stato != StatoFasi.InAttesa && fase.Stato != StatoFasi.Terminato) && batch.Id == null)
                                    batch = btc;
                        }
                    }

                    if(batch.Id == null)
                        foreach (var btc in listBatch)
                        {
                            foreach (var fase in btc.Fasi)
                            {
                                if(fase.Macchine.FirstOrDefault().Macchina != null)
                                    if (fase.Macchine.FirstOrDefault().Macchina.Id == macchina.Id && fase.Stato == StatoFasi.InAttesa)
                                        batch = btc;
                            }
                        }

                    //var batch = listBatchMacchina.LastOrDefault();
                    if (batch.Id != null)
                    {
                        var itemMacchina = new DashboardOeeMacchinaDettaglio();
                        //var eventiPezziBuoni = batch.Eventi.Where(x=> x.AggregazioneKey.Contains("DAY:"+ DateTime.Now.Day + DateTime.Now.Mount + DateTime.Now.Year));
                        itemMacchina.PezziScarto = batch.PezziScartati;
                        itemMacchina.Oee = batch.OEE;
                        itemMacchina.Oee_Rf = batch.OEE_Rf;
                        itemMacchina.Oee_Rv = batch.OEE_Rv;
                        itemMacchina.Oee_Rq = batch.OEE_Rq;
                        //itemMacchina.Valore1 = Convert.ToDouble(batch.BatchParametri.FirstOrDefault(x => x.Parametro?.Codice == "PesoPerPezzo").Parametro.ValoreDefault);
                        itemMacchina.Valore2 = batch.PercCompletamento;
                        
                        var app = GetTimeStatus(batch);
                        itemMacchina.Valore5 = app[StatoBatch.InLavorazione.ToString()];
                        itemMacchina.TempoLavorazione = secondToString(app[StatoBatch.InLavorazione.ToString()]);
                        itemMacchina.TempoEmergenza = secondToString(app[StatoBatch.InLavorazioneInEmergenza.ToString()]);
                        itemMacchina.TempoPausa = secondToString(app[StatoBatch.InLavorazioneInPausa.ToString()]);
                        // codice pezzo

                        if (batch.Materiale.Descrizione.Count() > 30)
                            itemMacchina.Descrizione1 = batch.Materiale.Descrizione.Substring(0, 27) + "...";
                        else
                            itemMacchina.Descrizione1 = batch.Materiale.Descrizione;

                        itemMacchina.Descrizione2 = Convert.ToString(batch.Stato);
                        itemMacchina.Descrizione4 = macchina.Nome.Replace(" ","");
                        itemMacchina.Descrizione6 = batch.OdP;
                        itemMacchina.Descrizione7 = macchina.Nome;
                        itemMacchina.PezziBuoni = 0;
                        var fase = batch.Fasi.FirstOrDefault(x => x.Stato != StatoFasi.InAttesa && x.Stato != StatoFasi.Terminato);
                        if (fase != null)
                        {
                            itemMacchina.PezziBuoni = fase.PezziProdotti;

                            if (fase.Stato == StatoFasi.InLavorazione)
                            {
                                //itemMacchina.Label = fase.DistintaBaseFase.InLavorazioneLabel?.ToUpper();
                                //itemMacchina.Colore = fase.DistintaBaseFase.InLavorazioneColore?.ToUpper();
                                itemMacchina.Label = "LAVORAZIONE";
                                itemMacchina.Colore = "#30c93d";
                            }
                            else if (fase.Stato == StatoFasi.InLavorazioneInPausa)
                            {
                                //itemMacchina.Label = fase.DistintaBaseFase.InLavorazioneInPausaLabel?.ToUpper();
                                //itemMacchina.Colore = fase.DistintaBaseFase.InLavorazioneInPausaColore?.ToUpper();
                                itemMacchina.Label = "PAUSA";
                                itemMacchina.Colore = "#e8c70e";
                            }
                            else if (fase.Stato == StatoFasi.InLavorazioneInEmergenza)
                            {
                                //itemMacchina.Label = fase.DistintaBaseFase.InLavorazioneInEmergenzaLabel?.ToUpper();
                                //itemMacchina.Colore = fase.DistintaBaseFase.InLavorazioneInEmergenzaColore?.ToUpper();
                                itemMacchina.Label = "EMERGENZA";
                                itemMacchina.Colore = "#db1a1a";
                            }
                            else if (fase.Stato == StatoFasi.InAttesa)
                            {
                                itemMacchina.Label = "ATTESA";
                                itemMacchina.Colore = "#C4C4C4";
                            }
                        }
                        else
                        {
                            itemMacchina.Label = "ATTESA";
                            itemMacchina.Colore = "#C4C4C4";
                        }

                        var dataFiltroInizio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                        var dataFiltroFine = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
                        //if (DateTime.Now.Hour >= 6)
                            //dataFiltroFine = dataFiltroFine.AddDays(1);
                        //else
                            dataFiltroInizio = dataFiltroInizio.AddDays(-1);

                        if (batch.BatchAvanzamenti != null && fase != null )
                        {
                            var avanzamenti = batch.BatchAvanzamenti.Where(a => a.BatchFaseMacchina.Macchina != null && a.BatchFaseMacchina.Macchina.Codice == macchina.Codice).ToList();

                            avanzamenti = avanzamenti.Where(a => ((a.Inizio >= dataFiltroInizio && a.Inizio <= dataFiltroFine) 
                                                                        || (a.Fine >= dataFiltroInizio && a.Fine <= dataFiltroFine || a.Fine == null) )
                                                                    && a.Stato != StatoFasi.Terminato).ToList();

                            if(itemMacchina.ListaAvanzamenti == null)
                                itemMacchina.ListaAvanzamenti = new List<DashboardOeeMacchinaAvanzamento>();
                            
                            if (avanzamenti.Count > 0)
                                avanzamenti.ForEach(avanzamento =>
                                {
                                    var itemAvanzamento = new DashboardOeeMacchinaAvanzamento();
                                    itemAvanzamento.Inizio = Convert.ToDateTime(avanzamento.Inizio);
                                    if(avanzamento.Fine != null)
                                        itemAvanzamento.Fine = Convert.ToDateTime(avanzamento.Fine);
                                    else
                                        itemAvanzamento.Fine = DateTime.Now;

                                    var distintaBaseFase = batch.Fasi.FirstOrDefault(x => x.DistintaBaseFase.Codice == avanzamento.CodiceFase);

                                    if (avanzamento.Stato == StatoFasi.InLavorazione)
                                    {
                                        //itemAvanzamento.StatoLavorazione = fase.DistintaBaseFase.InLavorazioneLabel?.ToUpper();
                                        //itemAvanzamento.Colore = fase.DistintaBaseFase.InLavorazioneColore?.ToUpper();
                                        itemAvanzamento.StatoLavorazione = "LAVORAZIONE";
                                        itemAvanzamento.Colore = "#30c93d";
                                    }
                                    else if (avanzamento.Stato == StatoFasi.InLavorazioneInPausa)
                                    {
                                        //itemAvanzamento.StatoLavorazione = fase.DistintaBaseFase.InLavorazioneInPausaLabel?.ToUpper();
                                        //itemAvanzamento.Colore = fase.DistintaBaseFase.InLavorazioneInPausaColore?.ToUpper();

                                        itemAvanzamento.StatoLavorazione = "PAUSA";
                                        itemAvanzamento.Colore = "#e8c70e";
                                    }
                                    else if (avanzamento.Stato == StatoFasi.InLavorazioneInEmergenza)
                                    {
                                        //itemAvanzamento.StatoLavorazione = fase.DistintaBaseFase.InLavorazioneInEmergenzaLabel?.ToUpper();
                                        //itemAvanzamento.Colore = fase.DistintaBaseFase.InLavorazioneInEmergenzaColore?.ToUpper();

                                        itemAvanzamento.StatoLavorazione = "EMERGENZA";
                                        itemAvanzamento.Colore = "#db1a1a";
                                    }

                                    //if (avanzamento.Stato == StatoFasi.InLavorazione)
                                    //itemAvanzamento.StatoLavorazione = Convert.ToString();
                                    itemAvanzamento.Macchina = macchina.Codice;
                                    //itemAvanzamento.Colore = macchina.Codice;
                                    itemMacchina.ListaAvanzamenti.Add(itemAvanzamento);
                                });
                        }
                        if (ret.listaMacchine == null)
                            ret.listaMacchine = new List<DashboardOeeMacchinaDettaglio>();
                        ret.listaMacchine.Add(itemMacchina);
                    }
                }

                // Oee stabilimento
                ret.Oee = Math.Round(ret.listaMacchine.Sum(x => x.Oee) / ret.listaMacchine.Count, 0);
                ret.Oee_Rf = Math.Round(ret.listaMacchine.Sum(x => x.Oee_Rf) / ret.listaMacchine.Count, 0);
                ret.Oee_Rv = Math.Round(ret.listaMacchine.Sum(x => x.Oee_Rv) / ret.listaMacchine.Count, 0);
                ret.Oee_Rq = Math.Round(ret.listaMacchine.Sum(x => x.Oee_Rq) / ret.listaMacchine.Count, 0);

                // Tonnelate stabilimento
                ret.Valore1 = Math.Round(ret.listaMacchine.Sum(x => x.Valore1 * x.PezziBuoni) / 1000, 0);
                // % scarto
                var pezziBuoni = ret.listaMacchine.Sum(x => x.PezziBuoni);
                var pezziScarti = ret.listaMacchine.Sum(x => x.PezziScarto);
                ret.Descrizione1 = Convert.ToString(Math.Round((Convert.ToDouble(pezziScarti) / Convert.ToDouble(pezziBuoni) * 100), 0)) + " %";
                ret.Descrizione2 = secondToString(Convert.ToInt32(ret.listaMacchine.Sum(x => x.Valore5)));
                ret.Valore2 = pezziBuoni;

                AreaMESManager.Instance.NotifyDashboardOee(new GetDataDashboardOee { Data = ret });
            }
            catch (Exception exc)
            {
                var e = exc;
            }
        }

        // Funzione che ritorna i tempi divisi per stato in secondi
        public Dictionary<string, int> GetTimeStatus (Batch batch)
        {
            var now = DateTime.Now;
            var ret = new Dictionary<string, int>();
            ret.Add(StatoBatch.InLavorazione.ToString(), 0);
            ret.Add(StatoBatch.InLavorazioneInEmergenza.ToString(), 0);
            ret.Add(StatoBatch.InLavorazioneInPausa.ToString(), 0);

            var listaIgnoraTempiPerCodiceFase = new Dictionary<string, Dictionary<StatoFasi, bool>>();
            batch.Fasi.ForEach(x =>
            {
                var listApp = new Dictionary<StatoFasi, bool>();
                listApp.Add(StatoFasi.InLavorazione, true);
                listaIgnoraTempiPerCodiceFase.Add(x.DistintaBaseFase.Codice, listApp);
                listaIgnoraTempiPerCodiceFase[x.DistintaBaseFase.Codice].Add(StatoFasi.InLavorazioneInPausa, true);
                listaIgnoraTempiPerCodiceFase[x.DistintaBaseFase.Codice].Add(StatoFasi.InLavorazioneInEmergenza, true);
            }
            );

            if (batch.BatchAvanzamenti != null)
            {
                foreach (var avanzamento in batch.BatchAvanzamenti)
                {
                    DateTime avanzamentoFine = new DateTime();
                    avanzamentoFine = Convert.ToDateTime(avanzamento.Fine);
                    DateTime avanzamentoInizio = new DateTime();
                    avanzamentoInizio = Convert.ToDateTime(avanzamento.Inizio);

                    if (avanzamentoFine == null || Convert.ToString(avanzamentoFine) == "01/01/0001 00:00:00")
                        avanzamentoFine = now;
                    int diffFineInizioInSec = calculateSeconds(avanzamentoFine) - calculateSeconds(avanzamentoInizio);
                    if(avanzamento.CodiceFase != null)
                    {
                        var app = listaIgnoraTempiPerCodiceFase[avanzamento.CodiceFase];
                        switch (avanzamento.Stato)
                        {
                            case StatoFasi.InLavorazione:
                                if (app[StatoFasi.InLavorazione])
                                    ret[StatoBatch.InLavorazione.ToString()] += diffFineInizioInSec;
                                break;
                            case StatoFasi.InLavorazioneInEmergenza:
                                if (app[StatoFasi.InLavorazioneInEmergenza])
                                    ret[StatoBatch.InLavorazioneInEmergenza.ToString()] += diffFineInizioInSec;
                                break;
                            case StatoFasi.InLavorazioneInPausa:
                                if (app[StatoFasi.InLavorazioneInPausa])
                                    ret[StatoBatch.InLavorazioneInPausa.ToString()] += diffFineInizioInSec;
                                break;
                        }
                    }
                }
            }
            return ret;
        }

        public int calculateSeconds(DateTime time)
        {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);//from 1970/1/1 00:00:00 to now
            TimeSpan result = time.Subtract(dt);
            int seconds = 0;
            seconds = Convert.ToInt32(result.TotalSeconds);
            return seconds;
        }

        public string secondToString(int seconds)
        {
            try
            {
                TimeSpan time = TimeSpan.FromSeconds(seconds);
                var day = Convert.ToString(time.Days);

                if (Convert.ToInt16(day) <= 0)
                    day = "";
                else
                    day = day + "g ";
                return day + time.ToString(@"hh\:mm\:ss");
            }
            catch (Exception exc)
            {
                Logger.DashboardOee.Error("Error  Timer  ", exc);
                return null;
            }
        }


    }
}