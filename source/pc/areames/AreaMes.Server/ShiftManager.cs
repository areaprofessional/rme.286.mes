﻿using AreaMes.Model;
using AreaMes.Model.Design;
using AreaMes.Server.Api.Design;
using AreaMes.Server.MongoDb;
using AreaMes.Server.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AreaMes.Server.Utils
{
    public class ShiftManager
    {
        //ILog logger;
        public static ShiftManager Instance { get; set; } = new ShiftManager();
        private System.Threading.Timer _timer;
        private bool _timerRunning = false;
        private object _timerLock = new object();
        private int _intervalTickRead;
        public List<TurnoDiLavoro> listOfTurniLavoroBefore = new List<TurnoDiLavoro>();

        public void Init()
        {

            Logger.ShiftManager.Info("Start configuration...");
            Logger.ShiftManager.Info("Create Timer configuration...");
            _timer = new System.Threading.Timer(async (a) =>
            {

                lock (_timerLock)
                {
                    if (_timerRunning) return;
                    _timerRunning = true;
                }

                try
                {
                    ShiftChange();
                }
                catch (Exception exc)
                {
                    Logger.ShiftManager.Error("Error  Timer  ", exc);
                }

                lock (_timerLock)
                    _timerRunning = false;
            }, null, 5000, 60 * 1000);
            Logger.ShiftManager.Info("Create Timer configuration... Completed");
        }


        public async void ShiftChange()
        {
            Logger.ShiftManager.Info("ShiftChange() starting... ");
            //Lista degli operatori trovati sul db
            Logger.ShiftManager.Debug("ShiftChange()           From Mongodb, Get<TurnoDiLavoro>()... ");
            var listOfAllTurni = await MongoRepository.Instance.All<TurnoDiLavoro>(new string[] { });
            Logger.ShiftManager.Debug("ShiftChange()           From Mongodb, Get<TurnoDiLavoro>()... completed");
            List<TurnoDiLavoro> listActiveTurni = new List<TurnoDiLavoro>();
            //per tutti i turni prelevati dal DB controllo se sono attivi in questo momento
            foreach (TurnoDiLavoro turno in listOfAllTurni)
            {
                // funzione per controllare se il turno è attivo
                var turnoApp = TurniDiLavoroController.checkIfShiftIsActive(DateTime.Now, turno,null);
                if (turnoApp.Id != null)
                    listActiveTurni.Add(turnoApp);
            }

            List<TurnoDiLavoro> listTurniChangedAdd = new List<TurnoDiLavoro>();
            //confronto la lista dei turni precedentemente attivi con quelli attuali
            foreach (var activeTurno in listActiveTurni)
            {
                var turn = listOfTurniLavoroBefore.FirstOrDefault(x => x.Id == activeTurno.Id);
                if (turn == null)
                {
                    listTurniChangedAdd.Add(activeTurno);
                }
            }

            List<TurnoDiLavoro> listTurniChangedConcluded = new List<TurnoDiLavoro>();
            //Viene controllato se un turno precedentemente attivo è terminato
            foreach (var turnoBefore in listOfTurniLavoroBefore)
            {
                var turn = listActiveTurni.FirstOrDefault(x => x.Id == turnoBefore.Id);
                if (turn == null)
                {
                    listTurniChangedConcluded.Add(turnoBefore);
                }
            }

            //listTurniChangedConcluded.Count > 0
            //if (!isEqual)
            //{
            Logger.ShiftManager.Debug("ShiftChange() update previous shift ...");
            foreach (var item in listTurniChangedConcluded)
            {
                List<TurnoDiLavoro> listTurniPropagation = new List<TurnoDiLavoro>();
                List<TurnoDiLavoro> turnopropadd = listTurniChangedAdd.FindAll(x => x.InizioHH == item.FineHH && x.InizioMM == item.FineMM && (x.Macchine.Count == 0 || x.Macchine.Any(y => item.Macchine.Any(z => z.Id == y.Id))));
                if (turnopropadd != null)
                    listTurniPropagation.AddRange(turnopropadd);

                Logger.ShiftManager.Debug("ShiftChange() start propagation ...");
                await MessageServices.Instance.Publish(CambioTurnoMessage.Name, new CambioTurnoMessage { Before = item, ListOfAfter = listTurniPropagation });
                Logger.ShiftManager.Debug("ShiftChange() start propagation ...  completed");

            }
            Logger.ShiftManager.Debug("ShiftChange() update previous shift ... completed");
            //}

            listOfTurniLavoroBefore = listActiveTurni;
            Logger.ShiftManager.Debug("ShiftChange() starting...  Completed");
            //checkIfShiftIsActive
        }
    }
}