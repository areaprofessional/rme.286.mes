﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Model.Design;
using AreaMes.Model.Dto;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using AreaMes.Server.Api.Design;
using AreaMes.Server.MongoDb;
using AreaMes.Server.SignalR;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Swan;
using Swan.Formatters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace AreaMes.Server.Utils
{
    public class DashboardOeeManager
    {
        //ILog logger;
        public static DashboardOeeManager Instance { get; set; } = new DashboardOeeManager();
        private System.Threading.Timer _timer;
        private bool _timerRunning = false;
        private object _timerLock = new object();
        private DateTime FiltroDal;
        private DateTime FiltroAl;

        public async Task<DashboardOee> GetDashboardOeeFilter()
        {
            var dtoFiltri = new DashboardOee();
            try
            {
                var allMacchine = await MongoRepository.Instance.GetWhereAsync<Macchina>(x => x.IsDisable == false);
                var allTurni = await MongoRepository.Instance.All<TurnoDiLavoro>();
                var allMateriali = await MongoRepository.Instance.All<Materiale>();
                var allBatch = await MongoRepository.Instance.All<Batch>(new string[] { }, new string[] { "OdP" });
                var dataInizio = DateTime.Now;
                var dataFine = DateTime.Now;

                dtoFiltri.ListaTurni = allTurni;
                dtoFiltri.ListaMacchine = allMacchine;
                dtoFiltri.ListaMateriali = allMateriali;
                var listCommesse = new List<ItemCommessa>();
                allBatch.ForEach(x => { listCommesse.Add(new ItemCommessa { Id = x.Id, Odp = x.OdP }); });
                dtoFiltri.ListaCommesse = listCommesse;
                dtoFiltri.FiltroDal = dataInizio;
                dtoFiltri.FiltroAl = dataFine;
            }
            catch (Exception exc)
            {
                var e = exc;
            }
            return dtoFiltri;
        }

        public async Task<ResponseDataDashboardOee> GetDataDashboardOee(RequestDataDashboardOee request)
        {
            Logger.DashboardOee.InfoFormat("Elaboration dashboard OEE...");
            var itemRet = new ResponseDataDashboardOee();
            try
            {
                var filtroDalEventi = DateTime.Now;
                if (request.FiltroDal != null)
                {
                    FiltroDal = request.FiltroDal = new DateTime(request.FiltroDal.Year, request.FiltroDal.Month, request.FiltroDal.Day, 0, 0, 0, 0);
                    //FiltroDal = request.FiltroDal = new DateTime(2022, 9, 1, 0, 0, 0, 0);
                    filtroDalEventi = request.FiltroDal;
                }

                var filtroAlEventi = DateTime.Now;
                if (request.FiltroAl != null)
                {
                    request.FiltroAl = new DateTime(request.FiltroAl.Year, request.FiltroAl.Month, request.FiltroAl.Day, 23, 59, 59, 999);
                    //request.FiltroAl = new DateTime(2022, 9, 30, 23, 59, 59, 999);
                    filtroAlEventi = request.FiltroAl;
                }

                List<Batch> batchList = new List<Batch>(BatchManager.Instance.BatchInMemory().Where(x => x.Stato != StatoBatch.InAttesa));
                //escludere quelli in memory e nell'arco temporale

                var batchListMongo = await MongoRepository.Instance.GetWhereAsync<Batch>(a => (request.ListIdCommesse.Count == 0 || request.ListIdCommesse.Contains(a.Id)) && ((a.Inizio >= request.FiltroDal && a.Inizio <= request.FiltroAl)
                                                                                                || (a.Fine > request.FiltroDal && a.Fine <= request.FiltroAl))
                                                                                                && (a.Stato == Model.Enums.StatoBatch.Terminato
                                                                                                || a.Stato == Model.Enums.StatoBatch.TerminatoAbortito
                                                                                                || a.Stato == Model.Enums.StatoBatch.InModifica));

                batchList = batchList.Concat(batchListMongo).ToList();
                batchList.Where(a => ((a.Inizio >= request.FiltroDal && a.Inizio <= request.FiltroAl) || (a.Fine > request.FiltroDal && a.Fine <= request.FiltroAl || a.Fine == null)));

                batchList = batchList.Where(a => request.ListIdCommesse.Count == 0 || request.ListIdCommesse.Contains(a.Id)).ToList();

                int totPezziBuoni;
                int totPezziScarto;
                int totPezzi;
                int stabilimentoPezziScarto = 0;
                int stabilimentoPezziBuoni = 0;

                ///PROVA TIPO PIVOT GRID
                Dictionary<string, List<TimeDictItem2>> groupedTime = new Dictionary<string, List<TimeDictItem2>>()
                {
                    { nameof(Macchina), new List<TimeDictItem2>() },
                    { nameof(TurnoDiLavoro), new List<TimeDictItem2>() },
                    { nameof(Batch), new List<TimeDictItem2>() }
                };


                var batchListPiana = new List<Batch>();
                var listItemTempi = new List<Dictionary<string, string>>();
                var listaTurni = await MongoRepository.Instance.GetWhereAsync<TurnoDiLavoro>(t => t.IsDisabilitato != true);
                var listMacchine = await MongoRepository.Instance.All<Macchina>();
                var listCausaliRiavvio = await MongoRepository.Instance.All<CausaliRiavvio>();
                var listCausaliScarto = await MongoRepository.Instance.All<TipoValori>();
                var listFermi = new List<DashboardOeeFermiItem>();
                var listScarti = new List<DashboardOeeScartiItem>();
                var listOeeField = new List<string>();
                var listOee = new List<double>();
                var listOee_Rf = new List<double>();
                var listOee_Rq = new List<double>();
                var listOee_Rv = new List<double>();

                var listOeeTrendGrafici = new List<BatchTrendParametri>();

                //Questa oscenità è stata fatta perchè prima del 1 Maggio 2023 i fermi non venivano segnati,
                //tuttavia sono presenti negli avanzamenti
                bool addFermiFromAva = request.FiltroDal < new DateTime(2023, 5, 1);

                var listOeeGrafici = new List<Dictionary<string, object>>();
                var listAccordion = new List<string>();
                //btc.Trend_Oee.Where(t => request.FiltroDal <= t.DataOra && t.DataOra <= request.FiltroAl).ToList();
                itemRet.ListEfficienza = new List<DashboardOeeEfficenza>();


                foreach (var btc in batchList)
                {
                    var btcNew = new Batch();
                    btcNew.BatchAvanzamenti = new List<BatchAvanzamento>();
                    btcNew.Eventi = new List<Evento>();

                    btcNew.DistintaBase = btc.DistintaBase;
                    btcNew.Fasi = btc.Fasi;

                    var serializedFermi = JsonConvert.SerializeObject(btc.Fermi);
                    btcNew.Fermi = JsonConvert.DeserializeObject<List<BatchFermi>>(serializedFermi);

                    bool add = false;
                    foreach (var fase in btc.Fasi.Where(x => x.Macchine.Count > 0))
                    {
                        if ((request.ListIdMacchine.Count == 0 ||
                            (fase.Macchine[0].Macchina != null && request.ListIdMacchine.Contains(fase.Macchine[0].Macchina.Id)))
                                && (request.ListIdMateriali.Count == 0 || request.ListIdMateriali.Contains(btc.Materiale.Id)))
                        {
                            add = true;
                            var avanzamenti = new List<BatchAvanzamento>();
                            if (btc.BatchAvanzamenti != null)
                                avanzamenti = btc.BatchAvanzamenti.Where(a => a.NumeroFase == fase.NumeroFase
                                                                                && ((a.Inizio >= request.FiltroDal && a.Inizio <= request.FiltroAl)
                                                                                    || (a.Fine >= request.FiltroDal && a.Fine <= request.FiltroAl || a.Fine == null))
                                                                                && (request.ListIdTurni.Count == 0 || request.ListIdTurni.Contains(a.Turno.Id))
                                                                                && (request.MostraFuoriTurno == true || (request.MostraFuoriTurno == false && a.Turno.Id != null && a.Turno.Id != string.Empty))
                                                                                && a.Stato != StatoFasi.Terminato
                                                                                && (a.Inizio >= request.FiltroDal && a.Inizio <= request.FiltroAl)).ToList();
                            btcNew.BatchAvanzamenti = btcNew.BatchAvanzamenti.Concat(avanzamenti).ToList();
                        }
                    }

                    var eventi = new List<Evento>();
                    if (btc.Eventi != null)
                        eventi = btc.Eventi.Where(e => (request.FiltroDal <= e.OraEvento && e.OraEvento <= request.FiltroAl)
                                                                        && (request.ListIdTurni.Count == 0 || request.ListIdTurni.Contains(e.Turno != null ? e.Turno.Id : ""))
                                                                        && (request.MostraFuoriTurno == true || (request.MostraFuoriTurno == false && e.Turno != null))).ToList();
                    btcNew.Eventi = eventi;

                    var lastTrendOee = btc.Trend_Oee.Where(t => request.FiltroDal >= t.DataOra).LastOrDefault();
                    // se il batch è da aggiungere viene elaborato
                    if (add)
                    {
                        btcNew.OdP = btc.OdP;
                        btcNew.Stato = btc.Stato;
                        btcNew.PezziScartati = btc.PezziScartati;
                        btcNew.Inizio = btc.Inizio;
                        btcNew.Fine = btc.Fine;
                        btcNew.OEE = btc.OEE;
                        btcNew.PezziDaProdurre = btc.PezziDaProdurre;
                        itemRet.ListBatch.Add(new Batch() { Id = btc.Id, OdP = btc.OdP });

                        btcNew.Trend_Oee = btc.Trend_Oee.Where(t => request.FiltroDal <= t.DataOra && t.DataOra <= request.FiltroAl).ToList();
                        var lastTrendOeeRf = btc.Trend_Oee_Rf.Where(t => t.DataOra < request.FiltroDal).ToList().LastOrDefault();
                        btcNew.Trend_Oee_Rf = btc.Trend_Oee_Rf.Where(t => request.FiltroDal <= t.DataOra && t.DataOra <= request.FiltroAl).ToList();
                        var lastTrendOeeRv = btc.Trend_Oee_Rv.Where(t => request.FiltroDal >= t.DataOra).LastOrDefault();
                        btcNew.Trend_Oee_Rv = btc.Trend_Oee_Rv.Where(t => request.FiltroDal <= t.DataOra && t.DataOra <= request.FiltroAl).ToList();
                        var lastTrendOeeRq = btc.Trend_Oee_Rq.Where(t => request.FiltroDal >= t.DataOra).LastOrDefault();
                        btcNew.Trend_Oee_Rq = btc.Trend_Oee_Rq.Where(t => request.FiltroDal <= t.DataOra && t.DataOra <= request.FiltroAl).ToList();

                        btcNew.Materiale = btc.Materiale;


                        //calcolo i pezzi prodotti per il periodo selezionato
                        totPezziBuoni = 0;
                        totPezziScarto = 0;
                        totPezzi = 0;

                        var fermi = new List<BatchFermi>();
                        if (btcNew.Fermi != null)
                            if (btcNew.Fermi.Count > 0)
                                fermi = new List<BatchFermi>(btcNew.Fermi.Where(f => ((f.Inizio >= request.FiltroDal && f.Inizio <= request.FiltroAl)
                                                                                 || (f.Inizio > FiltroDal && f.Inizio < FiltroAl)
                                                                                 || (f.Fine != null && f.Fine >= FiltroDal && f.Fine <= FiltroAl))
                                                                             && (request.ListIdMacchine.Count == 0 || request.ListIdMacchine.Contains(f.MacchinaId))
                                                                             && (request.ListIdTurni.Count == 0 || request.ListIdTurni.Contains(f.Turno.Id))
                                                                             && (request.ListIdMateriali.Count == 0 || request.ListIdMateriali.Contains(btcNew.Materiale.Id))
                                                                             && (request.MostraFuoriTurno == true || (request.MostraFuoriTurno == false && f.Turno.Id != null && f.Turno.Id != string.Empty))).ToList());

                        foreach (var fermo in fermi)
                        {
                            var macchinaFermo = listMacchine.FirstOrDefault(x => x.Id == fermo.MacchinaId);
                            var faseFermo = btcNew.DistintaBase.Fasi.FirstOrDefault(f => Convert.ToInt32(f.NumeroFase) == fermo.NumeroFase);
                            if (faseFermo != null && faseFermo.Nome.Contains("ATTREZZAGGIO")) { continue; }
                            if (macchinaFermo != null && macchinaFermo.Codice != "ATTREZZAGGIO" && macchinaFermo.Nome != "ATTREZZAGGIO")
                            {
                                var fermoApp = new BatchFermi();
                                fermoApp.Inizio = fermo.Inizio;
                                fermoApp.Fine = fermo.Fine;

                                fermoApp.MacchinaId = fermo.MacchinaId;
                                fermoApp.CausaliRiavvio = fermo.CausaliRiavvio;

                                var idMacchina = fermo.MacchinaId;

                                var idCausaliRiavvio = "";
                                if (fermoApp.CausaliRiavvio != null)
                                    idCausaliRiavvio = fermoApp.CausaliRiavvio.Id;

                                if (fermoApp.Inizio < request.FiltroDal)
                                    fermoApp.Inizio = request.FiltroDal;

                                if (fermoApp.Fine > request.FiltroAl)
                                    fermoApp.Fine = request.FiltroAl;

                                var appFermo = listFermi.FirstOrDefault(x => x.MacchinaId == idMacchina && x.CausaleId == idCausaliRiavvio);
                                var fermoInizio = Convert.ToDateTime(fermoApp.Inizio);
                                var tempoFermo = (Convert.ToDateTime(fermoApp.Fine != null ? fermoApp.Fine : request.FiltroAl) - fermoInizio).TotalSeconds;
                                if (appFermo == null)
                                {
                                    var dictApp = new DashboardOeeFermiItem();
                                    dictApp.Materiale = btc.Materiale?.Nome ?? string.Empty;
                                    dictApp.MaterialeDescrizione = btc.Materiale?.Descrizione ?? string.Empty;
                                    dictApp.MacchinaId = idMacchina;
                                    var appMacchina = listMacchine.FirstOrDefault(m => m.Id == idMacchina);
                                    if (appMacchina != null)
                                        dictApp.Macchina = appMacchina.Codice;
                                    dictApp.NumeroFermi = 1;
                                    dictApp.TempoFermo = tempoFermo;
                                    var causale = listCausaliRiavvio.FirstOrDefault(x => x.Id == idCausaliRiavvio);
                                    var nomeCausale = "";
                                    var codiceCausale = "";
                                    if (causale != null)
                                    {
                                        nomeCausale = causale.Causale;
                                        codiceCausale = causale.Codice;
                                    }
                                    else
                                        nomeCausale = "Da Giustificare";

                                    if (codiceCausale == "AreaMes_Default")
                                        codiceCausale = "";

                                    dictApp.CodiceCausale = codiceCausale;
                                    dictApp.Causale = nomeCausale;
                                    dictApp.CausaleId = idCausaliRiavvio;
                                    dictApp.Turno = fermo.Turno?.Nome ?? "Fuori turno";
                                    dictApp.Odp = btcNew.OdP;
                                    listFermi.Add(dictApp);
                                }
                                else
                                {
                                    appFermo.NumeroFermi++;
                                    appFermo.TempoFermo += tempoFermo;
                                }




                                //TODO aggiungero tempo fermo al dizionario percentuale fasi
                            }
                        }

                        var scarti = btcNew.Eventi.Where(x => x.TipoEvento == TipoEvento.qta_pzsca && (request.ListIdMacchine.Count == 0 || request.ListIdMacchine.Contains(x.Macchina.Id))
                                                                             && (request.ListIdTurni.Count == 0 || request.ListIdTurni.Contains(x.Turno.Id))
                                                                             && (request.ListIdMateriali.Count == 0 || request.ListIdMateriali.Contains(btcNew.Materiale.Id)) && int.TryParse(x.Valore, out int n)).ToList();
                        foreach (var scarto in scarti)
                        {
                            var macchinaScarto = listMacchine.FirstOrDefault(x => x.Id == scarto.Macchina.Id);
                            var scartoValore = Convert.ToInt32(scarto.Valore);
                            if (macchinaScarto != null && macchinaScarto.Codice != "ATTREZZAGGIO" && macchinaScarto.Nome != "ATTREZZAGGIO" && Convert.ToInt32(scarto.Valore) > 0 &&
                                (request.ListIdMacchine.Count == 0 || request.ListIdMacchine.Contains(macchinaScarto.Id)))
                            {
                                var idCausaliRiavvio = "";
                                if (scarto.TipoValore != null)
                                    idCausaliRiavvio = scarto.TipoValore.Id;

                                var appScarti = listScarti.FirstOrDefault(x => x.CausaleId == idCausaliRiavvio);
                                if (appScarti == null)
                                {
                                    var itemAdd = new DashboardOeeScartiItem();
                                    itemAdd.Macchina = macchinaScarto.Codice;
                                    itemAdd.Materiale = btc.Materiale?.Nome;
                                    itemAdd.MaterialeDescrizione = btc.Materiale?.Descrizione;
                                    itemAdd.NumeroScarti = Convert.ToInt32(scarto.Valore);
                                    var causale = listCausaliScarto.FirstOrDefault(x => x.Id == idCausaliRiavvio);
                                    var nomeCausale = "";
                                    var codiceCausale = "";
                                    if (causale != null)
                                    {
                                        nomeCausale = causale.Descrizione;
                                        codiceCausale = causale.Codice;
                                    }
                                    if (codiceCausale == "AreaMes_Default")
                                        codiceCausale = "";
                                    itemAdd.CodiceCausale = codiceCausale;
                                    itemAdd.Causale = nomeCausale;
                                    itemAdd.CausaleId = idCausaliRiavvio;
                                    listScarti.Add(itemAdd);
                                }
                                else
                                {
                                    int n;
                                    if (int.TryParse(scarto.Valore, out n))
                                    {
                                        appScarti.NumeroScarti += Convert.ToInt32(scarto.Valore);
                                    }
                                }
                            }
                        }

                        var listNumFasi = new List<string>();
                        var numeriFase = btcNew.DistintaBase.Fasi.Where(f => f.Nome != "ATTREZZAGGIO").Select(f => f.NumeroFase);
                        var avanzamtiLav = btcNew.BatchAvanzamenti.Where(ava => numeriFase.Contains(ava.NumeroFase)
                                                                   && (request.ListIdMacchine.Count == 0 || request.ListIdMacchine.Contains(ava.BatchFaseMacchina.Macchina.Id))
                                                                   && (request.ListIdTurni.Count == 0 || request.ListIdTurni.Contains(ava.Turno.Id))
                                                                   && (request.ListIdMateriali.Count == 0 || request.ListIdMateriali.Contains(btcNew.Materiale.Id))
                                                                   && (request.MostraFuoriTurno == true || (request.MostraFuoriTurno == false && ava.Turno.Id != null && ava.Turno.Id != string.Empty))
                                                                   && (ava.Stato == StatoFasi.InLavorazione || ava.Stato == StatoFasi.InLavorazioneInPausa)).ToList();

                        var tempoLavoro = avanzamtiLav.Sum(ava => (request.ListIdMacchine.Count == 0 || request.ListIdMacchine.Contains(ava.BatchFaseMacchina.Macchina.Id))
                                                                   && (request.ListIdTurni.Count == 0 || request.ListIdTurni.Contains(ava.Turno.Id))
                                                                   && ava.Stato == StatoFasi.InLavorazione
                                                                   && (request.MostraFuoriTurno == true || (request.MostraFuoriTurno == false && ava.Turno.Id != null && ava.Turno.Id != string.Empty)) ?
                                                                       (Convert.ToDateTime(ava.Fine != null ? ava.Fine : request.FiltroAl) - (Convert.ToDateTime(ava.Inizio) > request.FiltroDal ? Convert.ToDateTime(ava.Inizio) : request.FiltroDal)).TotalSeconds : 0);

                        var tempoPause = avanzamtiLav.Sum(ava => numeriFase.Contains(ava.NumeroFase)
                                                                   && (request.ListIdMacchine.Count == 0 || request.ListIdMacchine.Contains(ava.BatchFaseMacchina.Macchina.Id))
                                                                   && (request.ListIdTurni.Count == 0 || request.ListIdTurni.Contains(ava.Turno.Id))
                                                                   && ava.Stato == StatoFasi.InLavorazioneInPausa
                                                                   && (request.MostraFuoriTurno == true || (request.MostraFuoriTurno == false && ava.Turno.Id != null && ava.Turno.Id != string.Empty))
                                                                   && ava.Stato == StatoFasi.InLavorazioneInPausa ? (Convert.ToDateTime(ava.Fine != null ? ava.Fine : request.FiltroAl) - (Convert.ToDateTime(ava.Inizio) > request.FiltroDal ? Convert.ToDateTime(ava.Inizio) : request.FiltroDal)).TotalSeconds : 0);

                        if (btcNew.Stato == StatoBatch.Terminato)
                        {
                            var itemEff = new DashboardOeeEfficenza();
                            itemEff.Articolo = btcNew.Materiale.Codice + " - " + btcNew.Materiale.Nome;
                            itemEff.Commessa = btcNew.OdP;
                            itemEff.Grafico = new DashboardOeeEfficenzaGrafico();
                            var lastFase = btcNew.Fasi.LastOrDefault();
                            var tempo = btcNew.BatchAvanzamenti.LastOrDefault();
                            if (lastFase.PezziProdotti > 0 && tempoLavoro > 0)
                                itemEff.Grafico.PezziOraReale = itemEff.CicloReale = Convert.ToInt32(Math.Truncate(tempoLavoro) / lastFase.PezziProdotti);
                            else
                                itemEff.CicloReale = 0;

                            foreach (var faseApp in btcNew.DistintaBase.Fasi.Where(x => x.Macchine.Count > 0 && x.IsCiclica))
                            {
                                if (faseApp.IsCiclica)
                                    itemEff.Grafico.PezziOraTeorico = itemEff.CicloTeorico += Convert.ToInt32((faseApp.Macchine.Count >= 1 ? faseApp.Macchine.FirstOrDefault().Tempo : 0)
                                                                          + (faseApp.Manodopera.Count >= 1 ? faseApp.Manodopera.FirstOrDefault().Tempo : 0));
                            }
                            itemRet.ListEfficienza.Add(itemEff);
                        }

                        Logger.DashboardOee.Info($"Iterate btcNewAvanzamenti... complete");

                        AddOrUpdateSummaryTimeDict2(groupedTime, nameof(Batch), StatoFasi.InLavorazione, tempoLavoro, btcNew.OdP);
                        AddOrUpdateSummaryTimeDict2(groupedTime, nameof(Batch), StatoFasi.InLavorazioneInPausa, tempoPause, btcNew.OdP);

                        btcNew.PezziScartati = totPezziScarto;
                        stabilimentoPezziScarto += totPezziScarto;

                        totPezziBuoni = btcNew.Fasi.LastOrDefault()?.DistintaBaseFase.PezziProdotti ?? 0;
                        stabilimentoPezziBuoni += totPezziBuoni;
                        btcNew.PezziProdotti = totPezziBuoni;
                    }
                    listOeeField.Add("OEE");
                    listOeeGrafici = listOeeGrafici.OrderBy(x => x["x"]).ToList();
                    listOeeField = listOeeField.Distinct().ToList();
                    listOeeTrendGrafici = listOeeTrendGrafici.Concat(elaborateTrend(btcNew.Trend_Oee, "", "valore", lastTrendOee, Convert.ToDateTime(btcNew.Fine != null ? btcNew.Fine : request.FiltroAl))).ToList();
                    batchListPiana.Add(btcNew);
                }

                listOeeGrafici = calculateAvgTrend(listOeeTrendGrafici, "OEE").OrderBy(x => x["x"]).ToList();


                // calcolo fermi
                var tempoFermiAllBatch = listFermi.Sum(x => Convert.ToDouble(x.TempoFermo));
                var tempoFermiPiePareto = new List<DashboardOeeFermiGrafici>();
                foreach (var fermo in listFermi)
                {
                    fermo.TempoFermoInOre = GetHourTimeSpanStringFromSeconds(fermo.TempoFermo);
                    fermo.TempoFermoText = GetTimeSpanStringFromSeconds(fermo.TempoFermo);
                    var itemGrafici = tempoFermiPiePareto.FirstOrDefault(x => x.CausaleId == fermo.CausaleId);
                    if (itemGrafici == null)
                    {
                        itemGrafici = new DashboardOeeFermiGrafici
                        {
                            Causale = fermo.Causale,
                            CausaleId = fermo.CausaleId,
                            TempoFermo = fermo.TempoFermo,
                            NumeroFermi = 1,
                            PercentualeCumulativa = 0
                        };
                        tempoFermiPiePareto.Add(itemGrafici);
                    }
                    else
                    {
                        itemGrafici.TempoFermo += fermo.TempoFermo;
                        itemGrafici.NumeroFermi++;
                    }

                    AddOrUpdateSummaryTimeDict2(groupedTime, nameof(Macchina), StatoFasi.InLavorazioneInEmergenza, fermo.TempoFermo, fermo.Macchina);
                    AddOrUpdateSummaryTimeDict2(groupedTime, nameof(TurnoDiLavoro), StatoFasi.InLavorazioneInEmergenza, fermo.TempoFermo, fermo.Turno);
                    AddOrUpdateSummaryTimeDict2(groupedTime, nameof(Batch), StatoFasi.InLavorazioneInEmergenza, fermo.TempoFermo, fermo.Odp);
                }

                var totaleTempo = tempoFermiPiePareto.Sum(x => x.TempoFermo);
                var totaleNumeroFermi = tempoFermiPiePareto.Sum(x => x.NumeroFermi);
                tempoFermiPiePareto = tempoFermiPiePareto.OrderByDescending(x => x.TempoFermo).ToList();
                var lastCumulativePercentage = 0;
                foreach (var itemGrafici in tempoFermiPiePareto)
                {
                    var index = tempoFermiPiePareto.IndexOf(itemGrafici);

                    lastCumulativePercentage += Convert.ToInt32((itemGrafici.TempoFermo * 100) / tempoFermiAllBatch);
                    if (lastCumulativePercentage > 100)
                        lastCumulativePercentage = 100;
                    tempoFermiPiePareto[index].PercentualeCumulativa = lastCumulativePercentage;
                    tempoFermiPiePareto[index].PercentualeTempoDouble = Math.Round(tempoFermiPiePareto[index].TempoFermo * 100 / totaleTempo, 2);
                    tempoFermiPiePareto[index].PercentualeTempo = Convert.ToInt32(tempoFermiPiePareto[index].PercentualeTempoDouble);
                    tempoFermiPiePareto[index].PercentualeNumero = Convert.ToInt32(tempoFermiPiePareto[index].NumeroFermi * 100 / totaleNumeroFermi);
                }
                itemRet.ListFermi = listFermi;
                itemRet.ListFermiGrafici = tempoFermiPiePareto;


                //---------------------------------------------------------------------------------------------------------------------------------------
                // calcolo scarti
                var numeroScartiAllBatch = listScarti.Sum(x => x.NumeroScarti);
                var scartiPiePareto = new List<DashboardOeeScartiGrafici>();
                foreach (var scarto in listScarti)
                {
                    var itemGrafici = scartiPiePareto.FirstOrDefault(x => x.CausaleId == scarto.CausaleId);
                    if (itemGrafici == null)
                    {
                        itemGrafici = new DashboardOeeScartiGrafici();
                        itemGrafici.Causale = scarto.Causale;
                        itemGrafici.CausaleId = scarto.CausaleId;
                        itemGrafici.NumeroScarti = scarto.NumeroScarti;
                        itemGrafici.PercentualeCumulativa = 0;
                        scartiPiePareto.Add(itemGrafici);
                    }
                    else
                        itemGrafici.NumeroScarti += Convert.ToInt32(scarto.NumeroScarti);
                }

                scartiPiePareto = scartiPiePareto.OrderByDescending(x => x.NumeroScarti).ToList();
                var totaleScarti = scartiPiePareto.Sum(x => x.NumeroScarti);
                var lastCumulativePercentageScarti = 0;
                foreach (var itemGrafici in scartiPiePareto)
                {
                    var index = scartiPiePareto.IndexOf(itemGrafici);
                    scartiPiePareto[index].PercentualeDouble = itemGrafici.NumeroScarti * 100 / totaleScarti;
                    scartiPiePareto[index].Percentuale = Convert.ToInt32(scartiPiePareto[index].PercentualeDouble);
                    lastCumulativePercentageScarti += scartiPiePareto[index].Percentuale;
                    if (lastCumulativePercentageScarti > 100)
                        lastCumulativePercentageScarti = 100;
                    scartiPiePareto[index].PercentualeCumulativa = lastCumulativePercentageScarti;
                }
                itemRet.ListScarti = listScarti;
                itemRet.ListScartiGrafici = scartiPiePareto;
                //---------------------------------------------------------------------------------------------------------------------------------------
                // calcolo tempi
                Dictionary<string, Dictionary<string, DevicePercItem>> deviceFasePercItems = new Dictionary<string, Dictionary<string, DevicePercItem>>();
                var dictTempi = new Dictionary<string, Dictionary<string, double>>();
                double totTempi = 0;
                var listAvanzamenti = new Dictionary<string, List<DashboardOeeAvanzamenti>>();
                var listTempi = new List<Dictionary<string, string>>();

                foreach (var batchApp in batchListPiana)
                {
                    //batchApp.BatchAvanzamenti = batchApp.BatchAvanzamenti.Concat(btc.BatchAvanzamenti).ToList();
                    foreach (var ava in batchApp.BatchAvanzamenti.Where(ba => ba.Inizio > request.FiltroDal && ba.Fine < request.FiltroAl))
                    {
                        //grafici avanzamenti
                        if (ava.BatchFaseMacchina.Macchina != null && ava.BatchFaseMacchina.Macchina.Codice != null && !listAvanzamenti.ContainsKey(ava.BatchFaseMacchina.Macchina.Codice))
                        {
                            listAccordion.Add(ava.BatchFaseMacchina.Macchina.Codice);
                            listAvanzamenti.Add(ava.BatchFaseMacchina.Macchina.Codice, new List<DashboardOeeAvanzamenti>());
                        }
                        var avaItem = new DashboardOeeAvanzamenti();
                        var inizio = Convert.ToDateTime(ava.Inizio);
                        var dal = inizio > request.FiltroDal ? inizio : request.FiltroDal;
                        avaItem.Start = dal;
                        avaItem.End = ava.Fine != null ? Convert.ToDateTime(ava.Fine) : request.FiltroAl;
                        var fase = batchApp.DistintaBase.Fasi.FirstOrDefault(f => f.Codice == ava.CodiceFase);
                        if (fase != null)
                        {
                            if (fase.Nome.Contains("ATTREZZAGGIO"))
                            {
                                if (ava.BatchFaseMacchina.Macchina?.Codice != "ATTREZZAGGIO")
                                    continue;
                            }

                            avaItem.Fase = fase.Nome.ToUpper();
                            avaItem.StatoLavorazione = Convert.ToString(ava.Stato);
                            var colore = "";
                            if (ava.Stato == StatoFasi.InLavorazione)
                                colore = "#30C93D";
                            else if (ava.Stato == StatoFasi.InLavorazioneInPausa)
                                colore = "#E8C70E";
                            else if (ava.Stato == StatoFasi.InLavorazioneInEmergenza)
                                colore = "#DB1A1A";
                            avaItem.Colore = colore;
                        }

                        if (ava.BatchFaseMacchina.Macchina != null && ava.BatchFaseMacchina.Macchina.Codice != null)
                            listAvanzamenti[ava.BatchFaseMacchina.Macchina.Codice].Add(avaItem);
                        // tempi
                        if (ava.CodiceFase != null && (ava.Turno.Codice != null || request.MostraFuoriTurno))
                        {
                            string keyDict = string.Empty;

                            switch (ava.Stato)
                            {
                                case StatoFasi.InLavorazione:
                                    {
                                        keyDict = StatoFasi.InLavorazione.ToString();
                                        break;
                                    }
                                case StatoFasi.InLavorazioneInPausa:
                                    {
                                        keyDict = StatoFasi.InLavorazioneInPausa.ToString();
                                        break;
                                    }
                                case StatoFasi.InLavorazioneInEmergenza:
                                    {
                                        keyDict = StatoFasi.InLavorazioneInEmergenza.ToString();
                                        break;
                                    }
                                default:
                                    break;
                            }

                            var codiceFase = fase.Codice;
                            var codiceTurno = ava.Turno.Nome ?? "Fuori turno";
                            if (dictTempi.ContainsKey(codiceFase))
                            {
                                if (!dictTempi[codiceFase].ContainsKey(codiceTurno))
                                {
                                    dictTempi[codiceFase].Add(codiceTurno, 0);
                                }
                            }
                            else
                            {
                                dictTempi.Add(codiceFase, new Dictionary<string, double>());
                                dictTempi[codiceFase].Add(codiceTurno, 0);
                                dictTempi[codiceFase].Add("TOT", 0);
                            }
                            var appTempo = (Convert.ToDateTime(ava.Fine != null ? ava.Fine : request.FiltroAl) - dal).TotalSeconds;
                            var codiceMacchina = ava.BatchFaseMacchina.Macchina?.Codice ?? "";
                            string keyDictFasiPerc = fase.Nome.ToUpper();
                            if (deviceFasePercItems.TryGetValue(codiceMacchina, out Dictionary<string, DevicePercItem> fasePercentItems))
                            {
                                if (fasePercentItems.TryGetValue(keyDictFasiPerc, out DevicePercItem devicePercItem))
                                {
                                    AddOrUpdateSummaryTimeDict2(groupedTime, nameof(Macchina), ava.Stato, appTempo, codiceMacchina);
                                    AddOrUpdateSummaryTimeDict2(groupedTime, nameof(TurnoDiLavoro), ava.Stato, appTempo, codiceTurno);
                                    switch (ava.Stato)
                                    {
                                        case StatoFasi.InLavorazione:
                                            devicePercItem.TempLav += appTempo;
                                            break;
                                        case StatoFasi.InLavorazioneInPausa:
                                            devicePercItem.TempPausa += appTempo;
                                            break;
                                        case StatoFasi.InLavorazioneInEmergenza:
                                            if (addFermiFromAva)
                                            {
                                                AddFermoFromAva(appTempo, request, itemRet, groupedTime, ava, codiceTurno, codiceMacchina, batchApp.OdP);
                                            }

                                            devicePercItem.TempEmer += appTempo;
                                            break;
                                        default:
                                            break;
                                    }
                                    devicePercItem.TempTot += appTempo;
                                }
                                else
                                {
                                    CreateDevicePercItem(ava.Stato, keyDictFasiPerc, fase, appTempo, ref fasePercentItems);
                                    AddOrUpdateSummaryTimeDict2(groupedTime, nameof(Macchina), ava.Stato, appTempo, codiceMacchina);
                                    AddOrUpdateSummaryTimeDict2(groupedTime, nameof(TurnoDiLavoro), ava.Stato, appTempo, codiceTurno);
                                    if (ava.Stato == StatoFasi.InLavorazioneInEmergenza && addFermiFromAva)
                                    {
                                        AddFermoFromAva(appTempo, request, itemRet, groupedTime, ava, codiceTurno, codiceMacchina, batchApp.OdP);
                                    }
                                }
                            }
                            else
                            {
                                CreateDevicePercItem(ava.Stato, keyDictFasiPerc, fase, appTempo, ref fasePercentItems);
                                AddOrUpdateSummaryTimeDict2(groupedTime, nameof(Macchina), ava.Stato, appTempo, codiceMacchina);
                                AddOrUpdateSummaryTimeDict2(groupedTime, nameof(TurnoDiLavoro), ava.Stato, appTempo, codiceTurno);
                                deviceFasePercItems.Add(codiceMacchina, fasePercentItems);
                                if (ava.Stato == StatoFasi.InLavorazioneInEmergenza && addFermiFromAva)
                                {
                                    AddFermoFromAva(appTempo, request, itemRet, groupedTime, ava, codiceTurno, codiceMacchina, batchApp.OdP);
                                }
                            }

                            totTempi += appTempo;
                            dictTempi[codiceFase][codiceTurno] += appTempo;
                            dictTempi[codiceFase]["TOT"] += appTempo;
                        }
                    }


                    var totaleTempo2 = dictTempi.Sum(x => x.Value["TOT"]);
                    foreach (var tempo in dictTempi)
                    {
                        var dict = new Dictionary<string, string>();
                        var perc = tempo.Value["TOT"] * 100 / totaleTempo2;
                        foreach (var t in tempo.Value)
                            dict.Add(t.Key, SecondToString(t.Value));

                        dict.Add("campo", tempo.Key);
                        dict.Add("perc", Convert.ToString(Convert.ToInt32(perc)));
                        listTempi.Add(dict);
                    }
                }

                foreach (var deviceFasePercItem in deviceFasePercItems)
                {
                    foreach (var fasePercItem in deviceFasePercItem.Value)
                    {
                        var devicePercItem = fasePercItem.Value;

                        devicePercItem.PercLav = devicePercItem.TempLav * 100 / devicePercItem.TempTot;
                        devicePercItem.PercEmer = devicePercItem.TempEmer * 100 / devicePercItem.TempTot;
                        devicePercItem.PercPausa = devicePercItem.TempPausa * 100 / devicePercItem.TempTot;
                    }
                }

                //---------------------------------------------------------------------------------------------------------------------------------------
                itemRet.PezziElaborati = batchListPiana.Sum(x => x.PezziProdotti);
                itemRet.PezziTeorici = batchListPiana.Sum(x => x.TempoTeorico);
                itemRet.PezziTarget = batchListPiana.Sum(x => x.PezziDaProdurre);
                itemRet.Tempi = listTempi;
                itemRet.ListEfficienza = itemRet.ListEfficienza.OrderBy(x => x.Macchina).ToList();
                listAvanzamenti = listAvanzamenti.Select(x => new { Key = x.Key, Value = x.Value.OrderByDescending(y => y.Fase) }).ToDictionary(x => x.Key, x => x.Value.ToList());
                itemRet.ListAvanzamenti = listAvanzamenti;
                itemRet.ListAccordion = listAccordion.OrderBy(x => x).ToList();
                itemRet.DeviceFasePercItems = deviceFasePercItems;
                itemRet.ListBatch = itemRet.ListBatch.OrderBy(b => b.OdP).ToList();
                //---------------------------------------------------------------------------------------------------------------------------------------

                ///////////////////////////////////////////////////////////
                /////////////////////    SUMMARY    ///////////////////////
                ///////////////////////////////////////////////////////////

                //CalculateSummaryGridPercantage(itemRet.GroupedLavTime, itemRet.GroupedFermiTime, itemRet.GroupedPauseTime);
                CalculateSummaryGridPercantage2(groupedTime);

                itemRet.GroupedTime = groupedTime;

                CalculateScartiInfoSummary(ref itemRet, listScarti);

                //Calcolo percentuali e numeri fermi totali, per macchina e causale.
                CalculateFermiInfo(ref itemRet);

                //Calcolo percentuali e numeri di scarti totali per macchina e causale
                CalculateScartiInfo(ref itemRet);
            }
            catch (Exception exe)
            {
                Logger.DashboardOee.Error("Error ", exe);
            }

            Logger.DashboardOee.InfoFormat("Elaboration dashboard OEE... completed");
            return itemRet;
        }



        private static void AddFermoFromAva(double appTempo, RequestDataDashboardOee request, ResponseDataDashboardOee itemRet, Dictionary<string, List<TimeDictItem2>> groupedTime, BatchAvanzamento ava, string codiceTurno, string codiceMacchina, string odp)
        {
            var macchinaFermo = ava.BatchFaseMacchina.Macchina;
            var fermoApp = new BatchFermi();
            fermoApp.Inizio = ava.Inizio;
            fermoApp.Fine = ava.Fine;

            fermoApp.MacchinaId = macchinaFermo.Id;
            var idMacchina = fermoApp.MacchinaId;

            if (fermoApp.Inizio < request.FiltroDal)
                fermoApp.Inizio = request.FiltroDal;

            if (fermoApp.Fine > request.FiltroAl)
                fermoApp.Fine = request.FiltroAl;

            var appFermo = itemRet.ListFermi.FirstOrDefault(x => x.MacchinaId == idMacchina);
            if (appFermo == null)
            {
                var dictApp = new DashboardOeeFermiItem();
                dictApp.MacchinaId = idMacchina;
                if (macchinaFermo != null)
                    dictApp.Macchina = macchinaFermo.Codice;
                dictApp.NumeroFermi = 1;
                dictApp.TempoFermo = appTempo;
                dictApp.CodiceCausale = "";
                dictApp.Causale = "";
                dictApp.Turno = ava.Turno?.Nome ?? "Fuori turno";
                dictApp.Odp = odp;
                itemRet.ListFermi.Add(dictApp);
            }
            else
            {
                appFermo.NumeroFermi++;
                appFermo.TempoFermo += appTempo;
            }

            AddOrUpdateSummaryTimeDict2(groupedTime, nameof(Macchina), StatoFasi.InLavorazioneInEmergenza, appTempo, codiceMacchina);
            AddOrUpdateSummaryTimeDict2(groupedTime, nameof(TurnoDiLavoro), StatoFasi.InLavorazioneInEmergenza, appTempo, codiceTurno);
            AddOrUpdateSummaryTimeDict2(groupedTime, nameof(Batch), StatoFasi.InLavorazioneInEmergenza, appTempo, odp);
        }

        private static void CalculateSummaryGridPercantage(Dictionary<string, List<TimeDictItem>> groupedTimeList)
        {
            foreach (var dictItem in groupedTimeList.Values)
            {
                double sum = dictItem.Sum(i => i.TimeInSecond);
                dictItem.ForEach(i => i.Percentage = i.TimeInSecond * 100 / sum);
            }
        }

        private static void CalculateSummaryGridPercantage(
            Dictionary<string, List<TimeDictItem>> LavGroupedTimeList,
            Dictionary<string, List<TimeDictItem>> FermoGroupedTimeList,
            Dictionary<string, List<TimeDictItem>> PausaGroupedTimeList)
        {

            var unionDict = new Dictionary<string, List<TimeDictItem>>()
            {
                { nameof(Macchina), new List<TimeDictItem>() },
                { nameof(TurnoDiLavoro), new List<TimeDictItem>() },
                { nameof(Batch), new List<TimeDictItem>() }
            };

            foreach (var item in LavGroupedTimeList)
            {
                item.Value.ForEach(timeDictItem => AddOrUpdateSummaryTimeDict(unionDict, item.Key, timeDictItem));
            }

            foreach (var item in PausaGroupedTimeList)
            {
                item.Value.ForEach(timeDictItem => AddOrUpdateSummaryTimeDict(unionDict, item.Key, timeDictItem));
            }

            foreach (var item in FermoGroupedTimeList)
            {
                item.Value.ForEach(timeDictItem => AddOrUpdateSummaryTimeDict(unionDict, item.Key, timeDictItem));
            }

            foreach (var group in LavGroupedTimeList)
            {
                foreach (var item in group.Value)
                {
                    double? sum = unionDict[group.Key].FirstOrDefault(tdi => tdi.Key == item.Key)?.TimeInSecond;
                    if (sum.HasValue && sum != 0)
                    {
                        item.Percentage = item.TimeInSecond * 100 / sum.Value;
                    }
                }
            }

            foreach (var group in FermoGroupedTimeList)
            {
                foreach (var item in group.Value)
                {
                    double? sum = unionDict[group.Key].FirstOrDefault(tdi => tdi.Key == item.Key)?.TimeInSecond;
                    if (sum.HasValue && sum != 0)
                    {
                        item.Percentage = item.TimeInSecond * 100 / sum.Value;
                    }
                }
            }

            foreach (var group in PausaGroupedTimeList)
            {
                foreach (var item in group.Value)
                {
                    double? sum = unionDict[group.Key].FirstOrDefault(tdi => tdi.Key == item.Key)?.TimeInSecond;
                    if (sum.HasValue && sum != 0)
                    {
                        item.Percentage = item.TimeInSecond * 100 / sum.Value;
                    }
                }
            }
        }


        private static void CalculateSummaryGridPercantage2(Dictionary<string, List<TimeDictItem2>> dict)
        {
            foreach (var group in dict)
            {
                foreach (var item in group.Value)
                {
                    if (item.Time != 0)
                    {
                        item.LavPercentage = item.LavTimeInSecond * 100 / item.Time;
                        item.PauPercentage = item.PauTimeInSecond * 100 / item.Time;
                        item.EmerPercentage = item.EmerTimeInSecond * 100 / item.Time;
                    }
                }
            }
        }

        public async Task<List<ScartiDatiPiani>> GetScartiDatiPiani(RequestDataDashboardOee request)
        {
            List<Batch> batchList = await GetFilteredBatchList(request);
            var listCausaliScarto = await MongoRepository.Instance.All<TipoValori>();
            var listMacchine = await MongoRepository.Instance.All<Macchina>();
            var listTurni = await MongoRepository.Instance.All<TurnoDiLavoro>();
            var listMateriale = await MongoRepository.Instance.All<Materiale>();
            var listOperatori = await MongoRepository.Instance.All<Operatore>();
            List<ScartiDatiPiani> scartiDatiPiani = new List<ScartiDatiPiani>();
            foreach (var btc in batchList)
            {
                var listPezziScarto = btc.Eventi.Where(e => e.TipoEvento == TipoEvento.qta_pzsca).ToList();
                foreach (var scarto in listPezziScarto)
                {
                    DateTime dateTimeScarto = scarto.OraEvento ?? DateTime.MinValue;
                    if (dateTimeScarto < request.FiltroDal || dateTimeScarto > request.FiltroAl)
                    {
                        continue;
                    }
                    string macchinaCodice = string.Empty;
                    string materialeNome = string.Empty;
                    string materialeDescrizione = string.Empty;
                    string turnoNome = string.Empty;
                    string operatore = string.Empty;
                    string macchinaId = scarto.Macchina?.Id ?? string.Empty;
                    string materialeId = batchList.FirstOrDefault(b => b.Id == scarto.BatchId)?.Materiale?.Id ?? string.Empty;
                    string turnoId = scarto.Turno.Id;
                    if (macchinaId is object)
                    {
                        if (request.ListIdMacchine.Count > 0 && !request.ListIdMacchine.Contains(macchinaId))
                        { continue; }
                        macchinaCodice = listMacchine.FirstOrDefault(m => m.Id == macchinaId)?.Codice ?? string.Empty;
                    }
                    if (materialeId is object)
                    {
                        if (request.ListIdMateriali.Count > 0 && !request.ListIdMateriali.Contains(materialeId))
                        { continue; }
                        var materiale = listMateriale.FirstOrDefault(m => m.Id == materialeId);
                        materialeNome = materiale?.Nome ?? string.Empty;
                        materialeDescrizione = materiale?.Descrizione ?? string.Empty;
                    }
                    if (turnoId is object)
                    {
                        if (request.ListIdTurni.Count > 0 && !request.ListIdTurni.Contains(turnoId))
                        { continue; }
                        turnoNome = listTurni.FirstOrDefault(t => t.Id == turnoId).Nome ?? string.Empty;
                    }

                    if (scarto.Operatore?.Id is object)
                    {
                        operatore = listOperatori.FirstOrDefault(o => o.Id == scarto.Operatore.Id).Username;
                    }
                    string fase = btc.DistintaBase.Fasi.FirstOrDefault(f => f.Id == scarto.FaseId)?.Codice;
                    string causale = listCausaliScarto.FirstOrDefault(c => c.Id == scarto.TipoValore.Id)?.Descrizione ?? "";
                    var itemScarto = new ScartiDatiPiani()
                    {
                        Odp = btc.OdP,
                        Macchina = macchinaCodice,
                        Fase = fase,
                        Causale = causale,
                        CausaleCodice = scarto.TipoValore.Item?.Codice ?? "",
                        DataEvento = dateTimeScarto,
                        Operatore = operatore,
                        Quantità = Convert.ToInt32(scarto.Valore),
                        Turno = turnoNome,
                        Materiale = materialeNome,
                        MaterialeDescrizione = materialeDescrizione
                    };
                    scartiDatiPiani.Add(itemScarto);
                }
            }
            return scartiDatiPiani;
        }

        public async Task<List<FermiDatiPiani>> GetFermiDatiPiani(RequestDataDashboardOee request)
        {
            List<Batch> batchList = await GetFilteredBatchList(request);
            List<Macchina> listMacchine = await MongoRepository.Instance.All<Macchina>();
            List<CausaliRiavvio> listCausali = await MongoRepository.Instance.All<CausaliRiavvio>();
            List<FermiDatiPiani> listFermiDatiPiani = new List<FermiDatiPiani>();
            List<Materiale> listMateriali = await MongoRepository.Instance.All<Materiale>();
            foreach (var batch in batchList)
            {
                foreach (var fermo in batch.Fermi)
                {
                    if (fermo.Inizio > request.FiltroAl ||
                        (fermo.Inizio < request.FiltroDal && (fermo.Fine < request.FiltroDal)) ||
                        (request.ListIdMacchine.Count != 0 && !request.ListIdMacchine.Contains(fermo.MacchinaId)) ||
                        (request.ListIdTurni.Count != 0 && !request.ListIdTurni.Contains(fermo.Turno.Id)) ||
                        (request.ListIdMateriali.Count != 0 && !request.ListIdMateriali.Contains(batch.Materiale.Id)))
                    {
                        continue;
                    }
                    var fase = batch.Fasi.FirstOrDefault(fa => fa.NumeroFase == fermo.NumeroFase.ToString());
                    var materiale = batch.Materiale;
                    string codiceMacchina = string.Empty;
                    if (fase is object)
                    {
                        codiceMacchina = fase.Macchine.FirstOrDefault()?.Macchina?.Codice;
                    }
                    string causale = listCausali.FirstOrDefault(c => c.Id == fermo.CausaliRiavvio?.Id)?.Causale ?? "";
                    listFermiDatiPiani.Add(new FermiDatiPiani()
                    {
                        Inizio = fermo.Inizio,
                        Fine = fermo.Fine,
                        Causale = causale,
                        Odp = batch.OdP,
                        Fase = batch.Fasi.FirstOrDefault(f => f.NumeroFase == fermo.NumeroFase.ToString())?.DistintaBaseFase?.Codice ?? "",
                        Macchina = codiceMacchina,
                        Turno = fermo.Turno?.Nome ?? string.Empty,
                        OpBlocco = fermo.UserLocked,
                        OpSblocco = fermo.UserUnLocked,
                        Materiale = materiale?.Nome ?? string.Empty,
                        DescrizioneMateriale = materiale?.Descrizione ?? string.Empty
                    });
                }
            }
            return listFermiDatiPiani;
        }

        private async Task<List<Batch>> GetFilteredBatchList(RequestDataDashboardOee request)
        {
            Logger.DashboardOee.Info("GetFilteredBatchList...");
            // Ottiene la data e l'ora correnti come valori di default per i filtri di data
            var filtroDalEventi = DateTime.Now;
            var filtroAlEventi = DateTime.Now;

            // Verifica se è stato specificato il filtro di data di inizio (FiltroDal)
            if (request.FiltroDal != null)
            {
                // Imposta la data di inizio del filtro con l'ora 00:00:00 AM
                request.FiltroDal = new DateTime(request.FiltroDal.Year, request.FiltroDal.Month, request.FiltroDal.Day, 0, 0, 0, 0);
                filtroDalEventi = request.FiltroDal;
            }

            // Verifica se è stato specificato il filtro di data di fine (FiltroAl)
            if (request.FiltroAl != null)
            {
                // Se non è stato impostata la data di fine del filtro viene impostata l'ora 00:00:00 AM del giorno successivo
                request.FiltroAl = new DateTime(request.FiltroAl.Year, request.FiltroAl.Month, request.FiltroAl.Day, 0, 0, 0, 0);
                request.FiltroAl = request.FiltroAl.AddDays(1);
                filtroAlEventi = request.FiltroAl;
            }
            Logger.DashboardOee.Info($"filtroDal = '{filtroDalEventi}'; filtroAl = '{filtroAlEventi}'");

            Logger.DashboardOee.Info($"Getting batch (not in state InAttesa) in memory...");
            List<Batch> batchList = new List<Batch>(BatchManager.Instance.BatchInMemory().Where(x => x.Stato != StatoBatch.InAttesa));
            Logger.DashboardOee.Info($"Getting batch (not in state InAttesa) in memory... completed batchList.Count = {batchList.Count}");

            Logger.DashboardOee.Info($"Getting terminated batch in mongoDb...");
            // Ottiene i batch dal database Mongo che corrispondono ai criteri di filtro specificati
            var batchListMongo = await MongoRepository.Instance.GetWhereAsync<Batch>(a => ((a.Inizio >= request.FiltroDal && a.Inizio <= request.FiltroAl)
                                                                                                || (a.Fine > request.FiltroDal && a.Fine <= request.FiltroAl))
                                                                                            && (a.Stato == StatoBatch.Terminato
                                                                                                || a.Stato == StatoBatch.TerminatoAbortito
                                                                                                || a.Stato == StatoBatch.InModifica));

            Logger.DashboardOee.Info($"Getting terminated batch in mongoDb... completed batchListMongo = {batchListMongo.Count}");

            // Combina i batch presenti in memoria con quelli ottenuti dal database Mongo
            Logger.DashboardOee.Info($"Concateneting batchList and batchListMongo...");
            batchList = batchList.Concat(batchListMongo).ToList();
            Logger.DashboardOee.Info($"Concateneting batchList and batchListMongo... completed new list (saved in batchList) count = {batchList.Count}");

            //// Filtra ulteriormente la lista dei batch in base al periodo di tempo specificato
            //batchList.Where(a => ((a.Inizio >= request.FiltroDal && a.Inizio <= request.FiltroAl)
            //                        || (a.Fine > request.FiltroDal && a.Fine <= request.FiltroAl || a.Fine == null)));

            Logger.DashboardOee.Info("Filtering the list by the selected batch...");
            // Filtra la lista dei batch in base all'elenco degli ID delle commesse specificate nella richiesta
            batchList = batchList.Where(b => (request.ListIdCommesse.Count == 0 || request.ListIdCommesse.Contains(b.Id))
            && (request.ListIdMateriali.Count == 0 || request.ListIdMateriali.Contains(b.Materiale.Id)
            && (request.ListIdMacchine.Count == 0 || b.DistintaBase.Fasi.Any(f => request.ListIdMacchine.Contains(f.Macchine.FirstOrDefault()?.Macchina.Id))))).ToList();
            Logger.DashboardOee.Info($"Filtering the list by the selected batch... completed batchList.Count = {batchList.Count}");

            Logger.DashboardOee.Info("GetFilteredBatchList... completed");
            // Restituisce la lista dei batch filtrati
            return batchList;
        }

        private List<Dictionary<string, object>> calculateAvgTrend(List<BatchTrendParametri> listOee, string campo)
        {
            var listRet = new List<Dictionary<string, object>>();
            var listOeeGrouped = listOee.GroupBy(x => { return Convert.ToString(x.DataOra.Year) + Convert.ToString(x.DataOra.Month) + Convert.ToString(x.DataOra.Day) + Convert.ToString(x.DataOra.Hour); }).ToList();
            foreach (var item in listOeeGrouped)
            {
                var avg = Convert.ToInt32(item.Average(x => Convert.ToInt32(x.Valore)));
                var dictionary = new Dictionary<string, object>();
                dictionary.Add("x", item.FirstOrDefault().DataOra);
                dictionary.Add(campo, avg);
                listRet.Add(dictionary);
            }
            return listRet;
        }

        public async Task<List<ItemCommessa>> GetDataDashboardOeeCommesse(List<string> listIdMacchine)
        {
            var listCommesse = new List<ItemCommessa>();
            try
            {
                var allBatch = new List<Batch>();
                if (listIdMacchine.Count > 0)
                    allBatch = BatchManager.Instance.BatchInMemory().Where(b => b.DistintaBase.Fasi.Any() && b.DistintaBase.Fasi.FirstOrDefault().Macchine.Any(m => listIdMacchine.Contains(m.Macchina.Id))).ToList();
                else
                    allBatch = BatchManager.Instance.BatchInMemory();

                foreach (var batch in allBatch)
                    listCommesse.Add(new ItemCommessa { Id = batch.Id, Odp = batch.OdP });
            }
            catch (Exception exe)
            {
                _ = exe;
            }
            return listCommesse;
        }

        public async Task<List<Materiale>> GetDataDashboardOeeMateriali(List<string> listIdCommesse)
        {
            var listMateriali = new List<Materiale>();
            try
            {
                if (listIdCommesse.Count > 0)
                {
                    var listBatch = await MongoRepository.Instance.GetWhereAsync<Batch>(x => listIdCommesse.Contains(x.Id));
                    listBatch.ForEach(x => listMateriali.Add(x.Materiale));
                    listMateriali = listMateriali.Distinct().ToList();
                }
                else
                    listMateriali = await MongoRepository.Instance.All<Materiale>();
            }
            catch (Exception exe)
            {
                var ec = exe;
            }
            return listMateriali;
        }

        public string SecondToString(double sec)
        {
            return SecondToString(Convert.ToInt32(sec));
        }

        public int CalculateSeconds(DateTime time)
        {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);//from 1970/1/1 00:00:00 to now
            TimeSpan result = time.Subtract(dt);
            int seconds = 0;
            seconds = Convert.ToInt32(result.TotalSeconds);
            return seconds;
        }

        public string SecondToString(int seconds)
        {
            try
            {
                TimeSpan time = TimeSpan.FromSeconds(seconds);
                var hoursTot = time.Hours + (time.Days * 24);
                return (hoursTot > 10 ? Convert.ToString(hoursTot) : "0" + Convert.ToString(hoursTot)) + time.ToString(@"\:mm\:ss");
            }
            catch (Exception exc)
            {
                Logger.DashboardOee.Error("Error ", exc);
                return null;
            }
        }

        public List<BatchTrendParametri> elaborateTrend(List<BatchTrendParametri> list, string codiceMacchina, string suffisso, BatchTrendParametri lastTrend, DateTime FineBatch)
        {
            var listReturn = new List<BatchTrendParametri>();
            var minutiAddApp = 5;
            if (lastTrend != null)
            {
                var newItem = new BatchTrendParametri { Valore = lastTrend.Valore, DataOra = FiltroDal };
                list.Insert(0, newItem);
            }
            list = list.OrderBy(x => x.DataOra).ToList();


            for (int i = 0; i < list.Count; i++)
            {
                listReturn.Add(list[i]);
                var minutiAdd = minutiAddApp;
                var ii = i + 1;
                if (list.Count > ii)
                    while (list[i].DataOra.Year > 2000 && list[i].DataOra.AddMinutes(minutiAdd) < list[ii].DataOra)
                    {
                        listReturn.Add(new BatchTrendParametri
                        {
                            Valore = list[i].Valore,
                            DataOra = list[i].DataOra.AddMinutes(minutiAdd)
                        });
                        minutiAdd = minutiAdd + minutiAddApp;
                    }
            }
            listReturn = listReturn.OrderBy(x => x.DataOra).ToList();

            var lastItem = listReturn.LastOrDefault();
            if (lastItem != null && lastItem.DataOra < FiltroAl)
            {
                var dataAl = FiltroAl > DateTime.Now ? DateTime.Now : FiltroAl;
                var minutiAdd = minutiAddApp;
                while (lastItem.DataOra.AddMinutes(minutiAdd) < FineBatch && lastItem.DataOra.AddMinutes(minutiAdd) < dataAl)
                {
                    listReturn.Add(new BatchTrendParametri
                    {
                        Valore = lastItem.Valore,
                        DataOra = lastItem.DataOra.AddMinutes(minutiAdd)
                    });
                    minutiAdd = minutiAdd + minutiAddApp;
                }
            }
            return listReturn;
        }

        private void CalculateFermiInfo(ref ResponseDataDashboardOee itemRet)
        {
            Logger.DashboardOee.Info("CalculateFermiInfo...");
            //Calcolo percentuali fermi
            //Calcolo il tempo totale dei fermi su ogni batch con un Sum annidato, per quelli che non hanno una fine considero DateTime.Now come fine.
            GetTotTempiNumeroFermi(itemRet.ListFermi,
                out double totTempoFermi,
                out int totFermi,
                out Dictionary<string, double> totTempoFermiPerMacchine,
                out Dictionary<string, double> totTempoFermiPerCausale,
                out Dictionary<string, double> totTempoFermiPerMacchinaCausale,
                out Dictionary<string, int> totFermiPerMacchina,
                out Dictionary<string, int> totFermiPerCausale,
                out Dictionary<string, int> totFermiPerMacchinaCausale);

            for (int i = 0; i < itemRet.ListFermi.Count; i++)
            {
                var fermo = itemRet.ListFermi[i];
                var codiceMacchina = fermo.Macchina ?? "";
                var codiceCausale = fermo.Causale ?? "";
                var codiceMacchinaCausale = $"{codiceMacchina}_{codiceCausale}";
                fermo.PercentualeTempoTot = Math.Round(fermo.TempoFermo * 100 / totTempoFermi, 2);
                fermo.PercentualeFermiTot = Math.Round(fermo.NumeroFermi * 100 / (double)totFermi);
                if (totTempoFermiPerMacchine.TryGetValue(codiceMacchina, out double totTempoFermoMacchina) &&
                    totFermiPerMacchina.TryGetValue(codiceMacchina, out int totFermiMacchina))
                {
                    fermo.PercentualeTempoMacchina = Math.Round(fermo.TempoFermo * 100 / totTempoFermoMacchina, 2);
                    fermo.PercentualeFermiMacchina = Math.Round(fermo.NumeroFermi * 100 / (double)totFermiMacchina, 2);
                }


                if (totTempoFermiPerCausale.TryGetValue(codiceCausale, out double totTempoFermoCausale) &&
                   totFermiPerCausale.TryGetValue(codiceCausale, out int totFermiCausale))
                {
                    fermo.PercentualeTempoCausale = Math.Round(fermo.TempoFermo * 100 / totTempoFermoCausale, 2);
                    fermo.PercentualeFermiCausale = Math.Round(fermo.NumeroFermi * 100 / (double)totFermiCausale, 2);
                }

                if (totTempoFermiPerMacchinaCausale.TryGetValue(codiceMacchinaCausale, out double totTempoFermoMacchinaCausale) &&
                    totFermiPerMacchinaCausale.TryGetValue(codiceMacchinaCausale, out int totFermiMacchinaCausale))
                {
                    fermo.PercentualeTempoMacchinaCausale = Math.Round(fermo.TempoFermo * 100 / totTempoFermoMacchinaCausale, 2);
                    fermo.PercentualeFermiMacchinaCausale = Math.Round(fermo.NumeroFermi * 100 / (double)totFermiMacchinaCausale);
                }
            }

            var orderdTotTempoFermiPerCausale = totTempoFermiPerCausale.OrderByDescending(kvp => kvp.Value).Take(5);
            itemRet.FermiCausaliPercentSummary = orderdTotTempoFermiPerCausale.Select(kvp => $"{kvp.Key} ({Math.Round(kvp.Value * 100 / totTempoFermi, 2)}%)").ToList();
            itemRet.FermiCausaliTimeSummary = orderdTotTempoFermiPerCausale.Select(kvp => $"{kvp.Key} ({GetTimeSpanStringFromSeconds(kvp.Value)})").ToList();
            itemRet.FermiCausaliNumberSummary = totFermiPerCausale.OrderByDescending(kvp => kvp.Value).Select(kvp => $"{kvp.Key} ({kvp.Value})").Take(5).ToList();

            Logger.DashboardOee.Info("CalculateFermiInfo... completed");
        }

        private void GetTotTempiNumeroFermi(List<DashboardOeeFermiItem> listFermi, out double totTempoFermi, out int totFermi, out Dictionary<string, double> totTempoFermiPerMacchina, out Dictionary<string, double> totTempoFermiPerCausale, out Dictionary<string, double> tempoFermiPerMacchinaCausale, out Dictionary<string, int> totFermiPerMacchina, out Dictionary<string, int> totFermiPerCausale, out Dictionary<string, int> totFermiPerMacchinaCausale)
        {
            Logger.DashboardOee.Info("GetTotTempiNumeroFermi...");
            totTempoFermiPerCausale = new Dictionary<string, double>();
            totTempoFermiPerMacchina = new Dictionary<string, double>();
            tempoFermiPerMacchinaCausale = new Dictionary<string, double>();
            totFermiPerMacchina = new Dictionary<string, int>();
            totFermiPerCausale = new Dictionary<string, int>();
            totFermiPerMacchinaCausale = new Dictionary<string, int>();
            totTempoFermi = 0;
            totFermi = 0;
            for (int i = 0; i < listFermi.Count; i++)
            {
                var fermo = listFermi[i];
                totFermi += fermo.NumeroFermi;
                string keyMacchina = fermo.Macchina ?? "";
                string keyCausale = fermo.Causale ?? "";
                string keyMacchinaCausale = $"{keyMacchina}_{keyCausale}";

                //Aggiungo il tempo totale
                totTempoFermi += fermo.TempoFermo;

                //Aggiungo a Macchina
                if (totTempoFermiPerMacchina.Keys.Contains(keyMacchina))
                {
                    totTempoFermiPerMacchina[keyMacchina] += fermo.TempoFermo;
                    totFermiPerMacchina[keyMacchina] += fermo.NumeroFermi;
                }
                else
                {
                    totTempoFermiPerMacchina.Add(keyMacchina, fermo.TempoFermo);
                    totFermiPerMacchina.Add(keyMacchina, fermo.NumeroFermi);
                }

                //Aggiungo a Causale
                if (totTempoFermiPerCausale.Keys.Contains(keyCausale))
                {
                    totTempoFermiPerCausale[keyCausale] += fermo.TempoFermo;
                    totFermiPerCausale[keyCausale] += fermo.NumeroFermi;
                }
                else
                {
                    totTempoFermiPerCausale.Add(keyCausale, fermo.TempoFermo);
                    totFermiPerCausale.Add(keyCausale, fermo.NumeroFermi);
                }

                //Aggiungo a MacchinaCausale
                if (tempoFermiPerMacchinaCausale.Keys.Contains(keyMacchinaCausale))
                {
                    tempoFermiPerMacchinaCausale[keyMacchinaCausale] += fermo.TempoFermo;
                    totFermiPerMacchinaCausale[keyMacchinaCausale] += fermo.NumeroFermi;
                }
                else
                {
                    tempoFermiPerMacchinaCausale.Add(keyMacchinaCausale, fermo.TempoFermo);
                    totFermiPerMacchinaCausale.Add(keyMacchinaCausale, fermo.NumeroFermi);
                }
            }

            Logger.DashboardOee.Info("GetTotTempiNumeroFermi... completed");

        }

        private void CalculateScartiInfo(ref ResponseDataDashboardOee itemRet)
        {
            Logger.DashboardOee.Info("CalculateScartiInfo...");
            Dictionary<string, int> scartiPerMacchina = new Dictionary<string, int>();
            Dictionary<string, int> scartiPerCausale = new Dictionary<string, int>();
            Dictionary<string, int> scartiPerMacchinaCausale = new Dictionary<string, int>();
            int scartiTotali = 0;
            CalculateScartiGroupedDictionary(itemRet, ref scartiPerMacchina, ref scartiPerCausale, ref scartiPerMacchinaCausale, ref scartiTotali);

            foreach (var scarti in itemRet.ListScarti)
            {
                scarti.PercentualeScartiTot = Math.Round(scarti.NumeroScarti * 100.00 / scartiTotali, 2);

                var scartiTotaliMacchina = scartiPerMacchina[scarti.Macchina];
                scarti.PercentualeScartiPerMacchina = Math.Round(scarti.NumeroScarti * 100.00 / scartiTotaliMacchina, 2);

                var scartiTotaliCausale = scartiPerCausale[scarti.Causale];
                scarti.PercentualeScartiPerCausale = Math.Round(scarti.NumeroScarti * 100.00 / scartiTotaliCausale);

                var scartiTotaliMacchinaCausale = scartiPerMacchinaCausale[$"{scarti.Macchina}_{scarti.Causale}"];
                scarti.PercentualeScartiPerMacchinaCausale = Math.Round(scarti.NumeroScarti * 100.00 / scartiTotaliMacchinaCausale, 2);
            }

            Logger.DashboardOee.Info("CalculateScartiInfo... completed");

        }

        private static void CalculateScartiGroupedDictionary(ResponseDataDashboardOee itemRet, ref Dictionary<string, int> scartiPerMacchina, ref Dictionary<string, int> scartiPerCausale, ref Dictionary<string, int> scartiPerMacchinaCausale, ref int scartiTotali)
        {
            foreach (var scartiItem in itemRet.ListScarti)
            {
                TryToUpdateValue(ref scartiPerMacchina, scartiItem.Macchina, scartiItem);
                TryToUpdateValue(ref scartiPerCausale, scartiItem.Causale, scartiItem);
                TryToUpdateValue(ref scartiPerMacchinaCausale, $"{scartiItem.Macchina}_{scartiItem.Causale}", scartiItem);
                scartiTotali += scartiItem.NumeroScarti;
            }
        }

        private static void TryToUpdateValue(ref Dictionary<string, int> dict, string key, DashboardOeeScartiItem scarto)
        {
            if (dict.ContainsKey(key))
            {
                dict[key] += scarto.NumeroScarti;
            }
            else
            {
                dict.Add(key, scarto.NumeroScarti);
            }
        }

        private static void CalculateScartiInfoSummary(ref ResponseDataDashboardOee itemRet, List<DashboardOeeScartiItem> listScarti)
        {
            Logger.DashboardOee.Info("CalculateScartiInfoSummary...");
            Dictionary<string, int> dictApp = new Dictionary<string, int>();
            int totScarti = 0;

            Logger.DashboardOee.Info("iterate listScarti...");
            foreach (var scarto in listScarti)
            {
                var causale = scarto.Causale;
                if (dictApp.TryGetValue(causale, out int count))
                {
                    dictApp[causale] = count + scarto.NumeroScarti;
                }
                else
                {
                    dictApp.Add(causale, scarto.NumeroScarti);
                }
                totScarti += scarto.NumeroScarti;
            }
            Logger.DashboardOee.Info("iterate listScarti... completed");

            itemRet.ScartiCausaliPercentSummary = dictApp
                .OrderByDescending(kvp => kvp.Value)
                .Select(kvp => $"{kvp.Key} ({Math.Round((double)(kvp.Value * 100.0) / totScarti, 2)}%)")
                .ToList();
            itemRet.ScartiTotali = totScarti;
            Logger.DashboardOee.Info("CalculateScartiInfoSummary... completed");
        }

        private static string GetTimeSpanStringFromSeconds(double tempoLavoroBatchDouble)
        {
            var tempo = (int)tempoLavoroBatchDouble;
            var dd = tempo / 86400;
            tempo %= 86400;
            var hh = tempo / 3600;
            tempo %= 3600;
            var mm = tempo / 60;
            var ss = (tempo %= 60);
            return $"{dd}g {hh:D2}o:{mm:D2}m:{ss:D2}s";
        }

        //private static string GetHourTimeSpanStringFromSeconds(double tempoLavoroBatchDouble)
        //{
        //    var tempo = (int)tempoLavoroBatchDouble;
        //    var hh = tempo / 3600;
        //    tempo %= 3600;
        //    var mm = tempo / 60;
        //    var ss = (tempo %= 60);
        //    return $"{hh:D2}:{mm:D2}:{ss:D2}";
        //}

        private static double GetHourTimeSpanStringFromSeconds(double tempoLavoroBatchDouble)
        {
            return tempoLavoroBatchDouble / 3600;
        }

        private static void AddOrUpdateSummaryTimeDict(Dictionary<string, List<TimeDictItem>> dict, string groupKey, double tempo, string key)
        {
            if (tempo <= 0) { return; }
            if (key is null || key == string.Empty)
            {
                return;
            }
            TimeDictItem item = dict[groupKey].Where(it => it.Key == key).FirstOrDefault();
            if (item is null)
            {
                dict[groupKey].Add(new TimeDictItem()
                {
                    Key = key,
                    TimeInSecond = tempo,
                    Time = GetTimeSpanStringFromSeconds(tempo),
                    TimeInHour = GetHourTimeSpanStringFromSeconds(tempo),

                });
            }
            else
            {
                item.TimeInSecond += tempo;
                item.Time = GetTimeSpanStringFromSeconds(item.TimeInSecond);
                item.TimeInHour += GetHourTimeSpanStringFromSeconds(item.TimeInSecond);
            }
        }

        private static void AddOrUpdateSummaryTimeDict(Dictionary<string, List<TimeDictItem>> dict, string groupKey, TimeDictItem item)
        {
            if (item.TimeInSecond <= 0) { return; }
            if (item is null) { return; }

            TimeDictItem dictItem = null;
            dictItem = dict[groupKey].Where(it => it.Key == item.Key)?.FirstOrDefault();
            var seconds = item.TimeInSecond;
            if (dictItem is null)
            {
                dict[groupKey].Add(new TimeDictItem()
                {
                    Key = item.Key,
                    TimeInSecond = seconds,
                    Time = GetTimeSpanStringFromSeconds(seconds),
                    TimeInHour = GetHourTimeSpanStringFromSeconds(seconds),

                });
            }
            else
            {
                dictItem.TimeInSecond += seconds;
                dictItem.Time = GetTimeSpanStringFromSeconds(seconds);
                dictItem.TimeInHour += GetHourTimeSpanStringFromSeconds(seconds);
            }
        }

        private static void AddOrUpdateSummaryTimeDict2(Dictionary<string, List<TimeDictItem2>> dict, string groupKey, StatoFasi stato, double time, string key)
        {
            if (time <= 0) { return; }
            if (groupKey is null || groupKey == string.Empty)
            {
                return;
            }

            var group = dict[groupKey] ?? null;
            if (group is null) { return; }

            var item = group.FirstOrDefault(timeDictItem => timeDictItem.Key == key);
            var toAdd = false;
            if (item is null)
            {
                toAdd = true;
                item = new TimeDictItem2()
                {
                    Key = key,
                    Time = 0,
                    LavTimeInSecond = 0,
                    EmerTimeInSecond = 0,
                    PauTimeInSecond = 0,
                };
            }

            switch (stato)
            {
                case StatoFasi.InLavorazione:
                    {
                        item.LavTimeInSecond += time;
                        break;
                    }
                case StatoFasi.InLavorazioneInPausa:
                case StatoFasi.InLavorazioneInPausaS:
                    {
                        item.PauTimeInSecond += time;
                        break;
                    }
                case StatoFasi.InLavorazioneInEmergenza:
                    {
                        item.EmerTimeInSecond += time;
                        break;
                    }
                default:
                    return;
            }

            item.Time += time;

            if (toAdd)
            {
                group.Add(item);
            }
        }

        private static double GetSecondsDoubleFromTimeSpanString(string time)
        {
            //Questa funzione serve per convertire la stringa con questo formato ##g ##o:##m:##s
            string[] timeParts = time.Split(' ');

            int days = 0;
            int hours = 0;
            int minutes = 0;
            int seconds = 0;

            foreach (string part in timeParts)
            {
                if (part.EndsWith("g"))
                {
                    days = int.Parse(part.TrimEnd('g'));
                }
                else
                {
                    string[] timeValues = part.Split(':');
                    hours = int.Parse(timeValues[0].Substring(0, timeValues[0].Length - 1));
                    minutes = int.Parse(timeValues[1].Substring(0, timeValues[1].Length - 1));
                    seconds = int.Parse(timeValues[2].Substring(0, timeValues[2].Length - 1));
                }
            }

            double totalSeconds = (days * 24 * 60 * 60) + (hours * 60 * 60) + (minutes * 60) + seconds;
            return totalSeconds;
        }

        private static void CreateDevicePercItem(StatoFasi StatoAva, string keyDict, DistintaBaseFasi fase, double appTempo, ref Dictionary<string, DevicePercItem> fasePercentItems)
        {
            if (fasePercentItems is null)
            {
                fasePercentItems = new Dictionary<string, DevicePercItem>();
            }

            DevicePercItem devicePercentItem = new DevicePercItem()
            {
                TempTot = appTempo,
                ColorLav = fase.InLavorazioneColore?.ToUpper() ?? DistintaBaseFasi.InLavorazioneColoreDefaultValue,
                ColorEmer = fase.InLavorazioneInEmergenzaColore?.ToUpper() ?? DistintaBaseFasi.InLavorazioneInEmergenzaColoreDefaultValue,
                ColorPausa = fase.InLavorazioneInPausaColore?.ToUpper() ?? DistintaBaseFasi.InLavorazioneInPausaColoreDefaultValue,
                LabelLav = fase.InLavorazioneLabel ?? DistintaBaseFasi.InLavorazioneLabelDefaultValue,
                LabelEmer = fase.InLavorazioneInEmergenzaLabel ?? DistintaBaseFasi.InLavorazioneInEmergenzaLabelDefaultValue,
                LabelPausa = fase.InLavorazioneInPausaLabel ?? DistintaBaseFasi.InLavorazioneInPausaLabelDefaultValue,
            };

            switch (StatoAva)
            {
                case StatoFasi.InLavorazione:
                    devicePercentItem.TempLav = appTempo;
                    fasePercentItems.Add(keyDict, devicePercentItem);
                    break;
                case StatoFasi.InLavorazioneInPausa:
                    devicePercentItem.TempPausa = appTempo;
                    fasePercentItems.Add(keyDict, devicePercentItem);
                    break;
                case StatoFasi.InLavorazioneInEmergenza:
                    devicePercentItem.TempEmer = appTempo;
                    fasePercentItems.Add(keyDict, devicePercentItem);
                    break;
                default:
                    break;
            }
        }
    }
}