﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Model.Design;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using AreaMes.Server.Api.Design;
using AreaMes.Server.Api.Runtime;
using AreaMes.Server.MongoDb;
using AreaMes.Server.SignalR;
using AreaMes.Server.Utils;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AreaMes.Server
{
    public class BatchManager
    {
        private static BatchManager instance;
        public static BatchManager Instance { get { return instance; } }
        static BatchManager()
        {
            instance = new BatchManager();
        }


        private Assembly assembly { get; set; }
        private Type _batchType { get; set; }
        private Type _supervisorBatchType { get; set; }
        private Dictionary<string, BatchThread> listOfBatch = new Dictionary<string, BatchThread>();
        private List<TipoValori> listOfTipoValori = new List<TipoValori>();

        private SettingsManager _settingsManager;
        private SupervisorBatchLogicBase _supervisorBatchLogicBase;
        private Timer _saveTimer;
        private object _lockSave = new object();

        public async Task Init()
        {
            // caricamento dei parametri di sistema
            Logger.Default.InfoFormat("SettingsManager loading...", Const.AREAM2M_BATCH_LOGIC_DLL);
            _settingsManager = new SettingsManager();
            _settingsManager.Load();
            Logger.Default.InfoFormat("SettingsManager load correclty", Const.AREAM2M_BATCH_LOGIC_DLL);


            // caricamento dell'assembly custom
            Logger.Default.InfoFormat("Assembly '{0}' loading... ", Const.AREAM2M_BATCH_LOGIC_DLL);
            assembly = Assembly.LoadFile(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), Const.AREAM2M_BATCH_LOGIC_DLL));
            Logger.Default.InfoFormat("Assembly '{0}' load correclty", Const.AREAM2M_BATCH_LOGIC_DLL);


            // ricerca del tipo 
            Logger.Default.InfoFormat("Batch logic loading... ");
            _batchType = assembly.GetTypes().FirstOrDefault(p => typeof(AreaMes.Model.BatchLogic).IsAssignableFrom(p));
            Logger.Default.InfoFormat("Batch logic load correclty");

            // ricerca del tipo factory
            Logger.Default.InfoFormat("SupervisorBatch logic loading... ");
            _supervisorBatchType = assembly.GetTypes().FirstOrDefault(p => typeof(AreaMes.Model.SupervisorBatchLogicBase).IsAssignableFrom(p));
            if (_supervisorBatchType != null)
            {
                _supervisorBatchLogicBase = Activator.CreateInstance(_supervisorBatchType) as AreaMes.Model.SupervisorBatchLogicBase;
                _supervisorBatchLogicBase.Prepare(_settingsManager, StartAsync, MongoRepository.Instance, BatchInMemory, RuntimeTransportContextController.Instance);
                _supervisorBatchLogicBase.BatchLogicInMemory = BatchLogicInMemory;
                _supervisorBatchLogicBase.Init();
            }

            Logger.Default.InfoFormat("SupervisorBatch logic {0}", (_supervisorBatchType != null) ? "load correclty" : "NOT FOUNDED");



            // load dei batch 
            var lBatch = await MongoRepository.Instance.GetWhereAsync<Batch>(o => o.Stato != Model.Enums.StatoBatch.Terminato &&
                            o.Stato != Model.Enums.StatoBatch.Eliminato &&
                            o.Stato != Model.Enums.StatoBatch.TerminatoAbortito &&
                            o.Stato != Model.Enums.StatoBatch.InSospensione &&
                            o.Stato != Model.Enums.StatoBatch.InSospensioneInRipresa &&
                            o.Stato != Model.Enums.StatoBatch.InModifica);



            if (lBatch.Count() > 100)
            {
                Logger.Default.InfoFormat("Starting {0} batch threads, parallel mode...", lBatch.Count);

                Parallel.ForEach(lBatch, o => StartAsync(o));
            }
            else
            {
                Logger.Default.InfoFormat("Starting {0} batch threads...", lBatch.Count);

                foreach (var item in lBatch)
                    StartAsync(item);
            }

            Logger.Default.InfoFormat("Starting {0} batch threads... completed", lBatch.Count);



            _saveTimer = new Timer((a) =>
            {
                try
                {
                    lock (listOfBatch)
                        foreach (var item in listOfBatch.ToList())
                            lock (item.Value.Batch)
                                MongoRepository.Instance.Set(item.Value.Batch);
                }
                catch (Exception exc)
                {
                    Logger.Default.Error("Error on timer saving batch", exc);

                }
            }, null, 30 * 1000, 60 * 1000);


            //// Alberto.p, 09/09/2022, ricezione del messaggio di sospensione
            //MessageServices.Instance.Subscribe<BatchForceSuspending>(BatchForceSuspending.Name,
            //    async (o, e) =>
            //    {
            //        Logger.Default.Info($"Request Suspending Batch from operator '{e.Username}'...");

            //        var dicResult = new Dictionary<string, string>();
            //        foreach (var batchId in e.BatchIds)
            //        {
            //            try
            //            {
            //                Logger.Default.Info($"  Request Suspending Batch from operator '{e.Username}', BatchId:'{batchId}', request item from collection");
            //                var batchItem = this.Get(batchId);
            //                Logger.Default.Info($"  Request Suspending Batch from operator '{e.Username}', BatchId:'{batchId}', Odp:'{batchItem.OdP}'");
            //                RuntimeTransportContextController.Instance.SetBatch(new SetBatchInfo {  })

            //                batchItem.Sospensione = DateTime.Now;
            //                batchItem.Stato = StatoBatch.InModifica;
            //                Logger.Default.Info($"  Request Suspending Batch from operator '{e.Username}', BatchId:'{batchId}', Odp:'{batchItem.OdP}', set Sospensione:'{batchItem.Sospensione}', Stato:'{batchItem.Stato}'...");

            //                await AreaMes.Server.BatchManager.Instance.RemoveAsync(batchId);
            //                Logger.Default.Info($"  Request Suspending Batch from operator '{e.Username}', BatchId:'{batchId}', Odp:'{batchItem.OdP}', remove item from memory... completed");

            //                dicResult.Add(batchId, String.Empty);
            //            }
            //            catch (Exception exc)
            //            {
            //                dicResult.Add(batchId, exc.Message);
            //                Logger.Default.Error($"Error on suspeding batch '{batchId}', cause:{exc.Message}");
            //                Logger.Default.Error(exc);

            //            }
            //        }

            //        foreach (var item in dicResult)
            //        {
            //        }


            //        Logger.Default.Info($"Request Suspending Batch from operator '{e.Username}'... completed");

            //    });

        }

        public List<Batch> BatchInMemory()
        {
            lock (listOfBatch)
                return listOfBatch.Select(o => o.Value.Batch).ToList();
        }

        public List<Model.BatchLogic> BatchLogicInMemory()
        {
            lock (listOfBatch)
                return listOfBatch.Select(o => o.Value.Instance).ToList();
        }


        public async void StartAsync(Batch batch)
        {

            lock (listOfBatch)
                if (listOfBatch.ContainsKey(batch.Id))
                {
                    Logger.Default.InfoFormat("    The batch already exist and cannot create the thread '{0}', Odp:{1}...", batch.Id, batch.OdP);
                    return;
                }

            Logger.Default.InfoFormat("     Creating  batch thread '{0}', Odp:{1}...", batch.Id, batch.OdP);
            var batchInstance = Activator.CreateInstance(_batchType) as AreaMes.Model.BatchLogic;
            batchInstance.Prepare(_settingsManager);
            await batchInstance.Init(DataContainer.Instance,
                               MongoRepository.New,
                               MessageServices.Instance,
                               AreaMESManager.Instance,
                               AreaM2MClient.Instance,
                               RuntimeTransportContextController.Instance,
                               batch);
            var bt = new BatchThread
            {
                Batch = batch,
                Thread = new Thread(() => batchInstance.Start()),
                Instance = batchInstance
            };

            // await MongoRepository.Instance.SetAsync(batch);

            batch.DistintaBase.MaterialeInUscita?.Resolve();
            batch.DistintaBase.UnitaDiMisura?.Resolve();

            lock (listOfBatch)
                listOfBatch.Add(batch.Id, bt);

            bt.Thread.Start();

            if (_supervisorBatchLogicBase != null)
                _supervisorBatchLogicBase.OnBatchAdd(batchInstance);


            Logger.Default.InfoFormat("     Creating and running batch thread '{0}', Odp:{1}... completed", batch.Id, batch.OdP);
        }

        public Batch Get(string batchId)
        {
            BatchThread bt = null;
            lock (listOfBatch)
                if (listOfBatch.TryGetValue(batchId, out bt))
                    return bt.Batch;

            var res = MongoRepository.Instance.Collection<Batch>().Find<Batch>(Builders<Batch>.Filter.Eq("_id", batchId)).FirstOrDefault();


            return res;
        }

        public async Task RemoveAsync(string batchId)
        {
            BatchThread bt = null;
            try
            {
                lock (listOfBatch)
                    if (!listOfBatch.TryGetValue(batchId, out bt)) return;

                Logger.Default.InfoFormat("Removing batch thread '{0}', Odp:{1}...", batchId, bt.Batch.OdP);

                bt.Batch.TempoEffettivo = BatchUtils.CalculateEffectiveTime(bt.Batch);

                await MongoRepository.Instance.SetAsync(bt.Batch);

                bt.Thread.Abort();
                bt.Instance.Stop();
                bt.Instance = null;
                lock (listOfBatch)
                    listOfBatch.Remove(batchId);

                Logger.Default.InfoFormat("Removing batch thread '{0}', Odp:{1}, completed", batchId, bt.Batch.OdP);
            }
            catch (Exception exc)
            {
                Logger.Default.Error(String.Format("Removing batch thread '{0}', Odp:{1}, completed", batchId, bt != null ? bt.Batch.OdP : null), exc);
            }
        }

        ////////////////////////////////////////////////////////////////////////////
        ////////// TIPO VALORI /////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////

        public List<TipoValori> TipoValoriInMemory()
        {
            lock (listOfTipoValori)
                if (listOfTipoValori == null)
                {
                    listOfTipoValori = MongoRepository.Instance.Collection<TipoValori>().FindAsync(Builders<TipoValori>.Filter.Eq("TipoValore", "0")).Result.ToList();
                }

            return listOfTipoValori.ToList();
        }
    }

    public class BatchThread
    {
        public AreaMes.Model.BatchLogic Instance { get; set; }

        public Batch Batch { get; set; }

        public Thread Thread { get; set; }


    }


}
