﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AreaMes.Model;
using AreaMes.Model.Design;
using MongoDB.Driver;
using MongoDB.Bson.Serialization;
using AreaMes.Model.Runtime;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using AreaMes.Meta;
using System.Globalization;
using System.Linq.Expressions;
using System.Linq;

namespace AreaMes.Server.MongoDb
{
    public class MongoRepository : IRepository
    {

        private static MongoRepository _Instance;
        public static MongoRepository Instance
        {
            get { return _Instance ?? (_Instance = new MongoRepository()); }
        }

        public static MongoRepository New
        {
            get { var i = new MongoRepository(); i.Connect(); return i; }
        }


        private IMongoClient _client;
        private IMongoDatabase _database;
        public bool IsRun { get { return (_database != null); } }
        private static bool _IsFirstInit = true;
        private static object _IsFirstInit_lock = new object();

        public void Connect()
        {
            try
            {
                lock (this)
                {
                    Logger.MongoDb.Debug("Connect(),  new MongoClientSettings()... ");
                    MongoClientSettings settings = new MongoClientSettings();
                    settings.WaitQueueSize = 1024;
                    settings.WaitQueueTimeout = new TimeSpan(0, 5, 0);
                    settings.MinConnectionPoolSize = 1;
                    settings.MaxConnectionPoolSize = 1024;
                    settings.ConnectTimeout = new TimeSpan(0, 5, 0);
                    Logger.MongoDb.Debug("Connect(),  new MongoClientSettings()... completed");


                    Logger.MongoDb.Debug("Connect(),  new MongoServerAddress()... ");
                    int port = 27017;
                    if (!String.IsNullOrWhiteSpace(Const.AREAMES_MONGODB_PORT))
                        port = Convert.ToInt32(Const.AREAMES_MONGODB_PORT);


                    settings.Server = new MongoServerAddress(Const.AREAMES_MONGODB.Replace("mongodb://", ""), port); // TODO: da cambiare con replace su Const.AREAMES_MONGODB
                    _client = new MongoClient(settings);
                    Logger.MongoDb.Debug($"Connect(),       settings.Server: '{settings.Server}' ");
                    _database = _client.GetDatabase(Const.AREAMES_MONGODB_NAME);
                    Logger.MongoDb.Debug("Connect(),  new MongoServerAddress()... completed");

                    lock (_IsFirstInit_lock)
                    {
                        if (_IsFirstInit)
                        {
                            BsonClassMap.RegisterClassMap<Macchina>(cm => { cm.AutoMap(); cm.SetIgnoreExtraElements(true); });
                            BsonClassMap.RegisterClassMap<TipiMacchina>(cm => { cm.AutoMap(); cm.SetIgnoreExtraElements(true); });
                            BsonClassMap.RegisterClassMap<MacchinaCompetenza>(cm => { cm.AutoMap(); cm.SetIgnoreExtraElements(true); });
                            BsonClassMap.RegisterClassMap<Operatore>(cm => { cm.AutoMap(); cm.SetIgnoreExtraElements(true); });
                            BsonClassMap.RegisterClassMap<OperatoreCompetenza>(cm => { cm.AutoMap(); cm.SetIgnoreExtraElements(true); });
                            BsonClassMap.RegisterClassMap<UnitaDiMisura>(cm => { cm.AutoMap(); cm.SetIgnoreExtraElements(true); });
                            BsonClassMap.RegisterClassMap<Materiale>(cm => { cm.AutoMap(); cm.SetIgnoreExtraElements(true); });
                            BsonClassMap.RegisterClassMap<OperazioneFase>(cm => { cm.AutoMap(); cm.SetIgnoreExtraElements(true); });
                            BsonClassMap.RegisterClassMap<DistintaBase>(cm => { cm.AutoMap(); cm.SetIgnoreExtraElements(true); });
                            BsonClassMap.RegisterClassMap<DistintaBaseFasi>(cm => { cm.AutoMap(); cm.SetIgnoreExtraElements(true); });
                            BsonClassMap.RegisterClassMap<Batch>(cm => { cm.AutoMap(); cm.SetIgnoreExtraElements(true); });
                            _IsFirstInit = false;
                        }
                    }
                }


                //var sysAdmin = Instance.Collection<Operatore>().Find(Builders<Operatore>.Filter.Eq("Username", "sa")).FirstOrDefault();
                //if (sysAdmin==null)
                //    Instance.SetAsync(new Operatore { Id=Guid.NewGuid().ToString(), Username = "sa", Password = "area",AreaDiSicurezza_IsSuperAdmin = true, AreaDiSicurezza_Anagrafica=true, AreaDiSicurezza_Designer=true, AreaDiSicurezza_Produzione=true, AreaDiSicurezza_Tabelle=true  } );
                //BsonSerializer.RegisterSerializer(typeof(DateTime), DateTimeSerializer.LocalInstance);
                //BsonSerializer.RegisterSerializer(typeof(DateTime?), );
                //BsonSerializer.RegisterSerializer(typeof(DateTime?), new MyMongoDBDateTimeSerializer());

            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
                throw exc;
            }
        }

        public void CreateDefaultParameters()
        {
            Logger.MongoDb.InfoFormat("CreateDefaultParameters()...");

            //create default operator
            var sysAdmin = Instance.Collection<Operatore>().Find(Builders<Operatore>.Filter.Eq("Username", "sa")).FirstOrDefault();
            if (sysAdmin == null)
                Instance.SetAsync(new Operatore { Id = Guid.NewGuid().ToString(), Username = "sa", Password = "area", AreaDiSicurezza_IsSuperAdmin = true, AreaDiSicurezza_Anagrafica = true, AreaDiSicurezza_Designer = true, AreaDiSicurezza_Produzione = true, AreaDiSicurezza_Tabelle = true }).GetAwaiter().GetResult();

            //create default Tipivalori
            var tipiValori = Instance.Collection<TipoValori>().Find(Builders<TipoValori>.Filter.Eq("Codice", "AreaMes_Default")).FirstOrDefault();
            if (tipiValori == null)
                Instance.SetAsync(new TipoValori { Id = Guid.NewGuid().ToString(), Ordinamento = 0, Nome = "PezziScarti", Codice = "AreaMes_Default", Descrizione = "Da Giustificare", Tipo = 0, DataOra = DateTime.Now, Default = false, IsInternal = true }).GetAwaiter().GetResult();

            //create default CausaliRiavvio
            var CausaliRiavvio = Instance.Collection<CausaliRiavvio>().Find(Builders<CausaliRiavvio>.Filter.Eq("Codice", "AreaMes_Default")).FirstOrDefault();
            if (CausaliRiavvio == null)
                Instance.SetAsync(new CausaliRiavvio { Id = Guid.NewGuid().ToString(), Codice = "AreaMes_Default", Causale = "Da Giustificare", IsStopCausal = true, IsAboartCausal = false, Default = false, IsInternal = true }).GetAwaiter().GetResult();

            Logger.MongoDb.InfoFormat("CreateDefaultParameters()... completed");
        }

        public IMongoCollection<T> Collection<T>() where T : IDoc
        {
            return _database.GetCollection<T>(typeof(T).Name);
        }


        public Task<T> GetAsync<T>(string id) where T : IDoc, new()
        {
            return _database.GetCollection<T>(typeof(T).Name).Find((Builders<T>.Filter.Eq("Id", id))).FirstOrDefaultAsync();
        }

        public T Get<T>(string id) where T : IDoc, new()
        {
            return _database.GetCollection<T>(typeof(T).Name).Find((Builders<T>.Filter.Eq("Id", id))).FirstOrDefault();
        }


        public Task<List<T>> All<T>(string[] excludeFields = null, string[] includeFields = null) where T : IDoc, new()
        {
            return All<T>(excludeFields, includeFields, 0, 0);
        }

        public Task<List<T>> All<T>(string[] excludeFields, string[] includeFields, int page, int pageSize) where T : IDoc, new()
        {
            var query = Collection<T>().Find(Builders<T>.Filter.Empty);

            if ((excludeFields == null) && (includeFields == null))
                return query.ToListAsync();


            string project = "";

            if (excludeFields != null)
                for (int i = 0; i < excludeFields.Length; i++)
                    project += String.Format("{0}:0,", excludeFields[i]);

            if (includeFields != null)
                for (int i = 0; i < includeFields.Length; i++)
                    project += String.Format("{0}:1,", includeFields[i]);

            return query.Project<T>("{" + project + "}").ToListAsync();
        }

        public async Task<string> SetAsync<T>(T item) where T : IDoc, new()
        {
            try
            {
                item.BsonSizeMb = item.ToBson().Length;
                var sizeToString = Utility.SizeSuffix(item.BsonSizeMb);
                double perc = ((double)item.BsonSizeMb / 15728640);
                Logger.MongoDb.Debug($"Save {item.GetType()}, Id:'{item.Id}', '{((IDoc)item).ToString}', SizeMb:'{sizeToString}'({perc.ToString("P", CultureInfo.InvariantCulture)})... ");
                await Collection<T>().ReplaceOneAsync(x => x.Id == item.Id, item, new UpdateOptions { IsUpsert = true });
                Logger.MongoDb.Debug($"Save {item.GetType()}, Id:'{item.Id}', '{((IDoc)item).ToString}', SizeMb:'{sizeToString}'({perc.ToString("P", CultureInfo.InvariantCulture)})... completed");
            }
            catch (Exception exc)
            {
                Logger.MongoDb.Error(exc.Message);
                throw;
            }

            return item.Id;
        }

        public string Set<T>(T item) where T : IDoc, new()
        {
            return this.SetAsync(item).GetAwaiter().GetResult();
        }

        public Task<List<T>> GetWhereAsync<T>(Expression<Func<T, bool>> filterSpecification) where T : IDoc, new()
        {
            var b = Builders<T>.Filter.Where(filterSpecification);
            return _database.GetCollection<T>(typeof(T).Name).Find(b).ToListAsync();
        }

        public Task<List<T>> GetWhereAsync<T>(string campo, string valore) where T : IDoc, new()
        {
            return _database.GetCollection<T>(typeof(T).Name).Find(Builders<T>.Filter.Eq(campo, valore)).ToListAsync();
        }

        public Task<List<T>> GetWhereAsync<T>(Expression<Func<T, bool>> filterSpecification, FindOptions<T> findOptions) where T : IDoc, new()
        {
            return (_database.GetCollection<T>(typeof(T).Name).FindAsync(filterSpecification, findOptions).GetAwaiter().GetResult()).ToListAsync();
        }


        public bool Delete<T>(string id) where T : IDoc, new()
        {
            var res = Collection<T>().DeleteOne(x => x.Id == id);
            return res.DeletedCount == 1;
        }

        public async Task<bool> DeleteAsync<T>(string id) where T : IDoc, new()
        {
            var res = await Collection<T>().DeleteOneAsync(x => x.Id == id);
            return res.DeletedCount == 1;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }


    public class LocalDateTimeConvention : IMemberMapConvention
    {
        public string Name
        {
            get { return "LocalDateTime"; }
        }

        public void Apply(BsonMemberMap memberMap)
        {
            if (memberMap.MemberType == typeof(DateTime))
            {
                var dateTimeSerializer = new DateTimeSerializer(DateTimeKind.Local);
                memberMap.SetSerializer(dateTimeSerializer);
            }
            else if (memberMap.MemberType == typeof(DateTime?))
            {
                var dateTimeSerializer = new DateTimeSerializer(DateTimeKind.Local);
                var nullableDateTimeSerializer = new NullableSerializer<DateTime>(dateTimeSerializer);
                memberMap.SetSerializer(nullableDateTimeSerializer);
            }
        }
    }


    //public class MyMongoDBDateTimeSerializer : DateTimeSerializer
    //{
    //    //  MongoDB returns datetime as DateTimeKind.Utc, which cann't be used in our timezone conversion logic
    //    //  We overwrite it to be DateTimeKind.Unspecified
    //    public override DateTime Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
    //    {
    //        try
    //        {
    //            var obj = base.Deserialize(context, args);
    //            return new DateTime(obj.Ticks, DateTimeKind.Local);
    //        }
    //        catch (Exception)
    //        {
    //            return BsonNull.Value;
    //        }
    //    }

    //    //  MongoDB stores all datetime as Utc, any datetime value DateTimeKind is not DateTimeKind.Utc, will be converted to Utc first
    //    //  We overwrite it to be DateTimeKind.Utc, becasue we want to preserve the raw value
    //    public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, DateTime value)
    //    {
    //        var utcValue = new DateTime(value.HasValue ? value.Value.Ticks : 0, DateTimeKind.Local);
    //        base.Serialize(context, args, utcValue);
    //    }
    //}


}
