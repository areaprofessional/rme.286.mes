﻿using System;
using System.Threading.Tasks;
using AreaMes.Model;
using MongoDB.Driver;
using AreaMes.Meta;

namespace AreaMes.Server.MongoDb
{
    public static class MongoExtension
    {
        public static async Task<T> Resolve<T>(this IMongoDatabase database, MongoDBRef reference)
        {
            var filter = (Builders<T>.Filter.Eq("Id", reference.Id));
            return await database.GetCollection<T>(reference.CollectionName).Find(filter).FirstOrDefaultAsync();
        }

        public static void Uniquefy(this IDoc item)
        {
            if (String.IsNullOrWhiteSpace(item.Id))
                item.Id = Guid.NewGuid().ToString();
        }


        public static  void Resolve<T>(this ExternalDoc<T> item) where T : IDoc, new()
        {
            if (!String.IsNullOrWhiteSpace(item.Id) )
                item.Item = MongoRepository.New.GetAsync<T>(item.Id).Result;
        }

    }
}
