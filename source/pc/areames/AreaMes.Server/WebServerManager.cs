﻿using AreaMes.Meta;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EmbedIO;
using EmbedIO.Files;

namespace AreaMes.Server
{
    internal class WebServerManager
    {
        private static WebServerManager instance;
        public static WebServerManager Instance { get { return instance; } }
        static WebServerManager()
        {
            instance = new WebServerManager();
        }



        private WebServer _serverDesign;
        //private Unosquare.Labs.EmbedIO.WebServer _serverTablet;
        public  void Run()
        {

            //// creazione del servizio web DESIGNER
            //if (Directory.Exists(Const.AREAMES_WEBSERVER_DESIGN_PATH))
            //{
            //    _serverDesign = WebServer
            //        .Create(String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_DESIGN_PORT))
            //        .EnableCors()
            //        .WithLocalSession()
            //        .WithStaticFolderAt(Const.AREAMES_WEBSERVER_DESIGN_PATH);


            //    _serverDesign.RunAsync();
            //    Logger.Default.Info("Service WEB-SERVER Design running, on " + String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_DESIGN_PORT + ", local folder:" + Const.AREAMES_WEBSERVER_DESIGN_PATH));
            //}

            // creazione del servizio web DESIGNER
            if (Directory.Exists(Const.AREAMES_WEBSERVER_DESIGN_PATH))
            {
                string httpDesign = String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_DESIGN_PORT);


                // Our web server is disposable.
                _serverDesign = CreateWebServer(httpDesign, Const.AREAMES_WEBSERVER_DESIGN_PATH);

                
                    // Once we've registered our modules and configured them, we call the RunAsync() method.
                    _serverDesign.RunAsync();
                
                Logger.Default.Info("Service WEB-SERVER Design running, on " + String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_DESIGN_PORT + ", local folder:" + Const.AREAMES_WEBSERVER_DESIGN_PATH));
            }


            // creazione del servizio web TABLET
            if (Directory.Exists(Const.AREAMES_WEBSERVER_TABLET_PATH))
            {
                string httpDesign = String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_TABLET_PORT);


                // Our web server is disposable.
                _serverDesign = CreateWebServer(httpDesign, Const.AREAMES_WEBSERVER_TABLET_PATH);


                // Once we've registered our modules and configured them, we call the RunAsync() method.
                _serverDesign.RunAsync();

                Logger.Default.Info("Service WEB-SERVER Design running, on " + String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_TABLET_PORT + ", local folder:" + Const.AREAMES_WEBSERVER_TABLET_PATH));
            }


            // creazione del servizio web TABLET
            if (Directory.Exists(Const.AREAMES_WEBSERVER_MULTIMEDIA_PATH))
            {
                string httpDesign = String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_MULTIMEDIA_PORT);


                // Our web server is disposable.
                _serverDesign = CreateWebServer(httpDesign, Const.AREAMES_WEBSERVER_MULTIMEDIA_PATH);


                // Once we've registered our modules and configured them, we call the RunAsync() method.
                _serverDesign.RunAsync();

                Logger.Default.Info("Service WEB-SERVER Design running, on " + String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_MULTIMEDIA_PORT + ", local folder:" + Const.AREAMES_WEBSERVER_MULTIMEDIA_PATH));
            }

            //if (Directory.Exists(Const.AREAMES_WEBSERVER_TABLET_PATH))
            //{
            //    // creazione del servizio web TABLET
            //    _serverTablet = WebServer
            //        .Create(String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_TABLET_PORT))
            //        .EnableCors()
            //        .WithLocalSession()
            //        .WithStaticFolderAt(Const.AREAMES_WEBSERVER_TABLET_PATH);

            //    _serverTablet.RunAsync();
            //    Logger.Default.Info("Service WEB-SERVER Tablet running, on " + String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_TABLET_PORT + ", local folder:" + Const.AREAMES_WEBSERVER_TABLET_PATH));
            //}


            //if (Directory.Exists(Const.AREAMES_WEBSERVER_MULTIMEDIA_PATH))
            //{
            //    // creazione del servizio web TABLET
            //    _serverTablet = WebServer
            //        .Create(String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_MULTIMEDIA_PORT))
            //        .EnableCors()
            //        .WithLocalSession()
            //        .WithStaticFolderAt(Const.AREAMES_WEBSERVER_MULTIMEDIA_PATH);

            //    _serverTablet.RunAsync();
            //    Logger.Default.Info("Service WEB-SERVER Multimedia running, on " + String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_MULTIMEDIA_PORT + ", local folder:" + Const.AREAMES_WEBSERVER_MULTIMEDIA_PATH));
            //}
        }

        // Create and configure our web server.
        private static WebServer CreateWebServer(string url, string staticFolder)
        {
            var server = new WebServer(o => o
                    .WithUrlPrefix(url)
                    .WithMode(HttpListenerMode.EmbedIO))
                // First, we will configure our web server by adding Modules.
                .WithLocalSessionManager()
                .WithCors()
                .WithStaticFolder("/", staticFolder, false);

            // Listen for state changes.
            server.StateChanged += (s, e) => {
                var s1 = e.NewState;
            };
   
            return server;
        }
    }
}
