﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Model.Dto;
using AreaMes.Model.Runtime;
using AreaMes.Server.Utils;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AreaMes.Server.SignalR
{
    [HubName("AreaMESManager")]
    public class AreaMESManager : Hub, IRealTimeManager
    {
        private static IDisposable webApp;
        private static Timer _timerClock;
        private static string _lastDate;

        private static AreaMESManager instance;
        public static AreaMESManager Instance { get { return instance; } }

        static AreaMESManager()
        {
            instance = new AreaMESManager();
        }


        private static IHubContext InMemoryContext { get { return GlobalHost.ConnectionManager.GetHubContext<AreaMESManager>(); } }

        public  void Start()
        {
            webApp = WebApp.Start(String.Format("http://*:{0}", Const.AREAMES_REALTIMESERVER_PORT));
            _timerClock = new Timer((a) =>
            {
                var s = DateTime.Now.ToString("yyyyMMdd HHmmss");
                if (_lastDate != s)
                {
                    _lastDate = s;
                    InMemoryContext.Clients.All.GetDateAsync(s);
                }
            }, null, 2 * 1000, 5 * 1000);

            Logger.Default.Info("Service RUNTIME running, on " + String.Format("http://*:{0}", Const.AREAMES_REALTIMESERVER_PORT));

        }

        public static void Stop()
        {
            webApp.Dispose();
        }


        [HubMethodName("SetAction")]
        public async Task<bool> SetAction(Macchina s)
        {
            return await Task.Run(() => { return default(bool); });
        }


        [HubMethodName("SubscribeToBatch")]
        public async Task<BatchHeader> SubscribeToBatch(string batchId)
        {
            if (String.IsNullOrWhiteSpace(batchId)) return null;
            var batch = AreaMes.Server.BatchManager.Instance.Get(batchId);
            if (batch == null) return null;

            await InMemoryContext.Groups.Add(Context.ConnectionId, batchId);
            return ToBatchHeader(batch);
        }



        [HubMethodName("UnSubscribeToBatch")]
        public async Task<bool> UnSubscribeToBatch(string batchId)
        {
            await InMemoryContext.Groups.Remove(Context.ConnectionId, batchId);
            return true;
        }

        [HubMethodName("SubscribeToDashboardOee")]
        public async Task<DashboardOee> SubscribeToDashboardOee()
        {
            var date = await DashboardOeeManager.Instance.GetDashboardOeeFilter();
            return date;
        }

        [HubMethodName("GetDataDashboardOee")]
        public async Task<ResponseDataDashboardOee> GetDataDashboardOee(RequestDataDashboardOee request)
        {
            var date = await DashboardOeeManager.Instance.GetDataDashboardOee(request);
            return date;
        }

        [HubMethodName("GetScartiDatiPiani")]
        public async Task<List<ScartiDatiPiani>> GetScartiDatiPiani(RequestDataDashboardOee request)
        {
            try
            {
                var data = await DashboardOeeManager.Instance.GetScartiDatiPiani(request);
                return data;
            }
            catch (Exception ex)
            {
                _ = ex;
                return new List<ScartiDatiPiani>();
            }
            
        }

        [HubMethodName("GetFermiDatiPiani")]
        public async Task<List<FermiDatiPiani>> GetFermiDatiPiani(RequestDataDashboardOee request)
        {
            try
            {
                var data = await DashboardOeeManager.Instance.GetFermiDatiPiani(request);
                return data;
            }
            catch (Exception ex)
            {
                _ = ex;
                return new List<FermiDatiPiani>();
            }
            
        }

        [HubMethodName("GetDataDashboardOeeCommesse")]
        public async Task<List<ItemCommessa>> GetDataDashboardOeeCommesse(List<string> listIdMacchine)
        {
            var date = await DashboardOeeManager.Instance.GetDataDashboardOeeCommesse(listIdMacchine);
            return date;
        }

        [HubMethodName("GetDataDashboardOeeMateriali")]
        public async Task<List<Materiale>> GetDataDashboardOeeMateriali(List<string> listIdCommesse)
        {
            var date = await DashboardOeeManager.Instance.GetDataDashboardOeeMateriali(listIdCommesse);
            return date;
        }




        //[HubMethodName("SendLang")]
        //public async void SendLang(string  message)
        //{
        //    string lingua = Convert.ToString(message);

        //    try
        //    {

        //        var list = TranslationManager.GetTranslationItems(lingua);
        //        if(list==null || list.Count==0)
        //        {
        //            var lista = TranslationManager.GetTranslationItems("it");
        //            await InMemoryContext.Clients.All.OnSendlanguage(lista);
        //        }
        //        else
        //        await InMemoryContext.Clients.All.OnSendlanguage(list);
        //    }
        //    catch (Exception exc)
        //    {
        //        var lista = TranslationManager.GetTranslationItems("it");
        //        await InMemoryContext.Clients.All.OnSendlanguage(lista);
        //    }


        //}

        public void NotifyFaseChanged(string batchId, FaseChangedInfo fase)
        {
            InMemoryContext.Clients.Group(batchId).OnFaseChanged(batchId, fase);
        }


        public void NotifyBatchMaterialeChanged(string batchId, List<BatchMateriali> batchMateriali)
        {
            InMemoryContext.Clients.Group(batchId).OnBatchMaterialeChanged(batchId, batchMateriali);
        }

        public void NotifyVersamentoFaseChanged(string batchId, BatchFase batchFase)
        {
            InMemoryContext.Clients.Group(batchId).OnVersamentoFaseChanged(batchId, batchFase);
        }


        public void NotifyBatchChanged(string batchId,Batch batch)
        {
            InMemoryContext.Clients.Group(batchId).OnBatchChanged(batchId, ToBatchHeader(batch));
        }

        private BatchHeader ToBatchHeader(Batch batch)
        {
            return new AreaMes.Model.Dto.BatchHeader
            {
                Id = batch.Id,
                OdP = batch.OdP
            };
        }

        public void NotifyDashboardOee(GetDataDashboardOee message)
        {
            InMemoryContext.Clients.All.OnDashboardOee(message);
        }

    }
}
