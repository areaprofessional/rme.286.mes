﻿using AreaM2M.RealTime.Dto;
using AreaMes.Meta;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Server.SignalR
{
    public class AreaM2MClient : IAreaM2MClient
    {
        /// <summary>
        /// Riferimento all'oggetto di comunicazione con il server
        /// </summary>
        private static AreaM2MClient _Instance;
        public static AreaM2MClient Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new AreaM2MClient();

                return _Instance;
            }
        }
        private HubConnection HubConnection { get; set; }
        private IHubProxy HubProxy { get; set; }

        public static string AreaM2M_ServerIp { get; set; }
        public static int AreaM2M_ServerPort { get; set; }
        public static string AreaM2M_UserName { get; set; }
        public static string AreaM2M_Password { get; set; }
        private IDisposable _OnDeviceThingsChanged;

        private string tokenId;
        private Dictionary<string, string> _dicSecurityCode = new Dictionary<string, string>();

        public async Task Start()
        {
            try
            {
                Logger.M2M.Info("AreaM2M Client starting on " + Const.AREAM2M_SERVER_IP + ":" + Const.AREAM2M_SERVER_PORT + "...");

                HubConnection = new HubConnection(String.Format("http://{0}:{1}", Const.AREAM2M_SERVER_IP, Const.AREAM2M_SERVER_PORT));
                HubConnection.TransportConnectTimeout = new TimeSpan(0, 5, 0);
                HubProxy = HubConnection.CreateHubProxy("SignalRManager");
                HubConnection.TraceWriter = new DebugTextWriter(); ;
                HubConnection.TraceLevel = TraceLevels.All;
                HubConnection.Closed += async () =>
                {
                    try
                    {
                        Logger.M2M.Info("AreaM2M Client closed!");
                        // riconnessione al server
                        Logger.M2M.Info("AreaM2M Client Start, call Init()...");
                        await Init();
                        Logger.M2M.Info("AreaM2M Client Start, call Init()... completed");
                    }
                    catch (Exception exc)
                    {
                        Logger.M2M.Error($"AreaM2M Client closed on error, cause:'{exc.Message}'", exc);
                    }
                };
                HubConnection.Error += exception => {
                    Logger.M2M.Error("AreaM2M Client error, cause: " + exception);
                };
                HubConnection.Reconnecting += () => {
                    Logger.M2M.Info("AreaM2M Client reconnecting...");
                };
                HubConnection.StateChanged += async (e) => {
                    Logger.M2M.Info($"AreaM2M Client state changed, from {e.OldState} to {e.NewState}");

                    // solo nel caso sia avvenuta una connessione dopo n-tentativi di riconnessione.
                    // Attenzione: differente rispetto alla disconnessione effettiva, ovvero quando il canale è "closed". 
                    // In questo caso scatta l'evento 'Closed' gestito nella parte precedente del codice
                    if ((e.NewState == ConnectionState.Connected) && (e.OldState == ConnectionState.Reconnecting))
                    {
                        Logger.M2M.Info("AreaM2M Client Start, call Init(false)...");
                        await Init(false);  // viene invocato il metodo per eseguire il subscribe ma senza ricreare l'oggetto connessione
                        Logger.M2M.Info("AreaM2M Client Start, call Init(false)... completed");
                    }
                };

                Logger.M2M.Info("AreaM2M Client Start, call Init()...");
                await Init();
                Logger.M2M.Info("AreaM2M Client Start, call Init()... completed");
            }
            catch (Exception exc)
            {
                Logger.M2M.Error($"AreaM2M Client starting on " + Const.AREAM2M_SERVER_IP + ":" + Const.AREAM2M_SERVER_PORT + $" on error, cause:'{exc.Message}'", exc);
            }


        }

        private async Task Init(bool withRecreateConnection = true)
        {
            try
            {

                Logger.M2M.Info("AreaM2M Client init...");

                if (withRecreateConnection)
                {
                    Logger.M2M.Debug("AreaM2M Client init, HubConnection.Start()...");
                    await HubConnection.Start();
                    Logger.M2M.Debug("AreaM2M Client init, HubConnection.Start()... completed");
                }


                Logger.M2M.InfoFormat("AreaM2M Client login, user:'{0}', password:'{1}' ", AreaM2M_UserName, AreaM2M_Password);
                var res = await HubProxy.Invoke<ActionResult>("LoginAsync", AreaM2M_UserName, AreaM2M_Password);

                if (res.Success)
                {
                    tokenId = (string)res.Data;

                    Logger.M2M.InfoFormat("AreaM2M Client login successfully, tokenId:'{0}' ", tokenId);
                    Logger.M2M.InfoFormat("AreaM2M Client subscribe to things changed");

                    // distruzione dell'evento se già agganciato in precedenza
                    if (_OnDeviceThingsChanged != null) _OnDeviceThingsChanged.Dispose();

                    // agganciato l'evento
                    _OnDeviceThingsChanged = HubProxy.On<ValuesChangedResult>("OnDeviceThingsChanged", (s) =>
                    {
                        Logger.M2M.InfoFormat($"Device '{s.DeviceId}', change values message, update internal cache...");
                        DataContainer.Instance.Fill(s.Things, s.DeviceId);
                        foreach (var item in s.Things)
                            Logger.M2M.DebugFormat($"Device '{s.DeviceId}', change value '{item.Path}' from {item.Info.OldValue} to { item.Value}", s.DeviceId);
                        Logger.M2M.InfoFormat($"Device '{s.DeviceId}', change values message, update internal cache... completed");

                    });



                    // -----------------------------------------------------------------------------------
                    // logica per gestire la casistica di sottoscrizione a tutte le macchine, nel caso in cui la chiave di configurazione sia vuota
                    var listOfDevices = Const.AREAM2M_SUBSCRIBE_TO_DEVICES.Split(';');
                    var listOfSecurityCodes = Const.AREAM2M_SUBSCRIBE_TO_DEVICES_SECURITY_CODE.Split(';');
                    var listOfRegistriesDevices = new Dictionary<string, List<Registry>>();

                    if (string.IsNullOrWhiteSpace(Const.AREAM2M_SUBSCRIBE_TO_DEVICES))
                    {
                        var resSub = await HubProxy.Invoke<ActionResultGetDevicesSchema>("GetDevicesSchemaAsync", Const.AREAM2M_PROJECT, tokenId);
                        if (resSub.Success)
                        {
                            listOfDevices = resSub.Devices.Select(o => o.Device.Code).ToArray();  // vengono prelevati l'elenco dei devices
                            listOfRegistriesDevices = resSub.Devices.ToDictionary(x => x.Device.Code, x => x.Registries); // vengono associati per ogni devices, l'elenco dei registri
                            listOfSecurityCodes = resSub.Devices.Select(o => Const.AREAM2M_SUBSCRIBE_TO_DEVICES_SECURITY_CODE).ToArray(); // vengono creati gli n-security code
                        }
                    }
                    // -----------------------------------------------------------------------------------


                    int i = 0;
                    // viene richiesta la sottocrizione ai cambi di valore per ogni device
                    foreach (var deviceId in listOfDevices)
                    {
                        if (!_dicSecurityCode.ContainsKey(deviceId))
                            _dicSecurityCode.Add(deviceId, listOfSecurityCodes.ElementAt(i));

                        Logger.M2M.InfoFormat("AreaM2M Client subscribe to device '{0}'...", deviceId);
                        var resSub = await HubProxy.Invoke<ActionResult>("SubscribeToDeviceAsync", deviceId, listOfSecurityCodes.ElementAt(i), tokenId, true);

                        if (resSub.Success)
                        {
                            Logger.M2M.InfoFormat("AreaM2M Client subscribe to device '{0}'... completed", deviceId);
                            Logger.M2M.InfoFormat("AreaM2M Client subscribe to device '{0}', get schema... ", deviceId);
                            List<Registry> listOfRegistires;
                            if (!listOfRegistriesDevices.TryGetValue(deviceId, out listOfRegistires))
                            {
                                var ar = await HubProxy.Invoke<ActionResultGetSchema>("GetDeviceSchemaAsync", deviceId, listOfSecurityCodes.ElementAt(i), tokenId);
                                Logger.M2M.InfoFormat("AreaM2M Client subscribe to device '{0}', get schema completed ", deviceId);
                                if (ar.Success)
                                    listOfRegistires = ar.Registries;
                            }

                            Logger.M2M.InfoFormat("AreaM2M Client subscribe to device '{0}', in memory container... ", deviceId);
                            DataContainer.Instance.Create(listOfRegistires, deviceId);
                            Logger.M2M.InfoFormat("AreaM2M Client subscribe to device '{0}', in memory container created ", deviceId);

                            var memories = await GetDeviceThingsAsync(deviceId, tokenId, String.Empty);
                            if (memories.Success)
                            {
                                DataContainer.Instance.Fill(memories.Things, deviceId);
                                foreach (var item in memories.Things)
                                    Logger.M2M.InfoFormat("Device '{0}', change value '{1}' from '{2}' to '{3}'", memories.DeviceId, item?.Path, item.Info?.OldValue, item?.Value);
                            }
                        }
                        i++;
                    }

                    Logger.M2M.InfoFormat("AreaM2M Client started correctly");
                }
                else
                {
                    Logger.M2M.ErrorFormat($"AreaM2M Client login, user:'{0}', password:'{1}', with error:{res.Exception}", AreaM2M_UserName, AreaM2M_Password);
                    Logger.M2M.Info($"AreaM2M Client start Task.Delay for Init()...");
                    //Lorenzo R per gestire la caduta del token 22/02/22
                    await Task.Delay(10 * 1000).ContinueWith(async o =>
                    {
                        Logger.M2M.Info($"AreaM2M Client start Task.Delay for Init()... completed");
                        await Init();
                    });
                }

                Logger.M2M.Info("AreaM2M Client init... completed");
            }
            catch (Exception exc)
            {
                Logger.M2M.Error($"AreaM2M Client Init on error, cause:{exc.Message}...", exc);
            }

        }

        public async Task<bool> SetValueAsync(string deviceId, string path, object value)
        {
            Logger.M2M.InfoFormat("SetValueAsync(), deviceId:'{0}', path:'{1}', value:{2}...", deviceId, path, value);
            var res = await HubProxy.Invoke<ActionResult>("PublishThingsAsync", deviceId, _dicSecurityCode[deviceId], new string[] { path }, new object[] { value }, tokenId);
            Logger.M2M.InfoFormat("SetValueAsync(), deviceId:'{0}', path:'{1}', value:{2}, completed with result = {3}", deviceId, path, value, res.Success);
            return res.Success;
        }

        public async Task<ValuesChangedResult> GetDeviceThingsAsync(string deviceId, string tokenId, string groupId)
        {
            var ar = await HubProxy.Invoke<ValuesChangedResult>("GetDeviceThingsAsync", deviceId, _dicSecurityCode[deviceId], tokenId, groupId);
            return ar;
        }

        public async Task<bool> SetValueAsync(string deviceId, string[] path, object[] value)
        {
            Logger.M2M.InfoFormat("SetValueAsync(), deviceId:'{0}', multibag", deviceId);
            for (int i = 0; i < path.Length - 1; i++)
                Logger.M2M.InfoFormat("SetValueAsync(), deviceId:'{0}', path:'{1}', value:{2}...", deviceId, path[i], value[i]);

            var res = await HubProxy.Invoke<ActionResult>("PublishThingsAsync", deviceId, _dicSecurityCode[deviceId], path, value, tokenId);
            Logger.M2M.InfoFormat("SetValueAsync(), deviceId:'{0}', multibag completed with result = {1}", deviceId, res.Success);
            return res.Success;
        }
    }
    class DebugTextWriter : TextWriter
    {
        private StringBuilder buffer;

        public DebugTextWriter()
        {
            buffer = new StringBuilder();
        }

        public override void Write(char value)
        {
            switch (value)
            {
                case '\n':
                    return;
                case '\r':
                    Logger.M2M.Debug(buffer.ToString());
                    buffer.Clear();
                    return;
                default:
                    buffer.Append(value);
                    break;
            }
        }

        public override void Write(string value)
        {
            Logger.M2M.Debug(value);

        }
        #region implemented abstract members of TextWriter
        public override Encoding Encoding
        {
            get { throw new NotImplementedException(); }
        }
        #endregion
    }

}
