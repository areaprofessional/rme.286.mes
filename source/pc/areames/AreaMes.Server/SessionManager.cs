﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AreaMes.Model;

namespace AreaMes.Server
{
    internal class SessionManager
    {
        private Dictionary<string, SessionInfo> _sessions;
        private readonly object _lock = new object();


        public static  SessionManager Instance { get; set; }
        static SessionManager()
        {
            Instance = new SessionManager();
        }

        public SessionManager()
        {
            _sessions  =new Dictionary<string, SessionInfo>();
        }

        public bool Set(SessionInfo session)
        {
            lock (_lock)
            {
                SessionInfo ss = null;
                if (_sessions.TryGetValue(session.Id, out ss))
                {
                    ss = session;
                    return false;
                }

                _sessions.Add(session.Id, session);
                return true;
            }

        }

        public int Count()
        {
            lock (_lock)
            {
                return _sessions.Count;
            }
        }

        public bool Destroy(string sessionId)
        {
            lock (_lock)
            {
                SessionInfo ss = null;
                if (_sessions.TryGetValue(sessionId, out ss))
                    _sessions.Remove(sessionId);

                return true;
            }
        }

    }


    class SessionInfo
    {
        public string Id { get; set; }
        public DateTime StartedAt { get; set; }
        public string UserName { get; set; }
    }
}
