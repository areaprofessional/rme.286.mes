﻿var app = angular.module("appCfg", ['datatables', 'angular.chosen', 'ngCookies', 'treeControl'])




app.controller('cfgCtrl', ['$scope', '$rootScope', '$cookies', function ($scope, $rootScope, $cookies) {


    $scope.serverIP = "localhost";
    $scope.signalR = "9001";
    $scope.webApi = "9000";
    $scope.stationId = $.cookie("StationId");
    $scope.loginConRfid = $.cookie("loginConRfid");
    $scope.inactivityTimeout = 2147483647;
    
   
    $scope.confirm = function () {
        $.cookie("SigalRc", "http://" + $scope.serverIP + ":" + $scope.signalR, { expires: 70000  });
        $.cookie("WebApiC", "http://" + $scope.serverIP + ":" + $scope.webApi + "/api", { expires: 70000  });
        $.cookie("StationId", $scope.stationId, { expires: 70000  });
        $.cookie("LoginConRfid", $scope.loginConRfid, { expires: 70000  });
        $.cookie("InactivityTimeout", $scope.inactivityTimeout, { expires: 70000  });
        window.location.href = "login.html";
    }
}]);

