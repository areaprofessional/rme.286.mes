﻿var app = angular.module("rfidApp", ['datatables', 'angular.chosen', 'ngCookies', 'treeControl'])




app.controller('rfidController', ['$scope', '$rootScope', '$cookies', '$http', function ($scope, $rootScope, $cookies, $http) {

    urlApi = $.cookie("WebApiC");
    stationId = $.cookie("StationId");


    $scope.isLoaderActiveWrite = false;
    $scope.isLoaderActiveRead = false;
    $scope.isWriteOk = false;
    $scope.isReadOk = false;
    $scope.barcodeWrite = '';
    $scope.barcodeRead = '';
    $scope.resWrite = '';
    $scope.resRead = '';
    // -------------------------------------------------------------------------------------------------------
    // Fase di scrittura
    // -------------------------------------------------------------------------------------------------------
    $scope.clickSendBarcode = function () {

        $scope.isLoaderActiveRead = false;
        $scope.isLoaderActiveWrite = true;
        $scope.isWriteOk = false;

        // chiamata al server per la scrittura
        $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SendBarcode,
            { IdBatch: 'write-generic', IdFase: null, Barcode: $scope.barcodeWrite, Material: null, IsRfid: true, TimeoutRequest: 30, Username: null, StationId: stationId }
        ).success(function (res) {
            $scope.isLoaderActiveWrite = false;
            if (res.isCompleted) 
                $scope.isWriteOk = true;
            else
                $scope.resWrite = res.errorMessage;
            
        }).error(function (e) {
            console.log(e);
            $scope.isLoaderActiveWrite = false;
        });
    };


    // -------------------------------------------------------------------------------------------------------
    // Fase di lettura
    // -------------------------------------------------------------------------------------------------------
    $scope.clickReadBarcode = function () {

        $scope.isLoaderActiveRead = true;
        $scope.isLoaderActiveWrite = false;
        $scope.barcodeRead = '';

        // chiamata al server per la scrittura
        $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_ReadBarcode,
            { IdBatch: 'read-generic', IdFase: null, IsRfid: true, TimeoutRequest: 30, Username: null, StationId: stationId }
        ).success(function (res) {
            $scope.isLoaderActiveRead = false;

            if (res.isCompleted)
                $scope.barcodeRead = res.barcode;
            else
                $scope.resRead = res.errorMessage;

        }).error(function (e) {
                console.log(e);
                $scope.isLoaderActiveRead = false;
        });;
    }

}]);

