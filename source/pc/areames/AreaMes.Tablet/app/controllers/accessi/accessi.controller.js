﻿

// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------

app.controller('AccessiCtrl', ['$scope', '$http', 'DataFactory', '$rootScope', '$location', '$cookies',
function ($scope, $http, DataFactory, $rootScope, $location, $cookies) {

    if (!$.cookie("SigalRc"))
        window.location.href = "config.html";   // SignalR IP Connection SetUp

    urlApi = $.cookie("WebApiC");
    stationId = $.cookie("StationId");
    $scope.loginConRfid = $.cookie("LoginConRfid");
    $scope.StationId = $.cookie("StationId");

    $scope.isLoaderActiveRead = false;
    $scope.IsFullScreenOnEdit = false;
    $scope.items = [];
    $scope.currentItem = null;

    $scope.dtInstance = {};

    $scope.logOut = function () { DataFactory.ClearCredentials(); }
    
    if ($rootScope.globals != null) {
        $scope.LoggedUser = $rootScope.globals.currentUser.username;
        $rootScope.inactivityTime = $rootScope.globals.currentUser.extra;
    }
    else
        $rootScope.inactivityTime = "300000";
    
    $scope.login = function () {
        $scope.dataLoading = true;
        $scope.password = ($scope.password == null ? "" : $scope.password);
        DataFactory.Login($scope.username, $scope.password).then(function (response) {
            if (response.data != null) {
                var timeout = "30";
                DataFactory.SetCredentials(response.data.id, $scope.username, $scope.password, response.data.nome, response.data.cognome, response.data.lingua, response.data.azienda, timeout*60000);
            } else {
                $scope.error = "Username o password errate";
                $scope.dataLoading = false;
            }
        });
    };



    //---------------------------------------------------------------------------------
        setup = function () {
            this.addEventListener("mousemove", resetTimer, false);
            this.addEventListener("mousedown", resetTimer, false);
            this.addEventListener("keypress", resetTimer, false);
            this.addEventListener("DOMMouseScroll", resetTimer, false);
            this.addEventListener("mousewheel", resetTimer, false);
            this.addEventListener("touchmove", resetTimer, false);
            this.addEventListener("MSPointerMove", resetTimer, false);
            startTimer();
        }
        setup();

        function startTimer() {
            timeoutID = window.setTimeout(goInactive, $rootScope.inactivityTime);
        }

        function resetTimer(e) {
            window.clearTimeout(timeoutID);
            goActive();
        }

        function goInactive() {
            $scope.logOut();
            //window.location.href="login.html";
        }

        function goActive() {
            startTimer();
        }
    //-----------------------------------------------------------------------------------------------



    $scope.tryToLoginWithRfid = function () {
       
        setTimeout(function () {
            $scope.tryToLoginWithRfid();
        }, 3000);

        if ($scope.isLoaderActiveRead) return;
        $scope.clickReadBarcode();
    }

    // -------------------------------------------------------------------------------------------------------
    // Fase di lettura
    // -------------------------------------------------------------------------------------------------------
    $scope.clickReadBarcode = function () {

        $scope.isLoaderActiveRead = true;
        $scope.barcodeRead = '';

        // chiamata al server per la scrittura
        $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_ReadBarcode,
            { IdBatch: 'read-generic', IdFase: null, IsRfid: true, TimeoutRequest: 30, Username: null, StationId: stationId }
        ).success(function (res) {
            $scope.isLoaderActiveRead = false;

            if (res.isCompleted) {
                $scope.password = null;
                $scope.username = res.barcode;
                $scope.login();
            }
            else
                $scope.error = res.errorMessage;

        }).error(function (e) {
            console.log(e);
            $scope.isLoaderActiveRead = false;
        });;
    };


    if ($scope.loginConRfid)
        $scope.tryToLoginWithRfid();
}]);
