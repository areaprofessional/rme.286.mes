﻿
app.controller('AllSfoCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$location', '$rootScope',
function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $location, $rootScope) {

    $rootScope.signalRconnection = $.cookie("SigalRc");
    urlApi = $.cookie("WebApiC");

    $scope.viewState = Enums.ViewState['R'];
    $scope.title = function () { basicCrud_doTitle($scope, Enums); };
    $scope.filterIdStato = "";
    $scope.filterButtonBackGroundColor = "";
    $scope.lUser = $rootScope.globals.currentUser.username;
    basicCrud_doInitWatch($scope);
    $scope.items = [];
    $scope.OdpString = [];
    $scope.itemsOld = [];
    $scope.idMacchina = null;
    $scope.countTutti = 0;
    $scope.countCoda = 0;
    $scope.countProduzione = 0;
    $scope.countPausa = 0;
    $scope.countEmergenza = 0;
    $scope.filterForOdpVariable=0;
    $scope.filterOdp = "-1";
    $scope.Macchine = [];
    $scope.macchineFilter = {value: "-1"};
    $scope.filterIdStato = "-1";
    $scope.idMachine = "-1";


    $scope.options = "";
    //$scope.filterState = function (idStatoToString, loggedUser, idmacchina ) {

    //    if (idmacchina != null) $scope.idMachine = idmacchina;

    //    $scope.filterIdStato = idStatoToString;

    //    DataFactory.GetSfo(-1, loggedUser,"").then(function (response) {
    //        $scope.countTutti = response.data.length;

    //        var appCoda = response.data.filter(x => x.stato === "1");
    //        $scope.countCoda = appCoda.length;

    //        var appProduzione = response.data.filter(x => x.stato === "2");
    //        $scope.countProduzione = appProduzione.length;

    //        var appPausa = response.data.filter(x => x.stato === "3");
    //        $scope.countPausa = appPausa.length;

    //        var appEmergenza = response.data.filter(x => x.stato === "inLavorazioneInEmergenza");
    //        $scope.countEmergenza = appEmergenza.length;

    //        // vengono filtrati solo gli ordini per lo stato selezionato
    //        if(idStatoToString != "")
    //            response.data = response.data.filter(x => x.stato === idStatoToString);

    //        //angular.forEach(response.data, function (item, key) {
    //        //    angular.forEach(item.fasi, function (fase, key) {
    //        //        var machine = fase.macchine.find(z => z.id == $scope.idMachine)
    //        //        if (machine == null) {
    //        //            var pos = response.data.indexOf(item);
    //        //            if (pos != (-1))
    //        //                response.data.splice(pos, 1);
    //        //        }
    //        //});
    //        //});

    //        // se l'oggetto è presente nell'array locale, 
    //         //ma non in quello ricevuto dal server, allora viene cancellato
    //        angular.forEach($scope.items, function (value, key) {
    //            var itemFounded = response.data.find(o => o.id == value.id);
    //            if (!itemFounded)
    //                $scope.items.splice(list.indexOf(itemFounded), 1);
    //            if (itemFounded == null) {
    //                $scope.items = [];
    //            }
    //        });

    //        //filtro anche per odp selezionato
    //        if ($scope.filterOdp != "-1")
    //            response.data = response.data.filter(o => o.odP == $scope.filterOdp);

    //        // vengono ciclati gli elementi prelevati dal server
    //        angular.forEach(response.data, function (value, key) {
    //            var itemFounded = $scope.items.find(o => o.id == value.id);
    //            if (!itemFounded) {

    //              /*  if ($scope.idMachine == '-1' || $scope.idMachine == null)*/
    //                    $scope.items.push(value);
    //                //else {
    //                //    angular.forEach(value.fasi, function (item, key) {
    //                //        var machine = item.macchine.find(z => z.id == $scope.idMachine)
    //                //        if (machine != null)
    //                //            $scope.items.push(value);
    //                //    });

    //                /*}*/
    //            }
    //            else {
    //                // se l'elemento è presente vengono aggiornate le sue proprietà
    //                itemFounded.fasi = value.fasi;
    //                itemFounded.stato = value.stato;
    //                itemFounded.pezziProdotti = value.pezziProdotti;
    //                itemFounded.oee = value.oee;
    //                itemFounded.userLockedDate = value.userLockedDate;
    //                itemFounded.inizio = value.inizio;
    //                itemFounded.percCompletamento = value.percCompletamento;
    //            }
    //        });
    //    });
    //}


    //$scope.filterState = function (idStato, loggedUser, idmacchinaa) {
        
    //    if (idStato == null) idStato = "-1"; 
    //    //$rootScope.idStatoPag = idStato;
    //    if (idmacchinaa != null) $scope.idMachine = idmacchinaa;
    //    //else $scope.idMachine = -1;
    
    //    $scope.idMacchina = $location.search().idMacchina;
    //    $scope.filterIdStato = idStato;

    //    //DataFactory.GetSfoOdp(idStato, loggedUser, "").then(function (response) {
    //    //    $scope.OdpString = [];
    //    //    $scope.OdpString=response.data;
    //    //});

    //    DataFactory.GetSfo(idStato, loggedUser,"").then(function (response) {
    //        // se l'oggetto è presente nell'array locale, 
    //        // ma non in quello ricevuto dal server, allora viene cancellato
    //        angular.forEach($scope.items, function (value, key) {
    //            var itemFounded = response.data.find(o => o.id === value.id);
    //            //if (!itemFounded)
    //            //    $scope.items.splice(list.indexOf(itemFounded), 1);
    //            if (itemFounded == null) {
    //                $scope.items = [];
    //                $scope.OdpString = [];

    //            }
    //        });

    //         //filtro anche per odp selezionato
    //        if ($scope.filterOdp != "-1")
    //            response.data = response.data.filter(o => o.odP == $scope.filterOdp);

    //        // vengono ciclati gli elementi prelevati dal server
    //        angular.forEach(response.data, function (value, key) {
    //            var itemFounded = $scope.items.find(o => o.id === value.id);

    //            angular.forEach(value.distintaBase.fasi, function (value1, key1) {
    //                if (value1.numeroFase === "1") $scope.FaseSet = value1.stato;
    //                else $scope.Faselav = value1.stato;
    //            });

    //            if (!itemFounded) {

    //                // se l'elemento non è presente viene inserito
    //                if (idMacchina == '' || idMacchina == '-1' || idMacchina === undefined || idMacchina === null) {
    //                    $scope.items.push(value);
    //                    $scope.OdpString.push({ Text: value.odP, Value: value.odP });


    //                }
                        
    //                else {
    //                    angular.forEach(value.fasi, function (item, key) {
    //                        var machine = item.macchine.find(z => z.macchina.id === $scope.idMachine);
    //                        //var machine = item.macchine.find(z => z.id == $scope.idMachine)
    //                        if (machine != null) {
    //                            $scope.items.push(value);

    //                            $scope.OdpString.push({ Text: value.odP, Value: value.odP });

    //                        }
    //                    });
    //                }

    //            } else {
    //                var machineid = null;
    //                var stringId = "";
    //                angular.forEach(value.fasi, function (fase, key) {
    //                    machineid = fase.macchine.find(z => z.macchina.id === $scope.idMachine);

    //                    if (machineid!=null)
    //                        stringId=machineid.macchina.id;
    //                });


    //                if ($scope.idMachine == '-1' || $scope.idMachine === null || stringId === $scope.idMachine) {
    //                        // se l'elemento è presente vengono aggiornate le sue proprietà
    //                        itemFounded.fasi = value.fasi;
    //                        itemFounded.stato = value.stato;
    //                        itemFounded.pezziProdotti = value.pezziProdotti;
    //                        itemFounded.oee = value.oee;
    //                        itemFounded.userLockedDate = value.userLockedDate;
    //                        itemFounded.inizio = value.inizio;
    //                        itemFounded.percCompletamento = value.percCompletamento;
    //                        itemFounded.FaseLav = $scope.Faselav;
    //                        itemFounded.FaseSet = $scope.FaseSet;
    //                }
    //                else $scope.arrayEliminare.push(value);
    //            }
    //        });

    //        //ciclo per eliminare quelli che non mi servono
    //        angular.forEach($scope.arrayEliminare, function (value, key) {

    //            angular.forEach($scope.items, function (value1, key1) {

    //                if (value.id == $scope.items[key1].id)
    //                    $scope.items.splice(key1, 1);

    //            });
    //        });
    //        $scope.arrayEliminare = [];

    //        $scope.items = $scope.items.sort((a, b) => (a.odP > b.odP ? 1 : -1));
    //        $scope.OdpString = $scope.OdpString.sort((a, b) => (a.key > b.key ? 1 : -1));


    //        var appCoda = $scope.items.filter(x => x.stato === "inAttesa");
    //        $scope.countCoda = appCoda.length;

    //        var appProduzione = $scope.items.filter(x => x.stato === "inLavorazione");
    //        $scope.countProduzione = appProduzione.length;

    //        var appPausa = $scope.items.filter(x => x.stato === "inLavorazioneInPausa");
    //        $scope.countPausa = appPausa.length;

    //        var appEmergenza = $scope.items.filter(x => x.stato === "inLavorazioneInEmergenza");
    //        $scope.countEmergenza = appEmergenza.length;

    //        $scope.countTutti = $scope.items.length;

    //        /////////////////////////////////////////////////////////////

    //        $.each($scope.OdpString, function (text, key) {
    //            var option = new Option(key.Text, key.Text);
    //            $('#odpString').append($(option));
    //        });


    //        $('#odpString').trigger("chosen:updated");
    //    });

      
      
    //}
    $scope.filterState = function (idStato, loggedUser, idMacchina) {
        if (idStato == null) idStato = "-1";
        //$rootScope.idStatoPag = idStato;
        //if ($scope.Macchine.id != null) $scope.idMachine = $scope.Macchine.id;
        //else $scope.idMachine = -1;

        //$scope.idMacchina = $location.search().idMacchina;
        //$scope.items = [];

        $scope.filterIdStato = idStato;
        DataFactory.GetSfo(idStato, loggedUser, "").then(function (response) {
            // se l'oggetto è presente nell'array locale, 
            // ma non in quello ricevuto dal server, allora viene cancellato
            angular.forEach($scope.items, function (value, key) {
                var itemFounded = response.data.find(o => o.id === value.id);
                //if (!itemFounded)
                //    $scope.items.splice(list.indexOf(itemFounded), 1);
                if (itemFounded == null) {
                    $scope.items = [];
                    $scope.OdpString = [];
                }
            });

            //filtro anche per odp selezionato
            if ($scope.filterOdp != "-1")
                response.data = response.data.filter(o => o.odP == $scope.filterOdp);

            // vengono ciclati gli elementi prelevati dal server
            angular.forEach(response.data, function (value, key) {
                var itemFounded = $scope.items.find(o => o.id === value.id);
                angular.forEach(value.distintaBase.fasi, function (value1, key1) {
                    if (value1.numeroFase === "1") $scope.FaseSet = value1.stato;
                    else $scope.Faselav = value1.stato;
                });

                if (!itemFounded) {

                    // se l'elemento non è presente viene inserito
                    if (idMacchina == '' || idMacchina == '-1' || idMacchina === undefined || idMacchina === null) {
                        $scope.items.push(value);
                        $scope.OdpString.push({ Text: value.odP, Value: value.odP });
                    }
                        
                    else {
                        angular.forEach(value.fasi, function (fase, key) {
                            var alreadyInsert = $scope.items.find(x => x.odP == value.odP);
                            if (alreadyInsert == null) {
                                var listMacchine = fase.macchine.find(z => z.macchina?.id === idMacchina);
                                //var machine = item.macchine.find(z => z.id == $scope.idMachine)
                                if (listMacchine)
                                    $scope.items.push(value);
                                   $scope.OdpString.push({ Text: value.odP, Value: value.odP });
                            }
                        });
                    }

                }
                else {
                    var machineid = null;
                    var stringId = "";
                    angular.forEach(value.fasi, function (fase, key) {
                        machineid = fase.macchine.find(z => z.macchina?.id === idMacchina);
                        if (machineid)
                            stringId = machineid.macchina.id;
                    });


                    if (idMacchina == '' || idMacchina == '-1' || idMacchina === undefined || idMacchina === null || stringId === idMacchina) {
                        // se l'elemento è presente vengono aggiornate le sue proprietà
                        itemFounded.fasi = value.fasi;
                        itemFounded.stato = value.stato;
                        itemFounded.pezziProdotti = value.pezziProdotti;
                        itemFounded.oee = value.oee;
                        itemFounded.userLockedDate = value.userLockedDate;
                        itemFounded.inizio = value.inizio;
                        itemFounded.percCompletamento = value.percCompletamento;
                        itemFounded.FaseLav = $scope.Faselav;
                        itemFounded.FaseSet = $scope.FaseSet;
                    }
                    else $scope.arrayEliminare.push(value);
                }
            });

            //ciclo per eliminare quelli che non mi servono
            angular.forEach($scope.arrayEliminare, function (value, key) {

                angular.forEach($scope.items, function (value1, key1) {

                    if (value.id == $scope.items[key1].id) {
                        $scope.items.splice(key1, 1);
                        $scope.OdpString.splice(key1, 1);
                    }
                       

                });
            });
            $scope.arrayEliminare = [];

            $scope.items = $scope.items.sort((a, b) => (a.odP > b.odP ? 1 : -1));
            $scope.OdpString = $scope.OdpString.sort((a, b) => (a.odP > b.odP ? 1 : -1));


            var appCoda = $scope.items.filter(x => x.stato === "inAttesa");
            $scope.countCoda = appCoda.length;

            var appProduzione = $scope.items.filter(x => x.stato === "inLavorazione");
            $scope.countProduzione = appProduzione.length;

            var appPausa = $scope.items.filter(x => x.stato === "inLavorazioneInPausa");
            $scope.countPausa = appPausa.length;

            var appEmergenza = $scope.items.filter(x => x.stato === "inLavorazioneInEmergenza");
            $scope.countEmergenza = appEmergenza.length;

            $scope.countTutti = $scope.items.length;

         /////////////////////////////////////////////////////////////
            
            $.each($scope.OdpString, function (text, key) {
                var optionExists = $('#odpString option[value="' + key.Text + '"]').length > 0;
                if (!optionExists) {
                    var option = new Option(key.Text, key.Text);
                    $('#odpString').append($(option));
                }
            });


            $('#odpString').trigger("chosen:updated");
        });
    }
    ///////////////////////////////////////////////////////////
    //Funzione per filtrare per odp
    //////////////////////////////////////////////////////////

    $scope.filterForOdp = function (value) {
        $scope.filterState('-1', $scope.lUser);
        $scope.filterOdp = value;
        if (value != "-1") {
           
            $scope.items = $scope.items.filter(o => o.odP == value);
        }
    }

    DataFactory.getMacchine().then(function (response) {
        $scope.Macchine = response.data;
    });

    $scope.getBatchTime = function (idBatch, tType) {
        var t = 0;
        angular.forEach($scope.items, function(item, key){
            if( item.id==idBatch){            
            //Calcolo tempi teorici distinta base
            angular.forEach(item.distintaBase.fasi, function (fase, key) {
                t += fase.manodopera[0].tempo;
                t += fase.macchine[0].tempo;
            });
        }
        });
        var date = new Date(null);
        date.setSeconds(t);
        return date.toISOString().substr(11, 8);
    }

    // timer ogni 10 secondi per ricaricare i batch
    var timer = setInterval(function () {
        $scope.filterState($scope.filterIdStato, $scope.lUser, $scope.macchineFilter.value);
    }, 30000);
    // attendiamo 2 secondi e fa la chiamata
    setTimeout(function () {
        $scope.filterState($scope.filterIdStato, $scope.lUser);
    }, 2 * 100);
}]);
