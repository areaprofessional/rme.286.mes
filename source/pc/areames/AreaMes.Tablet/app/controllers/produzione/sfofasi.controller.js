﻿/*/*angular.module('SfoFasiCtrl', ['numericKeyboard']);*/
app.controller('SfoFasiCtrl',
    ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$location', '$rootScope', '$timeout',
        function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $location, $rootScope, $timeout, growl) {

            var connection;
            var hub;
            var timerFocus;
            var barcode = "";
            var boolean2 = false;
         //   var ObjectID = require('mongodb').ObjectID;
            $rootScope.signalRconnection = $.cookie("SigalRc");
            urlApi = $.cookie("WebApiC");
            stationId = $.cookie("StationId");
            $scope.nclick = 1;

            $scope.sidebar2Show = false;
            $scope.sidebarIsCollapsed = false;
            $scope.tempiEffettivoDistintaBaseFase = "00:00:00";

            $scope.clickBatchFaseMateriale = function (arg) {
                var ck = arg;   // per futuri utilizzi //
            }
            //gestione scarti 
            $scope.arrayFermiChange = [];
            $scope.arrayPzScaChange = [];
            $scope.addPzSca = [];
            $scope.pzScaNotUnjustified = 0;
            $scope.fermiNotUnjustified = 0;
            $scope.materialiPerFase = [];
            $scope.multimediaPerFase = [];
            $scope.selectedPhase = null;
            $scope.selectedBatchPhase = null;

            $scope.btnStateConcludi = {};
            $scope.btnStateStart = {};
            $scope.btnStatePause = {};
            $scope.btnStateAbort = {};

            $scope.stateStarted = {};
            $scope.stateFinished = {};
            $scope.stateAborted = {};

            $scope.userLocked = {};

            $scope.stateBatchStart = {};
            $scope.stateBatchPausaS = {};
            $scope.stateBatchEmergenza = {};
            $scope.stateBatchStopProduzione = {};
            $scope.stateBatchInEmergenza = {};

            //utilizzata per il tempo del batch in emergenza
            $scope.timeAllarme = { day: 00, hh: 00, mm: 00, ss: 00 };

            $scope.caricoPezziVersati = 0;
            $scope.isLoaderActiveWrite = false;
            $scope.isLoaderActiveRead = false;

            $scope.isVisibleButtonStartStop = true;
            $scope.isVisibleButtonEmergency = true;
            $scope.isVisibleProgressionPerc = false;
            $scope.modalWarningMessage = '';
            $scope.dataRiavvio;
            $scope.pezziParziali = 0;

            $scope.preliovocomponenti = { materiale: null, quantita: 0, quantitaScarto: 0, causale: '', materialeConsigliato:'',isChanged:false, isEnable:false};
            $scope.numpadMinus = function () {
                $scope.numpadClose('-1');
            }
            $scope.numpadPlus = function () {
                $scope.numpadClose('1');
            }

            //$scope.rawMaterial = {};
            $scope.numpadMinusPart = function () {
                $scope.selectedBatchPhase.pezziParziali -= 1;
            }
            $scope.numpadPlusPart = function () {
                $scope.selectedBatchPhase.pezziParziali += 1;
            }



            // funzione per popolare la combo dei tipi valori 
            DataFactory.GetTipoValori().then(function (response) {
               // $scope.GetTipoValori = jQuery.grep(response.data, function (a) { return a.tipo == typeValue_Prod_Cau_Scarto; });
                $scope.GetTipoValori = response.data;
            });

            DataFactory.getUnitaDiMisura().then(function (response) {
                $scope.GetUnitaDiMisura = response.data;
            });

            DataFactory.GetOperatori().then(function (response) {
                $scope.GetOperatore = response.data.find(a=>a.id == $scope.$parent.globals.currentUser.idUser);
            });


            DataFactory.GetMateriePrime("MAT").then(function (response) {
                $scope.rawMaterial = response.data;
            });
            $scope.numpadClose = function (param) {
                var data = {
                    idBatch: $scope.batch.id,
                    pezziVersati: param,
                    numeroFase: $scope.selectedBatchPhase.numeroFase,
                    Username: $rootScope.globals.currentUser.username
                }
                $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_AddPiecesToBatch, data).success(function (data) {
                })
                    .error(function (e) {
                        console.log(e);
                        $scope.onError(e);
                    });
            }

            connection = $.hubConnection($rootScope.signalRconnection);
            connection.logging = false;

            hub = connection.createHubProxy('AreaMESManager');
            hub.logging = false;

            hub.on('OnFaseChanged', function (batchId, message) {
                if (batchId == $scope.currentItem.id) {     //ignora messaggi per altri batch
                    var inizio = message.Current.Inizio;
                    var numFase = message.Current.NumeroFase;
                    var stato = message.Current.Stato;
                    var statoDecoded = message.Current.Nota;

                    $scope.selectedPhase = null;
                    //if ($scope.selectedPhase.numeroFase != numFase)
                    angular.forEach($scope.currentItem.distintaBase.fasi, function (fase, key) {
                        if (fase.numeroFase == numFase)
                            $scope.selectedPhase = fase;

                        //$scope.selectedPhase = fase;
                    })
                    $scope.loadFasiAlt();
                }
            });

            // registrazione delle variazioni del batch
            hub.on('OnBatchChanged', function (batchId, message) {
                if (batchId == $scope.currentItem.id) {     //ignora messaggi per altri batch
                    $scope.refreshBatch();
                  
                }
            });

            // registrazione delle variazioni sui materiali
            hub.on('OnBatchMaterialeChanged', function (batchId, response) {
                if (batchId == $scope.currentItem.id) {     //ignora messaggi per altri batch

                    if ($scope.currentItem.batchMateriali == null) return;
                    angular.forEach(response, function (value, key) {
                        var itemFounded = $scope.currentItem.batchMateriali.find(o => (o.barcode == value.barcode) && (o.numeroFase == value.numeroFase));
                        if (!itemFounded)
                            $scope.currentItem.batchMateriali.push(value);
                        else
                            itemFounded = value; 
                    });

                    $scope.materialiPerFase = $scope.getMaterialiPerFase();
                    $scope.loadFasiAlt();
                }
            });

            //NotifyVersamentoFaseChanged -> OnVersamentoFaseChanged
            hub.on('OnVersamentoFaseChanged', function (batchId, message) {
                if (batchId == $scope.currentItem.id) {     //ignora messaggi per altri batch
                    $scope.loadFasiAlt();
                }
            });

            // Viene intercettato l'evento di cambio stato in SignalR
            $(connection).bind("onStateChanged", function (e, data) {
                if (data.newState === $.signalR.connectionState.connected) {
                    doSubscribe($location.search().idBatch);
                }
                else if (data.newState === $.signalR.connectionState.disconnected) {
                    setTimeout(function () { connection.start(); }, 5 * 1000); // Restart connection after 5 seconds.
                }
            });

            connection.start();

            // ----------------------------------------------------------------------
            $scope.replaceDate = function (stringApp) {
                $scope.today = new Date();
                var dd = String($scope.today.getDate()).padStart(2, '0');
                var mm = String($scope.today.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = $scope.today.getFullYear();
                $scope.today = dd + '/' + mm + '/' + yyyy;
                return stringApp.replace($scope.today, '');
            };

            $scope.checkIfIncompleteBatch = function () {
                var isIncomplete = false;
                angular.forEach($scope.filteredFasi, function (fase, key) {
                    if (fase.stato != 'terminato') {
                        isIncomplete = true;;
                    }
                })
                return isIncomplete;
            }


            $scope.getMaterialiPerFase = function () {
                var ret = [];
                var nFase = $scope.selectedBatchPhase.numeroFase;
                angular.forEach($scope.currentItem.batchMateriali, function (mat, key) {
                    if ((mat.numeroFase == null) || ((mat.numeroFase != null && mat.numeroFase === nFase))) {
                        mat.isOnAction = false;
                        ret.push(mat);
                    }

                })
                return ret.reverse();
            }

            $scope.getMultimediaPerFase = function () {
                var ret = [];
                var nFase = $scope.selectedBatchPhase.numeroFase;
                angular.forEach($scope.currentItem.batchMultimedia, function (mat, key) {
                    if (mat.numeroFase != null && mat.numeroFase == nFase)
                        ret.push(mat);
                })
                angular.forEach($scope.currentItem.batchMultimedia, function (mat, key) {
                    if (mat.numeroFase == null)
                        ret.push(mat);
                })
                return ret;
            }

            //
            $scope.updateProgressBar = function (val) { val = $scope.currentItem.percCompletamento; }

            //
            $scope.loadFasiAlt = function () {
                $scope.items = [];
                $scope.idMacchina = $location.search().idBatch;

                DataFactory.mGetBatchFromId($location.search().idBatch, $rootScope.globals.currentUser.username).then(function (response) {

                    DataFactory.GetFasiFromBatch($location.search().idBatch, $rootScope.globals.currentUser.username).then(function (responseSub) {
                        $scope.filteredFasi = responseSub.data;

                        $scope.batch = response.data;
                        $scope.currentItem = $scope.batch;
                        // abilitazione pulsanti per il batch

                        var st = $scope.batch.stato;
                        $scope.stateBatchStart = (st == "inLavorazioneInIdle" || st == "inAttesa" || st == "inLavorazioneInEmergenza" || st == "inLavorazioneInPausaS");
                        $scope.stateBatchPausaS = (st == "inLavorazione");
                        $scope.stateBatchEmergenza = (st == "inLavorazione" || st == "inLavorazioneInIdle");
                        $scope.stateBatchStopProduzione = (st == "inLavorazione" || st == "inLavorazioneInEmergenza");
                        $scope.stateBatchInEmergenza = ($scope.batch.stato == "inLavorazioneInEmergenza");

                        createOrRefreshTreeView();
                        $scope.updateProgressBar(($scope.currentItem.pezziProdotti / $scope.currentItem.pezzidaProdurre * 100).toFixed(2));

                         //controllo se il batch è in emergenza avvio il timer 
                        if ($scope.batch.stato == "inLavorazioneInEmergenza") {
                            setTimeout(function () { $scope.onTickAlarmCalculateTime(); }, 500);
                            interval = setInterval(function () { $scope.onTickAlarmCalculateTime(); }, 10000);
                        }
                        //se il timer è avviato ma il batch non più in emergenza lo disabilito
                        else if (typeof interval !== "undefined") {
                            $scope.timeAllarme.day = 00;
                            $scope.timeAllarme.hh = 00;
                            $scope.timeAllarme.mm = 00;
                            $scope.timeAllarme.ss = 00;
                            clearInterval(interval);
                        }
                           

                        // -------------------------------------------------------------------------------------------------
                        // viene selezionata la prima fase in lavorazione, oppure, la prima in elenco
                        try {
                            var sel = null;
                            angular.forEach($scope.currentItem.distintaBase.fasi, function (bfase, key) {
                                if (sel == null) {
                                    if ($scope.selectedPhase != null) {
                                        if ($scope.selectedPhase.numeroFase == bfase.numeroFase)
                                            sel = bfase;
                                    } else if (bfase.stato == 'inLavorazione')
                                        sel = bfase;
                                }
                            });
                            if (sel == null) sel = $scope.currentItem.distintaBase.fasi[0];

                            $scope.showSelected(sel);

                            contsize();
                        }
                        catch (e) { }

                        $scope.controlPiecesUnjustified();
                        $scope.controlBlocksUnjustified();
                        // -------------------------------------------------------------------------------------------------
                    })
                });
            }



            //utilizzata per calcolare il tempo trascorso dalla data1 ( fermo macchina)  alla data2 (data now)
            $scope.onTickAlarmCalculateTime = function () {
                //inizializzo le variabili 
                $scope.$apply(function () {
                $scope.timeAllarme.day = 00;
                $scope.timeAllarme.hh = 00;
                $scope.timeAllarme.mm = 00;
                $scope.timeAllarme.ss = 00;

               //calcolo le variabili
                var lenght = $scope.currentItem.fermi.length;
                var dateNow = new Date();
                var dateStop = new Date($scope.currentItem.fermi[lenght - 1].inizio);
                var seconds = Math.floor((dateNow - (dateStop)) / 1000);
                var minutes = Math.floor(seconds / 60);
                var hours = Math.floor(minutes / 60);
                var days = Math.floor(hours / 24);

                hours = hours - (days * 24);
                minutes = minutes - (days * 24 * 60) - (hours * 60);
                seconds = seconds - (days * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);

                    //aggiorno le variabili
                 $scope.timeAllarme.day = days;
                 $scope.timeAllarme.hh = hours;
                 $scope.timeAllarme.mm = minutes;
                 $scope.timeAllarme.ss = seconds;
                
                });

            };


            //
            $scope.refreshBatch = function () {
                DataFactory.mGetBatchFromId($location.search().idBatch, $rootScope.globals.currentUser.username).then(function (response) {
                    $scope.batch = response.data;
                    $scope.currentItem = $scope.batch;
                    // abilitazione pulsanti per il batch
                    var st = $scope.batch.stato;
                    $scope.stateBatchStart = (st == "inLavorazioneInIdle" || st == "inAttesa" || st == "inLavorazioneInEmergenza" || st == "inLavorazioneInPausaS");
                    $scope.stateBatchPausaS = (st == "inLavorazione");
                    $scope.stateBatchEmergenza = (st == "inLavorazione" ||st == "inLavorazioneInIdle");
                    $scope.stateBatchStopProduzione = (st == "inLavorazione" || st == "inLavorazioneInEmergenza");
                    $scope.stateBatchInEmergenza = ($scope.batch.stato == "inLavorazioneInEmergenza");

                    $scope.updateProgressBar(($scope.currentItem.pezziProdotti / $scope.currentItem.pezzidaProdurre * 100).toFixed(2));

                    var fase = $scope.selectedBatchPhase; // refresh selectedPhase buttons
                    $scope.btnStateConcludi = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "terminato") && ($scope.batch.stato != "inLavorazioneInEmergenza") && (fase.stato != "inAttesa");
                    $scope.btnStateStart = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "inLavorazione") && ($scope.batch.stato != "inLavorazioneInEmergenza");
                    $scope.btnStatePause = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "inLavorazioneInPausa") && (fase.stato != "inAttesa") && (fase.stato != "abortito") && (fase.stato != "terminato") && ($scope.batch.stato != "inLavorazioneInEmergenza");
                    $scope.btnStateAbort = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "abortito");

                    //controllo se il batch è in emergenza avvio il timer 
                    if ($scope.batch.stato == "inLavorazioneInEmergenza") {
                        setTimeout(function () { $scope.onTickAlarmCalculateTime(); }, 500);
                        interval = setInterval(function () { $scope.onTickAlarmCalculateTime(); }, 10000);
                    }
                    //se il timer è attivo ma il batch non è più nello stato di emergenza disabilito il timer 
                    else if (typeof interval !== "undefined") {
                        $scope.timeAllarme.day = 00;
                        $scope.timeAllarme.hh = 00;
                        $scope.timeAllarme.mm = 00;
                        $scope.timeAllarme.ss = 00;
                        clearInterval(interval);
                    }

                    //ricalcola i pezzi da giustificare
                    $scope.controlPiecesUnjustified();
                    $scope.controlBlocksUnjustified();
                });
            }

            $scope.SuspendBatch = function () {
                var data = {
                    batchId: $scope.batch.id,
                    operatore: $scope.GetOperatore
                };
                $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SuspendBatch, data).success(function (data) {
                    window.location.href = '/allsfo.html';
                }).error(function (e) {
                    console.log(e);
                    $scope.onError(e);
                });
            }

            //funzione per abilitare e disabilitare i botton con le checkbox
            $scope.ChangeButtonModification = function (Item) {
                var a = document.getElementById(Item.id);
                if (a.checked === true) {
                    var b = document.getElementById('B' + Item.id);
                    angular.element(b.disabled = false);
                    $scope.arrayPzScaChange.push(Item);
                }

                if (a.checked === false) {
                    var b = document.getElementById('B' + Item.id);
                    angular.element(b.disabled = true);
                    var index = $scope.arrayPzScaChange.indexOf(Item);
                    if (index > -1) {
                        $scope.arrayPzScaChange.splice(index, 1);
                    }
                }
            }

            $scope.ChangeButtonModificationFermi = function (Item) {
                var a = document.getElementById(Item.id);
                if (a.checked === true) {
                    var b = document.getElementById('B' + Item.id);
                    angular.element(b.disabled = false);
                    $scope.arrayFermiChange.push(Item);
                }

                if (a.checked === false) {
                    var b = document.getElementById('B' + Item.id);
                    angular.element(b.disabled = true);
                    var index = $scope.arrayFermiChange.indexOf(Item);
                    if (index > -1) {
                        $scope.arrayFermiChange.splice(index, 1);
                    }
                }
            }

            //funzione per abilitare e disabilitare i botton con le checkbox
            $scope.changeCasualPieces = function () {
                //viene inizializzato un arrey di eventi 
                var eventi = [];

                //ciclo tutti gli eventi per settare la stella causale
                if ($scope.arrayPzScaChange.length>0) {
                    var id = $scope.arrayPzScaChange[0].tipoValore.id;
                    angular.forEach($scope.arrayPzScaChange, function (item, key) {
                        $scope.arrayPzScaChange[key].tipoValore.id = id;
                        eventi.push({ id: item.id, tipoValore: $scope.arrayPzScaChange[key].tipoValore, valore: item.valore })
                    })
                }
                
                //genero il pacchetto data da passare al server 
                var data = {
                    idBatch: $scope.currentItem.id,
                    Events: eventi
                }

                //chiamata al server 
                $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SetEvents , data).success(function (data) {
                    var x = data;
                    //inizializzzo la variabile 
                    $scope.arrayPzScaChange = [];
                })
                    .error(function (e) {
                        console.log(e);
                        $scope.onError(e);
                          //inizializzzo la variabile 
                        $scope.arrayPzScaChange = [];
                    });;
            }

            //funzione per abilitare e disabilitare i botton con le checkbox
            $scope.changeCasualBlock = function () {
                //viene inizializzato un arrey di eventi 
                var fermi = [];
                //ciclo tutti gli eventi per settare la stella causale
                if ($scope.arrayFermiChange.length > 0) {
                    var id = $scope.arrayFermiChange[0].tipoValore.id;
                    angular.forEach($scope.arrayFermiChange, function (item, key) {
                        $scope.arrayFermiChange[key].causaliRiavvio.id = id;
                        fermi.push($scope.arrayFermiChange[key]);
                    })
                }
                //genero il pacchetto data da passare al server 
                var data = {
                    idBatch: $scope.currentItem.id,
                    idCausal: $scope.arrayFermiChange[0].tipoValore.id,
                    blocks: fermi
                }
                //chiamata al server 
                $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SetBlock, data).success(function (data) {
                    var x = data;
                    //inizializzzo la variabile 
                    $scope.arrayFermiChange = [];
                }).error(function (e) {
                    console.log(e);
                    $scope.onError(e);
                    //inizializzzo la variabile 
                    $scope.arrayFermiChange = [];
                });
            }


            //funzione per inserire dei pezzi scarto
            $scope.insertPiecesSca = function () {
                //vengono inseriti tutti gli attributi 
                var data = {
                    idBatch: $scope.currentItem.id,
                    fase: $scope.selectedPhase,
                    Operatore: $scope.GetOperatore,
                    Source:1,
                    Valore: $scope.addPzSca[0].valore,
                    Macchina: $scope.selectedPhase.macchine[0].macchina,
                    TipoConteggio: 0,
                    Causale: $scope.addPzSca[0].tipoValore,
                    TipoEvento: 0,
                }
                //chiamata per inserimento dei dati 
                $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SetEvent_PiecesDiscard, data).success(function (data) {
                    var x = data;
                    //inizializzo la variabile di appoggio
                        $scope.addPzSca = [];
                  
                    
                })
                    .error(function (e) {
                        console.log(e);
                        $scope.onError(e);
                        $scope.addPzSca = [];
                    });;
            }



            //funzione per inserire dei prelievi materie prime
            $scope.insertComponent = function () {
                //vengono inseriti tutti gli attributi 

                var data = {
                    idBatch: $scope.currentItem.id,
                    fase: $scope.selectedPhase,
                    Operatore: $scope.GetOperatore,
                    Source: 1,
                    Valore: $scope.preliovocomponenti.quantita,
                    Macchina: $scope.selectedPhase.macchine[0].macchina,
                    TipoConteggio: 0,
                    Causale: $scope.preliovocomponenti.causale,
                    Materiale: { id: $scope.preliovocomponenti.materiale},
                    TipoEvento: 3,
                     
                }
                //chiamata per inserimento dei dati 
                $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SetEvent_PiecesDiscard, data).success(function (data) {
                    var x = data;
                    //inizializzo la variabile di appoggio
                    $scope.preliovocomponenti = { materiale: null, quantita: 0, quantitaScarto: 0, causale: '', isEnable: true, isChanged: false};


                })
                    .error(function (e) {
                        console.log(e);
                        $scope.onError(e);
                        $scope.preliovocomponenti = { materiale: null, quantita: 0, quantitaScarto:0, causale: '', isEnable: false, isChanged: false };
                    });;
            }


            //funzione per inserire dei prelievi materie prime
            $scope.scaComponent = function () {
                //vengono inseriti tutti gli attributi 

                var data = {
                    idBatch: $scope.currentItem.id,
                    fase: $scope.selectedPhase,
                    Operatore: $scope.GetOperatore,
                    Source: 1,
                    Valore: $scope.preliovocomponenti.quantitaScarto,
                    Macchina: $scope.selectedPhase.macchine[0].macchina,
                    TipoConteggio: 0,
                    Causale: $scope.preliovocomponenti.causale,
                    Materiale: { id: $scope.preliovocomponenti.materiale },
                    TipoEvento: 0,

                }
                //chiamata per inserimento dei dati 
                $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SetEvent_PiecesDiscard, data).success(function (data) {
                    var x = data;
                    //inizializzo la variabile di appoggio
                    $scope.preliovocomponenti = { materiale: null, quantita: 0, quantitaScarto: 0, causale: '', isEnable: true, isChanged: false };


                })
                    .error(function (e) {
                        console.log(e);
                        $scope.onError(e);
                        $scope.preliovocomponenti = { materiale: null, quantita: 0, quantitaScarto:0 , causale: '', isEnable: false, isChanged: false };
                    });;
            }

            //funzione che controllo quanti pezzi scarto non sono stati giustificati
            $scope.controlPiecesUnjustified = function () {
                if ($scope.currentItem.eventi != null) {
                    $scope.pzScaNotUnjustified = 0;
                    angular.forEach(jQuery.grep($scope.currentItem.eventi, function (a) { return a.tipoEvento == eventType_ScrapPiece }), function (item, key) {
                        if (item.tipoValore.item.codice =="AreaMes_Default") {    
                            $scope.pzScaNotUnjustified++;;
                        }
                    });
                    
                }
            }

            $scope.controlBlocksUnjustified = function () {
                if ($scope.currentItem.fermi != null) {
                    $scope.fermiNotUnjustified = 0;
                    angular.forEach($scope.currentItem.fermi, function (fermo, key) {

                        //angular.forEach(jQuery.grep($scope.currentItem.fermi, function (a) { return a.CausaliRiavvio == eventType_ScrapPiece }), function (item, key) {
                        if (fermo.causaliRiavvio.item.codice == "AreaMes_Default") {
                            $scope.fermiNotUnjustified++;;
                        }
                    });
                }
            }

            //
            $scope.refreshFase = function () {
                var changedId = $scope.selectedBatchPhase.numeroFase;

                DataFactory.mGetBatchFromId($location.search().idBatch, $rootScope.globals.currentUser.username).then(function (response) {
                    $scope.currentItem = response.data;
                    angular.forEach($scope.filteredFasi, function (fase, key) {
                        if (fase.numeroFase == changedId) {
                            $scope.showSelected($scope.selectedPhase);
                        }
                    })
                });
                $scope.updateProgressBar(($scope.currentItem.pezziProdotti / $scope.currentItem.pezzidaProdurre * 100).toFixed(2));
            }

            // opzioni per l'albero
            $scope.treeOptions = {
                nodeChildren: "fasi",
                dirSelectable: true,
                injectClasses: {
                    ul: "a1",
                    li: "a2",
                    liSelected: "a7",
                    iExpanded: "a3",
                    iCollapsed: "a4",
                    iLeaf: "a5",
                    label: "a6",
                    labelSelected: "a8"
                }
            }

            //
            $scope.showSelected = function (sel) {
                if (sel == null) return;

                $scope.shDetail = "";
                // inizializzazione dei models
                //resetAllFields();
                $scope.materialiPerFase = [];
                $scope.multimediaPerFase = [];

                //if (sel.macchine != null || sel.manodopera != null) {
                $scope.isEnabledDettagliFase = sel.abilitaAvanzamenti;
                $scope.isEnabledMateriali = sel.abilitaMateriali;
                $scope.isEnabledMultimedia = sel.abilitaMultimedia;
                $scope.selectedNode = sel;
                $scope.numeroFase = sel.numeroFase;
                $scope.nomeFase = sel.nome;
                $scope.descrizioneFase = sel.descrizione;
                $scope.type = sel.type;
                $scope.isAndon = sel.isAndon;
                $scope.isAutomatica = sel.isAutomatica;
                $scope.sidebar2Show = $scope.selectedNode.isAndon;
                $scope.sidebarIsCollapsed = $scope.selectedNode.isAndon;
                $scope.IsVisibleFaseCiclica = (sel.type == "0");
                $scope.abilitabarcode = sel.abilitabarcode;
                $scope.abilitaLetturaRfid = sel.abilitaLetturaRfid;
                $scope.abilitascritturaRfid = sel.abilitascritturaRfid;
                $scope.abilitaAvanzamentiStar = sel.abilitaAvanzamentiStar;
                $scope.abilitaAvanzamentiStop = sel.abilitaAvanzamentiStop;
                $scope.abilitaAvanzamentiPausa = sel.abilitaAvanzamentiPausa;
                $scope.visibilitastazione = sel.visibilitastazione;
                $scope.abilitaPulsanteEmergenza = sel.abilitaPulsanteEmergenza;
                $scope.abilitaFaseTerminataParzialmente = sel.abilitaFaseTerminataParzialmente;
                if ($scope.isEnabledMateriali == false) {
                    $scope.switchDetail(null, 2);
                }


                $scope.set_color = function (id, isEnabledMateriali, toogle) {
                    var change = document.getElementById("b3");
                    var change2 = document.getElementById("b2");

                    if (isEnabledMateriali == false && id == '2' || boolean2 == true) {
                        $scope.nclick++;
                        boolean2 = false;
                        return { "background-color": "#428bca", "color": "#ffffff" };
                    }
                        //&& $scope.nclick>0
                    else if (isEnabledMateriali == true && id == '1' && $scope.nclick > 0) {
                        $scope.nclick = 0;
                        return { "background-color": "#428bca", "color": "#ffffff" };
                    }
                    else {
                        return { "background-color": "#fff", "color": "#333" };
                    }
                };

                angular.forEach($scope.currentItem.fasi, function (fase, key) {
                    if (fase.numeroFase == sel.numeroFase) {    // da aggiornare usando fase.ID (da riportare dalla distinta alla creazione del batch //
                        $scope.selectedPhase = sel;             // FASE DISTINTA BASE
                        $scope.selectedBatchPhase = fase;

                        if ($scope.selectedBatchPhase.distintaBaseFase.componenti != null) {

                            var material = $scope.selectedBatchPhase.distintaBaseFase.componenti.find(x => x.descrizione == "MAT");
                            if (material != null)
                                $scope.preliovocomponenti = { materiale: material.materiale.id, quantita: material.qta, quantitaScarto: 0, causale: '', materialeConsigliato: material.materiale.id, isChanged: false, isEnable: true };
                            else
                                $scope.preliovocomponenti = { materiale: null, quantita: 0, quantitaScarto: 0, causale: '', materialeConsigliato: '', isChanged: false, isEnable: false };
                        }
                        else
                            $scope.preliovocomponenti = { materiale: null, quantita: 0, quantitaScarto:0, causale: '', materialeConsigliato: '', isChanged: false, isEnable: false };

                           

                            // FASE BATCH
                    }
                });

                // caricamento materiali
                if ($scope.isEnabledMateriali) {
                    $scope.materialiPerFase = $scope.getMaterialiPerFase();
                    $scope.shDetail = "1";
                }

                // caricamento multimedia
                if ($scope.isEnabledMultimedia)
                    $scope.multimediaPerFase = $scope.getMultimediaPerFase();


                angular.forEach($scope.dataForTheTree[0].fasi, function (treefase, key) {
                    if (treefase.numeroFase == $scope.selectedBatchPhase.numeroFase)
                        treefase.statoOperativo = $scope.selectedBatchPhase.stato;
                });

                if (sel.abilitaAvanzamenti == true) {
                    
                    // array per abilitazione macchine e competenze macchine
                    $scope.isEnabledMacchinaSelection = {};
                    $scope.isEnabledcompentenzaMacchineSelection = {};

                    // abilitazione checkbox di collegamento all'operazione ciclo
                    $scope.isEnabledLinkedOption = false;

                    angular.forEach($scope.CausaliFase, function (obj, key) {
                        // caso in cui ho a null tutti i gli oggetti macchine e competenze macchinari
                        //if (item.macchine[obj.id].macchina == null && item.macchine[obj.id].macchinaCompetenza == null)
                        $scope.isEnabledMacchinaSelection[obj.id] = true;
                    });

                    var t = 0;
                    
                    $scope.getTempoEffettivo(sel);
                    
                    var date = new Date(null);
                    var datef = new Date(null);
                    date.setSeconds(t);
                    $scope.tempiTeoriciDistintaBase = date.toISOString().substr(11, 8);
                    $scope.tempiTeoriciDistintaBaseFase = $scope.secToStringaFormattata(sel.tempoTeoricoSec); //converto il tempo teorico da secondi a HH:mm:ss


                    var fase = $scope.selectedBatchPhase;
                    // mostro il lucchetto se ho un utente diverso da quello dell'avvio
                    $scope.userLocked = (fase.userLocked != $rootScope.globals.currentUser.username && fase.userLocked != null) && (fase.stato != "inAttesa");
                    // se la fase è cominciata mostro il tempo di start
                    $scope.stateStarted = (fase.stato != "inAttesa");
                    // se la fase è terminata non faccio vedere più i pulsanti ma mostro un'altra finestra
                    $scope.stateFinished = (fase.stato == "terminato");
                    // se la fase è abortita non faccio vedere più i pulsanti ma mostro un'altra finestra
                    $scope.stateAborted = (fase.stato == "abortito");
                    // controlli su abilitazione pulsanti
                    $scope.btnStateConcludi = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "terminato") && ($scope.batch.stato != "inLavorazioneInEmergenza") && (fase.stato != "inAttesa");
                    $scope.btnStateStart = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "inLavorazione") && ($scope.batch.stato != "inLavorazioneInEmergenza");
                    $scope.btnStatePause = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "inLavorazioneInPausa") && (fase.stato != "inAttesa") && (fase.stato != "abortito") && (fase.stato != "terminato") && ($scope.batch.stato != "inLavorazioneInEmergenza");
                    $scope.btnStateAbort = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "abortito");
                }
                contsize();
            };

            $scope.getTempoEffettivo = function (sel) {
                var inizio = $scope.currentItem.fasi.find(x => x.numeroFase == sel.numeroFase).inizio;
                var fine = $scope.currentItem.fasi.find(x => x.numeroFase == sel.numeroFase).fine;
                if (inizio.substr(0, 19) != '0001-01-01T00:00:00') {
                    if (fine.substr(0, 19) == '0001-01-01T00:00:00') {
                        var d = new Date;
                        var mese = d.getMonth() + 1;
                        if (mese < 10)
                            mese = "0" + mese;

                        var giorno = d.getDate();
                        if (giorno < 10)
                            giorno = "0" + giorno;

                        var ore = d.getHours();
                        if (ore < 10)
                            ore = "0" + ore;

                        var minuti = d.getMinutes();
                        if (minuti < 10)
                            minuti = "0" + minuti;

                        var secondi = d.getSeconds();
                        if (secondi < 10)
                            secondi = "0" + secondi;


                        fine = [d.getFullYear(), mese, giorno].join('-') + 'T' + [ore, minuti, secondi].join(':');
                    }
                    $scope.tempiEffettivoDistintaBaseFase = $scope.dateDiffToString(inizio, fine);
                }
                else {
                    $scope.tempiEffettivoDistintaBaseFase = "00:00:00";
                }

            }

            //$scope.nodeDoubleClick = function () {
            //    var defExp = [];
            //    angular.forEach($scope.currentItem.distintaBase, function (fase, key) {
            //        if (fase.numeroFase == sel.numeroFase) {
            //            fillItemsInView(fase);
            //        }
            //    });
            //    $scope.selectedNode.expand();
            //    //$scope.defaultExpanded = defExp;
            //}

            // funzione per cambiare lo stato della fase $scope.selectedBatchPhase


            $scope.changeState = function (idStato) {
                $scope.selectedBatchPhase.stato = idStato;
                $scope.selectedBatchPhase.userLocked = $rootScope.globals.currentUser.username;


                


                var data = {
                    idBatch: $location.search().idBatch,
                    fase: $scope.selectedBatchPhase,
                    Username: $rootScope.globals.currentUser.username,
                }
                $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SetStateFase, data).success(function (data) {
                    var x = data;
                })
                    .error(function (e) {
                        console.log(e);
                        $scope.onError(e);
                    });
            }

            $scope.changeStateToStart = function (idStato) {
                var num = $scope.selectedBatchPhase.numeroFase;
                var toBeConfirmed = false
                if (num > 1) {
                    var previousPhase = num - 1;
                    angular.forEach($scope.filteredFasi, function (fase, key) {
                        if (fase.numeroFase == (num - 1) && fase.stato == 'inAttesa' && (fase.macchine != null || fase.manodopera != null)) {
                            toBeConfirmed = true;
                        }
                    });
                }
                if (toBeConfirmed) {
                    var element = angular.element('#notSequentialStartFase');
                    element.modal("show");
                }
                else
                    $scope.changeState(idStato);
            }

            // funzione per cambiare lo stato del batch
            $scope.changeStateBatch = function (idStato) {
                $scope.batch.stato = idStato;
                
                var eventDate = new Date().toISOString();
                try {
                    eventDate = new Date($scope.dataDiRiavvio).toISOString();
                }
                catch (e) { }

                var currentPhase = $scope.currentItem.distintaBase.fasi[(($scope.selectedBatchPhase.numeroFase) - 1)];
                

                $scope.selectedBatchPhase.stato = idStato;
                $scope.selectedBatchPhase.userLocked = $rootScope.globals.currentUser.username;
                var data = {
                    idBatch: $location.search().idBatch,
                    fase: $scope.selectedBatchPhase,
                    Username: $rootScope.globals.currentUser.username,
                }
                $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SetStateFase, data).success(function (data) {
                    var x = data;
                    var data2 = {
                        idBatch: $scope.batch.id,
                        idStato: idStato,
                        causaleRiavvio: $scope.selectedCausaleDiRiavvio,
                        dataRiavvio: eventDate,
                        notaRiavvio: $scope.notaDiRiavvio,
                        Username: $rootScope.globals.currentUser.username,
                        StationId: stationId,
                        NomeFase: $scope.selectedBatchPhase.distintaBaseFase.nome,
                        NumeroFase: $scope.selectedBatchPhase.distintaBaseFase.numeroFase,
                        Macchina: null,
                    }

                    //if (currentPhase != null && currentPhase.macchine[0].macchina !== null)
                    //    data.Macchina = currentPhase.macchine[0].macchina.id;

                    $scope.dataRiavvio = eventDate;
                    $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SetStateBatch, data2)
                        .success(function (data) { })
                        .error(function (e) { console.log(e); $scope.onError(e); });
                })
                    .error(function (e) {
                        console.log(e);
                        $scope.onError(e);
                    });;
            }

            $scope.onError = function (err) {
                $scope.modalWarningMessage = err.exceptionMessage;
                angular.element(document.querySelector('#modalWarning')).modal('toggle');
            }


            $scope.shDetail = 0;
            $scope.switchDetail = function (btn, arg) {
                if (arg == 2) {
                    boolean2 = true;
                }
                $scope.shDetail = arg;
            };


            $scope.clickSendBarcode = function (barcode, isRfid, item) {

                $scope.isLoaderActiveWrite = true;
                var data = {
                    IdBatch: $scope.batch.id,
                    IdFase: $scope.selectedBatchPhase.numeroFase,
                    Barcode: barcode,
                    Material: (item != null ? item.materiale : null),
                    IsRfid: isRfid,
                    TimeoutRequest: 30,
                    Username: $rootScope.globals.currentUser.username,
                    StationId: stationId
                }

                // reset dello stato prima della spedizione 
                angular.forEach($scope.materialiPerFase, function (mat, key) { if (mat.barcodeRfid === barcode) { mat.statoIngresso = ''; mat.isOnAction = true; } });

                // chiamata al server per la scrittura
                $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SendBarcode, data).success(function (res) {
                    $scope.isLoaderActiveWrite = false;

                    if (res.isOnError) {
                        //growl.success(res.errorMessage, config);
                        $scope.modalWarningMessage = res.errorMessage;
                        angular.element(document.querySelector('#modalWarning')).modal('toggle');
                        return;
                    }

                    angular.forEach($scope.materialiPerFase, function (mat, key) {
                        if (mat.barcodeRfid === barcode) {
                            mat.isOnAction = false;
                            mat.batchMaterialiStato = res.isCompleted ? 'completato' : '';
                            //growl.success("Scrittura avvenuta correttamente", config);
                        }

                    });
                })
                    .error(function (e) {
                        $scope.isLoaderActiveWrite = false;
                        //growl.success(e, config);
                        $scope.modalWarningMessage = res.errorMessage;
                        angular.element(document.querySelector('#modalWarning')).modal('toggle');
                        $scope.onError(e);
                    });;
            }
            // end SendBarcode

            $scope.clickReadBarcode = function (barcode, isRfid, item) {

                $scope.isLoaderActiveRead = true;
                var data = {
                    IdBatch: $scope.batch.id,
                    IdFase: $scope.selectedBatchPhase.numeroFase,
                    IsRfid: isRfid,
                    TimeoutRequest: 30,
                    Username: $rootScope.globals.currentUser.username,
                    StationId: stationId
                }

                // chiamata al server per la scrittura
                $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_ReadBarcode, data).success(function (res) {
                    $scope.isLoaderActiveRead = false;

                    angular.forEach($scope.materialiPerFase, function (mat, key) {
                        if (mat.barcode === res.barcode) {
                            mat.batchMaterialiStato = res.isCompleted ? 'completato' : '';
                            // growl.success("Scrittura avvenuta correttamente", config);
                        }

                    });

                })
                    .error(function (e) {
                        console.log(e);
                        $scope.isLoaderActiveRead = false;
                    });;
            }


            $scope.loadFasiAlt();


            // ----------------------------------------------------------------------------------------
            // GESTIONE LETTURA BARCODE
            // ----------------------------------------------------------------------------------------
            // funzione per attivare il focus sulla casella barcode. Ogni n-secondi viene impostato il focus
            $scope.focusBarcodeEnabled = function () {

                timerFocus = setInterval(function () {
                    $(window).focus();
                    $("#txtBarcode").focus().select();
                }, 10 * 500);

                $("#txtBarcode").focus().select();

                $("#txtbarcode").blur(function () {
                    setTimeout(function () { $("#txtbarcode").focus(); $("#txtbarcode").attr("disabled", true); }, 10 * 500);
                });
            };

            // funzione per disattivare il focus sulla casella barcode
            $scope.focusBarcodeDisabled = function () {
                clearInterval(timerFocus);
            };

            // evento scatenato alla pressione del pulsnte "Invio" sulla casella barcode
            $scope.onKeyPress = function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    var barcodeFixed = $scope.barcode.replace('\r', '');
                    $scope.clickSendBarcode(barcodeFixed, false, null);
                    $scope.barcode = ""; // reset del barcode
                }
            };
            // ----------------------------------------------------------------------------------------



            // ----------------------------------------------------------------------------------------
            // WATCH di variabili del model
            // ----------------------------------------------------------------------------------------

            // watch della variabile per gestire l'indice di TAB dei pulsanti "MULTIMEDIA", "MATERIALI"
            $scope.$watch('shDetail', function (newValue, oldValue, scope) {
                // disabilitazione del focus automatico
                if (newValue == "1") {
                    $scope.focusBarcodeEnabled();
                    $scope.materialiPerFase = $scope.getMaterialiPerFase();
                } else $scope.focusBarcodeDisabled();

                contsize();
            }, true);

            // ----------------------------------------------------------------------------------------
            // Popolamento principale delle voci ad albero di sinistra
            // ----------------------------------------------------------------------------------------

            function createOrRefreshTreeView() {
                $scope.defaultExpanded = [];
                var oNodeSelected = $scope.selectedNode;

                var batchFasi = angular.copy($scope.filteredFasi);
                $scope.dataForTheTree = []; //$scope.dataForTheTree[0].fasi = [];

                if ($scope.filteredFasi != null) {
                    var parentNode = 0;
                    var i = 0;
                    
                    $scope.dataForTheTree.push($scope.currentItem.distintaBase);
                    //***********************************************************************************************************************************************************
                    angular.forEach($scope.dataForTheTree[0].fasi, function (fase, key) {
                       
                        var result = $.grep(batchFasi, function (e) { return e.numeroFase == fase.numeroFase; });
                            if (result.length > 0) {
                                fase.statoOperativo = result[0].stato
                                fase.tipo = (fase.isCiclica ? "1" : "0");
                            }
                    });

                    for (var i = $scope.dataForTheTree[0].fasi.length - 1; i >= 0; i--) {
                        var faseProva = $scope.dataForTheTree[0].fasi[i];
                        var stazioniAbilitate = faseProva.visibilitastazione ? faseProva.visibilitastazione.trim() : "";
                        var elencoStazioniAbilitate = stazioniAbilitate.split(',');
                        var stazioneCorrenteAbilitata = $.grep(elencoStazioniAbilitate, function (e) { return e == stationId; });

                        if (stazioniAbilitate == "" || stazioniAbilitate == stationId || stazioniAbilitate == "0" || stazioneCorrenteAbilitata > 0) {
                            var fase = $scope.dataForTheTree[0].fasi[i];
                            var result = $.grep(batchFasi, function (e) { return e.numeroFase == fase.numeroFase; });
                            if (result.length == 0) {
                                $scope.dataForTheTree[0].fasi.splice(i, 1); //ELIMINA da TreeView la fase "Batch.DistintaBase.Fasi" GHOST
                            }
                        }
                        else { $scope.dataForTheTree[0].fasi.splice(i, 1); }

                    }//fine for

                }
                $scope.defaultExpanded = [$scope.dataForTheTree[0], $scope.dataForTheTree[0].fasi[0]];
                $scope.showSelected(oNodeSelected);

            }

            // ordinamento di json per campo richiesto
            function orderById(items, field, reverse) {
                var filtered = [];
                angular.forEach(items, function (item) {
                    filtered.push(item);
                });
                filtered.sort(function (a, b) {
                    return (a[field] > b[field] ? 1 : -1);
                });
                if (reverse) filtered.reverse();
                return filtered;
            };

            // **********************************************************************
            // 2016-02-12 Metodo per sottoscrivere la postazione locale al server
            // **********************************************************************
            function doSubscribe(batchId) {

                hub.invoke('SubscribeToBatch', batchId).done(function (res) {
                    // caricamento di ulteriori informazioni
                    var x = res;

                }).fail(function (error) {
                    // errore nella sottoscrizione
                });
            }

            function doUnSubscribe(batchId) {

                hub.invoke('UnSubscribeToBatch', batchId).done(function (res) {
                    // caricamento di ulteriori informazioni
                    var x = res;
                }).fail(function (error) {
                    // errore nella sottoscrizione
                });
            }

            $(document).ready(function () {

                $scope.selectedCausaleDiRiavvio = "";
                $scope.causaliRiavvio = [];
                $scope.causaliAborto = [];

                var d = new Date();
                //$scope.dataDiRiavvio = d.toLocaleDateString();
                $scope.notaDiRiavvio = "";
                DataFactory.GetCausaliRiavvio().then(function (response) {
                    angular.forEach(response.data, function (Causal, key) {
                        if (Causal.isStopCausal == true) $scope.causaliRiavvio.push(Causal);
                        if (Causal.isAboartCausal == true) $scope.causaliAborto.push(Causal);
                    })
                   // $scope.causaliRiavvio = angular.copy(response.data);
                });

            });

            $scope.openingModalSetCausaleEmergenza = function () {
                $scope.selectedCausaleDiRiavvio = $scope.causaliRiavvio[0];
                var d = new Date();
                $scope.dataDiRiavvio = d.toLocaleString();  //null;//d.toDatetimeString();
                $scope.notaDiRiavvio = "";
            }


            ///SOSPENDI SECTION ///
            $scope.pause = async function () {
                document.getElementsByClassName("loader")[0].style.display = "block";

                //if ($scope.currentItem.stato == "inAttesa")
                //    $scope.currentItem.statoPrecedente = $scope.currentItem.stato;
                $scope.currentItem.stato = "InSospensione";
                //basicCrud_doConfirm($scope, $http, subUrlBatch, Enums);

                let myPromise = new Promise((resolve, reject) => {
                    //document.getElementsByClassName("loader")[0].style.display = "block";
                    $http.post(urlApi + '/' + subUrlBatch, $scope.currentItem)
                        .then(function (data) {
                            if ($scope.viewState === Enums.ViewState['C']) {
                                $scope.currentItem = data.data;
                                $scope.reload();
                            }
                            else if ($scope.viewState === Enums.ViewState['U']) {
                                var elementPos = $scope.items.map(function (x) { return x.id; }).indexOf($scope.currentItem.id);
                                $scope.items[elementPos] = $scope.currentItem;
                            }

                            resolve();
                        }, function (e) {
                            reject(e);
                        });
                })
                myPromise.then(function (a) {
                    document.getElementsByClassName("loader")[0].style.display = "none";
                    closeModal('#modalConfirmPause');
                    $("#modalConfirmPauseEnd").modal();
                    $scope.reload();
                },
                    function (error) {
                        $scope.crudException(error);
                        closeModal('#modalConfirmPause');
                        $("#modalConfirmPauseEnd").modal();
                    }
                );
            };

            //-----------------------------
            // Funzione che ritorna la differenza di due date formattate nel seguente modo yyyy-mm-ddThh:mm:ss. Il risultato è espresso nel seguente modo hh:mm:ss
            //-----------------------------
            $scope.dateDiffToString = function (tempoInizio, tempoFine)
            {
                var sec = 0;
                if (tempoFine == "") {
                    tempoFine = Date.now();
                }
                tempoInizio = tempoInizio.substr(0, 19);
                tempoFine = tempoFine.substr(0, 19);
                var dataInizio = new Date(tempoInizio);
                var dataInizioSecondi = dataInizio.getTime() / 1000;

                var dataFine = new Date(tempoFine);
                var dataFineSecondi = dataFine.getTime() / 1000;

                res = dataFineSecondi - dataInizioSecondi;
                var app = $scope.secToStringaFormattata(res);
                return app;
            }

            $scope.secToStringaFormattata = function (secondi) {
                var secondi_app = secondi % 60;
                var ore = Math.trunc(secondi / 3600);

                var minuti = (secondi - (ore * 3600) - secondi_app) / 60;
                if (ore < 10)
                    ore = "0" + ore;

                if (minuti < 10)
                    minuti = "0" + minuti;

                if (secondi_app < 10)
                    secondi_app = "0" + secondi_app;
                return ore + ':' + minuti + ':' + secondi_app;
            }
        }]);