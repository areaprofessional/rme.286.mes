﻿app.controller('AllServiceCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$location', '$rootScope',
    function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $location, $rootScope) {

        $rootScope.signalRconnection = $.cookie("SigalRc");
        urlApi = $.cookie("WebApiC");
        $scope.viewState = Enums.ViewState['R'];

        $scope.title = function () { basicCrud_doTitle($scope, Enums); };
        $scope.filterIdStato = "-1";
        $scope.lUser = $rootScope.globals.currentUser.username;

        basicCrud_doInitWatch($scope);
        $scope.items = [];
        $scope.currentItem = null;
        $scope.FormaddMateriali = null;
        $scope.idMachine = -1;
        $scope.currentStatus = null;
        $scope.dtInstance = {};
        $scope.dtInstance02 = {};
        $scope.dtInstance03 = {};
        $scope.InitFilter = getParameterByName("i");
        $scope.ViewAsInModifica = ($scope.InitFilter == 'inModifica');
        $scope.ViewAsInAttesa = ($scope.InitFilter == 'inAttesa');
        $scope.ViewAsInLavorazione = ($scope.InitFilter == 'inLavorazione');
        $scope.ViewAsTerminato = ($scope.InitFilter == 'terminato');

        DataFactory.getMateriali().then(function (response) {
            $scope.Materiali = jQuery.grep(response.data, function (a) { return a.tipoMateriale === "manutenzione"});
        });

        DataFactory.getMacchine().then(function (response) {
            $scope.Macchine = jQuery.grep(response.data, function (a) { return a });
        });


        DataFactory.getTipomanutenzioni().then(function (response) {
            $scope.Tipomanutenzioni = jQuery.grep(response.data, function (a) { return a });
        });


        DataFactory.GetStatiBatch().then(function (response) {
            $scope.ListaStatiBatch = response.data;
        });


        $scope.reload = function () {
           // basicCrud_doReload($scope, $http, subUrlBatchService);
        };

        $scope.filterState = function (idStato, loggedUser, idmacchinaa) {
            //$rootScope.idStatoPag = idStato;
            if (idmacchinaa != null) $scope.idMachine = idmacchinaa;
            //else $scope.idMachine = -1;


            $scope.filterIdStato = idStato;
            DataFactory.getBatchService().then(function (response) {

                response.data = jQuery.grep(response.data, function (a) { return a.stato != "terminato" });
                if ($scope.filterIdStato != -1)
                    response.data = jQuery.grep(response.data, function (a) { return a.stato == $scope.filterIdStato });
                // se l'oggetto è presente nell'array locale, 
                // ma non in quello ricevuto dal server, allora viene cancellato
                angular.forEach($scope.items, function (value, key) {
                    var itemFounded = response.data.find(o => o.id == value.id);
                    //if (!itemFounded)
                    //    $scope.items.splice(list.indexOf(itemFounded), 1);
                    if (itemFounded == null) {
                        $scope.items = [];
                    }
                });

                // vengono ciclati gli elementi prelevati dal server
                angular.forEach(response.data, function (value, key) {
                    var itemFounded = $scope.items.find(o => o.id == value.id);

                    if (!itemFounded) {

                        // se l'elemento non è presente viene inserito
                        if ($scope.idMachine == '-1' || $scope.idMachine == null) {
                            $scope.items.push(value); }

                        else {
                            if (value.macchina.id == $scope.idMachine) {
                                $scope.items.push(value); } }

                    } else {
                        if ($scope.idMachine == '-1' || $scope.idMachine == null || itemFounded.macchina.id == $scope.idMachine) {
                            // se l'elemento è presente vengono aggiornate le sue proprietà
                            itemFounded.nome = value.nome;
                            itemFounded.operatore = value.operatore;
                            itemFounded.stato = value.stato;
                            itemFounded.creazione = value.creazione;
                            itemFounded.fine = value.fine;
                            itemFounded.materiali = value.materiali;
                            itemFounded.note = value.note;
                            itemFounded.tipomanutenzione = value.tipomanutenzione;
                            itemFounded.macchina = value.macchina;
                            itemFounded.inizio = value.inizio;
                        }
                        else $scope.arrayEliminare.push(value);

                    }
                });

                //ciclo per eliminare quelli che non mi servono
                angular.forEach($scope.arrayEliminare, function (value, key) {

                    angular.forEach($scope.items, function (value1, key1) {

                        if (value.id == $scope.items[key1].id)
                            $scope.items.splice(key1, 1);

                    });
                });
                $scope.arrayEliminare = [];

            });

            $scope.items = $scope.items.sort((a, b) => (a.odP > b.odP ? 1 : -1));

        }
        $scope.filterState($scope.filterIdStato, $scope.lUser)

        $scope.reload();

        var timer = setInterval(function () {
            $scope.filterState($scope.filterIdStato, $scope.lUser);
        }, 10 * 1000);

        setTimeout(function () {
            $scope.filterState($scope.filterIdStato, $scope.lUser);
        }, 2 * 1000);

        $scope.crudException = function (err) {
            $scope.exceptionMessageText = err.exceptionMessage;
            angular.element(document.querySelector('#modalWarning1')).modal('toggle');
        };

        

        // DataTables configurable options
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('stateSave', true)
            .withOption('order', [[1, 'desc']])
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        // DataTables configurable options
        $scope.dtOptions02 = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('stateSave', true)
            .withOption('order', [[1, 'desc']])
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        // DataTables configurable options
        $scope.dtOptions03 = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('stateSave', true)
            .withOption('order', [[1, 'desc']])
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        $scope.showItemDetail = function (item) {

                $scope.currentStatus = item.stato;


            basicCrud_doShowItemDetail($scope, $http, subUrlBatchService , Enums, item);

        };
        $scope.insert = function () {
            $scope.viewState = Enums.ViewState['C'];
            $scope.currentItem = new Object();
            $scope.isEnableWrite = true;
            $scope.currentStatus = "inModifica";
            $scope.currentItem.operatore = $scope.lUser;
            $scope.currentItem.materiali = [];
            $scope.currentItem.note = []
            ResetAllFields();
        };

        $scope.addMaterial = function () {

            closeModal('#modalAddMaterial');
            var arrayText = {quantita: $scope.FormaddMateriali.pezziDaProdurre, materiale: $scope.FormaddMateriali.codiceMateriale, operatore: $scope.lUser };
            $scope.currentItem.materiali.push(arrayText);
            $scope.FormaddMateriali.pezziDaProdurre = 1;
            $scope.FormaddMateriali.codiceMateriale = "";

        };
        

        $scope.addNote = function () {

            closeModal('#modalAddNota');
            $scope.date = new Date();
            var arrayText = { data: $scope.date, nota: $scope.FormaddMateriali.nota, operatore: $scope.lUser };
            
            $scope.currentItem.note.push(arrayText);
            $scope.FormaddMateriali.nota = "";
        };
        
        $scope.dtInstanceCallback = function () {
            //
        }

        $scope.changeStatus = function (item, state) {
            if (item === null) return;

           
            if (state === 1) {

                item.stato = "inLavorazione";
            }
            else if (state === 2) {

                item.stato = "inLavorazioneInPausai";
            }
            else if (state === 3) {

                item.stato = "terminato";
            }


            $scope.viewState = Enums.ViewState['C'];
            //item.stato = "1";
            $scope.currentItem = item;
            //$scope.currentItem.userLocked = $rootScope.globals.currentUser.username;

            basicCrud_doConfirm($scope, $http, subUrlBatchService, Enums);
        }

        $scope.update = function (event, item) {
                $scope.showItemDetail(item);

                if ($scope.currentItem != null) {

                    

                    var a = $scope.currentItem.macchina.item;
                    $scope.currentItem.macchina = a;
                    $scope.viewState = Enums.ViewState['U'];
                }

            
        };

        $scope.cancel = function () {
            $scope.viewState = Enums.ViewState['R']; ResetAllFields(); closeModal('#modalConfirmUndo');
        };

        $scope.refresh = function () {
            var objTemp = { id: $scope.currentItem.id };
            $scope.currentItem = { id: "reloading", Codice: "reloading" };
            basicCrud_doShowItemDetail($scope, $http, subUrlLine, Enums, objTemp, function (item) {
                $scope.showItemDetail($scope.currentItem);
                $scope.viewState = Enums.ViewState['U'];

                angular.forEach($scope.trcurrentItem, function (it) {
                    $scope.trshowItemDetail(it);
                })

            });

        }

        $scope.confirmAndClose = function () {
            basicCrud_doConfirm($scope, $http, subUrlBatchService, Enums);
            $scope.currentItem = [];
            $scope.reload();
            $scope.filterState($scope.filterIdStato, $scope.lUser);
        };

        $scope.crudException = function (err) {
            $scope.exceptionMessageText = err.exceptionMessage;
            angular.element(document.querySelector('#modalWarning1')).modal('toggle');
        };

        $scope.delete = function (item) {
            $scope.viewState = Enums.ViewState['D'];
            basicCrud_doDelete($scope, $http, subUrlBatchService, Enums)
        };

        $scope.title = function () { basicCrud_doTitle($scope, Enums); };

        basicCrud_doInitWatch($scope);

        $scope.reload();



        function closeModal(id) {
            angular.element(document.querySelector(id)).modal('hide');
        }

        function filter() {
            if ($scope.InitFilter) {
                $scope.items = $.grep($scope.items, function (e) { return e.stato.startsWith($scope.InitFilter); });
            }
        }

        // ordinamento di json per campo richiesto
        function orderById(items, field, reverse) {
            var filtered = [];
            angular.forEach(items, function (item) {
                filtered.push(item);
            });
            filtered.sort(function (a, b) {
                return (a[field] > b[field] ? 1 : -1);
            });
            if (reverse) filtered.reverse();
            return filtered;
        };


        // funzione per resettare tutti i campi
        function ResetAllFields() {

            //ResetDistintaCicli();
            //$scope.pcurrentItem = null;
            //$scope.parametersData = null;
            //$scope.nodesTableArr = null;
            //$scope.AnalisiData = null;
            //$scope.trcurrentItem = [];
            //$scope.data_multiBarChart = [{ key: '', values: [{ x: 0, y: 0 }] }];
        }

      

        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        // Definizione del filtro di visualizzazione in griglia
        function filter() {
            if ($scope.InitFilter) {
                $scope.items = $.grep($scope.items, function (e) {
                    return e.stato.startsWith($scope.InitFilter);
                });
            }
        }

        function generateData() {
            var yCount = $scope.trcurrentItem.length;
            var res = [];
            for (n = 0; n < $scope.trcurrentItem.length; n++) {
                var paramItem = $scope.trcurrentItem[n];
                res.push({ key: paramItem.nome, values: paramItem.data });
            }

            if (res.length > 0)
                return res;
            else
                return [{ key: '', values: [{ x: 0, y: 0 }] }];
        }

    
        $scope.toggleStatus = false;

        $scope.toggleDropdown = function (node, scopeNodes) {
            node.toggleStatus = node.toggleStatus == true ? false : true;
            $scope.toggleStatus = node.toggleStatus;
            $scope.toggleDropdownHelper(node.id, $scope.toggleStatus, scopeNodes);
        };

        $scope.toggleDropdownHelper = function (parentNodeId, toggleStatus, scopeNodes) {
            for (var i = 0; i < scopeNodes.length; i++) {
                node = scopeNodes[i];
                if (node.parentId == parentNodeId) {
                    if (toggleStatus == false)
                        $scope.toggleDropdownHelper(node.id, toggleStatus, scopeNodes);
                    scopeNodes[i].isShow = toggleStatus;
                }
            }
        };

    }]);


app.directive('date', function (dateFilter) {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {

            var dateFormat = attrs['date'] || 'yyyy-MM-dd';

            ctrl.$formatters.unshift(function (modelValue) {
                return dateFilter(modelValue, dateFormat);
            });
        }
    };
})




    
