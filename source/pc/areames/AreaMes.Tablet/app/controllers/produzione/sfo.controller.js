﻿// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('SfoCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory','$location', '$rootScope',
function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $location, $rootScope) {
            
    $rootScope.signalRconnection = $.cookie("SigalRc");
    urlApi = $.cookie("WebApiC");

    $scope.title = function () { basicCrud_doTitle($scope, Enums); };

    basicCrud_doInitWatch($scope);

    $scope.idMacchina = null;

    DataFactory.GetStatiBatch().then(function (response) {
        $scope.ListaStatiBatch = response.data;
    });

    $scope.filterState = function (idStato, loggedUser)
    {
        $scope.idMacchina = $location.search().idMacchina;
        $scope.items = [];
        
        DataFactory.GetSfoFromMachine($location.search().idMacchina, idStato, loggedUser).then(function (response) {
            $scope.items.push.apply($scope.items, response.data);
        });

    }

    $scope.filterState("-1", $rootScope.globals.currentUser.username)
}]);
