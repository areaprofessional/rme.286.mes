﻿//...
app.factory('DataFactory', ['$http', '$rootScope', '$timeout', '$cookies', '$window', function ($http, $rootScope, $timeout, $cookies, $window) {
    var datafactory = {};

    // metodo generale di ritorno
    datafactory.getAll = function (baseUrl) {
        
        return $http.get(baseUrl);
    }

    // metodo per ritornare tutti i tipi di macchine
    datafactory.getTipiMacchine = function () {

        return $http.get(urlApi + "/" + subUrlApi_TabelleTipoMacchina);
    }

    // metodo per ritornare i tipi manutenzione
    datafactory.getTipomanutenzioni = function () {

        return $http.get(urlApi + "/" + subUrlApi_TipiManutenzione);
    }


    // metodo per ritornare le macchine
    datafactory.getMacchine = function () {

        return $http.get(urlApi + "/" + subUrlApi_Macchina);
    }


    // metodo per ritornare tutte le competenze degli operatori
    datafactory.getCompetenzeOperatori = function () {

        return $http.get(urlApi + "/" + subUrlApi_OperatoreCompetenza);
    }

    // metodo per ritornare tutte le competenze delle macchine
    datafactory.getCompetenzeMacchina = function () {

        return $http.get(urlApi + "/" + subUrlApi_MacchinaCompetenza);
    }

    // metodo per ritornare tutti materiali
    datafactory.getMateriali = function () {

        return $http.get(urlApi + "/" + subUrlApi_Materiali);
    }

    // metodo per ritornare tutti materiali
    datafactory.GetMateriePrime = function (Categoria) {

        //return $http.get(urlApi + "/" + subUrlApi_Materiali);
        return $http.get(urlApi + "/" + subUrlRuntimeTransportContext + "/GetMaterialeFilter?Categoria=" + Categoria);
    }

    // metodo per ritornare tutti i batchservice
    datafactory.getBatchService = function () {

        return $http.get(urlApi + "/" + subUrlBatchService);
    }
    

    // metodo per ritornare le unità di misura
    datafactory.getUnitaDiMisura = function () {

        return $http.get(urlApi + "/" + subUrlApi_UnitaDiMisura);
    }

    // metodo per ritornare le operazioni ciclo
    datafactory.GetOperazioniCiclo = function () {
        return $http.get(urlApi + "/" + subUrlOperazioniCiclo);
    }

    // metodo per ritornare gli operatori
    datafactory.GetOperatori = function () {
        return $http.get(urlApi + "/" + subUrlApi_Operatore);
    }

    // metodo per ritornare le distinte basi
    datafactory.GetDistinteBasi = function () {
        return $http.get(urlApi + "/" + subUrlDistintaBase);
    }

    // metodo per ritornare i valori tabelle disponibili
    datafactory.GetTipoValori = function () {
        return $http.get(urlApi + "/" + subUrlApi_TipoValori);
    }
    // metodo per ritornare ordini 
    datafactory.GetSfoOdp = function (idStato, loggedUser, idStazione) {
        return $http.get(urlApi + "/" + subUrlRuntimeTransportContext + "/" + subUrlAction_GetSfoString + "?idStato=" + idStato + "&Username=" + loggedUser + "&idStazione=" + idStazione);
    }

    // metodo per ritornare ordini 
    datafactory.GetSfo = function (idStato, loggedUser, idStazione) {
        return $http.get(urlApi + "/" + subUrlRuntimeTransportContext + "/" + subUrlAction_GetSfo + "?idStato=" + idStato + "&Username=" + loggedUser + "&idStazione=" + idStazione);
    }

    // metodo per ritornare ordini per macchina
    datafactory.GetSfoFromMachine = function (idMacchina, idStato, loggedUser) {
        return $http.get(urlApi + "/" + subUrlRuntimeTransportContext + "/" + subUrlAction_GetSfoFromMachine + "?idMacchina=" + idMacchina + "&idStato=" + idStato + "&Username=" + loggedUser);
    }

    datafactory.GetFasiFromBatch = function (idBatch, loggedUser) {
        return $http.get(urlApi + "/" + subUrlRuntimeTransportContext + "/" + subUrlAction_GetFasiFromBatch + "?idBatch=" + idBatch + "&Username=" + loggedUser, { headers: { 'Cache-Control': 'no-cache' } });
    }

    datafactory.GetBatchFromId = function (idBatch) {
        return $http.get(urlApi + '/' + subUrlBatch + "/" + idBatch, { headers: { 'Cache-Control' : 'no-cache' } } );
    }

    datafactory.mGetBatchFromId = function (idBatch, loggedUser) {
        return $http.get(urlApi + '/' + subUrlRuntimeTransportContext + "/" + subUrlAction_GetBatch + "?idBatch=" + idBatch + "&Username=" + loggedUser, { headers: { 'Cache-Control': 'no-cache' } });
    }


    // metodo per ritornare le competenze manutenzione
    datafactory.GetCompetenzeManutenzione = function () {
        return $http.get(urlApi + "/" + subUrlApi_ManutenzioneCompetenza);
    }

    // metodo per ritornare le TipiManutenzione
    datafactory.GetTipiManutenzione = function () {
        return $http.get(urlApi + "/" + subUrlApi_TipiManutenzione);
    }

    // metodo per ritornare le TipiGuastiManutenzione
    datafactory.GetTipiGuastiManutenzione = function () {
        return $http.get(urlApi + "/" + subUrlApi_TipiGuastiManutenzione);
    }

    // metodo per ritornare le TipoSegnalazioneManutenzione
    datafactory.GetTipoSegnalazioneManutenzione = function () {
        return $http.get(urlApi + "/" + subUrlApi_TipiSegnalazioneManutenzione);
    }



    /***********************************************************/
    /*****Enums*************************************************/
    /***********************************************************/
    // otteniamo i livelli di accesso per gli utenti
    datafactory.GetLivelliAccesso = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetLivelliAccesso);
    }


    // otteniamo i tipi di materiale
    datafactory.GetTipiMateriale = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetTipiMateriale);
    }

    // otteniamo i tipi di causale
    datafactory.GetCausaliFase = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetCausaliFase);
    }

    // otteniamo gli stati della distinta fase
    datafactory.GetStatiDistintaBase = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetStatiDistintaBase);
    }

    // otteniamo gli stati del batch
    datafactory.GetStatiBatch = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetStatiBatch);
    }

    // otteniamo le causali di riavvio
    datafactory.GetCausaliRiavvio = function () {
        return $http.get(urlApi + "/" + subUrlApi_CausaliRiavvio);
    }

    datafactory.GetAziende = function () {
        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetAziende);
    }

    // metodo per logout
    datafactory.ClearCredentials = function () {
        $rootScope.globals = {};
        $cookies.remove('MesAuth');
        $http.defaults.headers.common.Authorization = 'Basic ';
        $window.location.href = 'login.html';
    }

    // metodo di autenticazione
    datafactory.Login = function (username, password) {

        /*----------------------------------------------*/
        //var response = { success: username === 'test' && password === 'test' };
        //if (response)
        //    return {username : "test", password : "password", nome : "nome1" , cognome : "cognome1"};
        return $http.get(urlApi + "/" + subUrlApi_Operatore + "/" + 'login?username=' + username + '&password=' + password);
    }

    // metodo per settare le credenziali
    datafactory.SetCredentials = function (id,username, password, nome, cognome, lingua, azienda, extra) {
        var authdata = username + ':' + password; //Base64.encode(username + ':' + password);

        $rootScope.globals = {
            currentUser: {
                idUser: id,
                username: username,
                authdata: authdata,
                nome: nome,
                cognome: cognome,
                lingua: lingua,
                azienda: azienda,
                extra: extra
            }
        };

        $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; 
        $cookies.put('MesAuth', JSON.stringify($rootScope.globals), $.cookie("InactivityTimeout"));
        //$window.location.href = 'elenco-macchine.html';  // ORIGINALE //

        // TO DO: href pagina dipendente da parametro di configurazione // 

        $window.location.href = 'allsfo.html';
    }

    return datafactory;
}]);