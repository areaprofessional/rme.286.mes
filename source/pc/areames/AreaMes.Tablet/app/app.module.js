﻿var urlApi = 'http://localhost:9000/api';
var urlLanguageDataTable = './app/localization/datatable/Italian.txt';
var subUrlApi_Macchina = "macchina";
var subUrlApi_TabelleTipoMacchina = "TipiMacchina";
var subUrlApi_MacchinaCompetenza = "macchinacompetenza";

var subUrlApi_CausaliRiavvio = "causaliriavvio";

var subUrlApi_Operatore = "operatore";
var subUrlApi_OperatoreCompetenza = "operatorecompetenza";

var subUrlApi_UnitaDiMisura = "unitadimisura";

var subUrlApi_Materiali = "materiale";

var subUrlOperazioniCiclo = "operazioniciclo";

var subUrlDistintaBase = "distintabase";

var subUrlBatch = "batch";

var subUrlApi_Enums = "enum";

var subUrlRuntimeTransportContext = "RuntimeTransportContext";

var subUrlBatchService = "service";

var subUrlApi_TipiManutenzione = "TipiManutenzione";
var subUrlApi_Aziende = "azienda";
var subUrlApi_ManutenzioneCompetenza = "manutenzionecompetenza";
var subUrlApi_TipiSegnalazioneManutenzione = "TipoSegnalazioneManutenzione";
var subUrlApi_TipiGuastiManutenzione = "TipiGuastiManutenzione";
var subUrlApi_TipoValori = "tipovalori";
// action specificche per caricare le enum
var subUrlAction_GetLivelliAccesso = "GetLivelliAccesso";
var subUrlAction_GetTipiMateriale = "GetTipiMateriale";
var subUrlAction_GetCausaliFase = "GetCausaliFase";
var subUrlAction_GetStatiDistintaBase = "GetStatiDistintaBase";
var subUrlAction_GetStatiBatch = "GetStatiBatch";
var subUrlAction_GetFasiFromBatch = "GetFasiFromBatch";
var subUrlAction_GetBatch = "GetBatch";
var subUrlAction_GetAziende = "GetAziende";

var subUrlAction_GetSfo = "GetSfo";
var subUrlAction_GetSfoString = "GetSfoOdp";
var subUrlAction_GetSfoFromMachine = "GetSfoFromMachine";
var subUrlAction_SetStateFase = "SetFase2";
var subUrlAction_SetEvents = "SetEvents";
var subUrlAction_SetBlock = "SetBlocks";
var subUrlAction_SetEvent_PiecesDiscard = "SetEvent_Pieces_Type";

var subUrlAction_SetStateBatch = "SetBatch2";
var subUrlAction_AddPiecesToBatch = "AddPiecesToBatch";
var subUrlAction_SendBarcode = "SendBarcode";
var subUrlAction_ReadBarcode = "ReadBarcode";
var customerName = "AreaMES";
var subUrlAction_GetTipoValori = "GetTipoValori";
var subUrlAction_Login = "Login";
var subUrlApi_Operatore = "operatore";

var subUrlAction_SuspendBatch = "SuspendBatch";

//variabili per gestione enum 
var codeDefaultjustified = "00_Default";
var eventType_ScrapPiece = "qta_pzsca";
var typeValue_Prod_Cau_Scarto = "prod_cau_scarto";
//var app = angular.module("AppMacchineRuntime", ['$rootScope', '$location', '$cookies', '$http', '$window', 'angular-growl', 'ngAnimate'])''angular-virtual-keyboard'
var app = angular.module("AppMacchineRuntime", ['angular-growl', 'ngAnimate', 'datatables', 'angular.chosen', 'ngCookies', 'treeControl', 'angular-virtual-keyboard'])
    .run(function ($window, $compile, $rootScope, $cookies, $http, $window) {

        /*
            * Function to 'Angular-fy' dynamically loaded content
            * by JQuery. This compiles the new html code and injects it
            * into the DOM so Angular 'knows' about the new code.
            */
        $window.angularfy = function (target, newHtml) {
            // must use JQuery to query for element by id.
            // must wrap as angular element otherwise 'scope' function is not defined.
            var targetScope = angular.element($(target)).scope();

            //        elem.replaceWith( $compile( newHtml )(elemScope)); Displays html wrong.
            //        elem.html( $compile( newHtml )(elemScope)); Does not work.
            $(target)['html']($compile(newHtml)(targetScope)); // Does work!
            targetScope.$apply();
        }

        $rootScope.globals = $cookies.getObject('MesAuth');
        if ($rootScope.globals != null) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line

        }
        var x = $window.location.href;
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if (x.indexOf('/login') == -1 && !$rootScope.globals) {
                $window.location.href = 'login.html';
            }
        });
    });


app.config(['growlProvider', function (growlProvider) {
    growlProvider.globalTimeToLive(8000);
    growlProvider.globalPosition('bottom-right');
}]);

//configurazione tastiera virtuale 
app.config(['VKI_CONFIG', function (VKI_CONFIG) {
    VKI_CONFIG.layout['Portugu\u00eas Brasileiro'] = {
        'name': "Portuguese (Brazil)", 'keys': [
            [["'", '"'], ["1", "!", "\u00b9"], ["2", "@", "\u00b2"], ["3", "#", "\u00b3"], ["4", "$", "\u00a3"], ["5", "%", "\u00a2"], ["6", "\u00a8", "\u00ac"], ["7", "&"], ["8", "*"], ["9", "("], ["0", ")"], ["-", "_"], ["=", "+", "\u00a7"], ["Canc", "Canc"]],
            [["Tab", "Tab"], ["q", "Q", "/"], ["w", "W", "?"], ["e", "E", "\u20ac"], ["r", "R"], ["t", "T"], ["y", "Y"], ["u", "U"], ["i", "I"], ["o", "O"], ["p", "P"], ["\u00b4", "`"], ["[", "{", "\u00aa"], ["Invio", "Invio"]],
            [["Caps", "Caps"], ["a", "A"], ["s", "S"], ["d", "D"], ["f", "F"], ["g", "G"], ["h", "H"], ["j", "J"], ["k", "K"], ["l", "L"], ["\u00e7", "\u00c7"], ["~", "^"], ["]", "}", "\u00ba"], ["/", "?"]],
            [["Shift", "Shift"], ["\\", "|"], ["z", "Z"], ["x", "X"], ["c", "C", "\u20a2"], ["v", "V"], ["b", "B"], ["n", "N"], ["m", "M"], [",", "<"], [".", ">"], [":", ":"], ["Shift", "Shift"]],
            [[" ", " ", " ", " "], ["AltGr", "AltGr"]]
        ], 'lang': ["pt-BR"]
    };

    VKI_CONFIG.layout.Numerico = {
        'name': "Numerico", 'keys': [
            [["1", '1'], ["2", "2"], ["3", "3"], ["Canc", "Bksp"]],
            [["4", "4"], ["5", "5"], ["6", '6'], ["Invio", "Enter"]],
            [["7", "7"], ["8", "8"], ["9", "9"], []],
            [["0", "0"], ["-"], ["+"], [","]]
        ], 'lang': ["pt-BR-num"]
    };

   //  CHANGE TEXT LANGUAGE 
     //VKI_CONFIG.i18n = {
     //	'00': "Exibir teclado numérico",
     //	'01': "Exibir teclado virtual",
     //	'02': "Selecionar layout do teclado",
     //	'03': "Teclas mortas",
     //	'04': "Ligado",
     //	'05': "Desligado",
     //	'06': "Fechar teclado",
     //	'07': "Limpar",
     //	'08': "Limpar campo",
     //	'09': "Versão",
     //	'10': "Diminuir tamanho do teclado",
     //	'11': "Aumentar tamanho do teclado"
     //};

     //VKI_CONFIG.relative = true;
     //VKI_CONFIG.sizeAdj = false;
    }])



function basicCrud_doTitle($scope, Enums) {

    switch ($scope.viewState) {
        case Enums.ViewState['R']:
            $scope.titolo = "Dettaglio " + $scope.currentItem.codice;
            break;
        case Enums.ViewState['U']:
            $scope.titolo = "Modifica " + $scope.currentItem.codice;
            break;
        case Enums.ViewState['C']:
            $scope.titolo = "Inserimento ";
            break;
    }
};


function basicCrud_doReload($scope, $http, subUrlApi) {
    $http.get(urlApi + '/' + subUrlApi)
        .success(function (data) {
            $scope.items = [];
            $scope.items.push.apply($scope.items, data);

            setTimeout(function () {
                if (($scope.dtInstance.dataTable) && ($scope.currentItem)) {
                    var o = $scope.dtInstance.dataTable.fnFindFullCellRowIndexes($scope.currentItem.id);
                    var $currentPage = Math.ceil(o[0] / $scope.dtInstance.dataTable.fnPagingInfo().iLength);
                    $scope.dtInstance.dataTable.fnPageChange($currentPage - 1);
                }
            }, 200);

        }).error(function (e) {
            console.log(e);
        });
}

function basicCrud_doConfirm($scope, $http, subUrlApi, Enums) {
    $http.post(urlApi + '/' + subUrlApi, $scope.currentItem)
        .success(function (data) {
            if ($scope.viewState === Enums.ViewState['C']) {
                $scope.currentItem.id = data;
                $scope.reload();
            }
            else if ($scope.viewState === Enums.ViewState['U']) {
                var elementPos = $scope.items.map(function (x) { return x.id; }).indexOf($scope.currentItem.id);
                $scope.items[elementPos] = $scope.currentItem;
            }

            $scope.viewState = Enums.ViewState['R'];
        })
        .error(function (e) {
            console.log(e);
            $scope.crudException(e)
        });
}

function basicCrud_doConfirmWithOutReload($scope, $http, subUrlApi, Enums) {
    $http.post(urlApi + '/' + subUrlApi, $scope.currentItem)
        .success(function (data) {
            if ($scope.viewState === Enums.ViewState['C']) {
                $scope.currentItem.id = data;
                //$scope.reload();
            }
            else if ($scope.viewState === Enums.ViewState['U']) {
                var elementPos = $scope.items.map(function (x) { return x.id; }).indexOf($scope.currentItem.id);
                $scope.items[elementPos] = $scope.currentItem;
            }

            //$scope.viewState = Enums.ViewState['R'];
        })
        .error(function (e) {
            console.log(e);
            $scope.crudException(e)
        });
}

function basicCrud_doShowItemDetail($scope, $http, subUrlApi, Enums, item) {
    if (($scope.currentItem == null) || (item.id !== $scope.currentItem.id)) {
        $http.get(urlApi + '/' + subUrlApi + '/' + item.id)
            .success(function (data) {
                $scope.currentItem = data;
                $scope.viewState = Enums.ViewState['R'];
            })
            .error(function (e) {
                console.log(e);
            });
    }
}

function basicCrud_doInitWatch($scope) {
    $scope.$watch('viewState', function (newValue, oldValue) {
        if (newValue === oldValue) return;
        $scope.title();
    });
    $scope.$watch('currentItem', function (newValue, oldValue) {
        if (newValue === oldValue) return;
        $scope.title();
    });
}

function basicCrud_doDelete($scope, $http, subUrlApi, Enums) {
    $http.delete(urlApi + '/' + subUrlApi, $scope.currentItem)
        .success(function (data) {
            $scope.reload();
            $scope.viewState = Enums.ViewState['R'];
            angular.element(document.querySelector('#modalConfirmDelete')).modal('hide');
        })
        .error(function (e) {
            console.log(e);
        });
}

