﻿
namespace AreaMes.Meta
{
    public interface IDoc
    {
        string Id { get; set; }
        int BsonSizeMb { get; set; }
        string ToString { get; }
    }

}
