﻿//using AreaM2M.RealTime.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Meta
{
    public interface IAreaM2MClient
    {
        Task<bool> SetValueAsync(string deviceId, string path, object value);

        Task<bool> SetValueAsync(string deviceId,  string[] path, object[] value);

        //Task<ValuesChangedResult> GetDeviceThingsAsync(string deviceId, string tokenId, string groupId);
    }
}
