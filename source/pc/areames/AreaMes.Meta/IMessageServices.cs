﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Meta
{
    public interface IMessageServices
    {
        //RegistrationInfo Subscribe(string messagename, Action<object, object> action);

        RegistrationInfo Subscribe<T>(string messagename, Action<object, T> action, SubscribeArgs subscribeInfo = null);

        RegistrationInfo Subscribe<T>(Action<object, T> action);

        void UnSubscribe(string messagename, long registrationid, Type type);

        void UnSubscribe(params RegistrationInfo[] infos);

        Task Publish<T>(string messagename, T item, object sender = null);

        Task<List<Exception>> PublishAndWaitAll<T>(string messagename, T item, object sender = null);
    }


    public class RegistrationInfo
    {
        public string MessageName { get; private set; }
        public long RegistrationId { get; private set; }
        public Type ArgumentType { get; private set; }

        public RegistrationInfo(string name, long id, Type argumenttype)
        {
            MessageName = name;
            RegistrationId = id;
            ArgumentType = argumenttype;
        }
    }

    public class SubscribeArgs
    {
        /// <summary>
        /// Valore numero che definisce la priorità. Se non valorizzato, viene impostato il valore di default.
        /// Priorità massima = 1
        /// Primorità minima (default) = 99
        /// </summary>
        public int Priority { get; set; } = DefaultPriority;

        /// <summary>
        /// Azione che permette di indicare se è necessario fermare la propagazione ai sottoscrittori successivi
        /// </summary>
        public Func<bool> CanStopPropagation { get; set; } = () => { return false; };

        public const int DefaultPriority = 99;
        public const int MaxPriority = 1;
        public const int LowPriority = DefaultPriority;


    }
}
