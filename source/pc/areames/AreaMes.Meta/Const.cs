﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Meta
{
    public static class Const
    {
        public static string AREAMES_MONGODB { get; set; }
        public static string AREAMES_MONGODB_NAME { get; set; }
        public static string AREAMES_APISERVER_IP { get; set; }
        public static string AREAMES_APISERVER_PORT { get; set; }
        public static string AREAMES_WEBSERVER_IP { get; set; }
        public static string AREAMES_WEBSERVER_TABLET_PORT { get; set; }
        public static string AREAMES_WEBSERVER_DESIGN_PORT { get; set; }
        public static string AREAMES_WEBSERVER_MULTIMEDIA_PORT { get; set; }
        public static string AREAMES_WEBSERVER_TABLET_PATH { get; set; }
        public static string AREAMES_WEBSERVER_DESIGN_PATH  { get; set; }
        public static string AREAMES_WEBSERVER_MULTIMEDIA_PATH { get; set; }
        public static string AREAM2M_SERVER_IP { get; set; }
        public static string AREAM2M_SERVER_PORT { get; set; }
        public static string AREAM2M_USERNAME { get; set; }
        public static string AREAM2M_PASSWORD { get; set; }
        public static string AREAM2M_PROJECT { get; set; }
        public static string AREAM2M_SUBSCRIBE_TO_DEVICES { get; set; }
        public static string AREAM2M_SUBSCRIBE_TO_DEVICES_SECURITY_CODE { get; set; }
        public static string AREAMES_REALTIMESERVER_PORT { get; set; }
        public static string AREAM2M_BATCH_LOGIC_DLL { get; set; }

        public static string AREAMES_MONGODB_PORT { get; set; }
    }
}
