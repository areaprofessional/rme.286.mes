﻿using AreaMes.Meta;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AreaMes.Server.Utils
{
    public class MessageServices : IMessageServices
    {
        private static Lazy<MessageServices> _service = new Lazy<MessageServices>(() => new MessageServices(), System.Threading.LazyThreadSafetyMode.ExecutionAndPublication);

        public static IMessageServices Instance { get { return _service.Value; } }

        private static long lastId = 0;
        private static Dictionary<string, Dictionary<Type, Dictionary<long, SubscribeInfoDelegate>>> _subscriptors = new Dictionary<string, Dictionary<Type, Dictionary<long, SubscribeInfoDelegate>>>();

        private MessageServices() { }

        //public RegistrationInfo Subscribe(string messagename, Action<object, object> action)
        //{
        //    return Subscribe<object>(messagename, action);
        //}

        /// <summary>
        /// Funzione per la sottoscrizione ai messaggi
        /// </summary>
        /// <typeparam name="T">Tipo del messaggio</typeparam>
        /// <param name="messagename">Nome/categoria del messaggio</param>
        /// <param name="action">Azione da eseguire alla ricezione del messaggio</param>
        /// <param name="subscribeArgs">Argomenti/ parametri di configurazioni aggiuntivi (opzionali)</param>
        /// <returns></returns>
        public RegistrationInfo Subscribe<T>(string messagename, Action<object, T> action, SubscribeArgs subscribeArgs = null)
        {
            lock (_subscriptors)
            {
                if (subscribeArgs == null) subscribeArgs = new SubscribeArgs();

                if (!_subscriptors.ContainsKey(messagename))
                    _subscriptors.Add(messagename, new Dictionary<Type, Dictionary<long, SubscribeInfoDelegate>>());

                var t = typeof(T);

                if (!_subscriptors[messagename].ContainsKey(t))
                    _subscriptors[messagename].Add(t, new Dictionary<long, SubscribeInfoDelegate>());

                var v = Interlocked.Increment(ref lastId);

                // sort by priority
                var subscriptorDics = _subscriptors[messagename][t];

                if (subscribeArgs.Priority != SubscribeArgs.DefaultPriority)
                {
                    var itemWithSamePriority = subscriptorDics.FirstOrDefault(o => o.Value.SubscribeInfo.Priority == subscribeArgs.Priority);
                    if (itemWithSamePriority.Value != null)
                        throw new Exception($"Cannot subscribe message because exist another subscriber with the same priority '{subscribeArgs.Priority}'.");
                }

                // add subscriber to dictionary
                subscriptorDics.Add(v, new SubscribeInfoDelegate { Delegate = action, SubscribeInfo = subscribeArgs });

                // re-sorted list and convert to dictionary
                reSortDictionary(ref subscriptorDics);

                return new RegistrationInfo(messagename, v, typeof(T));
            }
        }

        private void reSortDictionary(ref Dictionary<long, SubscribeInfoDelegate> item)
        {
            // ---------------------------------------------------------------------------
            // re-sorted list and convert to dictionary
            var listSorted = item.OrderBy(o => o.Value.SubscribeInfo.Priority).OrderBy(o => o.Key).ToList();
            item.Clear();
            foreach (var sItem in listSorted)
                item.Add(sItem.Key, sItem.Value);
            // ---------------------------------------------------------------------------


        }


        public RegistrationInfo Subscribe<T>(Action<object, T> action)
        {
            lock (_subscriptors)
            {
                var messagename = typeof(T).Name;
                return this.Subscribe<T>(messagename, action);

                // var messagename = typeof(T).Name;
                //if (!_subscriptors.ContainsKey(messagename))
                //    _subscriptors.Add(messagename, new Dictionary<Type, Dictionary<long, Delegate>>());

                //var t = typeof(T);

                //if (!_subscriptors[messagename].ContainsKey(t))
                //    _subscriptors[messagename].Add(t, new Dictionary<long, Delegate>());

                //var v = Interlocked.Increment(ref lastId);
                //_subscriptors[messagename][t].Add(v, action);

                //return new RegistrationInfo(messagename, v, typeof(T));
            }
        }

        public void UnSubscribe(string messagename, long registrationid, Type type)
        {
            lock (_subscriptors)
            {
                Dictionary<Type, Dictionary<long, SubscribeInfoDelegate>> m;
                _subscriptors.TryGetValue(messagename, out m);


                Dictionary<long, SubscribeInfoDelegate> t;
                if (m != null && m.TryGetValue(type, out t))
                {
                    if (t.ContainsKey(registrationid))
                        t.Remove(registrationid);

                    if (t.Count == 0)
                        m.Remove(type);

                    // re-sorted list and convert to dictionary
                    reSortDictionary(ref t);
                }

            }
        }
        public void UnSubscribe(params RegistrationInfo[] infos)
        {
            Interlocked.MemoryBarrier();

            foreach (var info in infos)
                UnSubscribe(info.MessageName, info.RegistrationId, info.ArgumentType);
        }


        public Task<List<Exception>> PublishAndWaitAll<T>(string messagename, T item, object sender = null)
        {
            bool waitForAllCompletion = true;
            var type = typeof(T);
            var listOfExc = new List<Exception>();
            var b = new TaskCompletionSource<bool>();
            bool resultAllCompletion = false;

            Dictionary<Type, Dictionary<long, SubscribeInfoDelegate>> m;
            Dictionary<Type, Dictionary<long, SubscribeInfoDelegate>> m2;

            _subscriptors.TryGetValue(messagename, out m);
            var m2IsValid = _subscriptors.TryGetValue(type.Name, out m2);

            if (m2IsValid && m2 != null)
            {


                Task.Run(() =>
                {
                    foreach (var s in m2.SelectMany(o => o.Value.Values))
                    {
                        try
                        {
                            s.Delegate.DynamicInvoke(sender, item);
                            var canStop = (bool)s.SubscribeInfo.CanStopPropagation.DynamicInvoke();
                            if (canStop) break;
                        }
                        catch (Exception exc)
                        {
                            listOfExc.Add(exc.InnerException);
                        }
                    }

                    b.SetResult(true);

                });

                resultAllCompletion = b.Task.Result;

            }

            b = new TaskCompletionSource<bool>();

            Dictionary<long, SubscribeInfoDelegate> t;
            if (m != null && m.TryGetValue(type, out t))
            {
                var subscriptors = new List<SubscribeInfoDelegate>();
                if (m2 != null)
                    subscriptors = t.Values.Except(m2?.SelectMany(o => o.Value.Values)).ToList();
                else
                    subscriptors = t.Values.ToList();

                Task.Run(() =>
                {
                    foreach (var s in subscriptors)
                    {
                        try
                        {
                            s.Delegate.DynamicInvoke(sender, item);
                            var canStop = (bool)s.SubscribeInfo.CanStopPropagation.DynamicInvoke();
                            if (canStop) break;
                        }
                        catch (Exception exc)
                        {
                            listOfExc.Add(exc.InnerException);
                        }

                    }

                    b.SetResult(true);
                });

                resultAllCompletion = b.Task.Result;
            }

            return Task.FromResult(listOfExc);
        }

        public Task Publish<T>(string messagename, T item, object sender = null)
        {
            var type = typeof(T);

            Dictionary<Type, Dictionary<long, SubscribeInfoDelegate>> m;
            Dictionary<Type, Dictionary<long, SubscribeInfoDelegate>> m2;

            _subscriptors.TryGetValue(messagename, out m);
            var m2IsValid = _subscriptors.TryGetValue(type.Name, out m2);

            if (m2IsValid && m2 != null)
            {
                Task.Run(() =>
                {
                    foreach (var s in m2.SelectMany(o => o.Value.Values))
                    {
                        s.Delegate.DynamicInvoke(sender, item);
                        var canStop = (bool)s.SubscribeInfo.CanStopPropagation.DynamicInvoke();
                        if (canStop) break;
                    }
                });
            }

            Dictionary<long, SubscribeInfoDelegate> t;
            if (m != null && m.TryGetValue(type, out t))
            {
                var subscriptors = new List<SubscribeInfoDelegate>();
                if (m2 != null)
                    subscriptors = t.Values.Except(m2?.SelectMany(o => o.Value.Values)).ToList();
                else
                    subscriptors = t.Values.ToList();

                Task.Run(() =>
                {
                    foreach (var s in subscriptors)
                    {
                        s.Delegate.DynamicInvoke(sender, item);
                        var canStop = (bool)s.SubscribeInfo.CanStopPropagation.DynamicInvoke();
                        if (canStop) break;
                    }
                });
            }




            return Task.FromResult<object>(null);
        }

        private class SubscribeInfoDelegate
        {
            public SubscribeArgs SubscribeInfo { get; set; }
            public Delegate Delegate { get; set; }
        }


    }



}
