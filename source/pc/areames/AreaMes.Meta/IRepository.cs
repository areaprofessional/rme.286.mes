﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace AreaMes.Meta
{
    public interface IRepository : IDisposable
    {
        Task<T> GetAsync<T>(string id) where T : IDoc, new();
        T Get<T>(string id) where T : IDoc, new();
        Task<List<T>> All<T>(string[] excludeFields = null, string[] includeFields = null) where T : IDoc, new();
        Task<List<T>> All<T>(string[] excludeFields, string[] includeFields, int page, int pageSize) where T : IDoc, new();
        Task<string> SetAsync<T>(T item) where T : IDoc, new();
        Task<List<T>> GetWhereAsync<T>(Expression<Func<T, bool>> filterSpecification) where T : IDoc, new();
        Task<bool> DeleteAsync<T>(string id) where T : IDoc, new();
        bool Delete<T>(string id) where T : IDoc, new();
    }
}
