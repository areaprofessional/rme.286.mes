﻿using AreaFramework.Wrapper.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.ArcaConnector.Entites
{
    [AreaMapper(TableName = "PRRisorsa")]
    public class Arca_Macchine : AreaFramework.Wrapper.Entity.AreaEntity
    {
        [AreaMapper(ColumnName = "Cd_PrRisorsa", IsPrimaryKey = true)]
        public string Id_Macchina { get; set; }

        [AreaMapper(ColumnName = "Descrizione")]
        public string Nome { get; set; }

        [AreaMapper(ColumnName = "Cd_PRReparto")]
        public string Cd_PRReparto { get; set; }

        [AreaMapper(ColumnName = "TimeUpd")]
        [Range(typeof(DateTime), "1/1/1900", "6/6/2079")]
        public DateTime UltimoAggionamento { get; set; }

        [AreaMapper(ColumnName = "Deleted")]
        public bool Eliminato { get; set; }
    }

}
