﻿using AreaFramework.Wrapper.Entity;
using AreaMes.Meta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.ArcaConnector.Entites
{
    [AreaMapper(TableName = "MesProdVersamenti")]
    public class Arca_MesProdVersamenti : AreaFramework.Wrapper.Entity.AreaEntity, IDoc
    {
        public string Id { get; set; }
        public int BsonSizeMb { get; set; }

        [AreaMapper(ColumnName = "Id_PrOL")]
        public int Id_PrOL { get; set; }

        [AreaMapper(ColumnName = "Id_PrOLAttivita")]
        public int Id_PrOLAttivita { get; set; }

        [AreaMapper(ColumnName = "Id_PrBL")]
        public int Id_PrBL { get; set; }

        [AreaMapper(ColumnName = "Id_PRBLAttivita")]
        public int Id_PRBLAttivita { get; set; }

        [AreaMapper(ColumnName = "Cd_DoSottoCommessa")]
        public string Cd_DoSottoCommessa { get; set; }

        [AreaMapper(ColumnName = "Cd_PrRisorsa")]
        public string Cd_PrRisorsa { get; set; }

        [AreaMapper(ColumnName = "Cd_Operatore")]
        public string Cd_Operatore { get; set; }

        [AreaMapper(ColumnName = "DataVersamento")]
        public DateTime DataVersamento { get; set; }

        [AreaMapper(ColumnName = "Id_Versamento", IsPrimaryKey = true)]
        public Guid Id_Versamento { get; set; }

        [AreaMapper(ColumnName = "QtaVersamentoBuono")]
        public int QtaVersamentoBuono { get; set; }

        [AreaMapper(ColumnName = "QtaVersamentoScarto")]
        public int QtaVersamentoScarto { get; set; }

        [AreaMapper(ColumnName = "CodiceScarto")]
        public string CodiceScarto { get; set; }

        [AreaMapper(ColumnName = "UltimoVersamento")]
        public bool UltimoVersamento { get; set; }

        public new string ToString
        {
            get { return Id_PrOL.ToString(); }
        }
    }

}
