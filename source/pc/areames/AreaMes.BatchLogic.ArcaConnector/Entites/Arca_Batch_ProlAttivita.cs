﻿using AreaFramework.Wrapper.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.ArcaConnector.Entites
{

     [AreaMapper(TableName = "PROLAttivita")]
    public class Arca_Batch_ProlAttivita : AreaFramework.Wrapper.Entity.AreaEntity
    {
        [AreaMapper(ColumnName = "Id_PrOL")]
        public int Id_Batch { get; set; }

        [AreaMapper(ColumnName = "Id_PrOLAttivita", IsPrimaryKey = true)]
        public int Id_Attivita_Batch { get; set; }

        [AreaMapper(ColumnName = "Cd_PrAttivita")]
        public string Attivita_Batch { get; set; }

        [AreaMapper(ColumnName = "Cd_PrRisorsa")]
        public string IdMacchina_Batch { get; set; }

        [AreaMapper(ColumnName = "AttivitaEsterna")]
        public bool AttivitaEsterna_Batch { get; set; }

        [AreaMapper(ColumnName = "Descrizione")]
        public string DescrizioneAttivita_Batch { get; set; }

        [AreaMapper(ColumnName = "Articolo")]
        public string DescrizioneArticolo_Batch { get; set; }

        [AreaMapper(ColumnName = "Quantita")]
        public int Quantita_Batch { get; set; }

        [AreaMapper(ColumnName = "TimeUpd")]
        [Range(typeof(DateTime), "1/1/1900", "6/6/2079")]
        public DateTime UltimoAggionamento { get; set; }

        [AreaMapper(ColumnName = "Deleted")]
        public bool Eliminato { get; set; }

        [AreaMapper(ColumnName = "Id_DBFase")]
        public int Id_DBFase { get; set; }
    }
}
