﻿using AreaFramework.Wrapper.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.ArcaConnector.Entites
{

        [AreaMapper(TableName = "PRBL")]
    public class Arca_Batch_Prbl : AreaFramework.Wrapper.Entity.AreaEntity
    {
        [AreaMapper(ColumnName = "Id_PrBL", IsPrimaryKey = true)]
        public int Id_Attivita_Batch { get; set; }

        [AreaMapper(ColumnName = "Anno")]
        public int Anno_Batch { get; set; }

        [AreaMapper(ColumnName = "Numero")]
        public int N_Ordine_Batch { get; set; }

        [AreaMapper(ColumnName = "Cd_PrRisorsa")]
        public string RisorsaAss_Batch { get; set; }

        [AreaMapper(ColumnName = "TimeUpd")]
        [Range(typeof(DateTime), "1/1/1900", "6/6/2079")]
        public DateTime UltimoAggionamento { get; set; }

        [AreaMapper(ColumnName = "Deleted")]
        public bool Eliminato { get; set; }

    }
}
