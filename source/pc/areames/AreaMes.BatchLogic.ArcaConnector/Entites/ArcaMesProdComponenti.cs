﻿using AreaFramework.Wrapper.Entity;
using AreaMes.Meta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.ArcaConnector.Entites
{
    [AreaMapper(TableName = "MesProdComponenti")]
    public class Arca_MesProdComponenti : AreaFramework.Wrapper.Entity.AreaEntity, IDoc
    {
        public string Id { get; set; }
        public int BsonSizeMb { get; set; }

        [AreaMapper(ColumnName = "Id_PrOL")]
        public int Id_PrOL { get; set; }

        [AreaMapper(ColumnName = "Id_PrOLAttivita")]
        public int Id_PrOLAttivita { get; set; }

        [AreaMapper(ColumnName = "Id_PrBL")]
        public int Id_PrBL { get; set; }

        [AreaMapper(ColumnName = "Id_PRBLAttivita")]
        public int Id_PRBLAttivita { get; set; }

        [AreaMapper(ColumnName = "Cd_AR")]
        public string Cd_AR { get; set; }

        [AreaMapper(ColumnName = "Cd_ARMisura")]
        public string Cd_ARMisura { get; set; }

        [AreaMapper(ColumnName = "Cd_DoSottoCommessa")]
        public string Cd_DoSottoCommessa { get; set; }

        [AreaMapper(ColumnName = "Cd_PrRisorsa")]
        public string Cd_PrRisorsa { get; set; }

        [AreaMapper(ColumnName = "Id_PrOLMateriale")]
        public int Id_PrOLMateriale { get; set; }

        [AreaMapper(ColumnName = "Id_PrBLMateriale")]
        public int Id_PrBLMateriale { get; set; }

        [AreaMapper(ColumnName = "DataConsumo")]
        public DateTime DataConsumo { get; set; }

        [AreaMapper(ColumnName = "Id_Versamento", IsPrimaryKey = true)]
        public Guid Id_Versamento { get; set; }

        [AreaMapper(ColumnName = "QtaConsumo")]
        public double QtaConsumo { get; set; }

        public new string ToString
        {
            get { return Id_PrOL.ToString(); }
        }

        //[AreaMapper(ColumnName = "CheckCambioMateriale")]
        //public bool CheckCambioMateriale { get; set; }

    }
}
