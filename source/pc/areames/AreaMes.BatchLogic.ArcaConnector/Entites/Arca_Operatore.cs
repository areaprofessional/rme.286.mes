﻿using AreaFramework.Wrapper.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.ArcaConnector.Entites
{
    [AreaMapper(TableName = "Operatore")]
    public class Arca_Operatore : AreaFramework.Wrapper.Entity.AreaEntity
    {
        [AreaMapper(ColumnName = "Id_Operatore", IsPrimaryKey = true)]
        public int Id_Operatore { get; set; }

        [AreaMapper(ColumnName = "Cd_Operatore")]
        public string Cd_Operatore { get; set; }

        [AreaMapper(ColumnName = "Nome")]
        public string Nome { get; set; }

        [AreaMapper(ColumnName = "Cognome")]
        public string Cognome { get; set; }

        [AreaMapper(ColumnName = "TimeUpd")]
        [Range(typeof(DateTime), "1/1/1900", "6/6/2079")]
        public  DateTime UltimoAggionamento { get; set; }

        [AreaMapper(ColumnName = "Deleted")]
        public bool Eliminato { get; set; }

    }
}
