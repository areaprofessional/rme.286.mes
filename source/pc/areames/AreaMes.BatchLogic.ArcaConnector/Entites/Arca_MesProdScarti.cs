﻿using AreaFramework.Wrapper.Entity;
using AreaMes.Meta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.ArcaConnector.Entites
{
    [AreaMapper(TableName = "MesProdScarti")]
    public class Arca_MesProdScarti : AreaFramework.Wrapper.Entity.AreaEntity, IDoc
    {
            public string Id { get; set; }
        public int BsonSizeMb { get; set; }

        [AreaMapper(ColumnName = "Id_Scarto", IsPrimaryKey = true,IsAutoIncrement =true)]
            public int Id_Scarto { get; set; }

            [AreaMapper(ColumnName = "Id_Versamento")]
            public Guid Id_Versamento { get; set; }

            [AreaMapper(ColumnName = "Codice")]
            public string Codice { get; set; }

            [AreaMapper(ColumnName = "Descrizione")]
            public string Descrizione { get; set; }

            [AreaMapper(ColumnName = "Qta")]
            public int Qta { get; set; }

            [AreaMapper(ColumnName = "Data")]
            public DateTime Data { get; set; }

            [AreaMapper(ColumnName = "Cd_Operatore")]
            public string Cd_Operatore { get; set; }

        public new string ToString
        {
            get { return Id_Scarto.ToString(); }
        }

    }
}
