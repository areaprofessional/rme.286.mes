﻿using AreaFramework.Wrapper.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.ArcaConnector.Entites
{


    [AreaMapper(TableName = "PROL")]
    public class Arca_Batch_Prol : AreaFramework.Wrapper.Entity.AreaEntity
    {

        [AreaMapper(ColumnName = "Id_PrOL", IsPrimaryKey = true)]
        public int Id_Batch { get; set; }

        [AreaMapper(ColumnName = "Anno")]
        public int Anno_Batch { get; set; }

        [AreaMapper(ColumnName = "Numero")]
        public int N_Ordine_Batch { get; set; }

        [AreaMapper(ColumnName = "Cd_AR")]
        public string CodiceArticolo_Batch { get; set; }

        [AreaMapper(ColumnName = "NotePrOL")]
        public string Note_Batch { get; set; }

        [AreaMapper(ColumnName = "Cd_DoSottoCommessa")]
        public string Commessa { get; set; }

        [AreaMapper(ColumnName = "DescrizioneCasaMotociclistica")]
        public string DescrizioneCasaMotociclistica { get; set; }

        [AreaMapper(ColumnName = "ModelloMoto")]
        public string ModelloMoto { get; set; }

        [AreaMapper(ColumnName = "VersioneMoto")]
        public string VersioneMoto { get; set; }

        [AreaMapper(ColumnName = "AnnoInizioMoto")]
        public int AnnoInizioMoto { get; set; }

        [AreaMapper(ColumnName = "AnnoFineMoto")]
        public int AnnoFineMoto { get; set; }

        [AreaMapper(ColumnName = "Cd_CF")]
        public string CodiceCliente_Batch { get; set; }

        [AreaMapper(ColumnName = "CFDesc")]
        public string DescrizioneCliente_Batch { get; set; }

        [AreaMapper(ColumnName = "TimeUpd")]
        [Range(typeof(DateTime), "1/1/1900", "6/6/2079")]
        public DateTime UltimoAggionamento { get; set; }

        [AreaMapper(ColumnName = "Deleted")]
        public bool Eliminato { get; set; }

        [AreaMapper(ColumnName = "Prodotto")]
        public bool Chiuso { get; set; }

        [AreaMapper(ColumnName = "DataObiettivo")]
        [Range(typeof(DateTime), "1/1/1900", "6/6/2079")]
        public DateTime DataObiettivo { get; set; }
        
    }
}
