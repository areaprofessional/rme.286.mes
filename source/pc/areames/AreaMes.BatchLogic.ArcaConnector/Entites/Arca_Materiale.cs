﻿using AreaFramework.Wrapper.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.ArcaConnector.Entites
{
    [AreaMapper(TableName = "AR")]
   public class Arca_Materiale : AreaFramework.Wrapper.Entity.AreaEntity
    {

        [AreaMapper(ColumnName = "Id_AR", IsPrimaryKey = true)]
        public int Id_Materiale { get; set; }

        [AreaMapper(ColumnName = "Cd_AR")]
        public string Nome { get; set; }

        [AreaMapper(ColumnName = "Descrizione")]
        public string Descrizione { get; set; }

        [AreaMapper(ColumnName = "Cd_ARGruppo1")]
        public string Cd_ARGruppo1 { get; set; }

        [AreaMapper(ColumnName = "TimeUpd")]
        [Range(typeof(DateTime), "1/1/1900", "6/6/2079")]
        public DateTime UltimoAggionamento { get; set; }

        [AreaMapper(ColumnName = "Deleted")]
        public bool Eliminato { get; set; }
    }

}
