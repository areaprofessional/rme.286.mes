﻿using AreaFramework.Wrapper.Entity;
using AreaMes.Meta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.ArcaConnector.Entites
{
    [AreaMapper(TableName = "MesProdTempi")]
    public class Arca_MesProdTempi : AreaFramework.Wrapper.Entity.AreaEntity, IDoc
    {
        public string Id { get; set; }
        public int BsonSizeMb { get; set; }

        [AreaMapper(ColumnName = "Id_PrOL")]
        public int Id_PrOL { get; set; }

        [AreaMapper(ColumnName = "Id_PrOLAttivita", IsPrimaryKey = true)]
        public int Id_PrOLAttivita { get; set; }

        [AreaMapper(ColumnName = "Id_PrBL")]
        public int Id_PrBL { get; set; }

        [AreaMapper(ColumnName = "Id_PRBLAttivita")]
        public int Id_PRBLAttivita { get; set; }

        [AreaMapper(ColumnName = "Id_Versamento")]
        public Guid Id_Versamento { get; set; }

        [AreaMapper(ColumnName = "Cd_DoSottoCommessa")]
        public string Cd_DoSottoCommessa { get; set; }

        [AreaMapper(ColumnName = "Cd_PrRisorsa")]
        public string Cd_PrRisorsa { get; set; }

        [AreaMapper(ColumnName = "TotaleTempoEsecuzione")]
        public int TotaleTempoEsecuzione { get; set; }

        [AreaMapper(ColumnName = "TotaleTempoFermi")]
        public int TotaleTempoFermi { get; set; }


        public new string ToString
        {
            get { return Id_PrOL.ToString(); }
        }

    }
}

