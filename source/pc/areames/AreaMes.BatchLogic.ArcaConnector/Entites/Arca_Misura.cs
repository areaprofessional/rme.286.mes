﻿using AreaFramework.Wrapper.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.ArcaConnector.Entites
{
        [AreaMapper(TableName = "ARMisura")]
        public class Arca_Misura : AreaFramework.Wrapper.Entity.AreaEntity {

        [AreaMapper(ColumnName = "Id_ARMisura", IsPrimaryKey = true)]
        public int Id_Misura { get; set; }

        [AreaMapper(ColumnName = "Cd_ARMisura")]
        public string Codice { get; set; }

        [AreaMapper(ColumnName = "Descrizione")]
        public string Descrizione { get; set; }

        [AreaMapper(ColumnName = "TimeUpd")]
        [Range(typeof(DateTime), "1/1/1900", "6/6/2079")]
        public DateTime UltimoAggionamento { get; set; }

        [AreaMapper(ColumnName = "Deleted")]
        public bool Eliminato { get; set; }
    }
}
