﻿using AreaFramework.Wrapper.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.ArcaConnector.Entites
{
    [AreaMapper(TableName = "xTableUpdated")]
    public class Arca_TableUpdated : AreaFramework.Wrapper.Entity.AreaEntity
    {
        [AreaMapper(ColumnName = "Id_xTableUpdated", IsPrimaryKey = true)]
        public int Id { get; set; }

        [AreaMapper(ColumnName = "NameTableUpdated")]
        public string Nome { get; set; }

    }
}
