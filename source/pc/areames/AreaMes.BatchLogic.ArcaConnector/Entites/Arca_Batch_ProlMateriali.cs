﻿using AreaFramework.Wrapper.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AreaMes.BatchLogic.ArcaConnector.Entites
{

    [AreaMapper(TableName = "PROLMateriale")]
    public class Arca_Batch_ProlMateriali : AreaFramework.Wrapper.Entity.AreaEntity
    {

        [AreaMapper(ColumnName = "Id_PrOLAttivita")]
        public int Id_PrOLAttivita { get; set; }

        [AreaMapper(ColumnName = "Id_PrOLMateriale", IsPrimaryKey = true)]
        public int Id_PrOLMateriale { get; set; }

        [AreaMapper(ColumnName = "Consumo")]
        public double Consumo { get; set; }

        [AreaMapper(ColumnName = "Cd_AR")]
        public string Cd_AR { get; set; }

        [AreaMapper(ColumnName = "Cd_ARMisura")]
        public string Cd_ARMisura { get; set; }

        [AreaMapper(ColumnName = "Cd_ARGruppo1")]
        public string Cd_ARGruppo1 { get; set; }

        [AreaMapper(ColumnName = "TimeUpd")]
        [Range(typeof(DateTime), "1/1/1900", "6/6/2079")]
        public DateTime TimeUpd { get; set; }

        [AreaMapper(ColumnName = "Deleted")]
        public bool Deleted { get; set; }
    }
}
