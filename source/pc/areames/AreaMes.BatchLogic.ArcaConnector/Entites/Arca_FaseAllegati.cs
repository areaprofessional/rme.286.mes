﻿using AreaFramework.Wrapper.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.ArcaConnector.Entites
{
    [AreaMapper(TableName = "xDBFaseAllegati")]
    public class Arca_FaseAllegati : AreaFramework.Wrapper.Entity.AreaEntity
    {

        [AreaMapper(ColumnName = "Id_xDBFaseAllegati", IsPrimaryKey = true)]
        public int Id_xDBFaseAllegati { get; set; }

        [AreaMapper(ColumnName = "Id_DBFase")]
        public int Id_DBFase { get; set; }

        [AreaMapper(ColumnName = "Path")]
        public string Path { get; set; }

    }
}
