﻿using AreaFramework.Wrapper.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.ArcaConnector.Entites
{

    [AreaMapper(TableName = "PRBLMateriale")]
    public class Arca_Batch_PrblMateriali : AreaFramework.Wrapper.Entity.AreaEntity
    {
        [AreaMapper(ColumnName = "Id_PrOLAttivita", IsPrimaryKey = true)]
        public int Id_Attivita_Batch { get; set; }

        [AreaMapper(ColumnName = "Cd_AR")]
        public string CodiceArticolo_Batch { get; set; }

        [AreaMapper(ColumnName = "TimeUpd")]
        [Range(typeof(DateTime), "1/1/1900", "6/6/2079")]
        public DateTime UltimoAggionamento { get; set; }

        [AreaMapper(ColumnName = "Deleted")]
        public bool Eliminato { get; set; }
    }
}
