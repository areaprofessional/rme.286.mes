﻿using AreaMes.BatchLogic.ArcaConnector.Entites;
using AreaMes.BatchLogic.ArcaConnector.Utility;
using AreaMes.Model;
using AreaMes.Meta;
using AreaMes.Server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using AreaMes.Server.MongoDb;
using AreaMes.Model.Runtime;
using AreaMes.Server;
using log4net;
using System.IO;
using System.Diagnostics;

namespace AreaMes.BatchLogic.ArcaConnector
{
    public class ArcaConnectorManager
    {
        ILog logger;
        private static ArcaConnectorManager instance = new ArcaConnectorManager();
        public static ArcaConnectorManager Instance { get { return instance; } }
        private System.Threading.Timer _timer;
        private bool _timerRunning = false;
        private bool _runningImportBatch = false;

        private object _timerLock = new object();
        private object _timerLockImportBatch = new object();
        private int _intervalTickRead;
        private DbManager _dbManager;
        private MongoRepository mongoInstanceGlobal;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intervalTickRead">Frequenza di aggiornamento, espressa i minuti</param>
        /// <param name="arca_connectionString">Stringa di connessione verso DB ARCA</param>
        public void Config(int intervalTickRead, string arca_connectionString)
        {
            try
            {
                Logger.ArcaConnector.Info("Start configuration...");
                Logger.ArcaConnector.Info($"    intervalTickRead:{intervalTickRead}");
                Logger.ArcaConnector.Info($"    connectionstring:{arca_connectionString}");



                _intervalTickRead = intervalTickRead;

                // --------------------------------------------------------------------------------------------------------
                // Broadcast MESSAGE : Cambio stati batch
                // --------------------------------------------------------------------------------------------------------

                MessageServices.Instance.Subscribe<FaseChangedInfo>(async (object sender, FaseChangedInfo message) =>
                {

                    try
                    {
                        Batch batchCompleteMdb = sender as Batch;
                        if (message.Current.Stato == Model.Enums.StatoFasi.Terminato)
                        {
                            var mongoInstanceLocal = MongoRepository.New;


                            Logger.ArcaConnector.InfoFormat("Phase {0} Odp: {1} is Completed", message.Current.NumeroFase, batchCompleteMdb.OdP);
                            Logger.ArcaConnector.InfoFormat("Start  Save  MES Time Phase {0}", message.Current.NumeroFase);
                            Logger.ArcaConnector.InfoFormat("Start  Create Code Guid ");
                            Guid idVersamento = Guid.NewGuid();
                            Logger.ArcaConnector.InfoFormat("Start  Create Code Guid  Completed Code: {0}", idVersamento.ToString());
                            //var batchCompleteMdb = BatchManager.Instance.Get(idBatch);// await mongoInstance.GetAsync<Batch>(batchId);

                            //---------------------------------------------------------------------------------------------------------------------------------------------
                            //---------------------------------------------------------versamento TEMPI
                            //---------------------------------------------------------------------------------------------------------------------------------------------
                            var mes_ProdTempi = new Arca_MesProdTempi();
                            mes_ProdTempi.Id_Versamento = idVersamento;
                            mes_ProdTempi.Id_PrOL = Convert.ToInt32(batchCompleteMdb.OdP);
                            var fase = batchCompleteMdb.DistintaBase.Fasi.FirstOrDefault(x => x.NumeroFase == message.Current.NumeroFase);
                            mes_ProdTempi.Id_PrOLAttivita = Convert.ToInt32(message.Current.CodiceFase);
                            mes_ProdTempi.Id_PrBL = Convert.ToInt32(fase?.Campo4);
                            mes_ProdTempi.Id_PRBLAttivita = Convert.ToInt32(fase?.Campo3);
                            Logger.ArcaConnector.InfoFormat("Start  GetTimeAlarmCalculate  Phase {0}", message.Current.NumeroFase);
                            mes_ProdTempi.TotaleTempoFermi = WorkingCalculate.GetTimeAlarmCalculate(batchCompleteMdb, message.Current.NumeroFase);
                            Logger.ArcaConnector.InfoFormat("Start  GetTimeAlarmCalculate  Phase {0} Completed", message.Current.NumeroFase);
                            Logger.ArcaConnector.InfoFormat("Start  GetTimeTotalCalculate  Phase {0}", message.Current.NumeroFase);
                            var dataInizio = batchCompleteMdb.BatchAvanzamenti.OrderBy(x => x.Inizio).FirstOrDefault(x => x.NumeroFase == message.Current.NumeroFase).Inizio;
                            mes_ProdTempi.TotaleTempoEsecuzione = Convert.ToInt32((DateTime.Now - Convert.ToDateTime(dataInizio ?? message.Current.Inizio)).TotalSeconds);
                            //mes_ProdTempi.Id_OrdineMes = Convert.ToInt32(batchCompleteMdb.OdP);
                            Logger.ArcaConnector.InfoFormat("Start  GetTimeTotalCalculate  Phase {0} Completed", message.Current.NumeroFase);
                            mes_ProdTempi.Cd_PrRisorsa = batchCompleteMdb.Fasi.FirstOrDefault(x => x.NumeroFase == message.Current.NumeroFase).Macchine[0].Macchina.Codice;
                            mes_ProdTempi.Uniquefy();
                            Logger.ArcaConnector.InfoFormat("Phase {0} Odp: {1} Save into MongoDb", message.Current.NumeroFase, batchCompleteMdb.OdP);
                            await mongoInstanceLocal.SetAsync(mes_ProdTempi);
                            Logger.ArcaConnector.InfoFormat("Phase {0} Odp: {1} Save into MongoDb Completed", message.Current.NumeroFase, batchCompleteMdb.OdP);
                            Logger.ArcaConnector.InfoFormat("Completed  Save  MES Time Phase {0} Odp {1}", message.Current.NumeroFase, batchCompleteMdb.OdP);



                            //---------------------------------------------------------------------------------------------------------------------------------------------
                            //---------------------------------------------------------versamento SCARTI
                            //---------------------------------------------------------------------------------------------------------------------------------------------
                            Logger.ArcaConnector.InfoFormat("Start  Save  MES Pieces Phase {0}", message.Current.NumeroFase);
                            int pezziScarto = 0;
                            Logger.ArcaConnector.InfoFormat("Search events into Mongobd {0}", message.Current.NumeroFase);
                            var eventi = batchCompleteMdb.Eventi.Where(x => x.TipoEvento == Model.Enums.TipoEvento.qta_pzsca && x.FaseId == message.Current.NumeroFase);
                            Logger.ArcaConnector.InfoFormat("Search events into Mongobd {0} Completed", message.Current.NumeroFase);
                            foreach (var evento in eventi)
                            {
                                try
                                {
                                    pezziScarto = pezziScarto + Convert.ToInt32(evento.Valore);
                                    var mesProdScarti = new Arca_MesProdScarti();
                                    mesProdScarti.Id_Versamento = idVersamento;
                                    mesProdScarti.Codice = evento.TipoValore.Item.Codice;
                                    mesProdScarti.Descrizione = evento.TipoValore.Item.Descrizione;
                                    mesProdScarti.Data = Convert.ToDateTime(evento.OraEvento);
                                    mesProdScarti.Qta = Convert.ToInt32(evento.Valore);
                                    mesProdScarti.Cd_Operatore = evento.Operatore.Item.Codice;
                                    mesProdScarti.Uniquefy();
                                    Logger.ArcaConnector.InfoFormat("Completed  Save  MES Scarti Phase {0} Odp {1}", message.Current.NumeroFase, batchCompleteMdb.OdP);
                                    await mongoInstanceLocal.SetAsync(mesProdScarti);
                                    Logger.ArcaConnector.InfoFormat("Completed  Save  MES Scarti Pieces {0} Odp {1}", message.Current.NumeroFase, batchCompleteMdb.OdP);
                                }
                                catch (Exception exc)
                                {
                                    Logger.ArcaConnector.Error("Error insert Scarti {0}", exc);
                                }

                            }



                            if (batchCompleteMdb.Stato == Model.Enums.StatoBatch.Terminato)
                            {
                                //---------------------------------------------------------------------------------------------------------------------------------------------
                                //---------------------------------------------------------versamento pezzi
                                //---------------------------------------------------------------------------------------------------------------------------------------------
                                var mesProdVersamenti = new Arca_MesProdVersamenti();
                                mesProdVersamenti.Id_Versamento = idVersamento;
                                mesProdVersamenti.Id_PrOL = Convert.ToInt32(batchCompleteMdb.OdP);
                                mesProdVersamenti.Id_PrOLAttivita = Convert.ToInt32(message.Current.CodiceFase);
                                mesProdVersamenti.Id_PrBL = Convert.ToInt32(fase?.Campo4);
                                mesProdVersamenti.Id_PRBLAttivita = Convert.ToInt32(fase?.Campo3);
                                mesProdVersamenti.Cd_PrRisorsa = batchCompleteMdb.Fasi.FirstOrDefault(x => x.NumeroFase == message.Current.NumeroFase).Macchine[0].Macchina.Codice;
                                mesProdVersamenti.QtaVersamentoBuono = batchCompleteMdb.Fasi.FirstOrDefault(x => x.NumeroFase == message.Current.NumeroFase).PezziProdotti;
                                mesProdVersamenti.QtaVersamentoScarto = pezziScarto;
                                mesProdVersamenti.DataVersamento = DateTime.Now;
                                mesProdVersamenti.Cd_Operatore = message.Username.ToString();
                                mesProdVersamenti.UltimoVersamento = true;

                                Logger.ArcaConnector.Debug("import_MES_Versamenti    Reading from Arca...");


                                Logger.ArcaConnector.Debug("import_MES_Versamenti    Reading from Arca... completed");



                                //---------------------------------------------------------------------------------------------------------------------------------------------
                                //---------------------------------------------------------Versamento 
                                //---------------------------------------------------------------------------------------------------------------------------------------------

                                Logger.ArcaConnector.InfoFormat("Phase {0} Odp: {1} Save into MongoDb", message.Current.NumeroFase, batchCompleteMdb.OdP);
                                mesProdVersamenti.Uniquefy();
                                Logger.ArcaConnector.InfoFormat("Phase {0} Odp: {1} Save into MongoDb Completed", message.Current.NumeroFase, batchCompleteMdb.OdP);
                                Logger.ArcaConnector.InfoFormat("Completed  Save  MES Time Phase {0} Odp {1}", message.Current.NumeroFase, batchCompleteMdb.OdP);
                                if (mesProdVersamenti.QtaVersamentoBuono > 0)
                                    await mongoInstanceLocal.SetAsync(mesProdVersamenti);
                                Logger.ArcaConnector.InfoFormat("Completed  Save  MES Time Pieces {0} Odp {1}", message.Current.NumeroFase, batchCompleteMdb.OdP);



                            }
                            //---------------------------------------------------------------------------------------------------------------------------------------------
                            //---------------------------------------------------------Prelievo COMPONENTI
                            //---------------------------------------------------------------------------------------------------------------------------------------------
                            //filtro se effettuare il prelievo delle materie prime (000) 

                            if (fase?.Componenti != null)
                            {
                                var componenteDistinta = fase?.Componenti.FirstOrDefault(x => x.Descrizione == "MAT");
                                if (componenteDistinta != null)
                                {
                                    //creazione del prelievo 
                                    var mesProdComponenti = new Arca_MesProdComponenti();
                                    mesProdComponenti.Id_Versamento = idVersamento;
                                    mesProdComponenti.Id_PrOL = Convert.ToInt32(batchCompleteMdb.OdP);
                                    mesProdComponenti.Id_PrOLAttivita = Convert.ToInt32(message.Current.CodiceFase);
                                    mesProdComponenti.Id_PrBL = Convert.ToInt32(fase?.Campo4);
                                    mesProdComponenti.Id_PRBLAttivita = Convert.ToInt32(fase?.Campo3);
                                    mesProdComponenti.Cd_PrRisorsa = batchCompleteMdb.Fasi.FirstOrDefault(x => x.NumeroFase == message.Current.NumeroFase).Macchine[0].Macchina.Codice;
                                    mesProdComponenti.Id_PrOLMateriale = Convert.ToInt32(componenteDistinta.Lotto);
                                    mesProdComponenti.DataConsumo = DateTime.Now;
                                    mesProdComponenti.QtaConsumo = Convert.ToDouble(componenteDistinta.Qta);
                                    mesProdComponenti.Cd_ARMisura = componenteDistinta.UnitaDiMisura;
                                    mesProdComponenti.Cd_AR = componenteDistinta.CodiceMateriale;

                                    //controllare se è stato effettuato un versamento parziale ed eventualmente filtro i prelievi successivi
                                    Logger.ArcaConnector.Debug("import_MES_Componenti    Reading from Arca...");
                                    var listOfComponentiSql = _dbManager.Get<Arca_MesProdComponenti>().OrderByDescending(x => x.DataConsumo).FirstOrDefault(x => x.Id_PrOL == Convert.ToInt32(batchCompleteMdb.OdP) && x.Id_PrOLAttivita == Convert.ToInt32(message.Current.CodiceFase) && x.Id_PrBL == Convert.ToInt32(fase?.Campo4) && x.Id_PRBLAttivita == Convert.ToInt32(fase?.Campo3));
                                    var listOfComponentiMongo = mongoInstanceLocal.All<Arca_MesProdComponenti>().Result.OrderByDescending(x => x.DataConsumo).FirstOrDefault(x => x.Id_PrOL == Convert.ToInt32(batchCompleteMdb.OdP) && x.Id_PrOLAttivita == Convert.ToInt32(message.Current.CodiceFase) && x.Id_PrBL == Convert.ToInt32(fase?.Campo4) && x.Id_PRBLAttivita == Convert.ToInt32(fase?.Campo3));
                                    if (listOfComponentiMongo != null) listOfComponentiMongo.DataConsumo = listOfComponentiMongo.DataConsumo.ToLocalTime();
                                    var variab = ((listOfComponentiMongo == null) ? ((listOfComponentiSql == null) ? null : listOfComponentiSql) : listOfComponentiMongo);

                                    var quantitaDaVersare = 0.0;
                                    var phase = batchCompleteMdb.DistintaBase.Fasi.FirstOrDefault(z => z.NumeroFase == fase.NumeroFase).Id;
                                    var ListOfVersamenti = batchCompleteMdb.Eventi.Where(x => x.BatchId == batchCompleteMdb.Id && x.FaseId == phase && x.UltimoAggiornamento > Convert.ToDateTime((variab != null) ? variab?.DataConsumo : new DateTime(2000, 01, 01, 0, 0, 0)) && x.TipoEvento == Model.Enums.TipoEvento.qta_prelievo).ToList();

                                    foreach (var item in ListOfVersamenti)
                                    {

                                        quantitaDaVersare += Convert.ToDouble(item.Valore.Replace('.', ','));
                                        mesProdComponenti.Cd_AR = item.Materiale.Item.Nome;
                                        //se il materiale da prelevare è diverso da quello prelevato alzo il bit
                                        // mesProdComponenti.CheckCambioMateriale = (item.Materiale.Id == componenteDistinta.Materiale.Id) ? false : true;
                                    }
                                    if (quantitaDaVersare > 0)
                                        mesProdComponenti.QtaConsumo = quantitaDaVersare;


                                    //se non sono stati trovati eventi di prelievo verso l'intera quantità e il materiale consigliato 
                                    //Richiesto da Alberto-Bombelli

                                    Logger.ArcaConnector.Debug("import_MES_Componenti    Reading from Arca... completed");

                                    Logger.ArcaConnector.InfoFormat("Phase {0} Odp: {1} Save into MongoDb", message.Current.NumeroFase, batchCompleteMdb.OdP);
                                    mesProdComponenti.Uniquefy();
                                    Logger.ArcaConnector.InfoFormat("Phase {0} Odp: {1} Save into MongoDb Completed", message.Current.NumeroFase, batchCompleteMdb.OdP);
                                    Logger.ArcaConnector.InfoFormat("Completed  Save  MES Time Phase {0} Odp {1}", message.Current.NumeroFase, batchCompleteMdb.OdP);
                                    await mongoInstanceLocal.SetAsync(mesProdComponenti);
                                    Logger.ArcaConnector.InfoFormat("Completed  Save  MES Time Pieces {0} Odp {1}", message.Current.NumeroFase, batchCompleteMdb.OdP);
                                    //}
                                    //se il batch non è termintato (Stato batch significa che non è ultima fase quindi creo una riga a buoto

                                    if (batchCompleteMdb.Stato != Model.Enums.StatoBatch.Terminato)
                                    {
                                        //---------------------------------------------------------------------------------------------------------------------------------------------
                                        //---------------------------------------------------------versamento pezzi
                                        //---------------------------------------------------------------------------------------------------------------------------------------------
                                        var mesProdVersamenti = new Arca_MesProdVersamenti();
                                        mesProdVersamenti.Id_Versamento = idVersamento;
                                        mesProdVersamenti.Id_PrOL = Convert.ToInt32(batchCompleteMdb.OdP);
                                        mesProdVersamenti.Id_PrOLAttivita = Convert.ToInt32(message.Current.CodiceFase);
                                        mesProdVersamenti.Id_PrBL = Convert.ToInt32(fase?.Campo4);
                                        mesProdVersamenti.Id_PRBLAttivita = Convert.ToInt32(fase?.Campo3);
                                        mesProdVersamenti.Cd_PrRisorsa = batchCompleteMdb.Fasi.FirstOrDefault(x => x.NumeroFase == message.Current.NumeroFase).Macchine[0].Macchina.Codice;
                                        mesProdVersamenti.QtaVersamentoBuono = 0;
                                        mesProdVersamenti.QtaVersamentoScarto = 0;
                                        mesProdVersamenti.DataVersamento = DateTime.Now;
                                        mesProdVersamenti.Cd_Operatore = message.Username.ToString();

                                        Logger.ArcaConnector.Debug("import_MES_Versamenti    Reading from Arca...");
                                        Logger.ArcaConnector.Debug("import_MES_Versamenti    Reading from Arca... completed");

                                        //---------------------------------------------------------------------------------------------------------------------------------------------
                                        //---------------------------------------------------------Versamento 
                                        //---------------------------------------------------------------------------------------------------------------------------------------------

                                        Logger.ArcaConnector.InfoFormat("Phase {0} Odp: {1} Save into MongoDb", message.Current.NumeroFase, batchCompleteMdb.OdP);
                                        mesProdVersamenti.Uniquefy();
                                        Logger.ArcaConnector.InfoFormat("Phase {0} Odp: {1} Save into MongoDb Completed", message.Current.NumeroFase, batchCompleteMdb.OdP);
                                        Logger.ArcaConnector.InfoFormat("Completed  Save  MES Time Phase {0} Odp {1}", message.Current.NumeroFase, batchCompleteMdb.OdP);
                                        await mongoInstanceLocal.SetAsync(mesProdVersamenti);
                                        Logger.ArcaConnector.InfoFormat("Completed  Save  MES Time Pieces {0} Odp {1}", message.Current.NumeroFase, batchCompleteMdb.OdP);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        Logger.ArcaConnector.Error("Error  Save  MES Time Phase  ", exc);
                    }
                });

                Logger.ArcaConnector.Info("     Creating DbManager...");
                _dbManager = new DbManager(arca_connectionString);
                Logger.ArcaConnector.Info("     Creating DbManager... completed");

                // --------------------------------------------------------------------------------------------------------
                //MI SOTTOSCRIVO ALL'IMPORT MANUALE
                // --------------------------------------------------------------------------------------------------------
                MessageServices.Instance.Subscribe(RequestStartImportBatch.Name, (object sender, RequestStartImportBatch o) =>
                {
                    if (mongoInstanceGlobal == null)
                        mongoInstanceGlobal = MongoRepository.New;

                    Logger.ArcaConnector.Info("Start RequestStartImportBatch... ");
                    Logger.ArcaConnector.Info($"RequestStartImportBatch From user: '{o.Username}'... ");
                    import_MES_Batch(arca_connectionString);
                    Logger.ArcaConnector.Info("Start RequestStartImportBatch... Completed ");
                });

                // avvio del timer principale
                _timer = new System.Threading.Timer(async (a) =>
                {

                    lock (_timerLock)
                    {
                        if (_timerRunning) return;
                        _timerRunning = true;
                    }

                    try
                    {
                        if (mongoInstanceGlobal == null)
                            mongoInstanceGlobal = MongoRepository.New;
                        //import_MES_Material(arca_connectionString);
                        import_Mes_CheckTableUpdated(arca_connectionString);

                        export_MES_Time(arca_connectionString);
                        export_MES_Pieces(arca_connectionString);
                        export_MES_Scrap(arca_connectionString);
                        export_MES_Component(arca_connectionString);

                    }
                    catch (Exception exc)
                    {
                        Logger.ArcaConnector.Error("Error  Timer  ", exc);
                    }

                    lock (_timerLock)
                        _timerRunning = false;

                }, null, 60 * 1000, Convert.ToInt32(intervalTickRead) * 60 * 1000);

                Logger.ArcaConnector.Info("Start configuration... completed");
            }
            catch (Exception exc)
            {
                Logger.ArcaConnector.Error($"Start configuration... on error, cause:'{exc.Message}'");
            }
        }

        public void Run()
        {
            //_timer.Interval = _intervalTickRead * 1000;
            //_timer.Elapsed += _timer_Elapsed;
            //_timer.Start();
        }

        public void Stop()
        {
            //_timer.Stop();
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            // verifica ordini
            // chiamate verso SQL Arca...
        }

        private async void import_MES_Operator(string arca_connectionString)
        {
            try
            {

                Logger.ArcaConnector.Info("import_MES_Operator() starting... ");

                Logger.ArcaConnector.Debug("import_MES_Operator()       Connect to ARCA DB... ");
                var _dbManager = new DbManager(arca_connectionString);
                Logger.ArcaConnector.Debug("import_MES_Operator()       Connect to ARCA DB... completed ");

                //Lista degli operatori trovati sul db
                Logger.ArcaConnector.Debug("import_MES_Operator()           From ARCA, Get<Arca_Operatore>()... ");
                var listOfNewOperatore = _dbManager.Get<Arca_Operatore>();
                Logger.ArcaConnector.Debug("import_MES_Operator()           From ARCA, Get<Arca_Operatore>()... completed ");


                Logger.ArcaConnector.Debug("import_MES_Operator()       Connect to MES DB... ");
                //var mongoInstance = MongoRepository.New;


                Logger.ArcaConnector.Debug("import_MES_Operator()           From MES, All<Operatore>()... ");
                var listOfOperatore = await mongoInstanceGlobal.All<Operatore>(new string[] { }); // campi esclusi
                Logger.ArcaConnector.Debug("import_MES_Operator()           From MES, All<Operatore>()... completed  ");

                Logger.ArcaConnector.Debug("import_MES_Operator()           From MES, All<OperatoreCompetenza>()... ");
                var competenzaOperatore = await mongoInstanceGlobal.All<OperatoreCompetenza>(new string[] { });
                Logger.ArcaConnector.Debug("import_MES_Operator()           From MES, All<OperatoreCompetenza>()... completed  ");


                Logger.ArcaConnector.Debug("import_MES_Operator()       Connect to MES DB... completed ");



                //ciclare tutti gli operaotir
                foreach (var user in listOfNewOperatore)
                {
                    Logger.ArcaConnector.Debug($"import_MES_Operator()       Check operator Id_Operatore:'{user.Id_Operatore}'... ");
                    //controllo se esiste già operatore
                    var userMdb = listOfOperatore.FirstOrDefault(x => x.Codice == Convert.ToString(user.Id_Operatore));
                    var newOperatore = new Operatore();
                    //se non esiste lo creo
                    if (userMdb == null && user.Eliminato == false)
                    {
                        Logger.ArcaConnector.Debug($"import_MES_Operator()           Id_Operatore:'{user.Id_Operatore}', doesn't exist into MES, will be imported... ");
                        newOperatore = new Operatore
                        {
                            Codice = Convert.ToString(user.Id_Operatore),
                            Nome = user.Nome,
                            Cognome = user.Cognome,
                            Username = user.Cd_Operatore,
                            Password = user.Cd_Operatore,
                            LastUpdate = DateTime.Now,
                            CreateDate = DateTime.Now,
                            AreaDiSicurezza_Anagrafica = true,
                            AreaDiSicurezza_Designer = true,
                            AreaDiSicurezza_Produzione = true,
                            AreaDiSicurezza_Tabelle = true,
                            Lingua = "It",
                            OperatoreCompetenza = new ExternalDoc<OperatoreCompetenza>() { Id = competenzaOperatore[0].Id }
                        };

                        newOperatore.Uniquefy();
                        await MongoRepository.Instance.SetAsync(newOperatore);
                        Logger.ArcaConnector.Debug($"import_MES_Operator()           Id_Operatore:'{user.Id_Operatore}', doesn't exist into MES, will be imported... completed");

                    }
                    // se esiste lo aggiorno
                    else if (userMdb != null && user.Eliminato == false)
                    {
                        Logger.ArcaConnector.InfoFormat("Update  Import Operator Code {0}", Convert.ToString(userMdb.Codice));


                        if (DateTime.Compare(user.UltimoAggionamento, userMdb.LastUpdate.ToLocalTime()) != -1)
                        {
                            newOperatore = new Operatore
                            {
                                Id = userMdb.Id,
                                Codice = Convert.ToString(user.Id_Operatore),
                                Nome = user.Nome,
                                Cognome = user.Cognome,
                                Username = user.Cd_Operatore,
                                Password = user.Cd_Operatore,
                                LastUpdate = DateTime.Now,
                            };
                            await MongoRepository.Instance.SetAsync(newOperatore);
                        }

                    }
                    //se operatore è stato eliminato ed è ancora in mongodb lo cancello
                    else if (userMdb != null && user.Eliminato == true)
                    {
                        Logger.ArcaConnector.InfoFormat("Delete Operator Code {0}", userMdb.Codice);
                        await MongoRepository.Instance.DeleteAsync<Operatore>(userMdb.Id);
                    }

                    Logger.ArcaConnector.Debug("import_MES_Operator()       From ARCA, Check operator Id_Operatore:{user.Id_Operatore}... completed ");

                }


                _dbManager.Dispose();
            }
            catch (Exception exc)
            {
                Logger.ArcaConnector.Error("Error Import Operator Code {0}", exc);
            }
            finally
            {
                Logger.ArcaConnector.Info("import_MES_Operator() starting... completed");
            }
        }

        private async void import_MES_Material(string arca_connectionString)
        {
            try
            {
                var _dbManager = new DbManager(arca_connectionString);
                Logger.ArcaConnector.Info("Start  Import MES_Material");
                //var mongoInstance = MongoRepository.New;
                //Lista dei materiali trovati sul db
                var listOfNewMaterial = _dbManager.Get<Arca_Materiale>();
                //lista dei  materiali presenti su mongo db
                var listOfMateriali = await mongoInstanceGlobal.All<Materiale>(new string[] { }); // campi esclusi

                foreach (var material in listOfNewMaterial)
                {

                    //controllo se esiste già l'operatore
                    var materialMdb = listOfMateriali.FirstOrDefault(x => x.Codice == Convert.ToString(material.Id_Materiale));
                    var newMaterial = new Materiale();
                    //se non esiste lo creo

                    if (materialMdb == null && material.Eliminato == false)
                    {
                        Logger.ArcaConnector.InfoFormat("Start  Import Material Code {0}", material.Id_Materiale);
                        newMaterial = new Materiale
                        {
                            Codice = Convert.ToString(material.Id_Materiale),
                            Nome = material.Nome,
                            Descrizione = material.Descrizione,
                            //Um= material.UnitaMisura,
                            CreateDate = DateTime.Now,
                            LastUpdate = DateTime.Now,
                            TipoMateriale = TipoMateriale.ProdottoFinito,
                            Categoria = material.Cd_ARGruppo1,
                        };
                        newMaterial.Uniquefy();
                        await mongoInstanceGlobal.SetAsync(newMaterial);
                    }

                    //se esiste lo aggiorno
                    else if (materialMdb != null && material.Eliminato == false)
                    {
                        if (DateTime.Compare(material.UltimoAggionamento, materialMdb.LastUpdate) != -1)
                        {
                            Logger.ArcaConnector.InfoFormat("Update  Import Material Code {0}", Convert.ToString(materialMdb.Codice));
                            newMaterial = new Materiale
                            {
                                Id = materialMdb.Id,
                                Codice = Convert.ToString(material.Id_Materiale),
                                Nome = material.Nome,
                                //Um = material.UnitaMisura,
                                LastUpdate = DateTime.Now,
                                Categoria = material.Cd_ARGruppo1,
                                TipoMateriale = TipoMateriale.ProdottoFinito,
                                Descrizione = material.Descrizione,

                            };
                            await mongoInstanceGlobal.SetAsync(newMaterial);
                        }
                    }

                    //se operatore è stato eliminato ed è ancora in mongodb lo cancello
                    else if (materialMdb != null && material.Eliminato == true)
                    {
                        Logger.ArcaConnector.InfoFormat("Delete Material Code {0}", materialMdb.Codice);
                        await mongoInstanceGlobal.DeleteAsync<Operatore>(materialMdb.Id);
                    }


                }

                Logger.ArcaConnector.Info("Close  Import MES_Material");
            }
            catch (Exception exc)
            {
                Logger.ArcaConnector.Error("Error Import Material Code {0}", exc);

            }
        }

        private async void import_MES_Machine(string arca_connectionString)
        {
            try
            {
                Logger.ArcaConnector.Info("import_MES_Machine() starting... ");

                Logger.ArcaConnector.Debug("import_MES_Machine()       Connect to ARCA DB... ");
                var _dbManager = new DbManager(arca_connectionString);
                Logger.ArcaConnector.Debug("import_MES_Machine()       Connect to ARCA DB... completed ");
                Logger.ArcaConnector.Debug("import_MES_Machine()       Connect to MESDB... ");
                //var mongoInstance = MongoRepository.New;
                Logger.ArcaConnector.Debug("import_MES_Machine()       Connect to MES DB... completed ");
                Logger.ArcaConnector.Debug("import_MES_Machine()           From ARCA, Get<Arca_Macchine>()... ");
                //Lista dei materiali trovati sul db
                var listOfNewMachine = _dbManager.Get<Arca_Macchine>();
                listOfNewMachine.Where(x => x.Cd_PRReparto == "PRODUZIONE").ToList();
                Logger.ArcaConnector.Debug("import_MES_Machine()           From ARCA, Get<Arca_Macchine>()... completed ");
                Logger.ArcaConnector.Debug("import_MES_Machine()           From MES, All<Macchina>()... ");
                //lista dei  materiali presenti su mongo db
                var listOfMacchine = await mongoInstanceGlobal.All<Macchina>(new string[] { }); // campi esclusi
                Logger.ArcaConnector.Debug("import_MES_Machine()           From MES, All<Arca_Macchine>()... ");
                Logger.ArcaConnector.Debug("import_MES_Machine()       Connect to MES DB... completed ");
                foreach (var machine in listOfNewMachine)
                {
                    Logger.ArcaConnector.Debug($"import_MES_Machine()       Check Machine Id_Macchina:'{machine.Id_Macchina}'... ");
                    //controllo se esiste già l'operatore
                    var macchinaMdb = listOfMacchine.FirstOrDefault(x => x.Codice == Convert.ToString(machine.Id_Macchina));
                    var newMacchina = new Macchina();
                    //se non esiste lo creo

                    if (macchinaMdb == null && machine.Eliminato == false)
                    {
                        Logger.ArcaConnector.Debug($"import_MES_Machine()           Id_Macchina:'{machine.Id_Macchina}', doesn't exist into MES, will be imported... ");
                        newMacchina = new Macchina
                        {
                            Codice = machine.Id_Macchina,
                            Nome = machine.Nome,
                            CreateDate = DateTime.Now,
                            LastUpdate = DateTime.Now,
                        };
                        newMacchina.Uniquefy();
                        await mongoInstanceGlobal.SetAsync(newMacchina);
                        Logger.ArcaConnector.Debug($"import_MES_Machine()           Id_Macchina:'{machine.Id_Macchina}', doesn't exist into MES, will be imported... completed");
                    }

                    //se esiste lo aggiorno
                    else if (macchinaMdb != null && machine.Eliminato == false)
                    {
                        if (DateTime.Compare(machine.UltimoAggionamento, macchinaMdb.LastUpdate.ToLocalTime()) != -1)
                        {
                            Logger.ArcaConnector.Debug($"import_MES_Machine()           Id_Macchina:'{machine.Id_Macchina}',exist into MES, will be update... ");
                            newMacchina = new Macchina
                            {
                                Codice = Convert.ToString(machine.Id_Macchina),
                                Nome = machine.Nome,
                                CreateDate = DateTime.Now,
                                LastUpdate = DateTime.Now

                            };
                            await mongoInstanceGlobal.SetAsync(newMacchina);
                            Logger.ArcaConnector.Debug($"import_MES_Machine()           Id_Macchina:'{machine.Id_Macchina}',exist into MES, will be update... Completed");
                        }
                    }

                    //se operatore è stato eliminato ed è ancora in mongodb lo cancello
                    else if (macchinaMdb != null && machine.Eliminato == true)
                    {
                        Logger.ArcaConnector.Debug($"import_MES_Machine()           Id_Macchina:'{machine.Id_Macchina}',Delete into MES, will be delete... ");
                        await mongoInstanceGlobal.DeleteAsync<Operatore>(macchinaMdb.Id);
                        Logger.ArcaConnector.Debug($"import_MES_Machine()           Id_Macchina:'{machine.Id_Macchina}',Delete into MES, will be delete... Completed ");
                    }


                }

            }
            catch (Exception exc)
            {

                Logger.ArcaConnector.Error("Error  import_MES_Machine  ", exc);
            }
            finally
            {
                Logger.ArcaConnector.Info("import_MES_Machine() starting... completed");

            }
        }

        private async void import_MES_Batch(string arca_connectionString)
        {
            lock (_timerLockImportBatch)
            {
                if (_runningImportBatch) return;
                _runningImportBatch = true;
            }

            var sp = Stopwatch.StartNew();
            try
            {
 
                var _dbManager = new DbManager(arca_connectionString);
                Logger.ArcaConnector.Info("import_MES_Batch     Start Import MES_Batch...");


                Logger.ArcaConnector.Debug("import_MES_Batch    Reading from Arca...");
                var listOfBatchHeader = _dbManager.Get<Arca_Batch_Prol>();
                listOfBatchHeader = listOfBatchHeader.Where(x => x.Chiuso == false).ToList();
                Logger.ArcaConnector.Debug($"import_MES_Batch    Reading from Arca... completed, total item found:'{listOfBatchHeader.Count()}'");


                Logger.ArcaConnector.Debug("import_MES_Batch    Reading from Mes...");
                var listOfBatch = await mongoInstanceGlobal.All<Batch>(new string[] { }, new string[] { "OdP", "Id" }); // campi esclusi
                var listOfMachine = await mongoInstanceGlobal.All<Macchina>(new string[] { });
                var listOfMaterial = await mongoInstanceGlobal.All<Materiale>(new string[] { });
                var listOfCompany = await mongoInstanceGlobal.All<Azienda>(new string[] { });
                Logger.ArcaConnector.Debug("import_MES_Batch    Reading from Mes... completed");

                int i = 1;
                sp.Start();
                foreach (var batchHeader in listOfBatchHeader)
                {

                    int tempoTotTeorico = 0;
                    var listOfDistinta = await mongoInstanceGlobal.All<DistintaBase>(new string[] { });

                    var batchId = batchHeader.Id_Batch;
                    Logger.ArcaConnector.Debug($"import_MES_Batch       Founded Arca batch n.{i}, batchId:{batchId}...");
                    Logger.ArcaConnector.Debug($"import_MES_Batch               . Commessa:'{batchHeader.Commessa}'");
                    Logger.ArcaConnector.Debug($"import_MES_Batch               . CodiceArticolo_Batch:'{batchHeader.CodiceArticolo_Batch}'");
                    Logger.ArcaConnector.Debug($"import_MES_Batch               . CodiceCliente_Batch:'{batchHeader.CodiceCliente_Batch}'");
                    Logger.ArcaConnector.Debug($"import_MES_Batch               . DescrizioneCliente_Batch:'{batchHeader.DescrizioneCliente_Batch}'");
                    Logger.ArcaConnector.Debug($"import_MES_Batch               . N_Ordine_Batch:'{batchHeader.N_Ordine_Batch}'");
                    Logger.ArcaConnector.Debug($"import_MES_Batch               . UltimoAggionamento:'{batchHeader.UltimoAggionamento}'");

                    Logger.ArcaConnector.Debug("import_MES_Batch           ARCA, Search PROLAttivita...");
                    var prolActivities = await _dbManager.SearchAsync<Arca_Batch_ProlAttivita>(x => x.Id_Batch == batchId);
                    //filtro per le attività esterna (due filtri perchè AttivitaEsterna_Batch può essere null)
                    //prolActivities = prolActivities.Where(x => x.AttivitaEsterna_Batch == false).ToList();
                    //le fasi in Arca sono inverse
                    prolActivities = prolActivities.Reverse().ToList();
                    Logger.ArcaConnector.Debug("import_MES_Batch           ARCA, Search  PROLAttivita... completed");

                    Logger.ArcaConnector.InfoFormat("import_MES_Batch           MES, Search  batch_id: {0}...", batchHeader.Id_Batch);
                    var batchMdb = listOfBatch.FirstOrDefault(x => x.OdP == Convert.ToString(batchHeader.Id_Batch));
                    Logger.ArcaConnector.InfoFormat("import_MES_Batch           MES, Search  batch_id: {0}... completed", batchHeader.Id_Batch);

                    //batch
                    var newBatch = new Batch();
                    //creo l'ordine da nuovo
                    if (batchMdb == null && batchHeader.Eliminato == false)
                    {
                        Logger.ArcaConnector.Debug($"import_MES_Batch           MES, Batch {batchHeader.Id_Batch} not Found, will be created");
                        Logger.ArcaConnector.Debug("import_MES_Batch           MES, Creating batch...");
                        //creo la distinta base
                        var materiale = listOfMaterial.Find(x => x.Nome == batchHeader.CodiceArticolo_Batch); // campi esclusi
                        if (materiale == null)
                        {

                            Logger.ArcaConnector.Info("import_MES_Batch           MES, Search  Create New Material from db mes");
                            var listOfNewMaterial = _dbManager.Get<Arca_Materiale>();

                            var material = listOfNewMaterial.FirstOrDefault(x => x.Nome == batchHeader.CodiceArticolo_Batch);

                            var newMaterial = new Materiale();

                            Logger.ArcaConnector.InfoFormat("Start  Import Material Code {0}", batchHeader.CodiceArticolo_Batch);
                            newMaterial = new Materiale
                            {
                                Codice = Convert.ToString(material.Id_Materiale),
                                Nome = material.Nome,
                                //Um = material.UnitaMisura,
                                CreateDate = DateTime.Now,
                                LastUpdate = DateTime.Now,
                                TipoMateriale = TipoMateriale.ProdottoFinito,
                                Descrizione = material.Descrizione,
                                Categoria = material.Cd_ARGruppo1,
                            };

                            newMaterial.Uniquefy();
                            await mongoInstanceGlobal.SetAsync(newMaterial);
                            Logger.ArcaConnector.InfoFormat("  Import Material Code {0} Completed", batchHeader.CodiceArticolo_Batch);
                            materiale = newMaterial;
                            Logger.ArcaConnector.Info("import_MES_Batch           MES, Search  Create New Material from db mes Completed");
                        }
                        Logger.ArcaConnector.Debug("import_MES_Batch           MES, Creating Distinta Base...");
                        var distintaBase = new DistintaBase();
                        distintaBase.LastUpdate = DateTime.Now;
                        distintaBase.CreateDate = DateTime.Now;
                        distintaBase.Codice = batchHeader.CodiceArticolo_Batch;
                        distintaBase.Nome = batchHeader.CodiceArticolo_Batch + " - " + materiale.Descrizione;
                        distintaBase.MaterialeInUscita = new ExternalDoc<Materiale>() { Id = materiale?.Id };
                        distintaBase.StatoDistintaBase = Model.Enums.StatoDistintaBase.Approvata;
                        distintaBase.IsDisabled = false;
                        distintaBase.AbilitaStopProduzione = true;
                        distintaBase.AbilitaAutoStartProduzione = true;
                        distintaBase.AbilitaEmergenza = true;
                        distintaBase.AbilitaEmergenza = true;
                        distintaBase.Azienda = new ExternalDoc<Azienda>() { Id = listOfCompany.First().Id };

                        Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         distintaBase.LastUpdate:{distintaBase.LastUpdate}");
                        Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         distintaBase.LaCreateDatestUpdate:{distintaBase.CreateDate}");
                        Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         distintaBase.Codice:{distintaBase.Codice}");
                        Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         distintaBase.Nome:{distintaBase.Nome}");
                        Logger.ArcaConnector.Debug("import_MES_Batch           MES, Creating Distinta Base... completed");

                        Logger.ArcaConnector.Debug("import_MES_Batch           MES, Creating Batch...");
                        //GESTIONE MOTO MODELLO 
                        var annoInizio = "";
                        var annoFine = "";
                        annoInizio = (batchHeader.AnnoInizioMoto > 0) ? batchHeader.AnnoInizioMoto.ToString() + "-" : "";
                        annoFine = (batchHeader.AnnoFineMoto > 0) ? batchHeader.AnnoFineMoto.ToString() : "";
                        //chiamata della priorità
                        var priority = await GetBatchAutoPriorityCounterInfo();


                        newBatch = new Batch
                        {
                            OdP = Convert.ToString(batchHeader.Id_Batch),
                            OdPParziale = Convert.ToString(batchHeader.N_Ordine_Batch),
                            Priorita = priority,
                            Commessa = batchHeader.Commessa,
                            OdV_DataConsegna = batchHeader.DataObiettivo.Date,
                            OdV_DescrizioneCliente = "Cliente: " + batchHeader.CodiceCliente_Batch + ", " + batchHeader.DescrizioneCliente_Batch + ", Note:" + batchHeader.Note_Batch + ", Modello Moto:" + batchHeader.DescrizioneCasaMotociclistica + " " + batchHeader.ModelloMoto + " " + batchHeader.VersioneMoto + " " + annoInizio + annoFine,
                            Stato = Model.Enums.StatoBatch.InModifica,
                            CreateDate = DateTime.Now,
                            LastUpdate = DateTime.Now,
                            Azienda = new ExternalDoc<Azienda>() { Id = listOfCompany.First().Id },
                            Materiale = materiale,
                            Revisione = batchHeader.DescrizioneCasaMotociclistica + " " + batchHeader.ModelloMoto + " " + batchHeader.VersioneMoto + " " + annoInizio + annoFine

                        };

                        Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newBatch.OdP:{newBatch.OdP}");
                        Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newBatch.OdPParziale:{newBatch.OdPParziale}");
                        Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newBatch.Commessa:{newBatch.Commessa}");
                        Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newBatch.OdV_DescrizioneCliente:{newBatch.OdV_DescrizioneCliente}");


                        newBatch.Uniquefy();
                        //instanzio i multimedia
                        newBatch.BatchMultimedia = new List<BatchMultimedia>();

                        List<Arca_FaseAllegati> listMultimediaAllegati = new List<Arca_FaseAllegati>();
                        //ciclo tutte le attivita per controllare se hanno associato un multimedia

                        foreach (var prolActivity in prolActivities)
                        {

                            if (prolActivity.Id_DBFase != null && prolActivity.Id_DBFase != 0)
                            {
                                var idDBFase = prolActivity.Id_DBFase;
                                var multimedia = await _dbManager.SearchAsync<Arca_FaseAllegati>(x => x.Id_DBFase == idDBFase);
                                if (multimedia.ToList().Count > 0 && multimedia?.ToList()?.First()?.Path != string.Empty)
                                {
                                    listMultimediaAllegati.Add(multimedia.First());
                                }
                            }
                        }

                        var countPrbl = 0;
                        //ciclo tutte le attivita legate al bath (ogni attività è una fase)
                        foreach (var prolActivity in prolActivities)
                        {
                            Logger.ArcaConnector.Debug($"import_MES_Batch           MES,            Elaborating single prolActivity...");
                            Logger.ArcaConnector.Debug($"import_MES_Batch           MES,                prolActivity.Id_Attivita_Batch:{prolActivity.Id_Attivita_Batch}");
                            Arca_Batch_PrblAttivita prblActivity = null;
                            Arca_Batch_Prbl prbl = null;
                            Logger.ArcaConnector.Debug("import_MES_Batch           MES, Search To ARCA prblActivity");
                            int prolActivity_Id_Attivita_Batch = Convert.ToInt32(prolActivity.Id_Attivita_Batch);
                            //scarico tutte le attività associate a questo ordine nella tabella PrblAttivita
                            var prblActivit = await _dbManager.SearchAsync<Arca_Batch_PrblAttivita>(x => x.Id_Attivita_Batch == prolActivity_Id_Attivita_Batch);
                            if (prblActivit.Count() != 0) prblActivity = prblActivit?.First();
                            Logger.ArcaConnector.Debug("import_MES_Batch           MES, Search To ARCA prblActivity_Id_PrBL_Batch");
                            int prblActivity_Id_PrBL_Batch = Convert.ToInt32(prblActivity?.Id_PrBL_Batch);
                            Logger.ArcaConnector.Debug($"import_MES_Batch                prolActivity.Id_PrBL_Batch:{prblActivity?.Id_PrBL_Batch}");

                            //scarico tutte le attività associate a questo ordine nella tabella Prbl
                            var prbl_list = await _dbManager.SearchAsync<Arca_Batch_Prbl>(x => x.Id_Attivita_Batch == prblActivity_Id_PrBL_Batch);
                            countPrbl += prbl_list.Count();
                            if (prbl_list.Count() != 0) 
                                prbl = prbl_list?.First();
                            //scarico tutte le attività associate a questo ordine nella tabella PrblMAteriali
                            Logger.ArcaConnector.Debug("import_MES_Batch           MES, Search To ARCA prblActivity_Id_PrBL_Batch Completed");
                            Logger.ArcaConnector.Debug("import_MES_Batch           MES, Search To ARCA prblMaterial");
                            //var prblMaterial = await _dbManager.SearchAsync<Arca_Batch_PrblMateriali>(x => x.Id_Attivita_Batch == prolActivity_Id_Attivita_Batch);

                            var prolMaterial = await _dbManager.SearchAsync<Arca_Batch_ProlMateriali>(x => x.Id_PrOLAttivita == prolActivity_Id_Attivita_Batch);

                            Logger.ArcaConnector.Debug("import_MES_Batch           MES, Search To ARCA prblMaterial Completed");

                            Logger.ArcaConnector.Debug("import_MES_Batch           MES, Create DistintaBaseFasiMacchina");
                            //inserisco la postazione e il tempo teorivo di lavorazione 
                            var macchina = new DistintaBaseFasiMacchina();
                            Logger.ArcaConnector.Debug("import_MES_Batch           MES, Create DistintaBaseFasiMacchina Completed");
                            Logger.ArcaConnector.Debug("import_MES_Batch           MES, Create TimeTheoric");
                            if (prbl != null)
                            {
                                var findMachine = listOfMachine.FirstOrDefault(x => x.Codice == (prbl?.RisorsaAss_Batch ?? "-"));
                                var externaldocMacchina = new ExternalDoc<Macchina>();
                                if (findMachine != null)
                                {
                                    externaldocMacchina.Id = findMachine.Id;
                                    macchina = new DistintaBaseFasiMacchina
                                    {
                                        Macchina = externaldocMacchina,
                                        Tempo = ((prblActivity?.TempoEsecuzione_Batch ?? 0) * (prblActivity?.FattoreMks_Batch ?? 0)) / (prblActivity?.Quantita_Batch ?? 0)
                                    };
                                }
                            }

                            tempoTotTeorico = tempoTotTeorico + Convert.ToInt32(macchina.Tempo);
                            var listmacchine = new List<DistintaBaseFasiMacchina>();
                            listmacchine.Add(macchina);
                            Logger.ArcaConnector.Debug("import_MES_Batch           MES, Create TimeTheoric Completed");
                            Logger.ArcaConnector.Debug("import_MES_Batch           MES, Create DistintaBaseFasiManoDopera");
                            //creo una distinta base operatore a 0 Sec
                            var distintaBaseFasiManoDopera = new DistintaBaseFasiManoDopera
                            {
                                CausaleFase = Model.Enums.CausaleFase.Tempi,
                                Descrizione = "",
                                Tempo = 0,
                            };
                            var listdistintaBaseFasiManoDopera = new List<DistintaBaseFasiManoDopera>();
                            listdistintaBaseFasiManoDopera.Add(distintaBaseFasiManoDopera);
                            Logger.ArcaConnector.Debug("import_MES_Batch           MES, Create DistintaBaseFasiManoDopera Completed");
                            Logger.ArcaConnector.Debug("import_MES_Batch           MES, Create listOfMaterialPhase ");
                            //creo i materiali della fase
                            List<ExternalDoc<Materiale>> listOfMaterialPhase = new List<ExternalDoc<Materiale>>();
                            List<DistintaBaseMateriale> listOfComponentPhase = new List<DistintaBaseMateriale>();
                            if (prolMaterial != null)
                            {

                                foreach (var item in prolMaterial)
                                {
                                    var materialPhase = listOfMaterial.Find(x => x.Nome == item.Cd_AR); // campi esclusi
                                    if (materialPhase == null)
                                    {
                                        Logger.ArcaConnector.Info("Start  Import MES_Material id ");
                                        var listOfNewMaterial = _dbManager.Get<Arca_Materiale>();

                                        var material = listOfNewMaterial.FirstOrDefault(x => x.Nome == batchHeader.CodiceArticolo_Batch);

                                        var newMaterial = new Materiale();

                                        Logger.ArcaConnector.InfoFormat("Start  Import Material Code {0}", batchHeader.CodiceArticolo_Batch);
                                        newMaterial = new Materiale
                                        {
                                            Codice = Convert.ToString(material.Id_Materiale),
                                            Nome = material.Nome,
                                            //Um = material.UnitaMisura,
                                            CreateDate = DateTime.Now,
                                            LastUpdate = DateTime.Now,
                                            Categoria = material.Cd_ARGruppo1,
                                            TipoMateriale = TipoMateriale.ProdottoFinito,
                                            Descrizione = material.Descrizione
                                        };
                                        newMaterial.Uniquefy();
                                        await mongoInstanceGlobal.SetAsync(newMaterial);
                                        Logger.ArcaConnector.InfoFormat("  Import Material Code {0} Completed", batchHeader.CodiceArticolo_Batch);
                                        materialPhase = newMaterial;
                                    }
                                    listOfMaterialPhase.Add(new ExternalDoc<Materiale>() { Id = materialPhase.Id });

                                    var newDistintabase = new DistintaBaseMateriale();
                                    newDistintabase.Materiale = new ExternalDoc<Materiale>() { Id = materialPhase.Id };
                                    newDistintabase.Qta = item.Consumo;
                                    newDistintabase.Lotto = Convert.ToString(item.Id_PrOLMateriale);
                                    newDistintabase.UnitaDiMisura = item.Cd_ARMisura;
                                    newDistintabase.Descrizione = item?.Cd_ARGruppo1;
                                    newDistintabase.CodiceMateriale = materialPhase.Nome;
                                    listOfComponentPhase.Add(newDistintabase);
                                }

                            }
                            Logger.ArcaConnector.Debug("import_MES_Batch           MES, Create listOfMaterialPhase  Completed");
                            Logger.ArcaConnector.Debug("import_MES_Batch           MES,     Creating DistintaBaseFasi...  ");


                            //Creazione di una fase 
                            var newFase = new DistintaBaseFasi
                            {
                                Id = Convert.ToString(prolActivities.ToList().IndexOf(prolActivity) + 1),
                                LastUpdate = DateTime.Now,
                                CreateDate = DateTime.Now,
                                NumeroFase = Convert.ToString(prolActivities.ToList().IndexOf(prolActivity) + 1),
                                Nome = prolActivity.DescrizioneAttivita_Batch,
                                Descrizione = prolActivity.DescrizioneAttivita_Batch,
                                Codice = Convert.ToString(prolActivity?.Id_Attivita_Batch),
                                IsAutomatica = true,
                                IsAndon = false,
                                IsCiclica = true,
                                AbilitaAvanzamenti = true,
                                AbilitaVersamentiManuali = false,
                                AbilitaVersamentiAutomatici = false,
                                AbilitaMultimedia = false,
                                AbilitaMateriali = false,
                                AbilitaPulsanteEmergenza = true,
                                Abilitabarcode = false,
                                AbilitaLetturaRfid = false,
                                AbilitascritturaRfid = false,
                                AbilitaAvanzamentiStar = true,
                                AbilitaAvanzamentiStop = true,
                                AbilitaAvanzamentiPausa = true,
                                AbilitaFaseTerminataParzialmente = false,
                                Visibilitastazione = "1,99",
                                StileDiVisualizzazioneVersamenti = 1,
                                Macchine = listmacchine,
                                Manodopera = listdistintaBaseFasiManoDopera,
                                TempoTeoricoSec = Convert.ToInt32(macchina.Tempo),
                                PezziDaProdurre = (prblActivity?.Quantita_Batch ?? 0),
                                PezziProdotti = (prblActivity?.Quantita_Batch ?? 0),
                                Campo2 = prolActivity?.Id_Attivita_Batch,
                                Campo3 = prblActivity?.Id_PRBLAttivita,
                                Campo4 = prblActivity?.Id_PrBL_Batch,
                                //Campo5= prblMaterial?.
                                Materiali = listOfMaterialPhase,
                                Componenti = listOfComponentPhase,
                                InLavorazioneColore = "#30c93d",
                                InLavorazioneLabel = "Lavorazione",
                                InLavorazioneAbilitaTotali = true,
                                InLavorazioneInPausaColore = "#e8c70e",
                                InLavorazioneInPausaLabel = "Pausa",
                                InLavorazioneInPausaAbilitaTotali = true,
                                InLavorazioneInEmergenzaColore = "#db1a1a",
                                InLavorazioneInEmergenzaLabel = "Emergenza",
                                InLavorazioneInEmergenzaAbilitaTotali = true,
                            };
                            Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newFase.Id={newFase.Id} ");
                            Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newFase.NumeroFase={newFase.NumeroFase} ");
                            Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newFase.Nome={newFase.Nome} ");
                            Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newFase.Descrizione={newFase.Descrizione} ");
                            Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newFase.Codice={newFase.Codice} ");
                            Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newFase.TempoTeoricoSec={newFase.TempoTeoricoSec} ");
                            Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newFase.PezziDaProdurre={newFase.PezziDaProdurre} ");
                            Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newFase.PezziProdotti={newFase.PezziProdotti} ");
                            Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newFase.Campo1={newFase.Campo1} ");
                            Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newFase.Campo2={newFase.Campo2} ");
                            Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newFase.Campo3={newFase.Campo3} ");
                            Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newFase.Campo4={newFase.Campo4} ");

                            //--------------------------------------------------------------------------------------------------------
                            // GESTIONE ABILITIZIONE MULTIMEDIA SOLO SE E' PRESENTE ALMENO UN DOCUMENTO
                            //--------------------------------------------------------------------------------------------------------
                            if (listMultimediaAllegati.Count() > 0) newFase.AbilitaMultimedia = true;
                            //--------------------------------------------------------------------------------------------------------

                            //--------------------------------------------------------------------------------------------------------
                            // INSERIMENTO DELL'ARRAY BatchMultimedia, inserisco tutti gli elementi trovati per tutte le fasi del batch 
                            //--------------------------------------------------------------------------------------------------------
                            foreach (var item in listMultimediaAllegati)
                            {
                                var batchMultimedia = new BatchMultimedia();
                                batchMultimedia.NumeroFase = newFase.NumeroFase;
                                batchMultimedia.Codice = item.Id_DBFase.ToString();
                                batchMultimedia.Tipo = BatchMultimediaTipo.Pdf;
                                batchMultimedia.Url = @"http:\\\" + Const.AREAMES_WEBSERVER_IP + ":" + Const.AREAMES_WEBSERVER_MULTIMEDIA_PORT + "/" + Path.GetFileName(item.Path);
                                newBatch.BatchMultimedia.Add(batchMultimedia);
                            }
                            //--------------------------------------------------------------------------------------------------------
                            newFase.Uniquefy();
                            Logger.ArcaConnector.Debug($"import_MES_Batch           MES,         newFase.Uniquefy();");

                            distintaBase.Fasi.Add(newFase);
                            Logger.ArcaConnector.Debug("import_MES_Batch           MES, Create DistintaBaseFasi  Completed");
                            //inserisco il numero di pezzi nel batch
                            if (newBatch.PezziDaProdurre == null || newBatch.PezziDaProdurre == 0)
                                newBatch.PezziDaProdurre = (prblActivity?.Quantita_Batch ?? 0);
                            Logger.ArcaConnector.Debug("import_MES_Batch           MES,     Creating DistintaBaseFasi...  completed");

                        }

                        var lastFase = distintaBase.Fasi.LastOrDefault();
                        lastFase.AbilitaVersamentiManuali = true;

                        //aggiorno il tempo teorico e la distinta base all'interno del batch e creo il bath e lo inserisco in Ram
                        newBatch.TempoTeorico = tempoTotTeorico;
                        newBatch.DistintaBase = distintaBase;

                        Logger.ArcaConnector.Debug("import_MES_Batch           MES, Creating Distinta base... ");
                        var distinta = listOfDistinta.FirstOrDefault(x => x.Nome == distintaBase.Nome);
                        if (distinta == null)
                        {
                            distintaBase.Uniquefy();
                            await mongoInstanceGlobal.SetAsync(distintaBase);
                        }
                        else
                        {
                            distintaBase.Id = distinta.Id;
                            await mongoInstanceGlobal.SetAsync(distintaBase);
                        }
                        Logger.ArcaConnector.Debug("import_MES_Batch           MES, Creating Distinta base... completed");

                        Logger.ArcaConnector.Debug("import_MES_Batch           MES, Creating Batch... completed");

                        // Logger.ArcaConnector.InfoFormat("import_MES_Batch           MES, Save New Batch Id: {0}", newBatch.Id);
                        // //await mongoInstance.SetAsync(newBatch);
                        // Logger.ArcaConnector.InfoFormat("import_MES_Batch           MES, PrepareBatchTo_InAttesa  Batch Id: {0}", newBatch.Id);
                        //// await BatchUtils.PrepareBatchTo_InAttesa(newBatch, mongoInstance);
                        // Logger.ArcaConnector.InfoFormat("import_MES_Batch           MES, PrepareBatchTo_InAttesa  Batch Id: {0} Completed", newBatch.Id);
                        //  BatchManager.Instance.StartAsync(newBatch);

                        if(countPrbl > 0)
                        {
                            await mongoInstanceGlobal.SetAsync(newBatch);
                            Logger.ArcaConnector.InfoFormat("import_MES_Batch           MES, Save New Batch Id: {0} Completed", newBatch.Id);
                        }
                        else
                        {
                            Logger.ArcaConnector.InfoFormat("import_MES_Batch           MES, Save New Batch Id: {0} Completed", newBatch.Id);
                        }
                    }
                    else if (batchMdb != null && batchHeader.Eliminato == true)
                    {
                        if (String.IsNullOrEmpty(batchMdb.Id)) await Task.FromResult(false);
                        Logger.ArcaConnector.InfoFormat("import_MES_Batch           MES, Delete Batch Odp {0}", batchMdb.OdP);
                        if (batchMdb.Stato != Model.Enums.StatoBatch.InAttesa && batchMdb.Stato != Model.Enums.StatoBatch.Terminato)
                        {
                            await AreaMes.Server.BatchManager.Instance.RemoveAsync(batchMdb.Id);
                        }
                        await MongoRepository.Instance.DeleteAsync<Batch>(batchMdb.Id);
                        Logger.ArcaConnector.InfoFormat("import_MES_Batch           MES, Delete Batch Odp {0} Completed", batchMdb.OdP);
                    }
                    else
                    {
                        Logger.ArcaConnector.InfoFormat("import_MES_Batch           MES, Search batch_id: {0}... no action request", batchHeader.Id_Batch);
                    }


                    i++;
                }
            }

            catch (Exception exc)
            {
                Logger.ArcaConnector.Error("Error Import Batch  ", exc);
            }
            finally
            {
                sp.Stop();
                Logger.ArcaConnector.Info($"Start Import MES_Batch... completed, total minutes: '{sp.Elapsed.TotalMinutes}'");

            }


            lock (_timerLockImportBatch)
                _runningImportBatch = false;
        }

        private async void export_MES_Time(string arca_connectionString)
        {
            try
            {
                Logger.ArcaConnector.Info("export_MES_Time() starting... ");

                Logger.ArcaConnector.Debug("export_MES_Time()       Connect to ARCA DB... ");
                var _dbManager = new DbManager(arca_connectionString);
                Logger.ArcaConnector.Debug("export_MES_Time()       Connect to ARCA DB... completed ");
                Logger.ArcaConnector.Debug("export_MES_Time()       Connect to MESDB... ");
                //var mongoInstance = MongoRepository.New;
                Logger.ArcaConnector.Debug("export_MES_Time()       Connect to MES DB... completed ");

                //Lista dei materiali trovati sul db
                // var listOfNewMachine = _dbManager.Se<Arca_Macchine>();
                //lista dei  materiali presenti su mongo db
                Logger.ArcaConnector.Debug("export_MES_Time()           From MES, Get<Arca_MesProdTempi>()... ");
                var listOfRow = await mongoInstanceGlobal.All<Arca_MesProdTempi>(new string[] { }); // campi esclusi
                Logger.ArcaConnector.Debug("export_MES_Time()           From MES, Get<Arca_MesProdTempi>()... Completed ");
                foreach (var row in listOfRow)
                {
                    try
                    {
                        Logger.ArcaConnector.InfoFormat("Update INTO DB ARCA Row Code {0}", row.Id);
                        var result = _dbManager.Set<Arca_MesProdTempi>(row);

                        Logger.ArcaConnector.InfoFormat("Delete FROM DB MES Row Code {0}", row.Id);
                        await mongoInstanceGlobal.DeleteAsync<Arca_MesProdTempi>(row.Id);
                    }
                    catch (Exception exc)
                    {
                        logger.Error(exc);
                    }

                }

            }
            catch (Exception exc)
            {
                logger.Error(exc);
            }
            finally
            {
                Logger.ArcaConnector.Info("export_MES_Time() starting... completed");

            }
        }

        private async void export_MES_Pieces(string arca_connectionString)
        {
            try
            {
                Logger.ArcaConnector.Info("export_MES_Pieces() starting... ");

                Logger.ArcaConnector.Debug("export_MES_Pieces()       Connect to ARCA DB... ");
                var _dbManager = new DbManager(arca_connectionString);
                Logger.ArcaConnector.Debug("export_MES_Pieces()       Connect to ARCA DB... completed ");
                Logger.ArcaConnector.Debug("export_MES_Pieces()       Connect to MESDB... ");
                //var mongoInstance = MongoRepository.New;
                Logger.ArcaConnector.Debug("export_MES_Pieces()       Connect to MES DB... completed ");
                Logger.ArcaConnector.Debug("export_MES_Pieces()           From MES, Get<Arca_MesProdVersamenti>()... ");

                //Lista dei materiali trovati sul db
                // var listOfNewMachine = _dbManager.Se<Arca_Macchine>();
                //lista dei  materiali presenti su mongo db
                var listOfRow = await mongoInstanceGlobal.All<Arca_MesProdVersamenti>(new string[] { }); // campi esclusi
                Logger.ArcaConnector.Debug("export_MES_Pieces()           From MES, Get<Arca_MesProdVersamenti>()...Completed ");
                foreach (var row in listOfRow)
                {
                    try
                    {
                        row.DataVersamento = row.DataVersamento.ToLocalTime();
                        Logger.ArcaConnector.InfoFormat("Update to DB ARCA Row Code {0}", row.Id);
                        var result = _dbManager.Set<Arca_MesProdVersamenti>(row);

                        Logger.ArcaConnector.InfoFormat("Delete From DB MES Row Code {0}", row.Id);
                        await mongoInstanceGlobal.DeleteAsync<Arca_MesProdVersamenti>(row.Id);
                    }
                    catch (Exception exc)
                    {
                        Logger.ArcaConnector.Error("Error  export_MES_Pieces  ", exc);
                    }

                }

            }
            catch (Exception exc)
            {
                Logger.ArcaConnector.Error("Error  export_MES_Pieces  ", exc);
            }
            finally
            {
                Logger.ArcaConnector.Info("export_MES_Pieces() starting... completed");

            }
        }

        private async void import_MES_UnitOfMeasure(string arca_connectionString)
        {
            try
            {
                Logger.ArcaConnector.Info("import_MES_UnitOfMeasure() starting... ");

                Logger.ArcaConnector.Debug("import_MES_UnitOfMeasure()       Connect to ARCA DB... ");
                var _dbManager = new DbManager(arca_connectionString);
                Logger.ArcaConnector.Debug("import_MES_UnitOfMeasure()       Connect to ARCA DB... completed ");
                Logger.ArcaConnector.Debug("import_MES_UnitOfMeasure()       Connect to MESDB... ");
                //var mongoInstance = MongoRepository.New;
                Logger.ArcaConnector.Debug("import_MES_UnitOfMeasure()       Connect to MES DB... completed ");
                Logger.ArcaConnector.Debug("import_MES_UnitOfMeasure()           From ARCA, Get<Arca_Misura>()... ");
                //Lista dei materiali trovati sul db
                var listOfNewUnitOfMeasure = _dbManager.Get<Arca_Misura>();
                Logger.ArcaConnector.Debug("import_MES_UnitOfMeasure()           From ARCA, Get<Arca_Misura>()... completed ");
                Logger.ArcaConnector.Debug("import_MES_UnitOfMeasure()           From MES, All<UnitaDiMisura>()... ");
                //lista dei  materiali presenti su mongo db
                var listOfUnitOfMeasure = await mongoInstanceGlobal.All<UnitaDiMisura>(new string[] { }); // campi esclusi
                Logger.ArcaConnector.Debug("import_MES_UnitOfMeasure()           From MES, All<UnitaDiMisura>()... ");
                Logger.ArcaConnector.Debug("import_MES_UnitOfMeasure()       Connect to MES DB... completed ");
                foreach (var UnitOfMeasure in listOfNewUnitOfMeasure)
                {
                    Logger.ArcaConnector.Debug($"import_MES_UnitOfMeasure()       Check  Id_Misura:'{UnitOfMeasure.Id_Misura}'... ");
                    //controllo se esiste già l'operatore
                    var UnitOfMeasureMdb = listOfUnitOfMeasure.FirstOrDefault(x => x.Codice == Convert.ToString(UnitOfMeasure.Codice));
                    var newUnitOfMeasure = new UnitaDiMisura();
                    //se non esiste lo creo

                    if (UnitOfMeasureMdb == null && UnitOfMeasure.Eliminato == false)
                    {
                        Logger.ArcaConnector.Debug($"import_MES_UnitOfMeasure()           Id_Misura:'{UnitOfMeasure.Id_Misura}', doesn't exist into MES, will be imported... ");
                        newUnitOfMeasure = new UnitaDiMisura
                        {
                            Codice = UnitOfMeasure.Codice,
                            Nome = UnitOfMeasure.Descrizione,
                            Descrizione = UnitOfMeasure.Descrizione,
                            CreateDate = DateTime.Now,
                            LastUpdate = DateTime.Now,
                        };
                        newUnitOfMeasure.Uniquefy();
                        await mongoInstanceGlobal.SetAsync(newUnitOfMeasure);
                        Logger.ArcaConnector.Debug($"import_MES_UnitOfMeasure()           Id_Misura:'{UnitOfMeasure.Id_Misura}', doesn't exist into MES, will be imported... completed");
                    }

                    //se esiste lo aggiorno
                    else if (UnitOfMeasureMdb != null && UnitOfMeasure.Eliminato == false)
                    {
                        if (DateTime.Compare(UnitOfMeasure.UltimoAggionamento, UnitOfMeasureMdb.LastUpdate.ToLocalTime()) != -1)
                        {
                            Logger.ArcaConnector.Debug($"import_MES_UnitOfMeasure()           Id_Misura:'{UnitOfMeasure.Id_Misura}',exist into MES, will be update... ");
                            newUnitOfMeasure = new UnitaDiMisura
                            {
                                Codice = UnitOfMeasure.Codice,
                                Nome = UnitOfMeasure.Descrizione,
                                Descrizione = UnitOfMeasure.Descrizione,
                                CreateDate = DateTime.Now,
                                LastUpdate = DateTime.Now,

                            };
                            await mongoInstanceGlobal.SetAsync(newUnitOfMeasure);
                            Logger.ArcaConnector.Debug($"import_MES_UnitOfMeasure()           Id_Misura:'{UnitOfMeasure.Id_Misura}',exist into MES, will be update... Completed");
                        }
                    }

                    //se operatore è stato eliminato ed è ancora in mongodb lo cancello
                    else if (UnitOfMeasureMdb != null && UnitOfMeasure.Eliminato == true)
                    {
                        Logger.ArcaConnector.Debug($"import_MES_UnitOfMeasure()           Id_Misura:'{UnitOfMeasure.Id_Misura}',Delete into MES, will be delete... ");
                        await mongoInstanceGlobal.DeleteAsync<Operatore>(UnitOfMeasureMdb.Id);
                        Logger.ArcaConnector.Debug($"import_MES_UnitOfMeasure()           Id_Misura:'{UnitOfMeasure.Id_Misura}',Delete into MES, will be delete... Completed ");
                    }


                }

            }
            catch (Exception exc)
            {
                Logger.ArcaConnector.Error("Error  import_MES_UnitOfMeasure  ", exc);

            }
            finally
            {
                Logger.ArcaConnector.Info("import_MES_UnitOfMeasure() starting... completed");

            }
        }

        private async void export_MES_Scrap(string arca_connectionString)
        {
            try
            {
                Logger.ArcaConnector.Info("export_MES_Scrap() starting... ");

                Logger.ArcaConnector.Debug("export_MES_Scrap()       Connect to ARCA DB... ");
                var _dbManager = new DbManager(arca_connectionString);
                Logger.ArcaConnector.Debug("export_MES_Scrap()       Connect to ARCA DB... completed ");
                Logger.ArcaConnector.Debug("export_MES_Scrap()       Connect to MESDB... ");
                //var mongoInstance = MongoRepository.New;
                Logger.ArcaConnector.Debug("export_MES_Scrap()       Connect to MES DB... completed ");
                Logger.ArcaConnector.Debug("export_MES_Scrap()           From MES, Get<Arca_MesProdScarti>()... ");

                //Lista dei materiali trovati sul db
                // var listOfNewMachine = _dbManager.Se<Arca_Macchine>();
                //lista dei  materiali presenti su mongo db
                var listOfRow = await mongoInstanceGlobal.All<Arca_MesProdScarti>(new string[] { }); // campi esclusi
                Logger.ArcaConnector.Debug("export_MES_Scrap()           From MES, Get<Arca_MesProdScarti>()...Completed ");
                foreach (var row in listOfRow)
                {
                    try
                    {
                        Logger.ArcaConnector.InfoFormat("Update to DB ARCA Row Code {0}", row.Id);
                        var result = _dbManager.Set<Arca_MesProdScarti>(row);

                        if (result != null)
                        {
                            Logger.ArcaConnector.InfoFormat("Delete From DB MES Row Code {0}", row.Id);
                            await mongoInstanceGlobal.DeleteAsync<Arca_MesProdScarti>(row.Id);

                        }

                    }
                    catch (Exception exc)
                    {
                        Logger.ArcaConnector.Error("export_MES_Scrap  error Delete {0}", exc);
                    }

                }

            }
            catch (Exception exc)
            {
                Logger.ArcaConnector.Error("Error  export_MES_Pieces  ", exc);
            }
            finally
            {
                Logger.ArcaConnector.Info("export_MES_Pieces() starting... completed");

            }
        }

        private async void export_MES_Component(string arca_connectionString)
        {
            try
            {
                Logger.ArcaConnector.Info("export_MES_Scrap() starting... ");

                Logger.ArcaConnector.Debug("export_MES_Scrap()       Connect to ARCA DB... ");
                var _dbManager = new DbManager(arca_connectionString);
                Logger.ArcaConnector.Debug("export_MES_Scrap()       Connect to ARCA DB... completed ");
                Logger.ArcaConnector.Debug("export_MES_Scrap()       Connect to MESDB... ");
                //var mongoInstance = MongoRepository.New;
                Logger.ArcaConnector.Debug("export_MES_Scrap()       Connect to MES DB... completed ");
                Logger.ArcaConnector.Debug("export_MES_Scrap()           From MES, Get<Arca_MesProdScarti>()... ");

                //Lista dei materiali trovati sul db
                // var listOfNewMachine = _dbManager.Se<Arca_Macchine>();
                //lista dei  materiali presenti su mongo db
                var listOfRow = await mongoInstanceGlobal.All<Arca_MesProdComponenti>(new string[] { }); // campi esclusi
                Logger.ArcaConnector.Debug("export_MES_Scrap()           From MES, Get<Arca_MesProdScarti>()...Completed ");
                foreach (var row in listOfRow)
                {
                    try
                    {
                        row.DataConsumo = row.DataConsumo.ToLocalTime();
                        Logger.ArcaConnector.InfoFormat("Update to DB ARCA Row Code {0}", row.Id);
                        var result = _dbManager.Set<Arca_MesProdComponenti>(row);

                        if (result != null)
                        {
                            Logger.ArcaConnector.InfoFormat("Delete From DB MES Row Code {0}", row.Id);
                            await mongoInstanceGlobal.DeleteAsync<Arca_MesProdComponenti>(row.Id);

                        }

                    }
                    catch (Exception exc)
                    {
                        Logger.ArcaConnector.Error("export_MES_Scrap  error Delete {0}", exc);
                    }

                }

            }
            catch (Exception exc)
            {
                Logger.ArcaConnector.Error("Error  export_MES_Pieces  ", exc);
            }
            finally
            {
                Logger.ArcaConnector.Info("export_MES_Pieces() starting... completed");

            }
        }
        private async void import_Mes_CheckTableUpdated(string arca_connectionString)
        {
            try
            {
                Logger.ArcaConnector.Info("import_Mes_CheckTableUpdated() starting... ");

                Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()       Connect to ARCA DB... ");
                var _dbManager = new DbManager(arca_connectionString);
                Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()       Connect to ARCA DB... completed ");
                Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()       Connect to MESDB... ");
                // var mongoInstance = MongoRepository.New;
                Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()       Connect to MES DB... completed ");
                Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()           From ARCA, Get<Arca_TableUpdated>()... ");
                var listOfNewUpdate = _dbManager.Get<Arca_TableUpdated>();
                Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()           From ARCA, Get<Arca_TableUpdated>()... completed ");


                //filtro per la tabella operatori eventualmente aggiorno ed elimino le righe nella tabella Arca_TableUpdated
                Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()               Filter listOfOperator ");
                var listOfOperator = listOfNewUpdate.Where(x => x.Nome == "Operatore").ToList();
                Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()               Filter listOfOperator... Completed ");
                if (listOfOperator.Count > 0)
                {
                    Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()               listOfOperator Import/Update Operator ");
                    import_MES_Operator(arca_connectionString);
                    Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()               listOfOperator Import/Update Operator Completed ");
                    foreach (var item in listOfOperator)
                    {
                        Logger.ArcaConnector.Debug($"import_Mes_CheckTableUpdated()       listOfOperator Delete Row item:'{ item.Nome}'... ");
                        _dbManager.Delete(item);
                        Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()               listOfOperator Delete Row Completed ");
                    }
                }

                //filtro per la tabella unità di misura eventualmente aggiorno ed elimino le righe nella tabella Arca_TableUpdated
                Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()               Filter listOfUnitOfMeasure ");
                var listOfUnitOfMeasure = listOfNewUpdate.Where(x => x.Nome == "AR").ToList();
                Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()               Filter listOfUnitOfMeasure... Completed ");
                if (listOfUnitOfMeasure.Count > 0)
                {
                    Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()               listOfUnitOfMeasure Import/Update UnitOfMeasure ");
                    import_MES_Material(arca_connectionString);
                    Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()               listOfUnitOfMeasure Import/Update UnitOfMeasure Completed ");

                    foreach (var item in listOfUnitOfMeasure)
                    {
                        Logger.ArcaConnector.Debug($"import_Mes_CheckTableUpdated()       listOfUnitOfMeasure Delete Row item:'{ item.Nome}'... ");
                        _dbManager.Delete(item);
                        Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()               listOfUnitOfMeasure Delete Row Completed ");
                    }
                }

                ////filtro per la tabella Macchine eventualmente aggiorno ed elimino le righe nella tabella Arca_TableUpdated
                //Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()               Filter listOfMachine ");
                //var listOfMachine = listOfNewUpdate.Where(x => x.Nome == "PRRisorsa").ToList();
                //Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()               Filter listOfMachine... Completed ");
                //if (listOfMachine.Count > 0)
                //{
                //    Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()               listOfMachine Import/Update Machine ");
                //    import_MES_Machine(arca_connectionString);
                //    Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()               listOfMachine Import/Update Machine Completed ");

                //    foreach (var item in listOfMachine)
                //    {
                //        Logger.ArcaConnector.Debug($"import_Mes_CheckTableUpdated()       listOfMachine Delete Row item:'{ item.Nome}'... ");
                //        _dbManager.Delete(item);
                //        Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()               listOfMachine Delete Row Completed ");
                //    }
                //}
                Logger.ArcaConnector.Debug("import_Mes_CheckTableUpdated()       Connect to MES DB... completed ");

            }
            catch (Exception exc)
            {

                Logger.ArcaConnector.Error("Error  import_Mes_CheckTableUpdated  ", exc);
            }
            finally
            {
                Logger.ArcaConnector.Info("import_Mes_CheckTableUpdated() starting... completed");

            }
        }
        public async Task<int> GetBatchAutoPriorityCounterInfo()
        {
            var allpa = await MongoRepository.Instance.All<Parametri>();
            var CNT_BATCH_PRIORITY = allpa.FirstOrDefault(o => o.Codice == "CNT_BATCH_PRIORITY");

            if (CNT_BATCH_PRIORITY != null)
            {
                var _cntPriorita = Convert.ToInt32(CNT_BATCH_PRIORITY.ValoreDefault);
                _cntPriorita += 1;
                CNT_BATCH_PRIORITY.ValoreDefault = Convert.ToString(_cntPriorita);

                await MongoRepository.Instance.SetAsync(CNT_BATCH_PRIORITY);

                return _cntPriorita;
            }
            return 0;
        }
    }
}
