﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AreaMes.Model.Runtime;
using AreaMes.Server.Utils;
using System.Threading;
using AreaMes.Model;
using AreaMes.Meta;
using AreaMes.BatchLogic.ArcaConnector.Utility;

namespace AreaMes.BatchLogic.ArcaConnector
{
    public class ConnectorManager: SupervisorBatchLogicBase
    {
        private Timer _timer;
        private bool _timerRunning = false;
        private object _timerLock = new object();

        private string _DB_CONNECTION_STRING { get; set; }
        private string _TIMER_SYNC_1_TICK { get; set; }
        private string _EVALUATE_PAUSE_INTERVAL { get; set; }
        private int _DB_POOL_ID { get; set; }
        private List<Tuple<TimeSpan, DailyTimer>> _EVALUATE_PAUSE_INTERVAL_TS = new List<Tuple<TimeSpan, DailyTimer>>();
        private string _LETTORE_RFID_1 { get; set; }
        private string _LETTORE_RFID_2 { get; set; }

        private string _PATH_MULTIMEDIA { get; set; }



        public override void Init()
        {

            #region Configurazione 

            Default.InfoFormat("Begin configuration");

            _DB_CONNECTION_STRING = Convert.ToString(this.GetSettingValue("DB_CONNECTION_STRING"));
            if (_DB_CONNECTION_STRING == null)
                throw new Exception(String.Format("Impossibile trovare la chiave di configurazione '{0}'", "DB_CONNECTION_STRING"));
            Default.InfoFormat(" DB_CONNECTION_STRING={0}", _DB_CONNECTION_STRING);


            try
            {
                if (!String.IsNullOrWhiteSpace(_EVALUATE_PAUSE_INTERVAL))
                {
                    var list = _EVALUATE_PAUSE_INTERVAL.Split(';');
                    foreach (var item in list)
                    {
                        var hour1 = Convert.ToInt32(item.Split(':')[0]);
                        var minute1 = Convert.ToInt32(item.Split(':')[1]);
                        var ts = new TimeSpan(hour1, minute1, 0);
                        var tm = new DailyTimer(ts, () => { setShiftAction(); });
                        _EVALUATE_PAUSE_INTERVAL_TS.Add(new Tuple<TimeSpan, DailyTimer>(ts, tm));
                    }
                }
            }
            catch (Exception)
            {
                throw new Exception(String.Format("Impossibile convertire gli intervalli '{0}' in tempi corretti. ", _EVALUATE_PAUSE_INTERVAL));
            }

            Default.InfoFormat("Configuration completed");
            #endregion


            // --------------------------------------------------------------------------------------------------------
            // Broadcast MESSAGE : Logon
            // --------------------------------------------------------------------------------------------------------
            MessageServices.Instance.Subscribe<Logon>(Logon.Name, async (o, e) => {

                Default.InfoFormat("User logon '{0}' ", e.Username);
                await doUpdate_sp_MES_poollist(e.Username);

            });



            // avvio del timer principale
            _timer = new Timer(async (a) => {

                lock (_timerLock)
                {
                    if (_timerRunning) return;
                    _timerRunning = true;
                }

                try
                {
                    //var dbDB = new DbManager(_DB_CONNECTION_STRING);
                    //var lPers = await import_MES_PERS(dbDB);
                    //await import_MES_INTERR(dbDB);
                    //dbDB.Dispose();
                }
                catch (Exception exc)
                {
                    Default.Error(exc);
                }


                lock (_timerLock)
                    _timerRunning = false;

                // ricerca nuovi ordini da DB
            }, null, 5000, Convert.ToInt32(_TIMER_SYNC_1_TICK) * 1000);
        }


        private async Task doUpdate_sp_MES_poollist(string username)
        {
            if (username != null)
            {
                try
                {
                    Default.InfoFormat("Executing doUpdate_sp_MES_poollist('{0}')...", username);
                    var dbDB = new DbManager(_DB_CONNECTION_STRING);
                    dbDB.ExecuteSp((_DB_POOL_ID == 0 ? "sp_MES_poollist_ALL" : "sp_MES_poollist"),
                        new Dictionary<string, object>() {
                                                { "as_pers_id", username } ,
                                                { "as_station_id", "SERVER1" },
                                                { "as_bomcheck", "Y" }
                        });
                    await import_DB_Batch(dbDB);
                    dbDB.Dispose();
                    Default.InfoFormat("Executed Sync doUpdate_sp_MES_poollist('{0}')", username);
                }
                catch (Exception exc)
                {
                    Default.ErrorFormat("Error Sync from DB: update pool list '{0}'. Cause:{1}", username, exc.Message);
                    Default.Error(exc);
                }
            }
        }


        //private async void doUpdate_BEAS_FTSTL_Write(int BELNR_ID, int BELPOS_ID, string material, string rfid)
        //{
        //    try
        //    {
        //        Default.InfoFormat("Executing doUpdate_BEAS_FTSTL on Write({0},{1},'{2}')...", BELNR_ID, BELPOS_ID, material);
        //        var dbDB = new DbManager(_DB_CONNECTION_STRING);
        //        dbDB.ExecuteNonQuery("update BEAS_FTSTL set UDF4 = 'Y', UDF5=SYSDATETIME() where BELNR_ID = @belnrid and BELPOS_ID = @belposid and ART1_ID=@item",
        //            new Dictionary<string, object>() {
        //                    { "belnrid", BELNR_ID } ,
        //                    { "belposid", BELPOS_ID },
        //                    { "item", material }
        //            });



        //        // ----------------------------------------------------------------------------------------------
        //        // salvataggio nella tabella esterna
        //        // ----------------------------------------------------------------------------------------------
        //        var mbarcode = new MES_BARCODE { BarCode_Rfid = rfid, BarCode_Mat = rfid, BELNR_ID = BELNR_ID, BELPOS_ID = BELPOS_ID };
        //        var split = rfid.Split('a');
        //        if (split.Count() == 2)
        //        {
        //            mbarcode.BarCode_Father = split[0];
        //            mbarcode.BarCode_Mat = split[1];
        //        }
        //        dbDB.Set(mbarcode);
        //        // ----------------------------------------------------------------------------------------------

        //        dbDB.Dispose();
        //        Default.InfoFormat("Executed doUpdate_BEAS_FTSTL on Write({0},{1},'{2}')...", BELNR_ID, BELPOS_ID, material);
        //    }
        //    catch (Exception exc)
        //    {
        //        Default.ErrorFormat("Error executing doUpdate_BEAS_FTSTL on Write({0},{1},'{2}')...", BELNR_ID, BELPOS_ID, material);
        //        Default.Error(exc);
        //    }
        //}

        //private async void doUpdate_BEAS_FTSTL_Read(string barcode)
        //{
        //    try
        //    {
        //        if (String.IsNullOrWhiteSpace(barcode)) return;

        //        var BELNR_ID = 0;
        //        //var BELPOS_ID = 0;

        //        Default.InfoFormat("Executing doUpdate_BEAS_FTSTL on Read('{0}')...", barcode);
        //        var dbDB = new DbManager(_DB_CONNECTION_STRING);
        //        var listOfMatRfidTagged = dbDB.Search<MES_BARCODE>(o => o.BarCode_Mat == barcode);

        //        if (listOfMatRfidTagged.Count() == 0) { Default.InfoFormat("Executed doUpdate_BEAS_FTSTL on Read('{0}')... item not found!", barcode); return; }
        //        if (listOfMatRfidTagged.Count() > 1) { Default.InfoFormat("Executed doUpdate_BEAS_FTSTL on Read('{0}')... too many result!", barcode); return; }

        //        BELNR_ID = listOfMatRfidTagged.FirstOrDefault().BELNR_ID;
        //        //BELPOS_ID = listOfMatRfidTagged.FirstOrDefault().BELPOS_ID;

        //        dbDB.ExecuteNonQuery("update BEAS_FTSTL set UDF6 = 'Y', UDF7=SYSDATETIME() where BELNR_ID = @belnrid and nummer=@nummer",
        //            new Dictionary<string, object>() { { "belnrid", BELNR_ID }, { "nummer", barcode } });
        //        dbDB.Dispose();
        //        Default.InfoFormat("Executed doUpdate_BEAS_FTSTL on Read({0},'{1}')...", BELNR_ID, barcode);
        //    }
        //    catch (Exception exc)
        //    {
        //        Default.ErrorFormat("Error executing doUpdate_BEAS_FTSTL on Read('{0}')...", barcode);
        //        Default.Error(exc);
        //    }
        //}




        //private async Task<List<MES_BATCH>> import_MES_BATCH(DbManager DB)
        //{
        //    var listofUser = DB.Get<MES_BATCH>();

        //    foreach (var user in listofUser)
        //    {
        //        //var opC = new OperatoreCompetenza { Codice = user.APLATZ_ID, Id = user.CARDNUMBER };
        //        //await Repository.SetAsync(opC);

        //        //await Repository.SetAsync(new Operatore
        //        //{
        //        //    //Id = user.DisplayName,
        //        //    //Username = user.CARDNUMBER,
        //        //    //OperatoreCompetenza = new ExternalDoc<OperatoreCompetenza>() { Id = user.CARDNUMBER }
        //        //});
        //    }
        //    //Default.InfoFormat("Executed import_MES_PERS()");
        //    return listofUser.ToList();
        //}

        //private async Task import_MES_INTERR(DbManager DB)
        //{
        //    var listof = DB.Get<MES_INTERR>();

        //    foreach (var user in listof)
        //        await Repository.SetAsync(new CausaliRiavvio { Codice = user.GRUNDID, Id = user.GRUNDID, Causale = user.GRUNDINFO });

        //    //Default.InfoFormat("Executed import_MES_INTERR()");
        //}

        private async Task import_DB_Batch(DbManager dbDB)
        {
            var newFromDB = await create_DB_Batch(dbDB);


            var listOfBatchInMemory = this.BatchInMemory.Invoke();
            foreach (var newBatch in newFromDB)
            {
                await BatchUtils.PrepareBatchTo_InAttesa(newBatch, Repository);
                var batchFounded = listOfBatchInMemory.FirstOrDefault(o => o.Id == newBatch.Id);

                if (batchFounded == null)
                    this.Start(newBatch);
                else
                    lock (batchFounded)
                        doCompareBatch(batchFounded, newBatch);
            }


            // verifica dei batch da eliminare in memoria
            lock (listOfBatchInMemory)
            {

                foreach (var item in listOfBatchInMemory.ToList())
                {
                    var batchExistInMemory = newFromDB.FirstOrDefault(o => o.Id == item.Id);
                    if (batchExistInMemory == null)
                    {
                        Default.InfoFormat("Sync from DB: BATCH '{0}' not found from DB. Will be removed from MES", item.Id);
                        MessageServices.Instance.Publish("SetBatchInfo", new SetBatchInfo { idBatch = item.Id, idStato = Model.Enums.StatoBatch.Terminato });
                    }
                }

            }

        }



        private async Task<List<Batch>> create_DB_Batch(DbManager DB)
        {

            //return await Task.Run(() => {

            var ret1 = new List<Batch>();


            //    var DBOrderClosed = DB.Get<BEAS_FTHAUPT>().Where(o => o.ABGKZ == "J");
            //    var DBPool = new List<MES_POOL>(DB.Get<MES_POOL>()).Where(o => !String.IsNullOrWhiteSpace(o.CARDNUMBER)).OrderBy(o => o.VON);
            //    var DBBatch = DBPool.GroupBy(o => o.BELNR_ID).Select(o => o.Key).ToList();
            //    var repoListOfComp = Repository.All<OperatoreCompetenza>().Result.ToList();

            //    foreach (var item in DBBatch)
            //    {
            //        try
            //        {
            //            if (DBOrderClosed.Any(o => o.BELNR_ID == item))
            //            {
            //                Default.InfoFormat("   Sync from DB: BATCH for Odp {0} is closed.", item);
            //                continue;
            //            }

            //            var mesWoBy = DB.Search<MES_WO>("BELNR_ID", item)
            //                   .OrderBy(o => o.BELNR_ID)
            //                   .ThenBy(o => o.POS_PADRE)
            //                   .ThenBy(o => o.BELPOS_ID)
            //                   .ThenBy(o => o.Origine)
            //                   .ThenBy(o => o.POS_fase)
            //                   .ThenBy(o => o.POS_mat)
            //                   .ToList();

            //            var firstItem = mesWoBy.FirstOrDefault(o => o.Origine == "1-Pos");

            //            // creazione della testata del batch
            //            var batch = new Batch
            //            {
            //                Id = firstItem.BELNR_ID.ToString(),
            //                OdP = firstItem.BELNR_ID.ToString(),
            //                BatchMateriali = new List<BatchMateriali>(),
            //                BatchMultimedia = new List<BatchMultimedia>(),
            //                OdV_DescrizioneCliente = firstItem.Commessa,
            //                OdPParziale = firstItem.Commessa,
            //                Commessa = firstItem.Commessa,
            //                Materiale = new Materiale { Nome = firstItem.Descrizione, Codice = firstItem.Assieme },
            //                DistintaBase = new DistintaBase
            //                {
            //                    Fasi = new List<DistintaBaseFasi>(),
            //                    Nome = firstItem.Commessa,
            //                    AbilitaEmergenza = false,
            //                    AbilitaStopProduzione = true,
            //                    AbilitaAutoStartProduzione = true
            //                }
            //            };
            //            batch.Stato = Model.Enums.StatoBatch.InAttesa;

            //            // viene cercata la fase di taggatura(in scrittura) per il materiale
            //            var matTagByBatch = mesWoBy.Where(o => o.Origine == "2-Mat" && Convert.ToString(o.TAG_YN).ToUpper() == "Y").ToList();


            //            foreach (var poolItem in DBPool.Where(o => o.BELNR_ID == item))
            //            {
            //                var woFase = mesWoBy.FirstOrDefault(o => o.BELPOS_ID == poolItem.BELPOS_ID &&
            //                                Convert.ToString(o.POS_fase) == Convert.ToString(poolItem.POS_fase) &&
            //                                o.Origine == "3-Fasi");
            //                //var woFaseAlt = mesWoBy.FirstOrDefault(o => o.BELPOS_ID == poolItem.BELPOS_ID && o.Origine == "3-Fasi");

            //                var woPos = mesWoBy.FirstOrDefault(o => o.BELPOS_ID == poolItem.BELPOS_ID && o.Origine == "1-Pos");
            //                //var woFaseTmp = mesWoBy.FirstOrDefault(o => o.BELPOS_ID == poolItem.BELPOS_ID && o.Origine == "3-Fasi");


            //                if (woFase == null)
            //                    continue;

            //                // ricerca dell'operatore per 
            //                var opComp = repoListOfComp.FirstOrDefault(o => o.Id == poolItem.CARDNUMBER);

            //                // creazione della distinta
            //                var distinta = new Model.DistintaBaseFasi
            //                {
            //                    NumeroFase = String.Format("{0}.{1}.{2}", woFase.Livello, woFase.BELPOS_ID, woFase.POS_fase),
            //                    Nome = poolItem.BEZ + ", " + woFase.Descrizione,
            //                    Codice = woFase.Assieme,
            //                    Descrizione = woFase.Descrizione,
            //                    Campo1 = Convert.ToString(Math.Round(woPos.QTY_Completata, 1)),
            //                    Campo2 = poolItem.BIS.ToString("dd MMM HH:mm"),
            //                    Campo3 = String.Format("Inizio previsto: {0}   -   Termine previsto: {1}", poolItem.VON.ToString("dd MMM HH:mm"), poolItem.BIS.ToString("dd MMM HH:mm")),
            //                    Campo4 = String.Format("{0} {1} (Magazino: {2})", Convert.ToString(Math.Round(woPos.QTY, 1)), woPos.UM, woPos.Magazzino),
            //                    Campo5 = woFase.FASE_MSGINFO,
            //                    AbilitaAvanzamenti = true,
            //                    Fasi = new List<DistintaBaseFasi>(),
            //                    Macchine = new List<DistintaBaseFasiMacchina>() { new DistintaBaseFasiMacchina() },
            //                    Manodopera = new List<DistintaBaseFasiManoDopera>() {  new DistintaBaseFasiManoDopera {
            //                        CausaleFase = Model.Enums.CausaleFase.Lavorazione,
            //                        Tempo = Convert.ToInt32(woFase.QTY),
            //                        OperatoreCompetenza = new ExternalDoc<OperatoreCompetenza> { Id = poolItem.CARDNUMBER }  } },
            //                    AbilitaMateriali = true,
            //                    AbilitaMultimedia = true,
            //                    AbilitaVersamentiManuali = true,
            //                    Visibilitastazione = String.Empty,
            //                    AbilitaAvanzamentiPausa = true,
            //                    AbilitaAvanzamentiStar = true,
            //                    AbilitaAvanzamentiStop = true,
            //                    Abilitabarcode = false,
            //                    AbilitaLetturaRfid = true,
            //                    AbilitascritturaRfid = true,
            //                };



            //                // inserimento della fase in distinta
            //                batch.DistintaBase.Fasi.Add(distinta);



            //                // ----------------------------------------------------------------------------------------
            //                // associazione dei materiali alla fase
            //                var woMat_temp = mesWoBy.Where(o => o.BELPOS_ID == poolItem.BELPOS_ID && o.Origine == "2-Mat").ToList();
            //                //var woMat_temp = mesWoBy.Where(o => o.POS_fase == Convert.ToString( poolItem.BELPOS_ID) && o.Origine == "2-Mat").ToList();
            //                var woMat_temp1 = woMat_temp.Where(o => !o.LEGMAT_FASE.HasValue).ToList();
            //                var woMat_temp2 = woMat_temp.Where(o => o.LEGMAT_FASE.HasValue && Convert.ToString(o.LEGMAT_FASE) == woFase.POS_fase).ToList();
            //                woMat_temp1.AddRange(woMat_temp2);

            //                if (woMat_temp2.Count == 0)
            //                    foreach (var subItem in woMat_temp)
            //                    {
            //                        var itemFounded = woMat_temp1.Any(o => o.BELPOS_ID == subItem.BELPOS_ID && o.Material == subItem.Material && subItem.BARCODE == o.BARCODE);
            //                        if (!itemFounded)
            //                            woMat_temp1.Add(subItem);
            //                    }


            //                foreach (var itemMat in woMat_temp1)
            //                {
            //                    int barcodePadre = 0;
            //                    // ricerca del materiale padre
            //                    var woMatPadre = mesWoBy.FirstOrDefault(o => o.BELPOS_ID == itemMat.POS_PADRE && o.Origine == "2-Mat" && o.Material == itemMat.Assieme);
            //                    if (woMatPadre != null)
            //                        barcodePadre = woMatPadre.BARCODE;

            //                    // viene cercata la fase di taggatura(in scrittura) per il materiale
            //                    var faseDiTaggatura = mesWoBy.FirstOrDefault(o => o.BELPOS_ID == itemMat.BELPOS_ID && o.BELNR_ID == itemMat.BELNR_ID
            //                                                                && o.Origine == "3-Fasi"
            //                                                                && Convert.ToString(o.APL_UDF1)?.ToUpper() == "Y");





            //                    var idMateriale = String.Format("{0}.{1}.{2}", itemMat.Livello, itemMat.BELPOS_ID, itemMat.POS_mat);
            //                    string barcode = Convert.ToString(itemMat.BARCODE);
            //                    if (String.IsNullOrWhiteSpace(barcode))
            //                        barcode = idMateriale;

            //                    batch.BatchMateriali.Add(new BatchMateriali
            //                    {
            //                        NumeroFase = distinta.NumeroFase,
            //                        Barcode = barcode,
            //                        QntPrevista = Math.Abs(Convert.ToDouble(itemMat.QTY)),
            //                        QntEffettiva = Math.Abs(Convert.ToDouble(itemMat.QTY_Completata)),
            //                        BarcodeRfid = barcode + (barcodePadre > 0 ? "a" + barcodePadre : String.Empty),
            //                        TipoGestione = Convert.ToString(itemMat.TAG_YN)?.ToUpper() == "Y" ? TipoGestione.Rfid : TipoGestione.Barcode,
            //                        TagAzione = (faseDiTaggatura != null ? BatchMaterialiTagAzione.Scrittura : BatchMaterialiTagAzione.Nd),
            //                        BatchMaterialiTipo = itemMat.QTY > 0 ? BatchMaterialiTipo.Scarico : BatchMaterialiTipo.Carico,
            //                        BatchMaterialiStato = (Convert.ToString(itemMat.STL_UDF4)?.ToUpper() == "Y" ? BatchMaterialiStato.Completato : BatchMaterialiStato.Nd),
            //                        Materiale = new Materiale
            //                        {
            //                            Id = idMateriale,
            //                            Codice = itemMat.Material,
            //                            Descrizione = itemMat.MaterialName,
            //                            Um = itemMat.UM,
            //                            TipoMateriale = TipoMateriale.Semilavorato,

            //                        }
            //                    });


            //                    // -------------------------------------------------------------------------
            //                    // Elementi multimediali per i materiali
            //                    int i = 0;
            //                    if (!String.IsNullOrWhiteSpace(itemMat.MAT_DOC))
            //                        foreach (var media in itemMat.MAT_DOC.Split(';'))
            //                        {
            //                            var newMedia = newMultimedia(media, idMateriale + ".mat" + i, distinta.NumeroFase);
            //                            if (newMedia.Tipo != BatchMultimediaTipo.None)
            //                                batch.BatchMultimedia.Add(newMedia);
            //                            i++;
            //                        }
            //                    // -------------------------------------------------------------------------

            //                }

            //                // Elementi multimediali per le fasi
            //                int j = 0;
            //                if (!String.IsNullOrWhiteSpace(woFase.POS_DOCS))
            //                    foreach (var media in woFase.POS_DOCS.Split(';'))
            //                    {
            //                        var newMedia = newMultimedia(woFase.POS_DOCS, distinta.NumeroFase + ".pos", distinta.NumeroFase);
            //                        if (newMedia.Tipo != BatchMultimediaTipo.None)
            //                            batch.BatchMultimedia.Add(newMedia);
            //                        j++;
            //                    }


            //                // Elementi multimediali per le fasi
            //                if (!String.IsNullOrWhiteSpace(woFase.FASE_DOC1))
            //                    batch.BatchMultimedia.Add(newMultimedia(woFase.FASE_DOC1, distinta.NumeroFase + ".fas1", distinta.NumeroFase));

            //                // Elementi multimediali per le fasi
            //                if (!String.IsNullOrWhiteSpace(woFase.FASE_DOC2))
            //                    batch.BatchMultimedia.Add(newMultimedia(woFase.FASE_DOC2, distinta.NumeroFase + ".fas2", distinta.NumeroFase));

            //                // Elementi multimediali per le fasi
            //                if (!String.IsNullOrWhiteSpace(woFase.FASE_DOC3))
            //                    batch.BatchMultimedia.Add(newMultimedia(woFase.FASE_DOC3, distinta.NumeroFase + ".fas3", distinta.NumeroFase));

            //            }


            //            // verifica attivazione pulsante di LEGGI per i TAG
            //            var listOfMatRfid = batch.BatchMateriali.Where(o => o.TipoGestione == TipoGestione.Rfid && o.TagAzione == BatchMaterialiTagAzione.Nd).ToList();
            //            var batchId = firstItem.BELNR_ID;
            //            var listOfMatRfidTagged = DB.Search<MES_BARCODE>(o => o.BELNR_ID == batchId);

            //            if (listOfMatRfidTagged.Count() > 0)
            //            {
            //                foreach (var matBatch in listOfMatRfid)
            //                {
            //                    var childItem = listOfMatRfidTagged.FirstOrDefault(o => o.BarCode_Mat == matBatch.Barcode);
            //                    if (childItem != null)
            //                        matBatch.TagAzione = BatchMaterialiTagAzione.Lettura;

            //                }
            //            }



            //            ret1.Add(batch);
            //        }
            //        catch (Exception exc)
            //        {
            //            Default.Error(exc);
            //        }
            //    }

            return ret1;
            //});




        }





        private void doCompareBatch(Batch b1, Batch b2)
        {

            if (!String.Equals(b1.OdP, b2.OdP))
                b1.OdP = b2.OdP;

            if (!String.Equals(b1.OdV_DescrizioneCliente, b2.OdV_DescrizioneCliente))
            {
                Default.InfoFormat("   Sync from DB: found BATCH difference for Odp {0}, {1}  '{2}' <> '{3}'", b1.OdP, "OdV_DescrizioneCliente", b1.OdV_DescrizioneCliente, b2.OdV_DescrizioneCliente);
                b1.OdV_DescrizioneCliente = b2.OdV_DescrizioneCliente;
            }

            if (!String.Equals(b1.Commessa, b2.Commessa))
            {
                Default.InfoFormat("   Sync from DB: found BATCH difference for Odp {0}, {1}  '{2}' <> '{3}'", b1.OdP, "Commessa", b1.Commessa, b2.Commessa);
                b1.Commessa = b2.Commessa;
            }

            if (!String.Equals(b1.Materiale.Nome, b2.Materiale.Nome))
            {
                Default.InfoFormat("   Sync from DB: found BATCH difference for Odp {0}, {1}  '{2}' <> '{3}'", b1.OdP, "Materiale.Nome", b1.Materiale.Nome, b2.Materiale.Nome);
                b1.Materiale.Nome = b2.Materiale.Nome;
            }


            if (!String.Equals(b1.Materiale.Codice, b2.Materiale.Codice))
            {
                Default.InfoFormat("   Sync from DB: found BATCH difference for Odp {0}, {1}  '{2}' <> '{3}'", b1.OdP, "Materiale.Codice", b1.Materiale.Codice, b2.Materiale.Codice);
                b1.Materiale.Codice = b2.Materiale.Codice;
            }


            if (!String.Equals(b1.DistintaBase.Nome, b2.DistintaBase.Nome))
            {
                Default.InfoFormat("   Sync from DB: found BATCH difference for Odp {0}, {1}  '{2}' <> '{3}'", b1.OdP, "DistintaBase.Nome", b1.DistintaBase.Nome, b2.DistintaBase.Nome);
                b1.DistintaBase.Nome = b2.DistintaBase.Nome;
            }

            if (!String.Equals(b1.DistintaBase.AbilitaAutoStartProduzione, b2.DistintaBase.AbilitaAutoStartProduzione))
            {
                Default.InfoFormat("   Sync from DB: found BATCH difference for Odp {0}, {1}  '{2}' <> '{3}'", b1.OdP, "DistintaBase.AbilitaAutoStartProduzione", b1.DistintaBase.AbilitaAutoStartProduzione, b2.DistintaBase.AbilitaAutoStartProduzione);
                b1.DistintaBase.AbilitaAutoStartProduzione = b2.DistintaBase.AbilitaAutoStartProduzione;
            }

            if (!String.Equals(b1.DistintaBase.AbilitaEmergenza, b2.DistintaBase.AbilitaEmergenza))
            {
                Default.InfoFormat("   Sync from DB: found BATCH difference for Odp {0}, {1}  '{2}' <> '{3}'", b1.OdP, "DistintaBase.AbilitaEmergenza", b1.DistintaBase.AbilitaEmergenza, b2.DistintaBase.AbilitaEmergenza);
                b1.DistintaBase.AbilitaEmergenza = b2.DistintaBase.AbilitaEmergenza;
            }

            if (!String.Equals(b1.DistintaBase.AbilitaStopProduzione, b2.DistintaBase.AbilitaStopProduzione))
            {
                Default.InfoFormat("   Sync from DB: found BATCH difference for Odp {0}, {1}  '{2}' <> '{3}'", b1.OdP, "DistintaBase.AbilitaStopProduzione", b1.DistintaBase.AbilitaStopProduzione, b2.DistintaBase.AbilitaStopProduzione);
                b1.DistintaBase.AbilitaStopProduzione = b2.DistintaBase.AbilitaStopProduzione;
            }

            #region Gestione materiali 

            var listBatchMaterialiTmp = new List<BatchMateriali>();

            foreach (var b2Mat in b2.BatchMateriali)
            {
                var b1Mat = (b1.BatchMateriali.FirstOrDefault(o => o.NumeroFase == b2Mat.NumeroFase && o.Materiale.Id == b2Mat.Materiale.Id));

                //1. Materiale esiste in entrambi i batch
                if (b1Mat != null)
                {
                    // esiste, vengono verificate le differenze
                    if (!String.Equals(b1Mat.NumeroFase, b2Mat.NumeroFase))
                    {
                        Default.InfoFormat("   Sync from DB: found MATERIAL difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Materiale.NumeroFase", b1Mat.NumeroFase, b2Mat.NumeroFase, b2Mat.Materiale.Id);
                        b1Mat.NumeroFase = b2Mat.NumeroFase;
                    }

                    if (!String.Equals(b1Mat.Barcode, b2Mat.Barcode))
                    {
                        Default.InfoFormat("   Sync from DB: found MATERIAL difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Materiale.Barcode", b1Mat.Barcode, b2Mat.Barcode, b2Mat.Materiale.Id);
                        b1Mat.Barcode = b2Mat.Barcode;
                    }

                    if (!String.Equals(b1Mat.BarcodeRfid, b2Mat.BarcodeRfid))
                    {
                        Default.InfoFormat("   Sync from DB: found MATERIAL difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Materiale.BarcodeRfid", b1Mat.BarcodeRfid, b2Mat.BarcodeRfid, b2Mat.Materiale.Id);
                        b1Mat.BarcodeRfid = b2Mat.BarcodeRfid;
                    }


                    if (!String.Equals(b1Mat.TagAzione, b2Mat.TagAzione))
                    {
                        Default.InfoFormat("   Sync from DB: found MATERIAL difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Materiale.TagAzione", b1Mat.TagAzione, b2Mat.TagAzione, b2Mat.Materiale.Id);
                        b1Mat.TagAzione = b2Mat.TagAzione;
                    }

                    if (!String.Equals(b1Mat.QntPrevista, b2Mat.QntPrevista))
                    {
                        Default.InfoFormat("   Sync from DB: found MATERIAL difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Materiale.QntPrevista", b1Mat.QntPrevista, b2Mat.QntPrevista, b2Mat.Materiale.Id);
                        b1Mat.QntPrevista = b2Mat.QntPrevista;
                    }


                    if (!String.Equals(b1Mat.QntEffettiva, b2Mat.QntEffettiva))
                    {
                        Default.InfoFormat("   Sync from DB: found MATERIAL difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Materiale.QntEffettiva", b1Mat.QntEffettiva, b2Mat.QntEffettiva, b2Mat.Materiale.Id);
                        b1Mat.QntEffettiva = b2Mat.QntEffettiva;
                    }


                    if (!String.Equals(b1Mat.TipoGestione, b2Mat.TipoGestione))
                    {
                        Default.InfoFormat("   Sync from DB: found MATERIAL difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Materiale.TipoGestione", b1Mat.TipoGestione, b2Mat.TipoGestione, b2Mat.Materiale.Id);
                        b1Mat.TipoGestione = b2Mat.TipoGestione;
                    }


                    if (!String.Equals(b1Mat.Materiale.Id, b2Mat.Materiale.Id))
                    {
                        Default.InfoFormat("   Sync from DB: found MATERIAL difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Materiale.Id", b1Mat.Materiale.Id, b2Mat.Materiale.Id, b2Mat.Materiale.Id);
                        b1Mat.Materiale.Id = b2Mat.Materiale.Id;
                    }


                    if (!String.Equals(b1Mat.Materiale.Codice, b2Mat.Materiale.Codice))
                    {
                        Default.InfoFormat("   Sync from DB: found difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Materiale.Codice", b1Mat.Materiale.Codice, b2Mat.Materiale.Codice, b2Mat.Materiale.Id);
                        b1Mat.Materiale.Codice = b2Mat.Materiale.Codice;
                    }


                    if (!String.Equals(b1Mat.Materiale.Descrizione, b2Mat.Materiale.Descrizione))
                    {
                        Default.InfoFormat("   Sync from DB: found difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Materiale.Descrizione", b1Mat.Materiale.Descrizione, b2Mat.Materiale.Descrizione, b2Mat.Materiale.Id);
                        b1Mat.Materiale.Descrizione = b2Mat.Materiale.Descrizione;
                    }


                    if (!String.Equals(b1Mat.Materiale.Um, b2Mat.Materiale.Um))
                    {
                        Default.InfoFormat("   Sync from DB: found difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Materiale.Um", b1Mat.Materiale.Um, b2Mat.Materiale.Um, b2Mat.Materiale.Id);
                        b1Mat.Materiale.Um = b2Mat.Materiale.Um;
                    }


                    if (!String.Equals(b1Mat.BatchMaterialiStato, b2Mat.BatchMaterialiStato))
                    {
                        Default.InfoFormat("   Sync from DB: found difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "BatchMaterialiStato", b1Mat.BatchMaterialiStato, b2Mat.BatchMaterialiStato, b2Mat.Materiale.Id);
                        b1Mat.BatchMaterialiStato = b2Mat.BatchMaterialiStato;
                    }

                    if (!String.Equals(b1Mat.BatchMaterialiTipo, b2Mat.BatchMaterialiTipo))
                    {
                        Default.InfoFormat("   Sync from DB: found difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "BatchMaterialiTipo", b1Mat.BatchMaterialiTipo, b2Mat.BatchMaterialiTipo, b2Mat.Materiale.Id);
                        b1Mat.BatchMaterialiTipo = b2Mat.BatchMaterialiTipo;
                    }
                }
                else
                {
                    // 2. Materiale non esiste nel batch originario, quindi va aggiunto
                    b1.BatchMateriali.Add(b2Mat);
                    Default.InfoFormat("   Sync from DB: add material '{0}', {1} ({2})", b1.OdP + "-" + b2Mat.NumeroFase, b2Mat.Materiale.Codice, b2Mat.Materiale.Barcode);
                }
            }//


            //3. Materiale non esiste nel nuovo batch
            foreach (var b1Mat in b1.BatchMateriali)
                if (!b2.BatchMateriali.Any(o => o.Materiale.Id == b1Mat.Materiale.Id))
                    listBatchMaterialiTmp.Add(b1Mat);

            foreach (var itemToDelete in listBatchMaterialiTmp)
            {
                Default.InfoFormat("   Sync from DB: remove material '{0}', {1} ({2})", b1.OdP + "-" + itemToDelete.NumeroFase, itemToDelete.Materiale.Codice, itemToDelete.Materiale.Barcode);
                b1.BatchMateriali.Remove(itemToDelete);
            }

            #endregion

            #region Gestion Fasi

            var listBatchFasimp = new List<BatchFase>();

            foreach (var b2Fasi in b2.Fasi)
            {
                // var b1Fasi = b1.DistintaBase.Fasi.FirstOrDefault(o => o.NumeroFase == b2Fasi.NumeroFase);
                var b1Fasi1 = b1.Fasi.FirstOrDefault(o => o.NumeroFase == b2Fasi.NumeroFase);

                //1. Fase esiste in entrambi i batch
                if (b1Fasi1 != null)
                {
                    var b1Fasi = b1Fasi1.DistintaBaseFase;

                    if (!String.Equals(b1Fasi.Nome, b2Fasi.DistintaBaseFase.Nome))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Nome", b1Fasi.Nome, b2Fasi.DistintaBaseFase.Nome, b1Fasi.NumeroFase);
                        b1Fasi.Nome = b2Fasi.DistintaBaseFase.Nome;
                    }

                    if (!String.Equals(b1Fasi.Codice, b2Fasi.DistintaBaseFase.Codice))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Codice", b1Fasi.Codice, b2Fasi.DistintaBaseFase.Codice, b1Fasi.NumeroFase);
                        b1Fasi.Codice = b2Fasi.DistintaBaseFase.Codice;
                    }

                    if (!String.Equals(b1Fasi.Descrizione, b2Fasi.DistintaBaseFase.Descrizione))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Descrizione", b1Fasi.Descrizione, b2Fasi.DistintaBaseFase.Descrizione, b1Fasi.NumeroFase);
                        b1Fasi.Descrizione = b2Fasi.DistintaBaseFase.Descrizione;
                    }

                    if (!String.Equals(b1Fasi.Campo1, b2Fasi.DistintaBaseFase.Campo1))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Campo1", b1Fasi.Campo1, b2Fasi.DistintaBaseFase.Campo1, b1Fasi.NumeroFase);
                        b1Fasi.Campo1 = b2Fasi.DistintaBaseFase.Campo1;
                    }

                    if (!String.Equals(b1Fasi.Campo2, b2Fasi.DistintaBaseFase.Campo2))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Campo2", b1Fasi.Campo2, b2Fasi.DistintaBaseFase.Campo2, b1Fasi.NumeroFase);
                        b1Fasi.Campo2 = b2Fasi.DistintaBaseFase.Campo2;
                    }

                    if (!String.Equals(b1Fasi.Campo3, b2Fasi.DistintaBaseFase.Campo3))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Campo3", b1Fasi.Campo3, b2Fasi.DistintaBaseFase.Campo3, b1Fasi.NumeroFase);
                        b1Fasi.Campo3 = b2Fasi.DistintaBaseFase.Campo3;
                    }

                    if (!String.Equals(b1Fasi.Campo4, b2Fasi.DistintaBaseFase.Campo4))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Campo4", b1Fasi.Campo4, b2Fasi.DistintaBaseFase.Campo4, b1Fasi.NumeroFase);
                        b1Fasi.Campo4 = b2Fasi.DistintaBaseFase.Campo4;
                    }

                    if (!String.Equals(b1Fasi.Campo5, b2Fasi.DistintaBaseFase.Campo5))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Campo5", b1Fasi.Campo5, b2Fasi.DistintaBaseFase.Campo5, b1Fasi.NumeroFase);
                        b1Fasi.Campo5 = b2Fasi.DistintaBaseFase.Campo5;
                    }

                    if (!String.Equals(b1Fasi.Campo6, b2Fasi.DistintaBaseFase.Campo6))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Campo6", b1Fasi.Campo6, b2Fasi.DistintaBaseFase.Campo6, b1Fasi.NumeroFase);
                        b1Fasi.Campo6 = b2Fasi.DistintaBaseFase.Campo6;
                    }

                    if (!String.Equals(b1Fasi.Campo7, b2Fasi.DistintaBaseFase.Campo7))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Campo7", b1Fasi.Campo7, b2Fasi.DistintaBaseFase.Campo7, b1Fasi.NumeroFase);
                        b1Fasi.Campo7 = b2Fasi.DistintaBaseFase.Campo7;
                    }

                    if (!String.Equals(b1Fasi.Campo8, b2Fasi.DistintaBaseFase.Campo8))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Campo8", b1Fasi.Campo8, b2Fasi.DistintaBaseFase.Campo8, b1Fasi.NumeroFase);
                        b1Fasi.Campo8 = b2Fasi.DistintaBaseFase.Campo8;
                    }

                    if (!String.Equals(b1Fasi.Campo9, b2Fasi.DistintaBaseFase.Campo9))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Campo9", b1Fasi.Campo9, b2Fasi.DistintaBaseFase.Campo9, b1Fasi.NumeroFase);
                        b1Fasi.Campo9 = b2Fasi.DistintaBaseFase.Campo9;
                    }

                    if (!String.Equals(b1Fasi.Campo10, b2Fasi.DistintaBaseFase.Campo10))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Campo10", b1Fasi.Campo10, b2Fasi.DistintaBaseFase.Campo10, b1Fasi.NumeroFase);
                        b1Fasi.Campo10 = b2Fasi.DistintaBaseFase.Campo10;
                    }

                    if (!String.Equals(b1Fasi.AbilitaAvanzamenti, b2Fasi.DistintaBaseFase.AbilitaAvanzamenti))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "AbilitaAvanzamenti", b1Fasi.AbilitaAvanzamenti, b2Fasi.DistintaBaseFase.AbilitaAvanzamenti, b1Fasi.NumeroFase);
                        b1Fasi.AbilitaAvanzamenti = b2Fasi.DistintaBaseFase.AbilitaAvanzamenti;
                    }

                    if (!String.Equals(b1Fasi.AbilitaMateriali, b2Fasi.DistintaBaseFase.AbilitaMateriali))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "AbilitaMateriali", b1Fasi.AbilitaMateriali, b2Fasi.DistintaBaseFase.AbilitaMateriali, b1Fasi.NumeroFase);
                        b1Fasi.AbilitaMateriali = b2Fasi.DistintaBaseFase.AbilitaMateriali;
                    }

                    if (!String.Equals(b1Fasi.AbilitaMultimedia, b2Fasi.DistintaBaseFase.AbilitaMultimedia))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "AbilitaMultimedia", b1Fasi.AbilitaMultimedia, b2Fasi.DistintaBaseFase.AbilitaMultimedia, b1Fasi.NumeroFase);
                        b1Fasi.AbilitaMultimedia = b2Fasi.DistintaBaseFase.AbilitaMultimedia;
                    }

                    if (!String.Equals(b1Fasi.AbilitaVersamentiManuali, b2Fasi.DistintaBaseFase.AbilitaVersamentiManuali))
                    {
                        Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "AbilitaVersamentiManuali", b1Fasi.AbilitaVersamentiManuali, b2Fasi.DistintaBaseFase.AbilitaVersamentiManuali, b1Fasi.NumeroFase);
                        b1Fasi.AbilitaVersamentiManuali = b2Fasi.DistintaBaseFase.AbilitaVersamentiManuali;
                    }


                    var b1FaseMa = b1Fasi.Manodopera.FirstOrDefault();
                    var b1FaseMa1 = b1Fasi1.DistintaBaseFase.Manodopera.FirstOrDefault();
                    if (b1FaseMa != null)
                    {
                        var b2FaseMa = b2Fasi.DistintaBaseFase.Manodopera.FirstOrDefault();

                        if ((b2FaseMa != null && (b1FaseMa.OperatoreCompetenza.Id != b2FaseMa.OperatoreCompetenza.Id)))
                        {
                            Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "OperatoreCompetenza", b1FaseMa.OperatoreCompetenza.Id, b2FaseMa.OperatoreCompetenza.Id, b1Fasi.NumeroFase);
                            b1FaseMa.OperatoreCompetenza.Id = b2FaseMa.OperatoreCompetenza.Id;
                            b1FaseMa1.OperatoreCompetenza.Id = b2FaseMa.OperatoreCompetenza.Id;
                        }


                        if ((b2FaseMa != null && (b1FaseMa.Tempo != b2FaseMa.Tempo)))
                        {
                            Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Tempo", b1FaseMa.Tempo, b2FaseMa.Tempo, b1Fasi.NumeroFase);
                            b1FaseMa.Tempo = b2FaseMa.Tempo;
                        }

                        if ((b2FaseMa != null && (b1FaseMa.Descrizione != b2FaseMa.Descrizione)))
                        {
                            Default.InfoFormat("   Sync from DB: found TASK difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Descrizione", b1FaseMa.Descrizione, b2FaseMa.Descrizione, b1Fasi.NumeroFase);
                            b1FaseMa.Descrizione = b2FaseMa.Descrizione;
                        }

                        if (b2FaseMa == null)
                        {
                            b1FaseMa.OperatoreCompetenza.Id = String.Empty;
                            b1FaseMa.Tempo = 0;
                        }
                    }
                }
                else
                {
                    // 2. Fase non esiste nel batch originario, quindi va aggiunta
                    Default.InfoFormat("   Sync from DB: add TASK '{0}', {1} ", b1.OdP + "-" + b2Fasi.NumeroFase, b2Fasi.DistintaBaseFase.Nome);
                    if (!b1.DistintaBase.Fasi.Any(o => o.NumeroFase == b2Fasi.DistintaBaseFase.NumeroFase))
                        b1.DistintaBase.Fasi.Add(b2Fasi.DistintaBaseFase);
                    b1.Fasi.Add(b2Fasi);
                }
            }

            //3. Fase non esiste nel nuovo batch, quindi viene inserita nella lista temporanea per la rimozione
            foreach (var b1Fase in b1.Fasi)
                if (!b2.Fasi.Any(o => o.NumeroFase == b1Fase.NumeroFase))
                    listBatchFasimp.Add(b1Fase);



            foreach (var itemToDelete in listBatchFasimp)
            {
                Default.InfoFormat("   Sync from DB: remove TASK '{0}', {1} ", b1.OdP + "-" + itemToDelete.NumeroFase, itemToDelete.DistintaBaseFase.Nome);
                b1.Fasi.Remove(itemToDelete);
            }


            #endregion

            #region Gestione multimedia 

            var listBatchMultimediaTmp = new List<BatchMultimedia>();

            foreach (var b2Mul in b2.BatchMultimedia)
            {
                var b1Mul = (b1.BatchMultimedia.FirstOrDefault(o => o.Codice == b2Mul.Codice));


                //1. Materiale esiste in entrambi i batch
                if (b1Mul != null)
                {
                    // esiste, vengono verificate le differenze
                    if (!String.Equals(b1Mul.Tipo, b2Mul.Tipo))
                    {
                        Default.InfoFormat("   Sync from DB: found MULTIMEDIA difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Tipo", b1Mul.Tipo, b2Mul.Tipo, b1Mul.NumeroFase);
                        b1Mul.Tipo = b2Mul.Tipo;
                    }

                    if (!String.Equals(b1Mul.Codice, b2Mul.Codice))
                    {
                        Default.InfoFormat("   Sync from DB: found MULTIMEDIA difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Codice", b1Mul.Codice, b2Mul.Codice, b1Mul.NumeroFase);
                        b1Mul.Codice = b2Mul.Codice;
                    }

                    if (!String.Equals(b1Mul.Url, b2Mul.Url))
                    {
                        Default.InfoFormat("   Sync from DB: found MULTIMEDIA difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Url", b1Mul.Url, b2Mul.Url, b1Mul.NumeroFase);
                        b1Mul.Url = b2Mul.Url;
                    }

                    if (!String.Equals(b1Mul.Descrizione, b2Mul.Descrizione))
                    {
                        Default.InfoFormat("   Sync from DB: found MULTIMEDIA difference for Odp {0}, {1}  '{2}' <> '{3}', Pos:{4}", b1.OdP, "Descrizione", b1Mul.Descrizione, b2Mul.Descrizione, b1Mul.NumeroFase);
                        b1Mul.Descrizione = b2Mul.Descrizione;
                    }
                }
                else
                {
                    // 2. Materiale non esiste nel batch originario, quindi va aggiunto
                    b1.BatchMultimedia.Add(b2Mul);
                    Default.InfoFormat("   Sync from DB: add MULTIMEDIA '{0}', {1} ({2})", b1.OdP + "-" + b2Mul.NumeroFase, b2Mul.Descrizione, b2Mul.Url);
                }
            }//


            //3. Materiale non esiste nel nuovo batch
            foreach (var b1Mul in b1.BatchMultimedia)
                if (!b2.BatchMultimedia.Any(o => o.NumeroFase == b1Mul.NumeroFase))
                    listBatchMultimediaTmp.Add(b1Mul);

            foreach (var itemToDelete in listBatchMultimediaTmp)
            {
                Default.InfoFormat("   Sync from DB: remove MULTIMEDIA '{0}', {1} ({2})", b1.OdP + "-" + itemToDelete.NumeroFase, itemToDelete.Descrizione, itemToDelete.Url);
                b1.BatchMultimedia.Remove(itemToDelete);
            }

            #endregion
        }

        private void setShiftAction()
        {
            Default.InfoFormat("Execute shift action");

            foreach (var batch in this.BatchInMemory.Invoke())
                lock (batch)
                {
                    var listTask = batch.Fasi.Where(o => o.Stato == Model.Enums.StatoFasi.InLavorazione ||
                                                         o.Stato == Model.Enums.StatoFasi.InLavorazioneInEmergenza).ToList();
                    foreach (var item in listTask)
                    {
                        item.Stato = Model.Enums.StatoFasi.InLavorazioneInPausa;
                        Default.InfoFormat("   Set task '{0}', {1} state to {2} ", batch.OdP + "-" + item.NumeroFase, item.DistintaBaseFase.Nome, "InLavorazioneInPausa");
                        _runtimeContext.SetFase(new SetFaseInfo { idBatch = batch.Id, fase = item });
                    }
                }
        }

    }
}
