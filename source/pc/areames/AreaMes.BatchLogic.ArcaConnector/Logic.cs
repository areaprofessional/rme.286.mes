﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AreaMes.Model.Runtime;
using AreaMes.Server.Utils;
using System.Threading;
using AreaMes.Model;
using AreaMes.Meta;
using System.IO;

namespace AreaMes.BatchLogic.ArcaConnector
{
    public class Logic : AreaMes.Model.BatchLogic
    {
        public override async Task Init(IDataContainer data, IRepository repository, IMessageServices messages, IRealTimeManager areaMESClient, IAreaM2MClient areaM2MClient, IRuntimeTransportContextController runtimeContext, Batch batch)
        {
            await base.Init(data, repository, messages, areaMESClient, areaM2MClient, runtimeContext, batch);

            try
            {

            }
            catch (Exception exc)
            {
                Default.Error(exc);
            }

        }

        protected override void OnDeviceValueChangedMessage(object id, DeviceValueChangedMessage message)
        {
            try
            {

            }
            catch (Exception exc)
            {
                Default.Error(exc);
            }

        }

        protected override void OnBatchChanged(object id, BatchChangedMessage message)
        {
            try
            {


            }
            catch (Exception exc)
            {
                Default.Error(exc);
            }
        }

        protected override void OnFaseChanged(object id, FaseChangedInfo message)
        {
            try
            {
                //BatchFase fase;
                //lock (this._batch)
                //    fase = this._batch.Fasi.ToList().FirstOrDefault(o => o.NumeroFase == message.Current.NumeroFase);
                //if (fase == null) throw new Exception("Task not found");

                //Default.InfoFormat("Set TASK {0}.'{1}' to '{2}'  by user '{3}'", this._batch.Id, fase.NumeroFase, fase.Stato, message.Username);

                //if ((message.Current.Stato == AreaMes.Model.Enums.StatoFasi.Terminato)
                //    || (message.Current.Stato == AreaMes.Model.Enums.StatoFasi.InLavorazioneInPausa))
                //{
                //    var dest = Path.Combine(GetSettingValue("SAP_OUTPUT_FOLDER_XML").ToString(), String.Format("toSAP_start_end_{0}_{1}_{2}.xml", id, fase.NumeroFase, DateTime.Now.ToString("yyyyMMdd HHmmss")));
                //    //var output = '';//ExportHelper.GetXmlAvanzamento(fase, this._batch);
                //    //File.WriteAllText(dest, output);
                //    Default.InfoFormat("Write xml file to SAP: {0}", dest);
                //}
            }
            catch (Exception exc)
            {
                Default.Error(exc);
            }

        }
    }
}
