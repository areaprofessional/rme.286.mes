﻿using AreaMes.Model.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.BatchLogic.ArcaConnector.Utility
{
    public class WorkingCalculate
    {

        public static int GetTimeAlarmCalculate(Batch batchItemm, string numeroFase)
        {
            var totalSecond = 0;
            var batchAvanzamentiFilter = batchItemm.BatchAvanzamenti.FindAll(x => x.NumeroFase == numeroFase);

            foreach (var batch in batchAvanzamentiFilter)
            {
                if(batch.Stato == Model.Enums.StatoFasi.InLavorazioneInEmergenza) {

                    if (batch.Inizio != null && batch.Fine != null)
                    {
                        var second = (Convert.ToDateTime(batch.Fine) - Convert.ToDateTime(batch.Inizio)).TotalSeconds;
                        totalSecond = totalSecond + Convert.ToInt32(second);
                    }
                }
             
            }

            return totalSecond;
        }

    }
}
