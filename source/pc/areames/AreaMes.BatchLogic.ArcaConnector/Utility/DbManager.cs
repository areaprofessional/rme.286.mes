﻿using AreaFramework.Wrapper;
using AreaFramework.Wrapper.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using AreaMes.BatchLogic.ArcaConnector.Entites;

namespace AreaMes.BatchLogic.ArcaConnector.Utility
{
    public class DbManager
    {
        private string _connectionString;
        private SqlConnection _connection;
        private Dictionary<Type, object> _dWrappers;
        private List<Type> _listOfReplicableType = new List<Type>();
        private Dictionary<Type, string> _dictTypeTableName = new Dictionary<Type, string>();
        private object _lockIdentity = new object();
        public DbManager(string connectionString)
        {
            if (!String.IsNullOrEmpty(connectionString))
                Connect(connectionString);
        }

        public DbManager Connect(string connectionString)
        {
            _connectionString = connectionString;

            _connection = new SqlConnection(_connectionString);
            _connection.Open();

            _dWrappers = new Dictionary<Type, object>();
            _dWrappers.Add(typeof(Arca_Operatore), new MES_OPWrapper(_connection));
            _dWrappers.Add(typeof(Arca_Materiale), new MES_MATWrapper(_connection));
            _dWrappers.Add(typeof(Arca_Misura), new MES_MISURAWrapper(_connection));
            _dWrappers.Add(typeof(Arca_Batch_Prol), new MES_PROLHWrapper(_connection));
            _dWrappers.Add(typeof(Arca_Batch_ProlAttivita), new MES_PROLATTWrapper(_connection));
            _dWrappers.Add(typeof(Arca_Batch_Prbl), new MES_PRBLWrapper(_connection));
            _dWrappers.Add(typeof(Arca_Batch_PrblAttivita), new MES_PRBLATTWrapper(_connection));
            _dWrappers.Add(typeof(Arca_Batch_PrblMateriali), new MES_PRBLMATWrapper(_connection));
            _dWrappers.Add(typeof(Arca_Macchine), new MES_MACHWrapper(_connection));
            _dWrappers.Add(typeof(Arca_MesProdTempi), new Mes_ProdTempiWrapper(_connection));
            _dWrappers.Add(typeof(Arca_MesProdVersamenti), new Mes_ProdVersamentiWrapper(_connection));
            _dWrappers.Add(typeof(Arca_MesProdScarti), new MesProdScarti(_connection));
            _dWrappers.Add(typeof(Arca_TableUpdated), new MES_TABLEUPDATEWrapper(_connection));
            _dWrappers.Add(typeof(Arca_Batch_ProlMateriali), new MES_PROLMATWrapper(_connection));
            _dWrappers.Add(typeof(Arca_MesProdComponenti), new MES_PRODCOMPONENTWrapper(_connection));
            _dWrappers.Add(typeof(Arca_FaseAllegati), new MES_PRODALLEGATIWrapper(_connection));
            return this;
        }

        private BaseWrapper<T> Wrapper<T>() where T : class
        {
            object tmp = null;

            if (_dWrappers.TryGetValue(typeof(T), out tmp))
                return (BaseWrapper<T>)tmp;

            return null;
        }//end


        public IEnumerable<T> Get<T>() where T : AreaEntity, new()
        {
            return this.Wrapper<T>().SelectAll();
        }


        public async Task<IEnumerable<T>> GetAsync<T>() where T : AreaEntity, new()
        {
            return await Task.Run(() => { return this.Get<T>(); });
        }

        public T Get<T>(Guid id) where T : AreaEntity, new()
        {
            return this.Wrapper<T>().SelectByID(id);
        }

        public async Task<T> GetAsync<T>(Guid id) where T : AreaEntity, new()
        {
            return await Task.Run(() => { return this.Get<T>(id); });
        }

        public IEnumerable<T> Search<T>(Expression<Func<T, bool>> e) where T : AreaEntity, new()
        {
            return this.Wrapper<T>().Select(e);
        }
        public async Task<IEnumerable<T>> SearchAsync<T>(Expression<Func<T, bool>> e) where T : AreaEntity, new()
        {
            return await Task.Run(() => { return this.Wrapper<T>().Select(e); });
        }

        public IEnumerable<T> Search<T>(string column, object value) where T : AreaEntity, new()
        {
            return this.Wrapper<T>().FindByField(column, value);
        }

        public async Task<IEnumerable<T>> SearchAsync<T>(string column, object value) where T : AreaEntity, new()
        {
            return await Task.Run(() => { return this.Search<T>(column, value); });
        }

        public int Delete<T>(T item) where T : AreaEntity, new()
        {
            return this.Wrapper<T>().Delete(item, null);
        }

        public T Set<T>(T item) where T : AreaEntity, new()
        {
            return this.Wrapper<T>().InsertOrUpdate(item, null);
        }

        public int ExecuteSp(string name, Dictionary<string, object> param)
        {
            var cmd = this._connection.CreateCommand();

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = name;

            foreach (var item in param)
            {
                var p = cmd.CreateParameter();
                p.Value = item.Value;
                p.ParameterName = item.Key;
                cmd.Parameters.Add(p);
            }


            return cmd.ExecuteNonQuery();
        }


        public int ExecuteNonQuery(string command, Dictionary<string, object> param)
        {
            var cmd = this._connection.CreateCommand();
            cmd.CommandText = command;

            foreach (var item in param)
            {
                var p = cmd.CreateParameter();
                p.Value = item.Value;
                p.ParameterName = item.Key;
                cmd.Parameters.Add(p);
            }

            return cmd.ExecuteNonQuery();
        }


        public void Dispose()
        {
            _dWrappers.Clear();
            _connection.Close();
        }
    }



    public class BaseWrapper<T> : WrapperRepository<T> where T : class
    {

        #region Constructors

        /// <summary>
        /// Costruttore
        /// Costruisce un oggetto di tipo BaseWrapper
        /// passandogli una connessione verso db e un
        /// wrapper container
        /// </summary>
        /// <param name="pConn"></param>
        /// <param name="pWrapperContainer"></param>
        public BaseWrapper(IDbConnection pConn) : base(pConn)
        {
            this.GenericAdapter = new AutomaticAdapterRepository<T>(pConn);
        }//end

        #endregion

        public int ExecuteStoredProc(string name, object[] param)
        {
            var cmd = this.GenericAdapter.Connection.CreateCommand();

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = name;
            foreach (var item in param)
                cmd.Parameters.Add(item);

            return cmd.ExecuteNonQuery();
        }

    }//end class

    public class MesProdScarti : BaseWrapper<Arca_MesProdScarti> { public MesProdScarti(IDbConnection pConn) : base(pConn) { } }
    public class Mes_ProdTempiWrapper : BaseWrapper<Arca_MesProdTempi> { public Mes_ProdTempiWrapper(IDbConnection pConn) : base(pConn) { } }
    public class Mes_ProdVersamentiWrapper : BaseWrapper<Arca_MesProdVersamenti> { public Mes_ProdVersamentiWrapper(IDbConnection pConn) : base(pConn) { } }
    public class MES_OPWrapper : BaseWrapper<Arca_Operatore> { public MES_OPWrapper(IDbConnection pConn) : base(pConn) { } }
    public class MES_MATWrapper : BaseWrapper<Arca_Materiale> { public MES_MATWrapper(IDbConnection pConn) : base(pConn) { } }
    public class MES_MISURAWrapper : BaseWrapper<Arca_Misura> { public MES_MISURAWrapper(IDbConnection pConn) : base(pConn) { } }
    public class MES_PROLHWrapper : BaseWrapper<Arca_Batch_Prol> { public MES_PROLHWrapper(IDbConnection pConn) : base(pConn) { } }
    public class MES_MACHWrapper : BaseWrapper<Arca_Macchine> { public MES_MACHWrapper(IDbConnection pConn) : base(pConn) { } }
    public class MES_PROLATTWrapper : BaseWrapper<Arca_Batch_ProlAttivita> { public MES_PROLATTWrapper(IDbConnection pConn) : base(pConn) { } }
    public class MES_PRBLWrapper : BaseWrapper<Arca_Batch_Prbl> { public MES_PRBLWrapper(IDbConnection pConn) : base(pConn) { } }
    public class MES_PRBLATTWrapper : BaseWrapper<Arca_Batch_PrblAttivita> { public MES_PRBLATTWrapper(IDbConnection pConn) : base(pConn) { } }
    public class MES_PRBLMATWrapper : BaseWrapper<Arca_Batch_PrblMateriali> { public MES_PRBLMATWrapper(IDbConnection pConn) : base(pConn) { } }
    public class MES_PROLMATWrapper : BaseWrapper<Arca_Batch_ProlMateriali> { public MES_PROLMATWrapper(IDbConnection pConn) : base(pConn) { } }
    public class MES_TABLEUPDATEWrapper : BaseWrapper<Arca_TableUpdated> { public MES_TABLEUPDATEWrapper(IDbConnection pConn) : base(pConn) { } }
    public class MES_PRODCOMPONENTWrapper : BaseWrapper<Arca_MesProdComponenti> { public MES_PRODCOMPONENTWrapper(IDbConnection pConn) : base(pConn) { } }
    public class MES_PRODALLEGATIWrapper : BaseWrapper<Arca_FaseAllegati> { public MES_PRODALLEGATIWrapper(IDbConnection pConn) : base(pConn) { } }
}
